import fs from 'node:fs';
import path from 'node:path';

const langFilePath = './lang/';
const stemFilePath = './stems/';

const separator = '-';

// Modified from https://stackoverflow.com/a/71426723/6761963
// Only suitable for english and english-like languages
const camelCase = str => str.replaceAll(/-/g, ' ').split(' ').map((e, i) => i ? e.charAt(0).toUpperCase() + e.slice(1).toLowerCase() : e.toLowerCase()).join('');

function handleFile(filename) {
	const source = path.join(stemFilePath, filename);
	const fnp = path.parse(filename).name;
	const [_, language] = fnp.split(separator);
	console.log('< Stems:', language, `(${source})`);

	// Read stem file
	const data = fs.readFileSync(source, { encoding: 'utf-8' });

	// Split stem file into lines
	const lines = data.split('\n').map(l => l?.trim()).filter(l => l?.length);
	console.log('? Entries:', lines.length);

	// Generate language file
	const json = { TOKEN: { Adjectives: {} } };
	const adjectives = json.TOKEN.Adjectives;
	for (const l of lines) {
		const key = camelCase(l);
		if (key in adjectives) console.warn('- Duplicate key:', key);
		adjectives[key] = l;
	}

	// Write language file
	const langFile = path.join(langFilePath, language + '.json');
	console.log('> Output file:', langFile);
	fs.writeFileSync(langFile, JSON.stringify(json, null, '\t'), { encoding: 'utf-8' });
}

const files = fs.readdirSync(stemFilePath, { encoding: 'utf-8' });
const stemFiles = files.filter(f => f.endsWith('.txt'));

stemFiles.forEach(handleFile);
