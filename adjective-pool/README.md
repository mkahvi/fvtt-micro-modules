# Adjective Pool

![Supported Foundry Versions](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fversion%3Fstyle%3Dflat%26url%3Dhttps%3A%2F%2Fgitlab.com%2Fmkahvi%2Ffvtt-micro-modules%2F-%2Fraw%2Fmaster%2Fadjective-pool%2Fmodule.json)

Greatly increases the size of the adjective pool Foundry's name randomizer has access to.

Derived from my older _NPC Name Randomizer_ module, but using systems provided by Foundry v11 to greatly simplify it.

✅ Recommended for general use.

## Install

Requires: Foundry v11

Manifest URL: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/master/adjective-pool/module.json>

## Migration

For people who have had Foundry's adjective randomizer disabled, the following script enables it for all existing actors and new ones. This also is useful for those who used the _NPC Name Randomizer_ module before.

```js
// Set default token setting
const dt = foundry.utils.deepClone(game.settings.get("core", "defaultToken"));
dt.prependAdjective = true;
await game.settings.set("core", "defaultToken", dt);

// Adjust already existing actors
await game.actors.updateAll({ "prototypeToken.prependAdjective": true });
```

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT license](./LICENSE).

## Credits

Initial list of adjectives acquired from from [Token Mold](https://github.com/Moerill/token-mold) module, it has since been extended and modified.
