const CFG = {
	id: 'map-note-tooltips',
	SETTINGS: {
		width: 'width',
		height: 'height',
		fade: 'fade',
	},
};

const timer = new class TimerManager {
	timer;
	delay = 250;

	start() {
		this.stop(); // Stop any previous timer
		this.timer = setTimeout(() => hover.end(), this.delay);
	}

	stop() {
		clearTimeout(this.timer);
	}
}();

const hover = new class HoverManager {
	end() {
		tooltip.element.classList.remove('active');
		tooltip.element.classList.add('inactive');

		tooltip.element.replaceChildren();
	}

	delay() {
		timer.stop();
		tooltip.element.classList.toggle('active', true);
	}

	cancel() {
		tooltip.element.classList.toggle('active', false);
		timer.start();
	}

	activate() {
		tooltip.element.classList.toggle('active', true);
		tooltip.element.classList.toggle('inactive', false);
	}
}();

const tooltip = new class Tooltip {
	/** @type {HTMLElement} */
	element;

	/** @type {boolean} */
	showLeft = false;

	/**
	 * Get note position as its bounding box along with its center.
	 *
	 * @param {Note} note
	 * @returns {{left:number,top:number,right:number,bottom:number,center}}
	 */
	getRect(note) {
		const width = note.width * canvas.stage.scale.x,
			// height = note.height * canvas.stage.scale.y,
			// Note position is its center rather than top-left corner like with tokens
			x = note.worldTransform.tx,
			y = note.worldTransform.ty,
			// top = y - height / 2,
			// bottom = top + height;
			left = x - width / 2,
			right = left + width;

		return { left, top, right, center: { x, y }, width };
	}

	/**
	 * Fill tooltip contents.
	 *
	 * @param {NoteDocument} note
	 */
	async fill(note) {
		const el = this.element;
		// Ensure previous constraints don't interfere
		el.style.removeProperty('--map-note-max-width');

		/** @type {JournalEntry} */
		const { entry, pageId } = note;
		if (!entry) return void el.replaceChildren();

		/** @type {JournalEntryPage[]} */
		const pages = pageId ? [entry.pages.get(pageId)] : [...entry.pages];

		const pageEls = [];

		if (pages.length > 1) {
			const h = document.createElement('h1');
			h.classList.add('journal-entry-title');
			h.innerText = entry.name;
			pageEls.push(h);
		}

		for (const page of pages) {
			const pageEl = document.createElement('div');
			pageEl.classList.add('journal-entry-page');
			// TODO: Markdown
			// Add page content
			pageEl.innerHTML = await TextEditor.enrichHTML(page.text.content, { async: true, secrets: page.isOwner });
			// Add title
			if (page.title.show || pages.length > 1) {
				const h = document.createElement(`h${page.title.level}`);
				h.innerText = page.name;
				pageEl.prepend(h);
			}

			// TODO: Page separator?
			pageEls.push(pageEl);
		}

		el.replaceChildren(...pageEls);
	}

	/**
	 * Find position for tooltip based on note position.
	 *
	 * @param {Note} note
	 */
	reposition(note) {
		const rect = this.getRect(note);

		const xp = rect.center.x / document.body.clientWidth;
		this.showLeft = xp > 0.5;

		const el = this.element;
		const offset = this.showLeft ? rect.left : document.body.clientWidth - rect.right;
		el.style.setProperty('--x-off', offset);
		el.classList.toggle('left', this.showLeft);
		el.classList.toggle('right', !this.showLeft);

		// Save position info for later re-adjustment
		this.position = rect;
	}

	/**
	 * Adjust tooltip post-render
	 *
	 * Size is not known properly before.
	 */
	adjust() {
		const el = this.element;
		const rect = el.getBoundingClientRect();

		const sh = document.body.clientHeight,
			sw = document.body.clientWidth;

		// Keep this distance from edges
		const clearance = 80;

		// Ensure the tooltip doesn't overflow the screen
		let width = rect.width;
		let constrain = false;
		// Overflow left
		if (this.showLeft) {
			const edge = this.position.left - width - 3;
			const underflow = edge - clearance;
			if (underflow < 0) {
				width += underflow;
				constrain = true;
			}
		}
		// Overflow right
		else {
			const edge = this.position.right + width + 3;
			const overflow = edge - (sw - clearance);
			if (overflow > 0) {
				width -= overflow;
				constrain = true;
			}
		}

		if (constrain) el.style.setProperty('--map-note-max-width', `${width}px`);

		el.style.setProperty('--width', width);
		el.style.setProperty('--height', rect.height);

		// Start with centering the tooltip
		const top = Math.clamped(this.position.center.y - (rect.height / 2), clearance, sh - rect.height - clearance);

		el.style.setProperty('--top', top);
	}

	/**
	 * Hover event for a note.
	 *
	 * @param {Note} note
	 * @param {boolean} hovering
	 */
	async onHover(note, hovering) {
		timer.stop();

		const noteDoc = note.document;
		if (!hovering) return hover.cancel(); // Not hovering
		if (!noteDoc.entry) return this.clear(); // No content
		const levels = CONST.DOCUMENT_OWNERSHIP_LEVELS ?? CONST.DOCUMENT_PERMISSION_LEVELS;

		if (!noteDoc.testUserPermission(game.user, levels.OBSERVER)) return this.clear(); // Insufficient permissions

		const p = this.fill(noteDoc);
		this.reposition(note);
		hover.activate();
		await p;
		this.adjust();
	}

	clear() {
		hover.cancel();
		this.element.replaceChildren();
	}

	/**
	 * Initialize tooltip element and relevant listeners.
	 */
	init() {
		const el = document.createElement('aside');
		el.id = 'map-note-tooltip';
		el.role = 'tooltip';
		el.classList.add('inactive');
		document.body.append(el);

		el.addEventListener('mouseleave', hover.cancel, { passive: true });
		el.addEventListener('mouseenter', hover.delay, { passive: true });
		this.element = el;
	}
}();

function updateMapNoteVars() {
	document.body.style.setProperty('--map-note-max-width', `${game.settings.get(CFG.id, CFG.SETTINGS.width)}%`);
	document.body.style.setProperty('--map-note-max-height', `${game.settings.get(CFG.id, CFG.SETTINGS.height)}%`);
	const delay = game.settings.get(CFG.id, CFG.SETTINGS.fade);
	document.body.style.setProperty('--map-note-fade-time', `${delay}ms`);
	timer.delay = delay + 50;
}

function registerSettings() {
	game.settings.register(CFG.id, CFG.SETTINGS.width, {
		name: 'MapNoteTooltips.Settings.Width.Label',
		hint: 'MapNoteTooltips.Settings.Width.Hint',
		type: Number,
		range: {
			min: 30,
			max: 80,
			step: 1,
		},
		default: 40,
		scope: 'client',
		onChange: () => updateMapNoteVars(),
		config: true,
	});

	game.settings.register(CFG.id, CFG.SETTINGS.height, {
		name: 'MapNoteTooltips.Settings.Height.Label',
		hint: 'MapNoteTooltips.Settings.Height.Hint',
		type: Number,
		range: {
			min: 20,
			max: 80,
			step: 1,
		},
		default: 60,
		scope: 'client',
		onChange: () => updateMapNoteVars(),
		config: true,
	});

	game.settings.register(CFG.id, CFG.SETTINGS.fade, {
		name: 'MapNoteTooltips.Settings.Fade.Label',
		hint: 'MapNoteTooltips.Settings.Fade.Hint',
		type: Number,
		range: {
			min: 0,
			max: 500,
			step: 10,
		},
		default: 200,
		scope: 'client',
		onChange: () => updateMapNoteVars(),
		config: true,
	});
}

Hooks.once('init', registerSettings);
Hooks.once('ready', () => updateMapNoteVars());
Hooks.on('hoverNote', tooltip.onHover.bind(tooltip));
Hooks.once('setup', tooltip.init.bind(tooltip));
