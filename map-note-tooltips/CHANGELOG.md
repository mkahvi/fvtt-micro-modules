# Change Log

## 0.2.0.2

- Foundry v12 compatibility

## 0.2.0.1

- Fix: On world launch before hovering any notes, you could find empty tooltip at top left of the screen.

## 0.2

- Fix: Hovering over a note with no journal linked would display previous note's entry.
- New: Include journal title for tooltips that link to entire journal instead of a page.

## 0.1 Initial
