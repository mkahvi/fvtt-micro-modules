# Custom Feature Types for Pathfinder 1e

![Supported Foundry Versions](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fversion%3Fstyle%3Dflat%26url%3Dhttps%3A%2F%2Fgitlab.com%2Fmkahvi%2Ffvtt-micro-modules%2F-%2Fraw%2Fmaster%2Fpf1-custom-feat-types%2Fmodule.json)

Allows easy registration of custom feat types.

## Usage Notes

Keys can only contain basic latin letters (A-Z), no numbers or special symbols.

## Install

Manifest URL: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/master/pf1-custom-feat-types/module.json>

Requires: PF1 v10 & Foundry v11

### PF1v9 and older

Manual install: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/fc4f7eb5a1f4246d26b6a6fd0dedb8dcd86a4722/pf1-custom-feat-types/pf1-custom-feat-types.zip>

### Foundry v10 and older

Manual install: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/d0bf6a6a8498042f63ab53c48a97e3b8c80f003b/pf1-custom-feat-types/pf1-custom-feat-types.zip>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT license](./LICENSE).
