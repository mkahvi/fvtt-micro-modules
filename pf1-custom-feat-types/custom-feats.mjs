const MODULE_ID = 'pf1-custom-feat-types';

const INVALID_INPUT = /[^a-zA-Z]+/g;

let origTypes;
const pathPrefix = 'custom_';
const pathify = (key) => `${pathPrefix}${key}`;

const baseSort = (index = 0) => pf1.config.sheetSections.features.trait.sort + 100 * index;

class FeatConfig extends FormApplication {
	constructor() {
		super();
		this._resetConfig();
	}

	get template() {
		return `modules/${MODULE_ID}/config.hbs`;
	}

	getData() {
		this._initSections();

		const sections = this.sections;

		return {
			sections,
		};
	}

	static get defaultOptions() {
		const options = super.defaultOptions;
		return {
			...options,
			title: 'Configure Custom Feat Types',
			classes: [...options.classes, 'custom-feat-types'],
			dragDrop: [{ dragSelector: '.types .entry.custom', dropSelector: '.types' }],
			height: 'auto',
			width: 'auto',
			id: 'custom-feat-types-config',
			submitOnChange: true,
			submitOnClose: false,
			closeOnSubmit: false,
			resizable: true,
		};
	}

	/**
	 * @param {DragEvent} event
	 */
	_onDragStart(event) {
		const el = event.target;

		const label = el.querySelector('.key');
		event.dataTransfer.setDragImage(label, 10, 10);

		const dragData = {
			key: el.dataset.key,
			action: 'custom-feat-sort',
		};

		event.dataTransfer?.setData('text/plain', JSON.stringify(dragData));
	}

	_canDragStart() {
		return true;
	}

	_canDragDrop() {
		return true;
	}

	/**
	 * @param {DragEvent} event
	 */
	_onDrop(event) {
		let json;
		try {
			json = JSON.parse(event.dataTransfer?.getData('text/plain'));
		}
		catch (err) {
			console.error(err);
		}

		if (json?.action != 'custom-feat-sort') return;
		const srcKey = json.key;

		const el = event.target.closest('.entry');

		const targetKey = el.dataset.key;
		if (srcKey === targetKey) return; // dropped onto self

		const sec = this.sections.find(k => k.key == targetKey);
		const type = this.config[srcKey];
		const newsort = sec.sort + 100;
		console.log('Moved', srcKey, 'from', type.sort, 'to', newsort);
		type.sort = newsort;

		this.render();
	}

	/**
	 * @param {JQuery} jq
	 */
	activateListeners(jq) {
		super.activateListeners(jq);
		const html = jq[0];

		html.querySelectorAll('a[data-action]').forEach(el => {
			el.addEventListener('click', this._deleteType.bind(this));
		});

		html.querySelectorAll('input').forEach(el => {
			if (el.classList.contains('key')) {
				el.addEventListener('change', this._keyEdit.bind(this), { passive: true });
				el.addEventListener('input', this._keyActiveEdit.bind(this), { passive: true });
			}
			else if (el.classList.contains('label')) {
				// el.addEventListener('change', this._labelEdit.bind(this), { passive: true });
			}
		});

		const buttons = html.querySelector('.buttons');
		buttons.querySelector('button[data-action="submit"]')
			.addEventListener('click', this._saveConfig.bind(this));
		buttons.querySelector('button[data-action="reset"]')
			.addEventListener('click', this._resetConfig.bind(this));
	}

	/**
	 * @param {Event} ev
	 */
	async _saveConfig(ev) {
		ev.preventDefault();

		await this._onSubmit(ev, { preventRender: true });

		this.close({ submit: false });

		game.settings.set(MODULE_ID, 'config', this.config);
	}

	/**
	 * @param {Event} ev
	 */
	_resetConfig(ev) {
		ev?.preventDefault();

		this.config = foundry.utils.deepClone(game.settings.get(MODULE_ID, 'config'));
		this._initSections();

		this.render();
	}

	_initSections() {
		const sections = [];

		// Add built-in sections
		for (const [key, data] of Object.entries(pf1.config.sheetSections.features)) {
			if (key.startsWith(pathPrefix)) continue;
			sections.push({
				_path: key,
				key,
				...data,
				builtIn: true,
			});
		}

		// Get custom sections
		let index = 1;
		for (const [key, data] of Object.entries(this.config)) {
			if (origTypes.includes(key)) continue; // Safeguard against masking

			sections.push({
				_path: pathify(key),
				key,
				...data,
				sort: data.sort ?? baseSort(index),
				custom: true,
			});
			index++;
		}

		// Sort sections
		sections.sort((a, b) => (a.sort || 0) - (b.sort || 0));

		this.sections = sections;
	}

	/**
	 * @param {Event} ev
	 */
	_keyEdit(ev) {
		const el = ev.target;
		const slug = el.value.slugify({ strict: true }).replace(/[^a-z]+/g, '');
		if (el.value !== slug) {
			console.log('SLUGGING:', el.value, '->', slug);
			el.value = slug;
			ui.notifications.warn('Key modified to safe format');
		}
	}

	/**
	 * @param {InputEvent} ev
	 */
	_keyActiveEdit(ev) {
		if (ev.isComposing) return;
		const input = ev.data;
		if (input?.length) {
			// Prevent unsupported inputs
			const re = new RegExp(INVALID_INPUT);
			if (re.test(input)) {
				const elem = ev.target;
				const oldPos = elem.selectionStart - input.length;
				re.lastIndex = 0;
				elem.value = elem.value.slugify({ strict: true }).replaceAll(re, '');
				elem.setSelectionRange(oldPos, oldPos);
			}
		}
	}

	/**
	 * @param {Event} ev
	 */
	_labelEdit(ev) {
		const el = ev.target;

		el.value = el.value.replaceAll(/[<>]/g, ''); // Remove HTML
	}

	/**
	 * @param {Event} ev
	 */
	_deleteType(ev) {
		ev.preventDefault();

		const el = ev.currentTarget;
		const key = el.dataset.key;

		delete this.config[key];

		this.render();
	}

	/**
	 * @override
	 * @param {Event} event
	 * @param {object} formData
	 */
	_updateObject(event, formData) {
		formData = foundry.utils.expandObject(formData);

		// Update data
		const types = Object.values(formData.types ?? {});
		this.config = {};
		for (const { key, sort, label } of types) {
			this.config[key] = { label, sort };
		}

		// New entry
		const { newEntry } = formData;

		const { key, label } = newEntry;
		if (key || label) {
			if (key && this.config[key]) {
				return void ui.notifications.error('May not use conflicting keys');
			}
			else if (!(key && label)) {
				return void ui.notifications.warn('Both key and label need to be filled');
			}
			else {
				this.config[key] = { label, sort: baseSort(1) };
			}
		}

		this.render();
	}
}
async function migrateTypes() {
	if (!game.user.canUserModify(game.user, CONST.USER_PERMISSIONS.SETTINGS_MODIFY)) return;

	const oldTypes = game.settings.get(MODULE_ID, 'types');
	if (oldTypes.length === 0) return console.log('--------- No migration needed');

	console.log('Custom Feats |', game.modules.get(MODULE_ID).version, '| Migrating...');

	const config = foundry.utils.deepClone(game.settings.get(MODULE_ID, 'config'));

	for (const { key, label, sort, builtIn } of oldTypes) {
		if (!key || !label || builtIn) continue;
		if (origTypes.includes(key)) continue;
		config[key] = { label, sort: Number(sort) };
	}

	await game.settings.set(MODULE_ID, 'config', config);
	await game.settings.set(MODULE_ID, 'types', []);
	console.log('Custom Feats | Migrated settings from 0.2 to 0.3 format.');
}

const applyTypes = () => {
	// Check for migration need
	const oldTypes = game.settings.get(MODULE_ID, 'types');
	if (oldTypes.length > 0 && !game.ready) {
		Hooks.once('ready', migrateTypes);
		console.log('Custom Feats |', game.modules.get(MODULE_ID).version, '| Need Migration');
		return;
	}

	const config = game.settings.get(MODULE_ID, 'config');

	let index = 1;

	// Remove previously added sections
	for (const key of Object.keys(pf1.config.sheetSections.features)) {
		if (key.startsWith(pathPrefix)) delete pf1.config.sheetSections.features[key];
	}

	// Add sections
	for (const [key, { label, plural, sort }] of Object.entries(config)) {
		if (origTypes.includes(key)) continue;

		let plabel = plural || label;
		plabel = game.i18n.has(plabel) ? game.i18n.localize(plabel) : plabel;

		// Fake language keys if they don't exist
		const langPath = `PF1.Subtypes.Item.feat.${key}`;
		if (!game.i18n.has(langPath + '.Single')) {
			foundry.utils.setProperty(game.i18n.translations, langPath + '.Single', label);
		}
		if (!game.i18n.has(langPath + '.Plural')) {
			foundry.utils.setProperty(game.i18n.translations, langPath + '.Plural', plabel);
		}

		// Fill types
		pf1.config.featTypes[key] = game.i18n.has(label) ? game.i18n.localize(label) : label;
		pf1.config.featTypesPlurals[key] = plabel;

		// Create section
		const sectId = pathify(key);
		pf1.config.sheetSections.features[sectId] = {
			category: 'features',
			filters: [{ type: 'feat', subTypes: [key] }],
			create: { type: 'feat', system: { subType: key } },
			interface: { create: true, actions: true, types: true },
			label: plabel,
			id: sectId,
			path: `features.${sectId}`,
			sort: sort ?? baseSort(index),
		};
		console.log('> Created section:', plabel, { key });

		index++;
	}

	console.log('Custom Feats |', game.modules.get(MODULE_ID).version, '| Applied');

	// Re-render sheets to apply the change
	Object.values(ui.windows).forEach(app => {
		if (app instanceof ActorSheet) app.render();
	});
};

const initializeCustomFeats = () => {
	origTypes = Object.keys(pf1.config.featTypes);

	game.settings.register(MODULE_ID, 'config', {
		type: Object,
		scope: 'world',
		config: false,
		default: {},
		onChange: () => applyTypes(),
		// TODO: Add automatic sheet re-render?
	});

	game.settings.register(MODULE_ID, 'types', {
		type: Array,
		default: [],
		scope: 'world',
		config: false,
		onChange: () => applyTypes(),
		// TODO: Add automatic sheet re-render?
	});

	game.settings.registerMenu(MODULE_ID, 'config', {
		label: 'Configure Types',
		type: FeatConfig,
		restricted: true,
	});
};

Hooks.once('init', initializeCustomFeats);

// PF1 sorts and translates the entries in i18nInit
Hooks.once('init', () => {
	Hooks.once('i18nInit', applyTypes);
});
