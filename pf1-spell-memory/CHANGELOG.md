# Change Log

## 0.3.0

- PF1 v10 compatibility.
- Fix: Do not monitor spells outside of actors.

## 0.2.1

- New: Spontaneous (Arcanist) memorization was not handled.
- Fix (workaround): Spontaneous casters would get weird memorization messages due to PF1 bug [#2302](https://gitlab.com/foundryvtt_pathfinder1e/foundryvtt-pathfinder1/-/issues/2302).

## 0.2.0

- New: Delete undone option, for deleting the message if changes become undone.
- Change: No changes card includes notification of no changes instead of emptiness.
- Fix: Undoing memorization no longer causes an error.

## 0.1.0 Initial
