const MODULE_ID = 'pf1-spell-memory';

const CFG = {
	SETTINGS: {
		gracePeriod: 'gracePeriod',
		interveningMessages: 'interveningMessages',
		deleteUndone: 'deleteUndone',
	},
	COLOR: {
		main: 'color:goldenrod',
		number: 'color:mediumpurple',
		unset: 'color:unset',
	},
	templatePath: null,
};

CFG.templatePath = `modules/${MODULE_ID}/memory-card.hbs`;

const signNum = (value) => value >= 0 ? `+${value}` : `${value}`;

let notMyMemoryCounter = 5000;
let lastSpellMemoryMsgId = null;

const levelLabels = {
	0: 'Cantrips',
	1: '1st Level',
	2: '2nd Level',
	3: '3rd Level',
	4: '4th Level',
	5: '5th Level',
	6: '6th Level',
	7: '7th Level',
	8: '8th Level',
	9: '9th Level',
};

/**
 * Generate currency card contents based on currency deltas.
 *
 * @param actor
 * @param memoryData
 */
function createCardContent(actor, memoryData) {
	const template = Handlebars.partials[CFG.templatePath];

	const templateData = {
		actor,
		spells: {},
	};

	const spells = [];
	Object.entries(memoryData).forEach(([spellId, delta]) => {
		if (spellId.startsWith('-=')) {
			delta ??= 0;
			spellId = spellId.replace(/^-=/, '');
			return;
		}

		const spell = actor.items.get(spellId);
		if (spell) spells.push({ item: spell, delta });
		else console.error('Spell', spellId, 'not found');
	});

	if (spells.length === 0 && game.settings.get(MODULE_ID, CFG.SETTINGS.deleteUndone))
		return null;

	templateData.changes = spells.length;

	spells.sort((a, b) => {
		const d = a.item.system.level - b.item.system.level;
		if (d !== 0) return d;
		return (a.item.sort ?? 0) - (b.item.sort ?? 0);
	});

	spells.forEach(({ item, delta })=> {
		const level = item.system.level;
		templateData.spells[level] ??= { header: levelLabels[level], items: [] };
		templateData.spells[level].items.push({ item, delta });
	});

	return template(templateData, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true });
}

async function printMemoryCard(actor, item, delta) {
	const msgData = {
		content: null,
		flags: { [MODULE_ID]: { memory: true, spells: {} } },
		speaker: ChatMessage.getSpeaker({ actor }),
		user: game.user.id,
		type: CONST.CHAT_MESSAGE_TYPES.OOC,
	};

	msgData.flags[MODULE_ID].spells[item.id] = delta;
	msgData.content = createCardContent(actor, msgData.flags[MODULE_ID].spells);

	ChatMessage.applyRollMode(msgData, CONST.DICE_ROLL_MODES.PRIVATE);

	return ChatMessage.create(msgData);
}

async function updateLastCard(actor, item, delta) {
	const cm = game.messages.get(lastSpellMemoryMsgId);
	if (!cm) return printMemoryCard(actor, item, delta);

	const speakerActor = ChatMessage.getSpeakerActor(cm.speaker);
	if (actor !== speakerActor) return; // shouldn't happen

	const spells = cm.getFlag(MODULE_ID, 'spells') ?? {};

	const newDelta = (spells[item.id] ?? 0) + delta;
	if (newDelta === 0) {
		spells[`-=${item.id}`] = null;
		delete spells[item.id];
	}
	else spells[item.id] = newDelta;

	const msgData = {
		flags: { [MODULE_ID]: { memory: true, spells } },
		content: createCardContent(actor, spells),
	};

	if (!msgData.content) {
		lastSpellMemoryMsgId = null;
		return cm.delete();
	}

	lastSpellMemoryMsgId = cm.id;
	return cm.update(msgData);
}

/**
 * @param {ChatMessage} cm
 * @param {JQuery} html
 */
function reformatChatMessage(cm, [html]) {
	const mem = cm.getFlag(MODULE_ID, 'memory');
	if (mem === undefined)
		return void notMyMemoryCounter++;

	if (cm.isAuthor) {
		notMyMemoryCounter = 0;
		lastSpellMemoryMsgId = cm.id;
	}

	const wt = html.querySelector('.whisper-to');
	if (wt) wt.style.display = 'none';

	html.style.borderColor = null;
	html.classList.add(MODULE_ID);
}

/**
 * @param {Acctor} actor Actor
 * @param {Item} item Container if relevant
 * @param {number} delta  Memorization delta
 */
function finalizeMemorization(actor, item, delta) {
	const cm = game.messages.get(lastSpellMemoryMsgId),
		lastStamp = cm?.timestamp ?? 0,
		timeSince = Date.now() - lastStamp,
		speakerActor = cm ? ChatMessage.getSpeakerActor(cm.speaker) : null,
		sameActor = actor ? actor === speakerActor : false,
		grace = game.settings.get(MODULE_ID, CFG.SETTINGS.gracePeriod),
		maxDistance = game.settings.get(MODULE_ID, CFG.SETTINGS.interveningMessages);

	console.log('%cSPELL MEMORY%c | Memorized:', CFG.COLOR.main, CFG.COLOR.unset, item.name, delta);

	if (!sameActor || notMyMemoryCounter > maxDistance || timeSince > grace * 1_000) {
		printMemoryCard(actor, item, delta);
	}
	else {
		if (sameActor && grace > 0) updateLastCard(actor, item, delta);
		else printMemoryCard(actor, item, delta);
	}
}

/**
 * Detect previous memorization quantity.
 *
 * @param {Item} spell
 * @param {object} update
 * @param {object} context
 */
function preUpdateSpell(spell, update, context) {
	delete spell._previouslyPrepared;
	if (spell.type !== 'spell') return;
	if (context.diff === false || context.recursive === false) return;

	if (!spell.actor) return;

	const prepUpdate = update.system?.preparation;
	if (!prepUpdate) return;

	let delta = 0;

	const spellbook = spell.spellbook;
	if (!spellbook) return void console.error('SPELL MEMORY | Spell with no spellbook', { spell }); // Weird spell

	const mode = spellbook.spellPreparationMode;

	// v10 and onward
	if (foundry.utils.isNewerVersion(game.system.version, '9.6')) {
		const newMax = prepUpdate.max;
		if (newMax === undefined) return;
		const oldMax = spell.system.preparation?.max ?? 0;
		delta = newMax - oldMax;
	}
	// v9.6 and older
	else {
		if (mode === 'prepared') {
			const newMax = prepUpdate.maxAmount;
			if (newMax === undefined) return;

			const oldMax = spell.system.preparation?.maxAmount ?? 0;
			delta = newMax - oldMax;
		}
		else {
			const newPrep = prepUpdate.spontaneousPrepared;
			if (newPrep === undefined) return;

			const oldPrep = spell.system.preparation.spontaneousPrepared;
			if (newPrep !== oldPrep)
				delta = newPrep ? 1 : -1;
		}
	}

	if (delta !== 0) spell._previouslyPrepared = delta;
}

/**
 * React to memorization change.
 *
 * @param {Item} item
 * @param _update
 * @param _options
 * @param {string} userId
 */
function updateSpell(item, _update, _options, userId) {
	if (game.user.id !== userId) return;
	if (item.type !== 'spell') return;

	const delta = item._previouslyPrepared;
	delete item._previouslyPrepared;
	if (delta == undefined) return;

	const actor = item.actor;
	if (!actor) return;

	finalizeMemorization(actor, item, delta);
}

function registerSettings() {
	game.settings.register(
		MODULE_ID,
		CFG.SETTINGS.gracePeriod,
		{
			name: 'Grace period',
			hint: 'Period (in seconds) for how long recent memorization message can be updated with new info. Reduces log spamming. 0 disables.',
			type: Number,
			default: 180,
			range: { min: 0, max: 1500, step: 5 },
			scope: 'world',
			config: true,
		},
	);

	game.settings.register(
		MODULE_ID,
		CFG.SETTINGS.interveningMessages,
		{
			name: 'Max intervening messages',
			hint: 'New memorization message is printed instead of updating regardless of the passed time if there\'s this many messages since.',
			type: Number,
			default: 3,
			range: { min: 0, max: 20, step: 1 },
			scope: 'world',
			config: true,
		},
	);

	game.settings.register(
		MODULE_ID,
		CFG.SETTINGS.deleteUndone,
		{
			name: 'Delete undone',
			hint: 'Delete memorization message if changes end up undone.',
			type: Boolean,
			default: false,
			scope: 'world',
			config: true,
		},
	);
}

Hooks.once('init', registerSettings);
Hooks.once('setup', () => loadTemplates([CFG.templatePath]));

Hooks.on('renderChatMessage', reformatChatMessage);

Hooks.on('preUpdateItem', preUpdateSpell);
Hooks.on('updateItem', updateSpell);

// TODO: Allow GM to signal other clients to forcibly end grace periods.
// TODO: Forcibly end grace period when combat starts.
