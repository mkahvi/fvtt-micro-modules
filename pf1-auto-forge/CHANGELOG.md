# Change Log

## 0.3.2

- New: Reforge dialog for adjusting details without triggering automatic secondary adjusts.

## 0.3.1

- PF1v10 compatibility.

## 0.3.0.2

- Fix: Flat cost removal not affecting the item price.

## 0.3.0.1

- Fix: Weird behaviour with price handling, especially if base enhancement level was 0.

## 0.3

- Change: Qualities no longer require enhancement bonus to enter (to support items that can have qualities at +0)
- Change: Qualities no longer are cleared if enhancement bonus is set to 0 for same reasons as above.

## 0.2.0.2

- Fix: Clearing qualities would not refund cost.

## 0.2

- New: Fields for qualities by bonus and flast cost

## 0.1 Initial
