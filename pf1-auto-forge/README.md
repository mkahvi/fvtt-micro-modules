# Auto-forge for Pathfinder 1e

![Supported Foundry Versions](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fversion%3Fstyle%3Dflat%26url%3Dhttps%3A%2F%2Fgitlab.com%2Fmkahvi%2Ffvtt-micro-modules%2F-%2Fraw%2Fmaster%2Fpf1-auto-forge%2Fmodule.json)

Automatically adjusts item price, hit points and hardness with masterwork and enhancement level.

Optionally (default enabled) enforces masterwork state on items when enhancement bonus is added.

Adds also formula fields for bonus eating qualities and flat cost qualities.

Has options for configuring:

- Masterwork value (defaults to 300 for weapons, 150 for armor/shields)
- Enhancement bonus hit point and hardness adjust (defaults to 10 and 2 respectively)

Relevant rules:

- <https://www.aonprd.com/Rules.aspx?Name=Magic%20Weapons&Category=Magic%20Items>
- <https://www.aonprd.com/Rules.aspx?Name=Magic%20Armor&Category=Magic%20Items>
- <https://aonprd.com/Rules.aspx?Name=Smashing%20an%20Object&Category=Breaking%20and%20Entering>

## Flaws

- Does not account for materials at all.
  - Cold Iron or similar is not handled for heightened enhancement costs.
- Does not support items in containers since PF1 does not simulate normal CRUD for them.
- Amulet of Mighty Fists or similar is not supported as they have their own price track that can not be linked to the item.

## Future/Plans

- Add configuration field for enhancement bonus for qualities
- Material support

## Install

Manifest URL: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/master/pf1-auto-forge/module.json>

### For PF1v9 and earlier

Manual install only: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/297e3bb62dfb5179d7d9f87879ef526f72b0eb0a/pf1-auto-forge/pf1-auto-forge.zip>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT license](./LICENSE)
