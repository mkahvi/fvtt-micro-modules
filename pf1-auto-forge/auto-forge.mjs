const CFG = {
	id: 'pf1-auto-forge',
	SETTINGS: {
		autoMwk: 'autoMwk',
		reforge: 'reforge',
		mwkWeapon: 'mwkWeaponGp',
		mwkArmor: 'mwkArmorGp',
		enhValues: 'enhValues',
		enhHardness: 'enhHardness',
		enhHitPoints: 'enhHitPoints',
	},
};

const defaultEnhValues = {
	weapon: [2000, 8000, 18000, 32000, 50000, 72000, 98000, 128000, 162000, 200000],
	armor: [1000, 4000, 9000, 16000, 25000, 36000, 49000, 64000, 81000, 100000],
};

const signNum = (n) => n < 0 ? `${n}` : `+${n}`;

/* global RollPF */

/**
 * @param {Item} item
 * @param {object} update
 * @param {object} context
 */
function autoForge(item, update, context) {
	if (context._autoForge === false) {
		delete context._autoForge;
		console.log('Auto-Forge | Reforge');
		return;
	}
	const flagsChanged = update.flags?.['pf1-auto-forge'] !== undefined;
	if (!update.system && !flagsChanged) return; // No system or flag data changed
	if (context.diff == false || context.recursive == false) return; // Ignore
	if (!['weapon', 'equipment'].includes(item.type)) return;

	update.system ??= {};

	const isWeapon = item.type === 'weapon';
	const isEquipment = item.type === 'equipment';
	const isArmor = isEquipment && item.subType === 'armor';
	const isShield = isEquipment && item.subType === 'shield';
	if (isEquipment && !(isArmor || isShield)) return;

	// Prepare enhancement detection
	const oldEnh = (isWeapon ? item.system.enh : item.system.armor?.enh) ?? 0;
	let enh = isWeapon ? update.system.enh : update.system.armor?.enh;
	if (enh === null) enh = 0;
	else enh ??= oldEnh;
	const enhChanged = enh !== oldEnh;

	// Auto masterwork
	const autoMwk = game.settings.get(CFG.id, CFG.SETTINGS.autoMwk);
	if (autoMwk) {
		let resetMwk = false;
		if (enh > 0)
			resetMwk = true;
		else if (oldEnh > 0 && !enhChanged)
			resetMwk = true;

		if (resetMwk && (update.system.masterwork === false || item.system.masterwork !== true)) {
			update.system.masterwork = true;
			console.log('Auto-Forge | Enforcing masterwork due to enhancement bonus');
		}
	}

	// Prepare mwk detection
	const oldMwk = item.system.masterwork ?? false;
	const mwk = update.system.masterwork ?? oldMwk;
	const mwkChanged = mwk !== oldMwk;

	// Prepare price detection
	const getValue = () => update.system.price ?? item.system.price ?? 0;

	// Masterwork changed
	if (mwkChanged) {
		let mwkValue = isWeapon ? game.settings.get(CFG.id, CFG.SETTINGS.mwkWeapon) : game.settings.get(CFG.id, CFG.SETTINGS.mwkArmor);
		// Don't adjust price if something else adjusted price
		if (!mwk) mwkValue = -mwkValue;
		update.system.price = getValue() + mwkValue;
		console.log('Auto-Forge | Masterwork', mwk, signNum(mwkValue), 'gp');
	}

	// Enhancement changed
	const enhChange = enh - oldEnh;
	if (enhChanged) {
		console.log('Auto-Forge | Base Enhancement', signNum(enhChange));
		// Adjust hardness
		const hardnessBonus = game.settings.get(CFG.id, CFG.SETTINGS.enhHardness);
		const hardnessChange = enhChange * hardnessBonus;
		update.system.hardness = (update.system.hardness ?? item.system.hardness ?? 0) + hardnessChange;
		console.log('Auto-Forge | Hardness', signNum(hardnessChange));

		// Adjust hit points
		const hitPointBonus = game.settings.get(CFG.id, CFG.SETTINGS.enhHitPoints);
		const hitPointChange = enhChange * hitPointBonus;
		update.system.hp ??= {};
		update.system.hp.value = (update.system.hp?.value ?? item.system.hp?.value ?? 0) + hitPointChange;
		update.system.hp.max = (update.system.hp?.max ?? item.system.hp?.max ?? 0) + hitPointChange;
		console.log('Auto-Forge | Hit Points', signNum(hitPointChange));
	}

	// Handle qualities and flat cost
	const qualityOld = item.getFlag('pf1-auto-forge', 'bonus') || '',
		flatOld = item.getFlag('pf1-auto-forge', 'flat') || '';
	const flags = update.flags?.['pf1-auto-forge'] ?? {};
	let qualityNew = flags.bonus?.trim(),
		flatNew = flags.flat?.trim();

	// Handle value deletion
	if (flags['-=bonus'] !== undefined) qualityNew ??= '';
	if (flags['-=flat'] !== undefined) flatNew ??= '';

	const qChanged = qualityNew !== undefined && qualityOld !== qualityNew,
		fChanged = flatNew !== undefined && flatOld !== flatNew;

	let qEnhOld = 0, qEnhNew = 0;
	if (qChanged || enhChanged) {
		qEnhOld = RollPF.safeRollSync(qualityOld || '0');
		qEnhOld = qEnhOld.err ? 0 : qEnhOld.total;

		if (qualityNew !== undefined) {
			qEnhNew = RollPF.safeRollSync(qualityNew || '0');
			qEnhNew = qEnhNew.err ? 0 : qEnhNew.total;
		}
		else qEnhNew = qEnhOld;

		const qDiff = qEnhNew - qEnhOld;
		if (qDiff !== 0)
			console.log('Auto-Forge | Quality bonus change:', signNum(qDiff), { old: qEnhOld, new: qEnhNew });
	}

	let fEnhOld = 0, fEnhNew = 0;
	if (fChanged) {
		fEnhOld = RollPF.safeRollSync(flatOld || '0');
		fEnhOld = fEnhOld.err ? 0 : fEnhOld.total;

		if (flatNew !== undefined) {
			fEnhNew = RollPF.safeRollSync(flatNew || '0');
			fEnhNew = fEnhNew.err ? 0 : fEnhNew.total;
		}
		else fEnhNew = fEnhOld;

		const fDiff = fEnhNew - fEnhOld;
		if (fDiff !== 0) {
			update.system.price = getValue() + fDiff;
			console.log('Auto-Forge | Flat cost change:', signNum(fDiff), { old: fEnhOld, new: fEnhNew });
		}
	}

	// Adjust price from enhancement bonus
	if (enhChanged || qChanged) {
		// TODO: Consider getValue() override instead
		const enhValuesCfg = game.settings.get(CFG.id, CFG.SETTINGS.enhValues);
		const enhValues = isWeapon ? enhValuesCfg.weapon : enhValuesCfg.armor;
		const oldTotalEnh = oldEnh + qEnhOld,
			newTotalEnh = enh + qEnhNew;
		const oldEnhValue = oldTotalEnh > 0 ? (enhValues[oldTotalEnh - 1] ?? enhValues.at(-1)) : 0;
		const newEnhValue = newTotalEnh > 0 ? (enhValues[newTotalEnh - 1] ?? enhValues.at(-1)) : 0;
		const enhValueChange = newEnhValue - oldEnhValue;
		update.system.price = getValue() + enhValueChange;

		const qEnhChange = qEnhNew - qEnhOld;

		console.log('Auto-Forge | Total Enhancement', signNum(enhChange + qEnhChange), '=', newTotalEnh, signNum(enhValueChange), 'gp');
	}
}

class AutoReforge extends DocumentSheet {
	static get defaultOptions() {
		const options = super.defaultOptions;
		return {
			...options,
			template: `modules/${CFG.id}/templates/reforge.hbs`,
			classes: [...options.classes, 'autoforge', 'reforge'],
			closeOnSubmit: false,
			submitOnClose: false,
			submitOnChange: false,
			sheetConfig: false,
			width: 500,
			height: 'auto',
			resizable: true,
		};
	}

	get title() {
		return game.i18n.format('AutoForge.Reforge.Title', { name: this.document.name });
	}

	get id() {
		return `auto-reforge-${this.document.uuid.replaceAll('.', '-')}`;
	}

	getData() {
		const item = this.document;
		const itemData = item.toObject();

		const bonus = item.getFlag('pf1-auto-forge', 'bonus'),
			flat = item.getFlag('pf1-auto-forge', 'flat');

		return {
			enh: {
				bonus,
				flat,
			},
			flags: itemData.flags,
			system: itemData.system,
		};
	}

	// Hide excess header elements
	_getHeaderButtons() {
		return super._getHeaderButtons().filter(b => b.class === 'close');
	}

	/**
	 *
	 * @param {Event} event
	 * @param {object} formData
	 */
	_updateObject(event, formData) {
		this.form.classList.add('saving');
		for (const el of this.form.elements) el.disabled = true;
		const { bonus, flat } = foundry.utils.expandObject(formData).reforge;

		this.document.update({
			flags: { [CFG.id]: { bonus, flat } },
		},
		{
			_autoForge: false,
		});

		this.close();
	}

	static open(item) {
		const app = new this(item);
		app.render(true);
		return app;
	}
}

/**
 * @param {ItemSheet} app
 * @param {JQuery<HTMLElement>} html
 * @param {object} options
 */
function autoForgeDisplay(app, [html], options) {
	const item = app.item;
	if (!['weapon', 'equipment'].includes(item.type)) return;

	const isWeapon = item.type === 'weapon';
	const isEquipment = item.type === 'equipment';
	const isArmor = isEquipment && item.subType === 'armor';
	const isShield = isEquipment && item.subType === 'shield';
	// const isWondrous = isEquipment && item.subType === 'wondrouds';
	// const isOther = isEquipment && item.subType === 'other';
	if (isEquipment && !(isArmor || isShield)) return;

	const autoMwk = game.settings.get(CFG.id, CFG.SETTINGS.autoMwk);

	const enh = (isWeapon ? item.system.enh : item.system.armor?.enh) || 0;
	if (enh > 0 && autoMwk) {
		const mwkEl = html.querySelector('[name="system.masterwork"]');
		if (mwkEl) {
			if (!mwkEl.checked)
				mwkEl.checked = true;
			else
				mwkEl.disabled = true;
			mwkEl.parentElement.dataset.tooltip = 'AutoForge.Tooltip.MwkEnforced';
		}
	}

	const enhEl = html.querySelector('input[name="system.enh"],input[name="system.armor.enh"]');
	const enhElP = enhEl?.closest('.form-group');
	if (enhElP) {
		const quality = item.getFlag('pf1-auto-forge', 'bonus'),
			flat = item.getFlag('pf1-auto-forge', 'flat');

		const rq = RollPF.safeRollSync(quality || '0');
		// const rf = RollPF.safeRollSync(flat || '0');

		const total = !rq.err ? enh + (rq.total || 0) : 0;

		const templateData = { quality, flat, total, reforge: game.settings.get(CFG.id, CFG.SETTINGS.reforge) };

		const div = document.createElement('template');
		div.innerHTML = CFG.editorTemplate(templateData, {
			allowProtoMethodsByDefault: true,
			allowProtoPropertiesByDefault: true,
		});

		div.content.querySelectorAll('input').forEach(el => {
			el.addEventListener('change', ev => {
				ev.preventDefault();
				ev.stopPropagation();
				ev.stopImmediatePropagation();

				const r = RollPF.safeRollSync(el.value || '0', {}, undefined, undefined, { maximize: true });
				if (r.err || !r.isDeterministic) {
					el.setCustomValidity(game.i18n.localize('AutoForge.Editor.InvalidFormula'));
					el.reportValidity();
				}
				else if (el.classList.contains('enh-quality') && (r.total + enh) > 10) {
					el.setCustomValidity(game.i18n.localize('AutoForge.Editor.OverEnhanced'));
					el.reportValidity();
				}
				else {
					const nval = el.value.trim() || null;
					if (nval)
						item.setFlag(CFG.id, el.dataset.autoForge, nval);
					else
						item.unsetFlag(CFG.id, el.dataset.autoForge);
				}
			});
		});

		div.content.querySelector('a.reforge')?.addEventListener('click', ev => {
			ev.preventDefault();
			AutoReforge.open(item);
		});

		enhElP.after(div.content);
	}
}

function registerSettings() {
	game.settings.register(CFG.id, CFG.SETTINGS.autoMwk, {
		name: 'AutoForge.Settings.AutoMwk',
		hint: 'AutoForge.Settings.AutoMwkHint',
		type: Boolean,
		default: true,
		scope: 'world',
		config: true,
	});

	game.settings.register(CFG.id, CFG.SETTINGS.mwkWeapon, {
		name: 'AutoForge.Settings.MwkWeapon',
		type: Number,
		default: 300,
		scope: 'world',
		config: true,
	});

	game.settings.register(CFG.id, CFG.SETTINGS.mwkArmor, {
		name: 'AutoForge.Settings.MwkArmor',
		type: Number,
		default: 150,
		scope: 'world',
		config: true,
	});

	game.settings.register(CFG.id, CFG.SETTINGS.enhHardness, {
		name: 'AutoForge.Settings.EnhHardness',
		type: Number,
		default: 2,
		scope: 'world',
		config: true,
	});

	game.settings.register(CFG.id, CFG.SETTINGS.enhHitPoints, {
		name: 'AutoForge.Settings.EnhHitPoints',
		type: Number,
		default: 10,
		scope: 'world',
		config: true,
	});

	game.settings.register(CFG.id, CFG.SETTINGS.enhValues, {
		type: Object,
		default: { ...defaultEnhValues },
		scope: 'world',
		config: false,
	});

	game.settings.register(CFG.id, CFG.SETTINGS.reforge, {
		name: 'AutoForge.Settings.Reforge',
		hint: 'AutoForge.Settings.ReforgeHint',
		type: Boolean,
		default: true,
		scope: 'client',
		config: true,
	});
}

Hooks.on('preUpdateItem', autoForge);
Hooks.on('renderItemSheet', autoForgeDisplay);
Hooks.once('init', registerSettings);
Hooks.once('setup', () => getTemplate(`modules/${CFG.id}/templates/enhancement-editor.hbs`).then(t => CFG.editorTemplate = t));
