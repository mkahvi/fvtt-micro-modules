const MODULE_ID = 'mass-import';

class Import {
	json;
	get name() { return this.json.name; }
	get id() { return this.json.id; }
	get type() { return this.json.type; }
	get img() { return this.json.img; }
	documentName;
	keepId = false;

	constructor({ json } = {}) {
		this.json = json;
	}
}

class MassImportDialog extends FormApplication {
	imports = new Collection();
	_debounceRefresh;

	constructor() {
		super();

		this._debounceRefresh = foundry.utils.debounce(() => this.render(), 200);
	}

	get template() {
		return `modules/${MODULE_ID}/import-dialog.hbs`;
	}

	static get defaultOptions() {
		const _default = super.defaultOptions;
		return {
			..._default,
			title: 'Mass Import',
			classes: [..._default.classes, 'mass-import-dialog'],
			dragDrop: [{ dropSelector: 'form' }],
			submitOnChange: true,
			submitOnClose: false,
			closeOnSubmit: false,
			width: 'auto',
			height: 'auto',
		};
	}

	getData() {
		const folders = {};
		game.folders?.forEach(f => {
			folders[f.type] ??= {};

			// Build full path to folder to account for tree structure
			const path = [f.name];
			let parent = f.folder;
			while (parent) {
				path.unshift(parent.name);
				parent = parent.folder;
			}

			folders[f.type][f.id] = path.join('/');
		});

		return {
			imports: this.imports,
			folders,
		};
	}

	/**
	 *
	 * @param {String} uid
	 */
	_processImport(uid) {
		const importData = this.imports.get(uid);
		importData.uid = uid;

		const json = importData.json;

		if (json.type) {
			if (json.command && ['script', 'chat'].includes(json.type))
				importData.documentName = 'Macro';
			else if (game.template.Actor.types.includes(json.type))
				importData.documentName = 'Actor';
			else if (game.template.Item.types.includes(json.type))
				importData.documentName = 'Item';
			else
				console.log('Unknown document type:', json);
		}
		else {
			if (Array.isArray(json.pages))
				importData.documentName = 'JournalEntry';
			else if (json.grid !== undefined)
				importData.documentName = 'Scene';
			else if (Array.isArray(json.results))
				importData.documentName = 'RollTable';
			else

				console.log('Unknown document type:', json);
		}

		this._debounceRefresh();
	}

	/**
	 * @param {ProgressEvent<FileReader>} ev
	 */
	_readFileComplete(ev) {
		const json = JSON.parse(ev.target.result);
		const uid = randomID();
		this.imports.set(uid, new Import({ json }));

		Promise.resolve()
			.then(() => this._processImport(uid));
	}

	/**
	 * @param {DragEvent} event
	 */
	async _onDrop(event) {
		event.preventDefault();

		for (const file of event.dataTransfer?.files ?? []) {
			const fr = new FileReader();

			fr.onload = this._readFileComplete.bind(this);

			if (file.type === 'application/json')
				fr.readAsText(file);
			else
				console.error(`Dropped file "${file.name}" is of unsupported type: ${file.type}`);
		}
	}

	/**
	 * @param {String} uid Import UID
	 */
	_deleteImport(uid) {
		this.imports.delete(uid);
		this.render();
	}

	/**
	 * @param {JQuery} jq
	 */
	activateListeners(jq) {
		super.activateListeners(jq);

		const html = jq[0];

		const actions = html.querySelector('form > .header .actions');
		actions.querySelector('.refresh-action').addEventListener('click', ev => {
			ev.preventDefault();
			this.render();
		});

		html.querySelectorAll('.drops .import.document[data-unique-id]').forEach(el => {
			const uid = el.dataset.uniqueId;
			const imp = this.imports.get(uid);
			el.addEventListener('contextmenu', ev => {
				const cls = CONFIG[imp.documentName].documentClass;
				const instance = new cls(imp.json);
				instance.sheet.render(true, { focus: true, editable: false });
			});

			const db = el.querySelector('.actions .delete-action');
			db.addEventListener('click', ev => {
				ev.preventDefault();
				this._deleteImport(uid);
			});
		});

		const ib = html.querySelector('.buttons .import-button');
		if (this.imports.size == 0) {
			ib.readOnly = true;
			ib.disabled = true;
		}
		else {
			ib.addEventListener('click', ev => {
				ev.preventDefault();
				ev.stopPropagation();
				ib.readOnly = true;
				ib.disabled = true;
				this._doImport = true;
				this.submit();
			});
		}
	}

	async _importDocuments() {
		const form = this.element[0].querySelector('form');
		form.classList.add('import-in-progress');
		for (const el of form.elements)
			el.disabled = true;

		ui.notifications.info('Mass Importing Documents...');

		const promises = [];
		for (const imp of this.imports) {
			try {
				const cls = CONFIG[imp.documentName].documentClass;
				const options = {};
				if (imp.keepId) options.keepId = true;
				const p = cls.create(imp.json, options);
				p
					.then(_ => ui.notifications.info(`Imported [${imp.documentName}]: ${imp.name}`))
					.catch(_ => ui.notifications.warn(`Import failed [${imp.documentName}]: ${imp.name}`));
				promises.push(p);
			}
			catch (err) {
				ui.notifications.error(err.message);
				console.error(err);
			}
		}

		await Promise.allSettled(promises);
		ui.notifications.info('Mass Import Complete!');
		this.close();
	}

	_updateObject(event, formData) {
		const fd = expandObject(formData);
		Object.entries(fd).forEach(([key, data]) => {
			const imp = this.imports.get(key);
			imp.json.name = data.name;
			imp.keepId = data.keepId ?? false;
			imp.json.folder = data.folder || null;
		});

		if (this._doImport)
			this._importDocuments();
	}
}

Hooks.once('ready', () => {
	game.modules.get(MODULE_ID).api = {
		import: () => new MassImportDialog().render(true, { focus: true }),
	};
});
