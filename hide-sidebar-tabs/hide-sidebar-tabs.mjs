const MODULE_ID = 'hide-sidebar-tabs';

/**
 * @param dir Application
 * @param {JQuery} html
 */
const hideSidebarTab = (dir, [html]) => {
	const id = dir.id
	const hide = game.settings.get(MODULE_ID, id);
	if (hide) {
		if (dir.documents.length > 0) return;
		const tabs = document.getElementById('sidebar-tabs');
		tabs.querySelector(`a.item[data-tab="${id}"]`)?.remove();
		dir.close(); // Close the entire app

		// Delayed removal
		Promise.resolve()
			.then(() => html.remove());
	}
}

Hooks.once('init', () => {
	game.settings.register(MODULE_ID, 'cards', {
		name: 'DOCUMENT.CardsPlural',
		hint: 'HideSidebarTabs.CardsHint',
		type: Boolean,
		default: true,
		scope: 'client',
		config: true
	});

	game.settings.register(MODULE_ID, 'playlists', {
		name: 'DOCUMENT.Playlists',
		hint: 'HideSidebarTabs.PlaylistsHint',
		type: Boolean,
		default: true,
		scope: 'client',
		config: true
	});
});

Hooks.on('renderCardsDirectory', hideSidebarTab);
Hooks.on('renderPlaylistDirectory', hideSidebarTab);
