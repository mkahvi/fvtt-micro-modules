const module = 'init-override';

let origFormula;

function setInitiative(formula) {
	formula ??= game.settings.get(module, 'formula')?.trim();
	CONFIG.Combat.initiative.formula = formula.length > 0 ? formula : origFormula;
}

Hooks.once('init', () => {
	game.settings.register(
		module,
		'formula',
		{
			name: 'Formula',
			hint: 'Leave empty to use original formula.',
			type: String,
			default: '',
			scope: 'world',
			config: true,
			onChange: setInitiative,
		});
});

Hooks.once('ready', () => {
	origFormula = CONFIG.Combat.initiative.formula;
	setInitiative();
});
