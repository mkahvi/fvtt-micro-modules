# Change Log

## 0.6.0

- Major refactor, everything's likely broken.
- Change: Drain config moved to Foundry flags from item dictionary flags. Migration will automatically convert existing cases.
- New: Basic API with access to the module's migration functions.
- New: Drain value is displayed on the refund card to clarify cases of using random drain.
- Removed: Dismiss button in refund cards. Users have easy access to message deletion nowadays.
- Change: Equipment, Weapons and Consumables are allowed as drain targets.
- Change: Configuration moved to advanced tab.
- Minimum system version increased to v10.8 and minimum Foundry version increased to v11

## 0.5.0

- Change: Actors are recognized by their UUID rather than ID, allowing unlinked actors to work correctly.
- Support for Foundry v9 and older dropped

## 0.4.1

- i18n: Improved support
- Fix: Formula with ternaries did not work (Or any any other colon use).
- Minor refactoring

## 0.4.0.1

- Hotfix for v10 permission checking.

## 0.4.0

- Foundry v10 compatibilty

## 0.3.0.3

- Fix: Uneditable sheets were not respected correctly.
- Maintenance update.

## 0.3.0.2

- Fix: Chat card wraps its contents weird [#21]

## 0.3.0.1

- Removed debug output
- Fix: Scene identification (for identifying token)
- Fix: Chat message mode simplified, making it more reliable.
- Fix: Bad versioning causing update flow to break.

## 0.3.0 Skipped due to bad versioning

## 0.2.3

- New: Item Hint interop [#12]
- Maintenance

## 0.2.1.1

- Fix: Handling for missing resource items.

## 0.2.1

- New: Grouping & display actual resource name

## 0.2.0

- Fix: Card was not visible to owner.
- Change: Lazy template loading.
- General maintenance
- Support for Foundry VTT 0.7 dropped.

## 0.1.2.1

- Fix: UI resets in Foundry 0.7 [#8]

## 0.1.2

- Foundry 0.7 support.

## 0.1.1.1

- Fix: .split of undefined

## 0.1.1

- Fix: Update items only if there's something to update
- Improved output
- Remove debug output

## 0.1.0 Initial release
