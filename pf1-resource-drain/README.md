# Resource Drain for Pathfinder 1e

![Supported Foundry Versions](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fversion%3Fstyle%3Dflat%26url%3Dhttps%3A%2F%2Fgitlab.com%2Fmkahvi%2Ffvtt-micro-modules%2F-%2Fraw%2Fmaster%2Fpf1-resource-drain%2Fmodule.json)

Drains resource every round it is active.

Drain happens at start of the round and is kept active until the start of the next round when it is disabled if you are out of associated resource. Early drain is to avoid situations where you have multiple abilities that use the same resource and you no longer have enough to keep the current round going.

A chat card is printed to optionally disable the associated buff and refund the cost for this round if desired.

☐ It's a bit awkward, but it does what it says it does.

## Flaws

- If multiple things drain the same resources, this does not currently give choice which to end if you have resources left only for some.

## Usage

### Chat cards

Normally you'll see a chat card showing resource counter with remaining resource after paying for the buff:  
![Normal](./img/screencaps/chat-card.png)

Last round is signaled differently just to make sure that the 0 remaining charges is not a mistake:  
![Last Round](./img/screencaps/last-round.png)

Once the last round is done an the buff is still active, it is de-activated and announced:  
![Out of Resource](./img/screencaps/out-of-resource.png)

The end & refund button causes the buff cost to be refunded and the buff disabled. This is compromise to make it easier to end the buff without needing to manually needing to adjust resources, or running into other problems via late consumption of the resource.

Dismiss button simply removes the chat card.

### User-friendly configuration

Bottom of details tab of a buff has resource drain configuration UI:  
![Config](./img/screencaps/friendly-config.png)

### Manual configuration

Add `resourceDrain` dictionary flag in a buff with resource name and drain formula entered as its value, separated by colon: For example: `rage:1` would drain resource `rage` by `1`.

![Example](./img/screencaps/manual-config.png)

### Friendly notes

By making the drain value negative, you instead make it charge up the resource.

The value is treated as a formula with the buff's roll data.

## Install

Manifest URL: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/master/pf1-resource-drain/module.json>

Manual install for v10: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/d33975440e6403cc296715484153e592abee554a/pf1-resource-drain/pf1-resource-drain.zip>

Manual install for v9 and older: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/blob/c85afbc73f003fae988e43f99c2e80e006f6eeeb/pf1-resource-drain/pf1-resource-drain.zip>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
