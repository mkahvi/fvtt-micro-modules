const CFG = /** @type {const} */ ({
	id: 'mkah-pf1-resource-drain',
	SETTING: {
		migration: 'migration',
		recordKey: 'resourceDrainState',
		resourceKey: 'resourceDrain',
		refundCardKey: 'refundCard',
		debug: 'debug',
	},
});

const editorTemplate = /** @type {const} */ (`modules/${CFG.id}/editor.hbs`);
const cardTemplate = /** @type {const} */ (`modules/${CFG.id}/refund-card.hbs`);

const logtitle = '🪫 RESOURCE DRAIN';

/**
 * Test if migration is needed.
 *
 * @param {Item} item
 * @returns {boolean} - True if migration is needed
 */
function testMigration(item) {
	const flag = item.getItemDictionaryFlag(CFG.SETTING.resourceKey);
	if (flag === undefined) return false;
	return true;
}

/**
 * Migrate dictionary flags to Foundry flags.
 *
 * @param {Item} item
 * @returns {Promise|undefined} - Undefined if no migration was needed, promise otherwise
 */
function migrateItem(item) {
	const flag = item.getItemDictionaryFlag(CFG.SETTING.resourceKey);
	if (flag === undefined) return;

	console.debug(logtitle, '| Migrating | Item |', item.name, `[${item.uuid}]`);

	const [key, value] = getOldKeyVal(flag);

	let itemId, resId;
	if (key.startsWith('#')) itemId = key.slice(1);
	else resId = key;

	return item.update({
		system: {
			flags: {
				dictionary: {
					[`-=${CFG.SETTING.resourceKey}`]: null,
				},
			},
		},
		flags: {
			[CFG.id]: {
				resourceDrain: {
					itemId,
					resource: resId,
					value,
				},
			},
		},
	});
}

async function migrateActor(actor) {
	const migrations = [];
	for (const item of actor.items) {
		if (testMigration(item)) {
			migrations.push(item);
		}
	}

	if (migrations.length) {
		console.debug(logtitle, '| Migrating | Actor |', actor.name, `[${actor.uuid}]`);
		console.debug(logtitle, '| Migrating | Actor |', migrations.length, 'item(s)');
		for (const item of migrations) {
			await migrateItem(item);
		}
		return migrations;
	}
	else {
		// console.debug(logtitle, '| Migratio Not Needed |', actor.name, `[${actor.uuid}]`);
		return [];
	}
}

const getOldKeyVal = (source) => {
	if (!source || typeof source !== 'string') return ['', ''];
	const idx = Math.max(source.indexOf(':') ?? 0, 0),
		rdKey = source.substring(0, idx) ?? '',
		rdValue = source.substring(idx + 1) ?? '';
	return [rdKey, rdValue];
};

class DrainData {
	/** @type {string} */
	itemId;
	/** @type {string} */
	value;
	/** @type {string} */
	resource;

	get key() {
		if (this.itemId) return `#${this.itemId}`;
		else return this.resource;
	}

	/** @type {Item} */
	#parent;
	/** @type {Item} */
	#target;

	constructor(data, parent) {
		this.itemId = data.itemId?.trim();
		this.value = data.value?.trim();
		this.resource = data.resource?.trim();
		this.#parent = parent;

		const actor = parent.actor;
		if (actor) {
			if (this.itemId) this.#target = actor.items.get(this.itemId);
			else this.#target = actor.system.resources?.[this.resource]?.item;
		}
	}

	get parent() { return this.#parent; }
	get target() { return this.#target; }

	static fromItem(item) {
		const data = item.getFlag(CFG.id, CFG.SETTING.resourceKey);
		if (!data) return;

		return new this(data, item);
	}
}

class CombatRecord { id; round; }

/**
 * @param {Document} thing
 * @param {*} permission
 * @returns {User[]}
 */
const getUsers = (thing, permission) => game.users.filter(user => thing.testUserPermission(user, permission));

async function printRefundCards(combat, refundables) {
	const actor = combat.combatant.actor,
		token = combat.combatant.token,
		scene = combat.scene;

	const msgs = [];

	for (const rf of refundables) {
		const flags = {};
		flags[CFG.id] = {
			refund: {
				delta: rf.delta,
				item: rf.item,
				resource: rf.resource,
				target: rf.target.id,
				actor: actor.uuid,
				buff: rf.buff.id,
			},
		};

		const target = rf.target;

		const templateData = {
			buff: rf.buff,
			roll: { raw: rf.roll, anchor: rf.roll.toAnchor().outerHTML },
			target,
			resource: {
				value: target.charges,
				max: target.maxCharges,
			},
			lastRound: rf.lastRound,
		};

		const msg = {
			content: await renderTemplate(cardTemplate, templateData),
			speaker: ChatMessage.getSpeaker({ actor, token, scene }),
			type: CONST.CHAT_MESSAGE_TYPES.OTHER,
			flags,
		};

		ChatMessage.applyRollMode(msg, CONST.DICE_ROLL_MODES.PRIVATE);

		msgs.push(msg);
		// console.debug(logtitle, '| Message Data:', msg);
	}

	return ChatMessage.create(msgs);
};

async function drainResources(actor, combat) {
	const lastState = actor.getFlag(CFG.id, CFG.SETTING.recordKey) ?? new CombatRecord();
	if (lastState.id === combat.id && lastState.round === combat.round) {
		console.debug(logtitle, '| Already applied this turn for', actor.name);
		return;
	}

	const activeOwners = () => getUsers(actor, CONST.DOCUMENT_OWNERSHIP_LEVELS.OWNER).filter(u => u.active && !u.isGM).sort((a, b) => a.id.localeCompare(b.id));
	const activeGMs = () => game.users.filter(u => u.isGM && u.active).sort((a, b) => a.id.localeCompare(b.id));
	const actualUser = activeOwners()[0] ?? activeGMs()[0];
	if (actualUser?.id !== game.user.id) return;

	const updates = [];
	const buffs = actor.itemTypes.buff ?? [];
	const activeBuffs = buffs.filter(i => i.isActive);

	// console.debug(logtitle, '|', { actor, buffs, activeBuffs });

	const deactivatedBuffs = [];
	const refundables = [];

	for (const buff of activeBuffs) {
		const rdData = DrainData.fromItem(buff);
		if (!rdData) continue;

		const { itemId, value: rdValue, resource, target: rItem } = rdData;

		console.debug(logtitle, '|', buff.name, rdData);

		if (!rdData.target) {
			console.warn(logtitle, '|', buff.name, '| Resource Not Found:', { itemId, resourceId: resource, buff });
			continue;
		}

		const rdTarget = rItem?.name || resource || itemId;
		console.debug(logtitle, '|', buff.name, '| Drain target:', rdTarget);

		const rs = {
			_id: rItem.id,
			value: rItem.charges,
			max: rItem.maxCharges,
			target: rItem,
		};

		// In case another thing changed it and we queued update for it
		const rsCached = updates.find(u => u._id === rs._id);
		if (rsCached) rs.value = rsCached.system.uses.value;

		const rd = buff.getRollData();
		/* global RollPF */
		const rdRoll = await RollPF.safeRoll(rdValue, rd, CFG.SETTING.resourceKey);
		const rDelta = rdRoll.total;
		if (!Number.isSafeInteger(rDelta)) {
			console.error(logtitle, '|', buff.name, '| Formula:', rdValue, '| Invalid Result:', rDelta);
			continue;
		}
		console.debug(logtitle, '|', buff.name, '| Drain value:', rDelta);
		if (rDelta === 0) continue; // no point in adjusting things

		const old = rs.value;
		const newValue = Math.clamped(rs.value - rDelta, 0, rs.max);

		let updateResource = true;
		if (rDelta > 0) {
			console.debug(logtitle, '|', buff.name, 'drained', rdTarget, 'by', rDelta, 'from', old, 'to', newValue);
			if (old < rDelta) {
				// Not enough to keep running = disable buff
				console.debug(logtitle, '|', buff.name, 'ran out of juice.', buff);
				updates.push({ _id: buff.id, 'system.active': false });
				deactivatedBuffs.push(buff);
				updateResource = false; // resource already out
			}
			else {
				refundables.push({ buff, delta: rDelta, roll: rdRoll, resource: rdTarget, target: rs.target, lastRound: newValue < rDelta });
				if (newValue < rDelta) console.debug(logtitle, '| Last round:', rdTarget);
			}
		}
		else { // going up
			console.debug(logtitle, '|', buff.name, 'charged', rdTarget, 'by', rDelta, 'from', old, 'to', newValue);
		}

		if (updateResource) updates.push({ _id: rs._id, 'system.uses.value': newValue });
	}

	if (updates.length > 0) {
		console.debug(logtitle, '|', { actor, combat, combatRound: combat.round, updates });

		if (lastState.id !== combat.id || lastState.round !== combat.round) {
			lastState.id = combat.id;
			lastState.round = combat.round;
			const actorUpdate = {};
			actorUpdate[`flags.${CFG.id}.${CFG.SETTING.recordKey}`] = lastState;
			console.debug(logtitle, '| Setting flag:', actorUpdate);
			await actor.update(actorUpdate, { render: false });
		}

		await actor.updateEmbeddedDocuments('Item', updates);
	}
	else {
		console.debug(logtitle, '| No updates:', actor.name, actor);
	}

	if (deactivatedBuffs.length) {
		const flags = { [CFG.id]: { refund: { outOfResources: true } } };

		const token = combat.combatant.token;

		const msgData = {
			content: '<div>' + game.i18n.format('ResourceDrain.OutOfResources', { resource: deactivatedBuffs.map(b => b.name).join(', ') }) + '</div>',
			type: CONST.CHAT_MESSAGE_TYPES.OTHER,
			speaker: ChatMessage.getSpeaker({ token, actor, scene: token.scene }),
			flags,
		};
		ChatMessage.applyRollMode(msgData, CONST.DICE_ROLL_MODES.PRIVATE);
		await ChatMessage.create(msgData);
	}

	if (refundables.length) await printRefundCards(combat, refundables);
};

function updateCombatEvent(combat) {
	if (!combat.started) return;

	const actor = combat.combatant?.actor;
	if (!actor?.isOwner) return; // Simple early test

	drainResources(actor, combat);
};

async function dismiss(ev, cm) {
	ev.preventDefault();
	ChatMessage.deleteDocuments([cm.id]);
};

async function refund(ev, cm) {
	ev.preventDefault();

	const rf = cm.getFlag(CFG.id, 'refund');
	const actor = await fromUuid(rf.actor);
	if (!actor) return void ui.notifications.error('ResourceDrain.ActorNotFound', { localize: true });

	const buff = actor.items.get(rf.buff);
	const resource = actor.items.get(rf.target);

	if (!resource) return void ui.notifications.error('ResourceDrain.ResourceNotFound', { localize: true });
	if (!buff) return void ui.notifications.error('ResourceDrain.BuffNotFound', { localize: true });

	const rsMax = resource.maxCharges;
	const rsValue = resource.charges;

	console.debug(logtitle, '| Refund |', resource.name, { value: rf.delta }, { refund: rf, buff, resource });

	if (buff.isActive) {
		const updates = [];
		updates.push({ _id: buff.id, 'system.active': false });
		updates.push({ _id: resource.id, 'system.uses.value': Math.min(rsMax, rsValue + rf.delta) });
		actor.updateEmbeddedDocuments('Item', updates);
	}
	else {
		console.warn(logtitle, '| Refund triggered with the buff was disabled');
	}

	return dismiss(ev, cm);
};

/**
 * @param {ChatMessage} cm
 * @param {JQuery} html
 */
function hookRefundMessageButtons(cm, [html]) {
	if (!cm.getFlag(CFG.id, 'refund')) return;

	html.classList.add('resource-drain', 'refund-card');

	html.querySelector('.message-header .whisper-to')?.remove();

	html.querySelector('button[data-action="refund"]')?.addEventListener('click', (ev) => refund(ev, cm));
};

/**
 * @param {ItemSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 */
async function renderItemSheetDrainEditor(sheet, [html], options) {
	if (!options.editable) return;
	const actor = sheet.actor;
	if (!actor) return;
	const item = sheet.document;
	if (item?.type !== 'buff') return;

	const tab = html.querySelector('.tab[data-tab="advanced"]');
	if (!tab) return;

	const rs = DrainData.fromItem(item) ?? {};

	// Sort out existing resources
	const typed = {};
	Object.entries(actor.system.resources).forEach(entry => {
		const [resourceKey, resourceData] = entry;
		const resourceItem = actor.items.get(resourceData._id);
		if (!resourceItem) return;
		const type = resourceItem.type;
		typed[type] ??= { label: CONFIG.Item.typeLabels[type], resources: {} };
		typed[type].resources[resourceKey] = { item: resourceItem, type };
	});

	const addItemResource = (rItem) => {
		if (rItem.system.identified !== true) return;
		if (!rItem.isCharged) return;

		const type = rItem.type;

		typed[type] ??= { label: CONFIG.Item.typeLabels[type], items: [] };
		typed[type].items.push(rItem);
	};

	actor.itemTypes.equipment.forEach(addItemResource);
	actor.itemTypes.weapon.forEach(addItemResource);
	actor.itemTypes.consumable.forEach(addItemResource);

	// Sort types
	const typeValues = {
		feat: 500,
		equipment: 400,
		weapon: 300,
		consumable: -500,
	};
	const sortTypes = (t1, t2) => {
		const v1 = typeValues[t1] ?? 0,
			v2 = typeValues[t2] ?? 0;
		return v2 - v1;
	};

	const drainTargets = [];
	const typeOrder = Object.keys(typed).sort(sortTypes);
	typeOrder.forEach(type => {
		const typeData = typed[type];
		const res = Object.entries(typeData.resources ?? {});
		const items = typeData.items ?? [];
		if (res.length == 0 && items.length == 0) return;
		const data = { type, label: typeData.label, data: typeData, items: [] };
		drainTargets.push(data);

		res.forEach(([key, rData]) => data.items.push({ value: key, label: item ? `${rData.item.name} [${key}]` : key }));
		items.forEach(item => data.items.push({ value: `#${item.id}`, itemId: item.id, label: item.name }));
	});

	const testEval = (tformula) => {
		return Roll.defaultImplementation.safeRollSync(tformula || '0', item.getRollData(), undefined, undefined, { minimize: true });
	};

	const _formula = rs.value;
	const roll = await testEval(_formula);
	const templateData = {
		css: 'resource-drain',
		formula: _formula,
		drainTargets,
		drainTarget: rs.key,
		evaluated: roll,
		error: roll.err,
	};

	const renderedHTML = await renderTemplate(editorTemplate, templateData);
	const d = document.createElement('div');
	d.innerHTML = renderedHTML;

	const targetInput = d.querySelector('.resource-drain-target'),
		formulaInput = d.querySelector('.resource-drain-formula');
	// const fEval = d.querySelector('.resource-drain-eval');

	tab.append(...d.children);

	if (!rs.key) {
		formulaInput.readOnly = true;
		formulaInput.disabled = true;
		formulaInput.placeholder = game.i18n.localize('ResourceDrain.MissingResource');
	}

	function unsave() {
		return item.unsetFlag(CFG.id, CFG.SETTING.resourceKey);
	}

	function save({ itemId, resource, value } = {}) {
		if (!itemId && !resource) return unsave();
		return item.setFlag(CFG.id, CFG.SETTING.resourceKey, {
			itemId,
			resource,
			value,
		});
	}

	const saveTarget = async (ev) => {
		ev.preventDefault();
		const key = ev.target.value;
		let itemId, resource;
		if (key.startsWith('#')) itemId = key.slice(1);
		else resource = key;
		save({ itemId, resource, value: rs.value });
	};

	async function saveFormula(event) {
		event.preventDefault();

		const nformula = event.target.value.trim();

		if (nformula) {
			const nroll = await testEval(nformula);
			if (nroll.err) {
				console.warn(logtitle, '| Test eval failed:', nformula, '\n', nroll.err);
				event.target.reportValidity('Formula is invalid!');
				// delDR = true;
			}
			else {
				item.setFlag(CFG.id, `${CFG.SETTING.resourceKey}.value`, nformula);
			}
		}
		else {
			item.unsetFlag(CFG.id, `${CFG.SETTING.resourceKey}.value`);
		}
	}

	targetInput.addEventListener('change', saveTarget);
	formulaInput.addEventListener('change', saveFormula);
};

async function migrateWorld() {
	console.log(logtitle, '| Migrating | World | Start');
	try {
		for (const actor of game.actors) {
			await migrateActor(actor);
		}
	}
	finally {
		console.log(logtitle, '| Migrating | World | End');
	}
}

Hooks.on('renderItemSheetPF', renderItemSheetDrainEditor);
Hooks.on('updateCombat', updateCombatEvent);
Hooks.on('renderChatMessage', hookRefundMessageButtons);

Hooks.once('init', () => {
	game.settings.register(CFG.id, CFG.SETTING.migration, {
		type: Number,
		default: 0,
		config: false,
		scope: 'world',
	});

	game.modules.get(CFG.id).api = {
		migrate: {
			world: migrateWorld,
			item: migrateItem,
			actor: migrateActor,
		},
	};
});

Hooks.once('ready', async () => {
	if (!game.users.activeGM?.isSelf) return;

	const migration = game.settings.get(CFG.id, CFG.SETTING.migration);
	if (migration === 1) return;

	await migrateWorld();

	game.settings.set(CFG.id, CFG.SETTING.migration, 1);
});
