const CFG = {
	id: 'sync-settings',
	COLORS: {
		main: 'color:orangered',
		unset: 'color:unset'
	},
	disabledPeriod: 250, // Period in ms for sync setting dialog is disabled
};

/**
 * Eat inptus for a fraction of a second on dialog opening
 * @param {Element} html
 */
function eatInputs(html) {
	const app = html.closest('.app[data-appid]');

	app.classList.add('sync-disabled');

	const disableClicks = () => ev => {
		ev.preventDefault();
		ev.stopPropagation();
		ev.stopImmediatePropagation();
	};

	app.addEventListener('click', disableClicks);
	app.querySelectorAll('button,input').forEach(el => el.disabled = true);

	setTimeout(() => {
		app.removeEventListener('click', disableClicks);
		app.querySelectorAll('button,input').forEach(el => el.disabled = false);
		app.classList.remove('sync-disabled');
	}, CFG.disabledPeriod);
}

// Make checkboxes and select all button work nicely
function HookCheckboxes(html) {
	const settings = Array.from(html.querySelectorAll('input.setting'));
	const selectAll = html.querySelector('input.select-all');

	const testChecked = () => settings.some(el => el.checked);
	const testUnchecked = () => settings.some(el => !el.checked);

	/**
	 * @param {Event} ev
	 */
	const toggleAll = async (_ev) => {
		if (selectAll.indeterminate)
			selectAll.indeterminate = false;

		const haveChecked = testChecked(),
			haveUnchecked = testUnchecked();

		settings.forEach(el => {
			if (haveUnchecked)
				el.checked = true;
			else if (haveChecked)
				el.checked = false;
		});

		selectAll.checked = haveUnchecked;
	};

	const invertAll = async (_ev) => {
		settings.forEach(el => {
			el.checked = !el.checked;
		});

		const haveChecked = testChecked(),
			haveUnchecked = testUnchecked();

		if (!(haveChecked && haveUnchecked))
			selectAll.checked = haveChecked;
	}

	settings.forEach(el => {
		el.addEventListener('click', async (_ev) => {
			const c = testChecked(),
				u = testUnchecked();

			if (c && u)
				selectAll.indeterminate = true;
			else {
				selectAll.indeterminate = false;
				selectAll.checked = c;
			}
		}, { passive: true });
	});

	selectAll.addEventListener('click', toggleAll, { passive: true });
	selectAll.addEventListener('contextmenu', invertAll, { passive: true });
}

async function transmitSettings(data) {
	console.log('%cSYNC SETTINGS%c | Sent:', CFG.COLORS.main, CFG.COLORS.unset, deepClone(data));

	game.socket.emit(`module.${CFG.id}`, data);
}

class SyncTransmitDialog extends FormApplication {
	constructor(categories) {
		super();

		this.category = categories[0].id;
		this.categories = categories.filter(cat => cat.settings.some(setting => setting.scope != 'world'));
		this.categories.forEach(category => {
			category.settings.forEach(setting => {
				setting._id = setting.id
					.substring(setting.namespace.length + 1);
			});
		});
	}

	get template() {
		return `modules/${CFG.id}/sync-transmit-dialog.hbs`;
	}

	static get defaultOptions() {
		const _default = super.defaultOptions;
		return {
			..._default,
			classes: [..._default.classes, 'sync-dialog', 'sync-transmit-dialog'],
			title: 'Synchronize Settings to Clients',
			submitOnClose: false,
			closeOnSubmit: true,
			submitOnChange: false,
			height: 'auto',
		};
	}

	getData() {
		const context = super.getData();

		context.rng = randomID();

		const cat = this.category;
		context.category = cat;

		// TODO: Figure a way to include modules with only hidden settings
		context.categories = this.categories;

		// context.settings = this.categories.find(cat => cat.id === cat).settings;
		context.settings = Array.from(game.settings.settings.values())
			.filter(setting => setting.namespace == cat && setting.scope != 'world');

		if (cat == 'core') {
			// Allow only subset of core settings to be synced
			context.settings = context.settings
				.filter(s => ['language', 'chatBubbles', 'chatBubblesPan', 'notesDisplayToggle'].includes(s.key));
		}

		return context;
	}

	/**
	 * @param {JQuery} jq
	 */
	activateListeners(jq) {
		super.activateListeners(jq);

		const html = jq[0];

		const app = this;

		HookCheckboxes(html);

		const categorySelector = html.querySelector('select[name="category"]');
		categorySelector
			?.addEventListener('change', async ev => {
				app.category = ev.target.value;
				app.render();
			});

		html.querySelector('button')?.addEventListener('click', ev => {
			ev.target.disable = true;
		}, { passive: true });
	}

	_updateObject(event, formData) {
		const namespace = formData.category;

		const settings = {};

		for (const [fkey, value] of Object.entries(formData)) {
			if (value === true) {
				const key = fkey.replace(/^settings\./, '');
				settings[key] = game.settings.get(namespace, key);
			}
		}

		if (foundry.utils.isEmpty(settings)) {
			ui.notifications.info('No settings to synchronize');
			return;
		}

		ui.notifications.info('Synchronizing...');
		const msgData = { namespace, settings };
		transmitSettings(msgData);
	}

	/**
	 * @param {Event} ev
	 * @param {Object} categories
	 * @returns {SyncTransmitDialog}
	 */
	static open(ev, categories) {
		ev.preventDefault();
		ev.stopPropagation();

		const d = new SyncTransmitDialog(categories);
		d.render(true);
		return d;
	}
}

/**
 * @param {Object} data
 * @param {String} userId
 */
async function receiveSettings(data, userId) {
	console.log('%cSYNC SETTINGS%c | Received:', CFG.COLORS.main, CFG.COLORS.unset, deepClone(data));

	const sender = game.users.get(userId);
	if (sender.isGM !== true) {
		console.error('SYNC SETTING | Received setting synchronization message from non-GM:', { sender }, { data });
		return;
	}

	if (game.user.isGM) return;

	const { namespace, settings } = data;

	const getNamespaceLabel = () => {
		switch (namespace) {
			case 'core': return game.i18n.localize('PACKAGECONFIG.Core');
			case game.system.id: return game.system.title;
			default: return game.modules.get(namespace).title;
		}
	}

	const templateData = {
		namespace,
		rng: randomID(),
		sender: game.users.get(userId),
		label: getNamespaceLabel(),
		settings: {},
	};

	Object.entries(settings).forEach(([key, value]) => {
		templateData.settings[key] = {
			value,
			namespace,
			key,
			name: game.settings.settings.get(`${namespace}.${key}`)?.name,
		}
	});

	const rv = await Dialog.confirm({
		content: await renderTemplate(`modules/${CFG.id}/sync-receive-dialog.hbs`, templateData),
		title: 'Synchronize Settings?',
		yes: (html) => new FormDataExtended(html.querySelector('form')).object,
		no: () => null,
		render: (html) => {
			eatInputs(html);
			HookCheckboxes(html);
		},
		defaultYes: true,
		rejectClose: false,
		options: {
			classes: ['dialog', 'sync-dialog', 'sync-receive-dialog'],
			jQuery: false
		}
	});

	if (rv === null) {
		console.log('%cSYNC SETTINGS%c | Rejected:', CFG.COLORS.main, CFG.COLORS.unset, deepClone(data));
		return;
	}

	// Remove unaccepted settings
	Object.entries(rv).forEach(([key, accepted]) => {
		console.log(key, { accepted });
		if (!accepted) delete settings[key];
	});

	console.log('%cSYNC SETTINGS%c | Applying:', CFG.COLORS.main, CFG.COLORS.unset, { namespace, settings: deepClone(settings) });

	for (const [key, value] of Object.entries(settings))
		await game.settings.set(namespace, key, value);

	Dialog.confirm({
		content: '<p>Reload to ensure settings take effect?</p>',
		title: 'Reload?',
		yes: () => window.location.reload(),
		no: () => null,
		defaultYes: true,
		rejectClose: false,
		options: {
			classes: ['dialog', 'sync-dialog', 'sync-reload-dialog'],
			jQuery: false
		}
	});
}

/**
 * @param {SettingsConfig} app
 * @param {JQuery} html
 * @param {Object} options
 */
function injectSyncButton(app, [html], options) {
	if (!game.user.isGM) return;

	const cats = options.categories;

	const saveButton = html.querySelector('form footer button[type="submit"]');

	const syncButton = document.createElement('button');
	syncButton.type = 'button';
	syncButton.classList.add('sync-setting');
	syncButton.style.cssText += 'flex: 0;white-space:nowrap;';
	syncButton.innerHTML = '<i class="fas fa-sync-alt"></i> Sync Clients';

	syncButton.addEventListener('click', ev => SyncTransmitDialog.open(ev, deepClone(cats)));

	saveButton?.after(syncButton);

	// "Fix" button layout
	saveButton?.parentElement?.classList.add('flexrow');
}

Hooks.once('init', () => {
	game.socket.on(`module.${CFG.id}`, receiveSettings);

	// TODO: Add option to allow non-GM to send sync messages
	// TODO: Add option to limit recipients
});

Hooks.once('ready', () => {
	if (game.user.isGM)
		Hooks.on('renderSettingsConfig', injectSyncButton);
});
