/**
 * @param {string} node
 * @param {string} content
 * @param {string[]} classes
 * @param {object} options
 * @param {boolean} options.isHTML
 * @returns {Element}
 */
const createNode = (node, content, classes = [], { isHTML = false } = {}) => {
	const d = document.createElement(node);
	if (content) {
		if (isHTML) d.innerHTML = content;
		else d.textContent = content;
	}
	if (classes.length) d.classList.add(...classes);
	return d;
}

class AppInstance {
	element = null;
	app = null;
	constructor(app) {
		this.app = app;
	}

	initUI() {
		const a = createNode('li', this.app.title || game.i18n.format('AppMenu.Nameless', { id: this.app.appId }), ['application']);
		a.dataset.appId = this.app.appId;
		this.element = a;
	}
}

class AppCategory {
	element = null;
	fallback = null;
	title = null;
	apps = {};

	initUI(title, noApps) {
		const aa = createNode('ul', null, ['applications', 'app-list', 'empty']);
		const aaf = createNode('li', game.i18n.localize(noApps), ['no-apps']);
		const aat = createNode('h4', game.i18n.localize(title));
		aa.append(aaf);
		this.element = aa;
		this.fallback = aaf;
		this.title = aat;
	}
}

class AppMenu {
	main;
	applist = new AppCategory();
	jrnlist = new AppCategory();
	doclist = new AppCategory();

	constructor() {
		// Constant Access Icon
		const d = createNode('div');
		d.id = 'app-menu-module';
		const i = createNode('i', null, ['fa-solid', 'fa-toilet-paper', 'icon']);
		d.append(i);
		document.body.append(d);

		this.main = d;

		// Application Window
		const w = createNode('div', null, ['window']);
		this.applist.initUI('AppMenu.Apps', 'AppMenu.NoApps');
		this.doclist.initUI('AppMenu.Documents', 'AppMenu.NoDocs');
		this.jrnlist.initUI('AppMenu.Journals', 'AppMenu.NoJournals');

		w.append(
			this.doclist.title,
			this.doclist.element,
			this.jrnlist.title,
			this.jrnlist.element,
			this.applist.title,
			this.applist.element,
		);
		d.append(w);

		this.window = w;

		Hooks.on('closeApplication', this._onAppClose.bind(this));
		Hooks.on('closeActorSheet', this._onAppClose.bind(this));
		Hooks.on('closeItemSheet', this._onAppClose.bind(this));

		Hooks.on('renderApplication', this._onAppOpen.bind(this));
		Hooks.on('renderActorSheet', this._onAppOpen.bind(this));
		Hooks.on('renderItemSheet', this._onAppOpen.bind(this));

		Object.values(ui.windows)
			.forEach(this._onAppOpen);
	}

	refreshActive(list) {
		const emptyList = Object.keys(list.apps).length === 0;
		list.element.classList.toggle('empty', emptyList);
	}

	/**
	 * @param {number} appId
	 * @param {Event} event
	 */
	_focusSheet(appId, event) {
		event.preventDefault();

		ui.windows[appId].bringToTop();
	}

	_getAppList(app) {
		if (app instanceof ActorSheet || app instanceof ItemSheet)
			return this.doclist;
		else if (app instanceof JournalSheet || app instanceof JournalPageSheet)
			return this.jrnlist;
		else
			return this.applist;
	}

	/**
	 * @param {Application} app
	 */
	_onAppOpen(app) {
		// Journal pages trigger weird renderApplication hooks and never close, _state is none for them, so we ignore those instead
		if (app._state === Application.RENDER_STATES.NONE) return;
		// Ignore apps that don't go in ui.windows (e.g. scene controls and token HUD)
		if (!ui.windows[app.appId]) return;

		// Check ignore list
		const ignoreList = game.settings.get('app-menu', 'ignore')
			.split(',').map(i => i.trim());
		const clsName = app.constructor.name;
		if (ignoreList.some(ignore => clsName === ignore)) return void console.log('AppMenu | Ignoring:', clsName);

		// TODO: Add more categories
		const list = this._getAppList(app);
		const apps = list.apps;
		apps[app.id] ??= new AppInstance(app);
		if (apps[app.id].element) return;
		apps[app.id].initUI();
		list.element.append(apps[app.id].element);

		apps[app.id].element.addEventListener('click', ev => this._focusSheet(app.appId, ev));

		this.refreshActive(list);
	}

	/**
	 * @param {Application} app
	 */
	_onAppClose(app) {
		const list = this._getAppList(app);
		const apps = list.apps;
		const r = apps[app.id]
		if (!r) return;
		const el = r.element;
		delete apps[app.id];
		el.remove();

		this.refreshActive(list);
	}
}

Hooks.once('ready', () => new AppMenu());

Hooks.once('init', () => {
	game.settings.register('app-menu', 'ignore', {
		name: 'AppMenu.IgnoreList',
		hint: 'AppMenu.IgnoreListHint',
		type: String,
		default: 'TokenActionHud,SmallTimeApp',
		scope: 'world',
		config: true,
	});
});
