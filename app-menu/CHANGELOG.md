# Change Log

## 0.2.0

- Change: Replaced Dialog/App distinction with Document/Journal/App distinction
- Fix: Special handling for journals and journal pages to track them better.
- Fix: Ignore strange unrendered apps (related to above).
- New: i18n support added

## 0.1.0.2

- Fix: Item sheets were not detected.
  Caused by baseApplication being set that this module did not listen to.
- Fix: Use bringToTop() instead of forcing re-render and focus.
  Removes excess render and more reliably brings apps to top.

## 0.1.0 Initial
