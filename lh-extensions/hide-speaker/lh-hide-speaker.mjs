Hooks.on('little-helper.chat.tooltip.meta', (cm, tt) => {
	if (game.user.isGM) return; // Don't do anything for GM
	const spk = tt.querySelector('.speaker');
	if (!spk) return; // No speaker

	// Remove speaker and the preceding <hr>
	const prev = spk.previousElementSibling;
	if (prev?.tagName === 'HR') prev.remove();
	spk.remove();
});
