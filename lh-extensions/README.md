# Extensions for Little Helper

These are modules that extend or alter behaviour of Little Helper in unsupported way.

The functionality of these modules likely will never be part of the main module as they go against the intent of the main module.

---

Main module: <https://gitlab.com/koboldworks/pf1/little-helper>
