# Change Log

## 0.3.0.2

- Fix errors with containers.

## 0.3.0.1

- Fix configuration being editable when sheet was not.
