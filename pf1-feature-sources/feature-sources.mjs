const CFG = {
	module: 'mkah-pf1-feature-sources',
	FLAGS: {
		text: 'source',
		level: 'source_level',
		type: 'source_type',
	}
}

const templatePath = `modules/${CFG.module}/editor.hbs`;
let editorTemplate;
Hooks.once('init', () => getTemplate(templatePath).then(t => editorTemplate = t));

const saveRecord = (item, key, value) => value !== null ?
	item.setItemDictionaryFlag(key, value) :
	item.removeItemDictionaryFlag(key);

const convertNum = (num) => num === null ? num : parseInt(num, 10);

/**
 * @param {ItemSheet} sheet
 * @param {JQuery} html
 * @param {Object} _options
 */
async function insertSourceEditor(sheet, [html], _options) {
	const item = sheet?.object;
	// if (item?.type !== "feat") return;

	const details = html.querySelector('.tab.details');
	if (!details) return;

	const templateData = {
		source: {
			text: item.getItemDictionaryFlag(CFG.FLAGS.text) ?? '',
			level: item.getItemDictionaryFlag(CFG.FLAGS.level) ?? '',
			type: item.getItemDictionaryFlag(CFG.FLAGS.type) ?? '',
		},
		sourceTypes: duplicate(game.i18n.translations.MKAh.Source.Types),
	};

	const div = document.createElement('div');
	div.innerHTML = editorTemplate(templateData);

	details.prepend(...div.childNodes);

	const _onChangeSource = async (ev) => {
		ev.preventDefault();
		ev.stopPropagation();
		const el = ev.target;

		el.disabled = true;
		const source = el.dataset.source;
		let value = el.value.trim() || null;
		if (source === 'level') value = convertNum(value);
		await saveRecord(item, CFG.FLAGS[source], value);

		el.disabled = false;
	};

	const fs = details.querySelector('.mkah-feature-sources');
	fs.querySelectorAll('input,select')
		.forEach(el => {
			if (sheet.isEditable)
				el.addEventListener('change', _onChangeSource);
			else {
				el.disabled = true;
				el.readOnly = true;
			}
		});
}

Hooks.on('renderItemSheetPF', insertSourceEditor);
