/**
 * Resolve identifier string
 *
 * @param {string} ident
 * @param {object} [options]
 * @param {boolean} [options.actor]
 * @param {boolean} [options.item]
 * @param options.name
 * @returns {Document}
 */
const getDocumentFromIdentifier = (ident, { actor = true, item = true, name = true } = {}) => {
	if (/^\w{16}$/.test(ident)) {
		return (actor ? game.actors.get(ident) : null) ?? (item ? game.items.get(ident) : null);
	}

	let re;
	if (name) {
		re = /^name:\s*(?<value>.*)$/i.exec(ident);
		const value = re?.groups.value;
		if (value)
			return (actor ? game.actors.getName(value) : null) ?? (item ? game.items.getName(value) : null);
	}

	if (actor) {
		re = /^actor:\s*(?<value>.*)$/i.exec(ident);
		const value = re?.groups.value;
		if (value)
			return game.actors.get(value) ?? (name ? game.actors.getName(value) : null);
	}

	if (item) {
		re = /^item:\s*(?<value>.*)$/i.exec(ident);
		const value = re?.groups.value;
		if (value)
			return game.items.get(value) ?? (name ? game.items.getName(value) : null);
	}

	return fromUuidSync(ident);
};

const resolveIdentifier = (ident) => {
	const doc = getDocumentFromIdentifier(ident);
	return { doc, name: doc?.name, uuid: doc?.uuid };
};

const generateValueContent = (ident, path, label) => {
	const a = document.createElement('a');
	a.classList.add('mmm-text-enricher', 'value');
	a.dataset.path = path;
	const { doc, uuid, name } = resolveIdentifier(ident);

	let value, broken = false, hidden = false;
	if (doc) {
		const canSee = doc instanceof foundry.abstract.Document ? doc.testUserPermission(game.user, 'OBSERVER') : true;
		if (canSee) {
			// <i class="fa-solid fa-eye-slash"></i>
			if (/^@/.test(path))
				value = foundry.utils.getProperty(doc.getRollData(), path.slice(1));
			else
				value = foundry.utils.getProperty(doc, path);

			if (value === undefined) {
				a.classList.add('value-not-found');
				broken = true;
			}
		}
		else {
			hidden = true;
		}
	}
	else {
		broken = true;
	}

	if (broken) {
		a.classList.add('document-not-found');
		const icon = document.createElement('i');
		icon.classList.add('fa-solid', 'fa-link-slash');
		a.append(icon, ' ');
	}

	if (hidden) {
		a.classList.add('no-permission');
		const icon = document.createElement('i');
		icon.classList.add('fa-solid', 'fa-eye-slash');
		a.append(icon, ' ');

		value = 'Secret';
	}

	if (label) a.append(`${label}: `);
	a.append(value);

	if (hidden) {
		a.dataset.tooltip = 'Insufficient Permissions';
	}
	else {
		a.dataset.identity = ident;
		a.dataset.uuid = uuid;

		let tooltip = `Source: ${uuid ?? ident}<br>Path: ${path}`;
		if (!doc) tooltip += '<br>Document Not Found';
		else if (value === undefined) tooltip += '<br>Value Not Found';
		a.dataset.tooltip = tooltip;
	}

	return a;
};

const registerEnrichers = () => {
	CONFIG.TextEditor.enrichers.push(
		{
			pattern: /@Value\[(?<ident>.*?);(?<path>.*?)](?:{(?<label>.*?)})?/gm,
			enricher: (match, options) => {
				const { ident, path, label } = match.groups;
				return generateValueContent(ident, path, label);
			},
		},
	);
};

const _onClickEnriched = (ev) => {
	// Do magical things
};

/**
 * Add click listeners.
 * Mimic TextEditor.activateListeners() for compatibility.
 * UNUSED FOR NOW.
 */
const addClickListeners = () => {
	const body = $('body');
	body.on('click', 'a.mmm-text-enricher', _onClickEnriched);
};

Hooks.once('init', registerEnrichers);
// Hooks.once('ready', addClickListeners);
