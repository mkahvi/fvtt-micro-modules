# Additional Text Enrichers

![Supported Foundry Versions](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fversion%3Fstyle%3Dflat%26url%3Dhttps%3A%2F%2Fgitlab.com%2Fmkahvi%2Ffvtt-micro-modules%2F-%2Fraw%2Fmaster%2Ftext-enrichers%2Fmodule.json)

## Examples

```html
@Value[Identifier;Path]{Label}
```

**Identifier** can be:

- UUID
- item:name
- item:ID
- actor:name
- actor:ID

**Path** must be a data path to the desired variable. Prefix with `@` to resolve roll data.

### Actor by name

```html
@Value[actor:John Carter;name]{Name}
```

### Advanced path

```html
@Value[actor:VDNPt6YQ4ngN8fkS;system.attributes.hd.total]{HD}
```

### Items directory item by ID

```html
@Value[item:u3MdbToblPf5hnIG;name]
```

### Document by UUID

```html
@Value[Compendium.pf1.items.qozwdcnkxulxsepe;name]
```

Warning: Compendium documents only fetch their index entry, advanced values are not normally available.

## Flaws

The values are fetched on render, so if you want something that is recorded for posterity, this is not very useful.

## Install

Manifest URL: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/master/text-enrichers/module.json>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
