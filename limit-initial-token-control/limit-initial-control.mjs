const MODULE_ID = 'limit-initial-token-control';

function _initializeLimitedTokenControl() {
	const isReload = this._reload.scene === this.scene.id;
	let panToken = null;

	// Initial tokens based on reload data
	let controlledTokens = isReload ? this._reload.controlledTokens.map(id => canvas.tokens.get(id)) : [];
	if (!isReload && !game.user.isGM) {
		// Initial tokens based on primary character
		controlledTokens = game.user.character?.getActiveTokens() || [];

		// Based on owned actors
		if (!controlledTokens.length) {
			controlledTokens = canvas.tokens.placeables.filter(t => t.actor?.testUserPermission(game.user, 'OWNER'));
		}

		// Based on observed actors
		if (!controlledTokens.length) {
			const observed = canvas.tokens.placeables.filter(t => t.actor?.testUserPermission(game.user, 'OBSERVER'));
			panToken = observed.shift() || null;
		}
	}

	// Initialize Token Control
	if (controlledTokens.length === 1) {
		for (const token of controlledTokens) {
			if (!panToken) panToken = token;
			token.control({ releaseOthers: false });
		}
	}

	// Initialize Token targets
	const targetedTokens = isReload ? this._reload.targetedTokens : [];
	for (const id of targetedTokens) {
		const token = canvas.tokens.get(id);
		token.setTarget(true, { releaseOthers: false, groupSelection: true });
	}

	// Pan camera to controlled token
	if (panToken && !isReload) this.pan({ x: panToken.center.x, y: panToken.center.y, duration: 250 });
}

Hooks.once('init', () => {
	/* global libWrapper */
	if (!(game.release?.generation >= 10))
		libWrapper.register(MODULE_ID, 'Canvas.prototype._initializeTokenControl', _initializeLimitedTokenControl, 'OVERRIDE');
});

Hooks.on('canvasReady', (canvas) => {
	if (!(game.release?.generation >= 10)) return;
	// BUG: Can't detect scene reload
	if (game.user.isGM) return; // GM doesn't need this

	// Initial tokens based on primary character
	const controlled = canvas.tokens.controlled;
	// Release tokens if more than one is selected
	if (controlled.length > 1) controlled.forEach(t => t.release());
});
