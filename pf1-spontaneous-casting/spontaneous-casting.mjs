// Spontaneous Casting

/**
 * BUGS/FLAWS:
 * - Slot cost is ignored
 *
 * TODO: Smart caching
 * - Store result of constructSpellBookInfo
 * - Invalidate it if any spell is updated
 * - Invalidate it if any spell is added or deleted
 * - Invalidate it if actor data relating to spellbooks is updated
 * - Otherwise keep it
 *
 * That should have sufficiently high cache retention and correctness.
 *
 * TODO: Better communication
 * - Display placeholder/faded casting button for valid spells?
 *
 * TODO: Add API to do spontaneous casting in code?
 */

import { CFG, getDefaultConfig } from './src/config.mjs';
import { SpellSubstitutionDialog } from './src/substitution-dialog.mjs';
import { injectItemCastingConfig } from './src/item-config.mjs';
import { migrateItem } from './src/migration.mjs';
import { constructSpellBookInfo } from './src/spellbook.mjs';
import { SpellLevelData } from './src/spellbook-data.mjs'; // Imported for typing only

/**
 * @param {ItemSpellPF} spell
 * @param {Actor} actor
 * @param {BookData} book
 * @param {Token} token
 */
async function substituteCasting(spell, actor, book, { token } = {}) {
	if (!(actor instanceof Actor)) throw new Error('No valid actor provided');
	if (!(spell instanceof Item && spell.type === 'spell')) throw new Error('');
	book ??= constructSpellBookInfo(actor);
	if (!book) throw new Error('Failed to retrieve spellbook information');
	console.log(CFG.label, '| Attempting to cast:', spell.name, spell, book);
	return new Promise(resolve => new SpellSubstitutionDialog(spell, actor, book, resolve, { token }).render(true, { focus: true }));
}

/**
 * @param {Actor} actor
 * @param {Element} html
 * @param {BookData} book
 */
function injectCastingConfig(actor, html, book) {
	const settings = html.querySelector('.spell-settings');
	if (!settings) return;

	const allCfgs = actor.getFlag(CFG.id, CFG.FLAGS.config);
	const config = allCfgs?.[book.id] ?? getDefaultConfig(book);

	let validRE = true;
	try {
		if (config.match.length > 0)
			new RegExp(config.match);
	}
	catch (err) {
		validRE = false;
	}

	const isEditable = actor.isOwner;

	const templateData = {
		book: book.id,
		module: CFG.id,
		config,
		validRE,
		uuid: actor.uuid,
		canUse: book.canUse,
		isEditable: actor.isOwner,
	};

	const div = document.createElement('div');
	div.innerHTML = Handlebars.partials[CFG.CONFIG_TEMPLATE_FILE](templateData, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true });
	if (!isEditable) div.querySelectorAll('input').forEach(el => {
		el.readOnly = true;
		el.disabled = true;
	});

	if (book.canUse) {
		// Add listeners to update values
		div.querySelectorAll('input[type="checkbox"]')
			.forEach(cbel => cbel.addEventListener('change', function () {
				/** @type {Element} */
				const el = this;
				el.disabled = true; // will be undone by refresh
				actor.update({ [el.name]: Boolean(el.checked) });
			}));

		const rel = div.querySelector('input.regexp-match');
		rel.addEventListener('change', function () {
			let validRE = true;
			/** @type {Element} */
			const el = this;
			el.disabled = true; // will be undone by refresh
			let re;
			try {
				re = new RegExp(el.value);
			}
			catch (err) {
				console.error(err);
				validRE = false;
			}
			console.debug(CFG.label, '| Match:', el.value, re, { valid: validRE });
			const valid = ['far', 'fa-check-circle', 'test-result', 'valid'],
				invalid = ['fas', 'fa-times-circle', 'test-result', 'invalid'];
			let next = el.nextElementSibling;
			if (next?.tagName !== 'I') {
				next = document.createElement('i');
				el.after(next);
			}
			next.classList.remove(...validRE ? invalid : valid);
			next.classList.add(...validRE ? valid : invalid);
			if (validRE) actor.update({ [el.name]: el.value });
		}, { passive: true });
	}

	settings.append(...div.childNodes);
}

/**
 * Test if spell is valid within spontaneous casting config.
 *
 * @param {ItemSpellPF} spell
 * @param {CastingConfig} cfg
 */
function isValid(spell, cfg) {
	let valid = false;
	if (cfg.any) valid = true;
	else if (cfg.domain && (spell.system.domain ?? false)) valid = true;
	else if (cfg.marked && spell.getFlag('world', CFG.MARK)) valid = true;
	else if (cfg.matchRE?.test(spell.name)) valid = true;
	// console.log(spell.name, { valid });
	return valid;
}

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 */
function injectSpellbookInterface(sheet, [html]) {
	const actor = sheet.actor,
		items = actor.items;

	if (!['character', 'npc'].includes(actor.type)) return;

	const books = constructSpellBookInfo(actor);
	if (!books) return;

	const token = sheet.token;

	/**
	 *
	 * @param {Element} el
	 * @param {Item} spell
	 * @param {BookData} book
	 */
	function addSpontaneousSubstitutionButton(el, spell, book) {
		const uses = el.querySelector('.spell-uses');

		// Main element
		const d = document.createElement('div');
		d.dataset.bookId = book.id;
		d.dataset.spellId = spell.id;
		d.classList.add('spontaneous-substitution');
		d.dataset.tooltip = 'SpellSubstitution.Button.Hint';

		// Icon
		const sb = document.createElement('i');
		sb.classList.add('substitute-casting', 'fas', 'fa-lightbulb');
		d.append(sb);

		uses.prepend(d);

		// Add click handler
		d.addEventListener('click', () => substituteCasting(spell, actor, book, { token }));
	}

	// Scan over spells on sheet
	const spellbooks = html.querySelector('.tab[data-group="primary"][data-tab="spellbook"]'),
		bookTabs = spellbooks?.querySelectorAll('.tab[data-group="spellbooks"]');

	if (!bookTabs) return void console.debug(CFG.label, '| No book tabs found');

	/**
	 * @param {Element} spellEl
	 * @param {object} options
	 * @param {BookData} options.book
	 * @param {CastingConfig} options.cfg
	 */
	function handleSpell(spellEl, { book, cfg } = {}) {
		const isSpontaneous = book.isSpontaneous,
			levels = book.spells.level;

		const spell = items.get(spellEl.dataset.itemId);
		if (!spell) return;

		const spellData = spell.system;
		if (spellData.atWill) return;

		if (!isValid(spell, cfg)) return;

		const chargeCost = spell.defaultAction?.getChargeCost() ?? 0;
		if (chargeCost == 0) return;

		const level = spellData.level;

		/** @type {SpellLevelData} */
		const levelData = levels[level];

		spellEl.classList.add('valid-for-spontaneous-casting');

		let canSubstitute = false, needSubstitution = false;
		if (isSpontaneous) {
			if (!levelData.available) {
				needSubstitution = true;
				canSubstitute = levelData.higher?.canSubstitute ?? false;
			}
		}
		// Prepared
		else {
			if (spell.charges == 0) {
				needSubstitution = true;
				canSubstitute = levelData.canConsumeFor(spell);
			}
		}

		if (needSubstitution)
			spellEl.classList.add('need-spontaneous-casting');

		if (canSubstitute) {
			spellEl.classList.add('can-substitute');
			addSpontaneousSubstitutionButton(spellEl, spell, book);
		}
	}

	/**
	 * @param {Element} tab
	 */
	function handleBookTab (tab) {
		const bookId = tab.dataset.tab;

		/** @type {BookData} */
		const book = books[bookId];
		if (!book) return void console.warn('Book', bookId, 'not found');

		const autoSlots = book.autoSpellLevelCalculation ?? false;
		// Ignore spell point books (autoslots disable spell points)
		if (!autoSlots && book.usesSpellPoints) return void console.debug(CFG.label, '|', book.label || book.name);

		injectCastingConfig(actor, tab, book);

		const cfg = book.config;
		if (!cfg.enabled) return void console.debug(CFG.label, '|', book.id, 'disabled');

		tab.querySelectorAll('li.item[data-item-id]')
			.forEach(el => handleSpell(el, { book, cfg }));
	}

	bookTabs.forEach(handleBookTab);
}

Hooks.on('renderItemSheet', injectItemCastingConfig);
Hooks.on('renderActorSheet', injectSpellbookInterface);

Hooks.once('ready', () => {
	game.modules.get(CFG.id).api = {
		cast: (spell, actor, book) => substituteCasting(spell, actor ?? spell.parent, book),
		generateCache: (actor) => constructSpellBookInfo(actor),
		migrate: migrateItem,
	};
});
