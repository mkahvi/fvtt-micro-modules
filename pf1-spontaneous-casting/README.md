# Spontaneous Casting for Pathfinder 1

![Supported Foundry Versions](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fversion%3Fstyle%3Dflat%26url%3Dhttps%3A%2F%2Fgitlab.com%2Fmkahvi%2Ffvtt-micro-modules%2F-%2Fraw%2Fmaster%2Fpf1-spontaneous-casting%2Fmodule.json)

Allows to spontaneously cast unprepared spell at cost of prepared one.

The spontaneous casting button is not displayed until you are out of normal casts of a spell.

☐ It seems to work?

## Config

Spontaneous casting is configured in spellbook config.

Per spell config is in details tab of each spell near slot cost.

### Example regular expressions

|Class|Example|Description|
|:---|:---|:---|
|Cleric/Oracle|`\bCure\b`|All Cure spells.|
|Cleric/Oracle|`\bInflict\b`|All Inflict spells.|
|Druid|`\bSummon Nature.s Ally\b`|All Summon Nature's Ally spells.|

You can use [regexr](https://regexr.com/), [regex101](https://regex101.com/) or other resources if you need help with [regexp](https://en.wikipedia.org/wiki/Regular_expression).

## Install

Manifest URL: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/master/pf1-spontaneous-casting/module.json>

### Pathfinder v9 or older

Manual install only: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/7a52c7e5056533cb2939987248516619c473c00b/pf1-spontaneous-casting/pf1-spontaneous-casting.zip>

### Foundry v9

Manual install only: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/5c4d14c486d92cfb8c47f1fd01d42362464a025f/pf1-spontaneous-casting/pf1-spontaneous-casting.zip?inline=false>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
