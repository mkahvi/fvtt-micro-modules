# Change Log

## 1.5.0.4

- Maintenance

## 1.5.0.3

- Foundry v12 compatibility

## 1.5.0.2

- Fix: Domain Sac option still did not work correctly with prepared spellbooks. [#44]
- Change: Substitution dialog includes icon for domain spells.

## 1.5.0.1

- Fix: Domain Sac option did not function correctly with prepared spellbooks. [#44]

## 1.5.0

- New: Substitution card now displays content links instead of plain labels.
- Fix: Dialog error when it is somehow opened for a spell with no substitution options.
- New: Added _Allow Domain Sacrifice_ setting. Default disabled.
  Spells marked as domain can no longer be sacrificed for another while this is disabled.
  However excess slots used past domain slot config are allowed still.

## 1.4.0

- PF1v10 compatibility. Minimum required version increased to v10.

## 1.3.0

- PF1v9 compatibility. Minimum required version increased to v9.

## 1.2.0

- PF1 0.82.3 compatibility. Minimum required version increased to .4

## 1.1.0

- Fix: Spell marking interface was missing with v10
- Fix: Max spell charges display was empty
- PF1 0.82.2 compatibility
- Support for Foundry v9 and older dropped

## 1.0.0

- Fix: Cannot read properties of undefined (reading 'data')... at getSystemData... at getData [#27]
- Change: Substitution dialog can now be instantly passed by hitting enter.
- Change: Substitution chat card attempts to respect token name.
- New: Substitution card is deleted if spellcast is cancelled. Optionally this can be disabled.
- New: Translation support.
- Version pumped to 1.0.0 to signal feature completeness

## 0.3.0

- Foundry v10 compatibility

## 0.2.2

- Bundling via esbuild

## 0.2.1.2 Fix for errors with unsupported sheets

## 0.2.1.1 Fix for malformed translation file

## 0.2.1

- Internal restructuring
- Ignore basic actor type.
- Respect sheet being in readonly state.

## 0.2.0

- Basic API
- Per spellbook configuration
- Spell point using spellbooks are ignored.

## 0.1.0 Initial release
