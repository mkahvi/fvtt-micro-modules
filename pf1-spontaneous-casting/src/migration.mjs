import { CFG } from './config.mjs';

function limitPrecision(number, decimals = 2) {
	const mult = Math.pow(10, decimals);
	return Math.floor(number * mult) / mult;
}

/**
 * @param {Item} item
 */
export async function migrateItem(item) {
	if (item.flags?.world?.[CFG.MARK] !== undefined) return null;

	if (!item.hasItemBooleanFlag('spontaneousCasting')) return null;

	const updateData = {
		flags: {
			world: {
				[CFG.MARK]: true,
			},
		},
		system: {
			flags: {
				boolean: {
					'-=spontaneousCasting': null,
				},
			},
		},
	};

	return item.update(updateData);
}

// Migration
Hooks.once('pf1PostReady', async () => {
	const activeGM = game.users.activeGM;
	if (activeGM) {
		if (!activeGM?.isSelf) return;
	}

	const migration = game.settings.get(CFG.id, 'migration');
	const mod = game.modules.get(CFG.id);
	if (migration === mod.version || isNewerVersion(migration, mod.version)) return void console.debug('Spontaneous Casting | Migration | Not needed');

	console.log('Spontaneous Casting | Migrating...');

	const actors = game.actors.filter(a => a.isOwner);
	let pct10 = Math.max(25, Math.floor(actors.length / 10));
	if (actors.length < 50) pct10 = 100;
	let i = 0, migrated = 0;
	for (const actor of actors) {
		i += 1;
		if (i % pct10 === 0) console.log('Spontaneous Casting | Migrating', limitPrecision((i / actors.length) * 100, 1), '% |', i, 'out of', actors.length);
		for (const item of actor.items) {
			const rv = await migrateItem(item);
			if (rv) migrated += 1;
		}
	}

	console.log('Spontaneous Casting | Migration Completed |', migrated, 'Item(s) Updated');

	if (game.users.activeGM?.isSelf) game.settings.set(CFG.id, 'migration', mod.version);
});
