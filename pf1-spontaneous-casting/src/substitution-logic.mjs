import { CFG } from './config.mjs';
import { simplifySpellData } from './utility.mjs';

/**
 * @param {Actor} actor
 * @param {object} cardData
 * @returns {Promise<ChatMessage>}
 */
async function generateSubstitutionCard(actor, cardData) {
	const token = cardData.token ?? actor.token;

	const cmData = {
		speaker: {
			actor: actor.id,
			token: token?.id,
			alias: token?.name,
			scene: token?.parent?.id,
		},
	};

	if (game.release.generation >= 12)
		cmData.style = CONST.CHAT_MESSAGE_STYLES.OTHER;
	else
		cmData.type = CONST.CHAT_MESSAGE_TYPES.OTHER;

	if (cardData.consumeLevel)
		cardData.consumeLevelLabel = `<span class='slot-level'>${cardData.consumeLevel}</span>`;

	cardData.sacrificeLink = cardData.sacrifice?.document?.toAnchor().outerHTML;
	cardData.spellLink = cardData.spell.document.toAnchor().outerHTML;

	cmData.content = Handlebars.partials[CFG.SUBSTITUTION_CARD_FILE](cardData, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true });

	ChatMessage.applyRollMode(cmData, game.settings.get('core', 'rollMode'));
	return ChatMessage.create(cmData);
}

async function undoSubstitutionCard(card) {
	if (!card) return;
	if (game.settings.get(CFG.id, CFG.SETTINGS.removeCard)) {
		return card.delete();
	}
	else {
		const content = card.content
			.replace('spell-substitution', 'spell-substitution cancelled');
		return card.update({ content }).then(() => console.debug(CFG.label, '| Cancelled substitution card updated'));
	}
}

export async function spontaneousSubstituteCast({ spell, actor, book, consumeLevel, token } = {}, resolve) {
	const targetLevel = spell.system.level;

	const clamp = Math.clamp ?? Math.clamped; // v12 changed the function

	if (clamp(targetLevel, 1, 9) !== targetLevel) throw new Error('Spell level is malformed');
	if (clamp(consumeLevel, 2, 9) !== consumeLevel) throw new Error('Subsitute level is malformed');
	if (targetLevel === consumeLevel) throw new Error('Levels are equal, something went wrong.');

	const td = book.spells.level[targetLevel];
	const cd = book.spells.level[consumeLevel];
	const consumeOldValue = cd.value;
	const targetOldValue = td.value;

	console.debug(CFG.label, '| Adjusting spell slots to allow spellcast.');
	console.log(CFG.label, '| Consuming level', consumeLevel, 'slot to cast level', targetLevel, 'spell.');
	await actor.update({
		// [`data.attributes.spells.spellbooks.${book.id}.spells.spell${targetLevel}.value`]: targetOldValue + 1,
		[`data.attributes.spells.spellbooks.${book.id}.spells.spell${consumeLevel}.value`]: consumeOldValue - 1,
	});
	console.log(CFG.label, '| Casting spell:', spell.name);

	let scard;

	// Chat message
	if (game.settings.get(CFG.id, CFG.SETTINGS.chatCard)) {
		scard = await generateSubstitutionCard(actor, {
			isSpontaneous: true,
			targetLevel,
			consumeLevel,
			spell: simplifySpellData(spell),
			token,
		});
	}

	const rv = await spell.use({ cost: 0 });
	if (rv === undefined) {
		console.log(CFG.label, '| Spellcast cancelled, reverting slot shift.');

		await actor.update({
			// [`data.attributes.spells.spellbooks.${book.id}.spells.spell${targetLevel}.value`]: targetOldValue,
			[`data.attributes.spells.spellbooks.${book.id}.spells.spell${consumeLevel}.value`]: consumeOldValue,
		})
			.then(_ => console.log(CFG.label, '| Reversion complete'));

		undoSubstitutionCard(scard);

		resolve(undefined);
	}
	else {
		resolve(rv);
	}
}

export async function preparedSubstituteCast({ spell, actor, book, spellId, token } = {}, resolve) {
	const sacrificeSpellId = spellId;
	// console.log(spell.name, book)
	const sacrifice = actor.items.get(sacrificeSpellId);
	if (!sacrifice) throw new Error('Spell chosen for sacrifice not found.');

	const sacprep = sacrifice.system.preparation;
	const oldSacPrep = sacprep.value;
	const spellData = spell.system;
	const subprep = spell.system.preparation;
	const oldTargetPrep = subprep.value;

	if (oldSacPrep < 1) throw new Error('Spell chosen for sacrifice lacks preparations.');

	console.log(CFG.label, '| Adjusting spell slots to allow spellcast.');
	console.log(CFG.label, '| Consuming', sacrifice.name, 'slot to cast', spell.name, 'spell.');

	let scard;
	// Chat message
	if (game.settings.get(CFG.id, CFG.SETTINGS.chatCard)) {
		scard = await generateSubstitutionCard(actor, {
			isSpontaneous: false,
			targetLevel: spellData.level,
			sacrifice: simplifySpellData(sacrifice),
			spell: simplifySpellData(spell),
			token,
		});
	}

	await actor.updateEmbeddedDocuments('Item',
		[
			{ _id: sacrifice.id, 'system.preparation.value': oldSacPrep - 1 },
			// { _id: spell.id, 'system.preparation.value': oldTargetPrep + 1 },
		]);

	const rv = await spell.use({ cost: 0 });

	if (!rv) {
		console.log(CFG.label, '| Spellcast cancelled, reverting slot shift.');

		await actor.updateEmbeddedDocuments('Item',
			[
				{ _id: sacrifice.id, 'system.preparation.value': oldSacPrep },
				// { _id: spell.id, 'system.preparation.value': oldTargetPrep },
			])
			.then(_ => console.log(CFG.label, '| Reversion complete'));

		undoSubstitutionCard(scard);

		resolve(undefined);
	}
	else {
		resolve(rv);
	}
}
