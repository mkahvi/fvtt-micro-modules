export function simplifySpellData(spell) {
	const spellData = spell.system;
	return {
		id: spell.id,
		name: spell.name,
		img: spell.img,
		level: spellData.level,
		book: spellData.spellbook,
		atWill: spellData.atWill,
		domain: spellData.domain,
		charges: spell.charges,
		maxCharges: spell.maxCharges,
		spellbook: spellData.spellbook,
		document: spell,
	};
}
