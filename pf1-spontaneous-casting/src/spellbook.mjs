import { CFG, getDefaultConfig } from './config.mjs';
import { BookData } from './spellbook-data.mjs';
import { CastingConfig } from './casting-config.mjs';
import { simplifySpellData } from './utility.mjs';

/**
 * @param {Actor} actor
 * @param {string} chosen - Specific book id to construct instead of all
 * @returns {Object.<string, BookData>}
 */
export function constructSpellBookInfo(actor, chosen) {
	// Raw book data
	const actorData = actor.system;
	const rawbooks = actorData.attributes.spells.spellbooks;

	const cfg = actor.getFlag(CFG.id, CFG.FLAGS.config) ?? {};

	// Build simplified book data
	/** @type {Object.<string, BookData>} */
	const books = {};
	let nbooks = 0;
	for (const [bookId, bookData] of Object.entries(rawbooks)) {
		if (!bookData.inUse) continue;
		if (chosen && bookId !== chosen) continue;
		nbooks++;
		const book = new BookData(bookId, bookData, actor);
		book.config = cfg[bookId] ? new CastingConfig(cfg[bookId]) : getDefaultConfig(book);
		books[bookId] = book;
	}
	if (nbooks == 0) return; // void console.debug(CFG.label, '| No books for', actor.name, rawbooks);

	// Distribute spells to books
	// Ignoring at-will spells.
	actor.itemTypes.spell?.filter(i => !i.system.atWill)
		.forEach(i => {
			const spell = simplifySpellData(i);
			/** @type {BookData} */
			const book = books[spell.spellbook];
			book?.addSpell(spell);
		});

	// actor.__spontaneousCastingCache = books;

	return books[chosen] ?? books;
}
