import { CastingConfig } from './casting-config.mjs';

class SpontaneousCastingFlags {
	static config = /** @type {const} */ ('config');
}

class SpontaneousCastingSettings {
	static chatCard = /** @type {const} */ ('chatCard');
	static removeCard = /** @type {const} */ ('removeCard');
	static sacDomain = /** @type {const} */ ('sacDomain');
	static debug = /** @type {const} */ ('debug');
}

export class CFG {
	static FLAGS = /** @type {const} */ SpontaneousCastingFlags;
	static SETTINGS = /** @type {const} */ SpontaneousCastingSettings;

	static id = /** @type {const} */ ('pf1-spontaneous-casting');
	static label = /** @type {const} */ ('SUBSTITUTION');

	static CONFIG_TEMPLATE_FILE = /** @type {const} */ (`modules/${CFG.id}/template/spell-config.hbs`);
	static SUBSTITUTION_CARD_FILE = /** @type {const} */ (`modules/${CFG.id}/template/substitution-card.hbs`);

	static MARK = /** @type {const} */ ('allowSpontaneousCast');
}

Hooks.once('init', () => {
	game.settings.register(CFG.id, CFG.SETTINGS.chatCard, {
		name: 'SpellSubstitution.Config.Card.Label',
		hint: 'SpellSubstitution.Config.Card.Hint',
		type: Boolean,
		default: true,
		config: true,
		scope: 'world',
	});

	game.settings.register(CFG.id, CFG.SETTINGS.removeCard, {
		name: 'SpellSubstitution.Config.RemoveCard.Label',
		hint: 'SpellSubstitution.Config.RemoveCard.Hint',
		type: Boolean,
		default: true,
		config: true,
		scope: 'world',
	});

	game.settings.register(CFG.id, CFG.SETTINGS.sacDomain, {
		name: 'SpellSubstitution.Config.SacrificeDomain.Label',
		hint: 'SpellSubstitution.Config.SacrificeDomain.Hint',
		type: Boolean,
		default: false,
		config: true,
		scope: 'world',
	});

	game.settings.register(CFG.id, 'migration', {
		type: String,
		default: '1.0',
		scope: 'world',
		config: false,
	});
});

Hooks.once('setup', () => loadTemplates([CFG.CONFIG_TEMPLATE_FILE, CFG.SUBSTITUTION_CARD_FILE]));

export function getDefaultConfig(book) {
	const name = book.name;

	if (/\bdruid\b/i.test(name)) return CastingConfig.druid;
	if (/\bcleric\b/i.test(name)) return CastingConfig.cleric;
	return CastingConfig.default;
}
