export class CastingConfig {
	/**
	 * @type {Boolean}
	 */
	enabled;

	/**
	 * @type {Boolean}
	 */
	any;

	/**
	 * @type {Boolean}
	 */
	domain;

	/**
	 * @type {Boolean}
	 */
	marked;

	/**
	 * @type {String}
	 */
	match;

	matchRE;

	constructor({ enabled = true, any = false, domain = false, marked = true, match = '' } = {}) {
		this.enabled = Boolean(enabled);
		this.any = Boolean(any);
		this.domain = Boolean(domain);
		this.marked = Boolean(marked);
		this.match = String(match);
		if (this.match) this.matchRE = new RegExp(this.match);
	}

	static get default() {
		return new CastingConfig();
	}

	static get cleric() {
		return new CastingConfig({ match: '\b(cure|inflict)\b' });
	}

	static get druid() {
		return new CastingConfig({ match: '\bsummon nature.s ally\b' });
	}
}
