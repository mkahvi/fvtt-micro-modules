import { CFG } from './config.mjs';
import { migrateItem } from './migration.mjs';

/**
 * @param {ItemSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 */
export function injectItemCastingConfig(sheet, [html], options) {
	const actor = sheet.actor;
	if (!actor) return;

	const spell = sheet.item;
	if (spell.type !== 'spell') return;

	if (spell.hasItemBooleanFlag('spontaneousCasting'))
		return migrateItem(spell);

	const cfg = actor.getFlag(CFG.id, CFG.FLAGS.config) ?? {};
	if (cfg[spell.system.spellbook]?.marked !== true) return;

	const slotCost = html.querySelector('[name="system.slotCost"]');
	if (!slotCost) return;

	const g = document.createElement('div');
	g.classList.add('form-group', 'spontaneous-casting', 'flexrow');
	g.innerHTML = `
	<label><input type='checkbox' class='spontaneous-casting'> ${game.i18n.localize('SpellSubstitution.Item.Enable')}</label>
	<p class='spontaneous-casting-hint'>${game.i18n.localize('SpellSubstitution.Item.Hint')}</p>
	`;

	async function toggleFlag(checked) {
		if (checked)
			spell.setFlag('world', CFG.MARK, true);
		else
			spell.unsetFlag('world', CFG.MARK);
	}

	const input = g.querySelector('input');
	input.checked = spell.getFlag('world', CFG.MARK) ?? false;

	input.addEventListener('change', function(ev) {
		ev.preventDefault();
		ev.stopPropagation();

		this.disabled = true;
		toggleFlag(this.checked);
	});

	slotCost.closest('.form-group')?.after(g);
}
