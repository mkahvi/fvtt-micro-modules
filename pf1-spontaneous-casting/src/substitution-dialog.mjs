import { CFG } from './config.mjs';
import { spontaneousSubstituteCast, preparedSubstituteCast } from './substitution-logic.mjs';
import { simplifySpellData } from './utility.mjs';
import { constructSpellBookInfo } from './spellbook.mjs';
import { SpellLevelData } from './spellbook-data.mjs'; // For typing

export class SpellSubstitutionDialog extends FormApplication {
	/** @type {ItemSpellPF} */
	spell;
	/** @type {ActorPF} */
	actor;
	/** @type {BookData} */
	book;

	/**
	 * @param {ItemSpellPF} spell
	 * @param {ActorPF} actor
	 * @param {BookData} book
	 * @param resolve
	 * @param root0
	 * @param root0.token
	 */
	constructor(spell, actor, book, resolve, { token } = {}) {
		super();
		this.spell = spell;
		this.actor = actor;

		// Regen book for sanity
		book = constructSpellBookInfo(actor, book.id);

		this.book = book;
		this.resolve = resolve;
		this.token = token;

		this.options.title = book.isSpontaneous ? 'Spell substitution...' : 'Spontaneous casting...';
	}

	get template() {
		return `modules/${CFG.id}/template/substitution-dialog.hbs`;
	}

	getData() {
		const book = this.book;
		const spell = this.spell;
		const spellData = spell.system;

		const context = {
			...super.getData(),
			spell: simplifySpellData(spell),
			actor: this.actor,
			book,
		};

		const allowDomainSac = game.settings.get(CFG.id, CFG.SETTINGS.sacDomain);

		context.levels = [];
		const startLevel = spellData.level + (book.isSpontaneous ? 1 : 0);
		for (let i = startLevel; i < 10; i++) {
			/** @type {SpellLevelData} */
			const ld = book.spells.level[i];

			// Spontaneous, any slots available
			if (book.isSpontaneous) {
				if (!ld.available) continue;
			}
			// Prepared, any spells with charges left
			else {
				if (!ld.canConsumeFor(spell, false)) continue;
				for (const spell of ld.spells) {
					spell.available = !allowDomainSac && spell.domain ? ld.domainAvailable : true;
				}
				if (!ld.spells.some(s => s.available)) continue;
			}
			context.levels.push(ld);
		}

		return context;
	}

	static get defaultOptions() {
		const _default = super.defaultOptions;
		return {
			..._default,
			id: CFG.id,
			classes: [..._default.classes, CFG.id, 'spell-substitution'],
			submitOnClose: false,
			closeOnSubmit: true,
			submitOnChange: false,
		};
	}

	/**
	 * @param {JQuery} jq
	 */
	activateListeners(jq) {
		super.activateListeners(jq);

		const html = jq[0];

		const first = html.querySelector('input[type="radio"]');
		if (!first) return;
		first.checked = true; // Select first option
		first.focus(); // Focus first option
	}

	_updateObject(event, formData) {
		const spell = this.spell;
		const book = this.book;

		if (book.isSpontaneous)
			spontaneousSubstituteCast({ spell, actor: this.actor, book, consumeLevel: formData.level, token: this.token }, this.resolve);
		else
			preparedSubstituteCast({ spell, actor: this.actor, book, spellId: formData.spell, token: this.token }, this.resolve);

		this.resolve = null;
		// spell.use();
	}

	async close(options = {}) {
		await super.close(options);
		this.resolve?.(undefined);
	}
}
