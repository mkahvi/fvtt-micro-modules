import { CFG } from './config.mjs';

/**
 * Information about a spell level.
 */
export class SpellLevelData {
	/** @type {number} */
	level;

	/** @type {number} */
	base;

	/** @type {number} */
	max = 0;

	/** @type {number} */
	value = 0;

	/**
	 * Has slots available on this level.
	 *
	 * @returns {boolean}
	 */
	get available() {
		// console.log(this.level, { available: this.value, yes: this.value > 0 });
		return this.value > 0 && this.max > 0;
	}

	/**
	 * Can substitute with same or higher level slot.
	 *
	 * @returns {boolean}
	 */
	get canSubstitute() {
		if (this.available) return true;
		return this.higher?.canSubstitute ?? false;
	}

	/** @type {ItemSpellPF[]} */
	spells = [];

	prepared = 0;

	domain = 0;

	domainSlots = 0;

	/**
	 * @param {number} level
	 * @param {object} levelData
	 * @param {object} bookData
	 */
	constructor(level, levelData, bookData) {
		this.level = level;

		this.base = levelData.base ?? null;
		this.max = levelData.max ?? null;
		this.value = levelData.value ?? null;
		this.domainSlots = bookData.domainSlotValue ?? 0;
	}

	/**
	 * @param {ItemSpellPF} spell
	 */
	addSpell(spell) {
		if (spell.atWill) return;

		// Do not add spells with no charges
		if (!(spell.maxCharges > 0)) return;

		this.spells.push(spell);
		this.prepared += Math.max(0, spell.charges);
		if (spell.domain) this.domain++;
	}

	/**
	 * @param {ItemSpellPF} spell
	 * @param {boolean} checkHigher
	 * @returns {boolean} - True if this level or higher can be used for
	 */
	canConsumeFor(spell, checkHigher = true) {
		const allowDomainSac = game.settings.get(CFG.id, CFG.SETTINGS.sacDomain);

		const { domain, normal } = this.spells.filter(i => i.id !== spell.id).reduce((all, s) => {
			if (s.domain && !allowDomainSac) {
				all.domain += s.charges;
			}
			else
				all.normal += s.charges;
			return all;
		}, { domain: 0, normal: 0 });

		const domainAvailable = Math.max(0, domain - this.domainSlots);

		const available = domainAvailable + normal;

		this.domainAvailable = domainAvailable > 0;

		return available > 0 || (checkHigher ? (this.higher?.canConsumeFor(spell) || false) : false);
	}

	/** @type {SpellLevelData} */
	higher;
}

/**
 * Information about entire spellbook.
 */
export class BookData {
	id;
	/** @type {Actor} */
	actor;

	raw;

	spells = {
		/** @type {ItemSpellPF[]} */
		all: [],
		/** @type {Object.<number, SpellLevelData>} */
		level: {},
	};

	/** @type {CastingConfig} */
	config;

	/** @type {string} */
	get name() {
		const r = this.raw;
		if (r.altName) return r.altName;
		if (r.class === '_hd') return 'Racial';
		if (r.class === '') return r.name;
		const rd = this.rollData;
		return rd.classes[r.class]?.name ?? r.name;
	}

	#rollData;
	get rollData() {
		if (!this.#rollData) this.#rollData = this.actor.getRollData();
		return this.#rollData;
	}

	/** @type {boolean} */
	get isSpontaneous() {
		return this.raw.spontaneous;
	}

	get usesSpellPoints() {
		return this.raw.spellPoints?.useSystem === true;
	}

	get canUse() {
		return !this.usesSpellPoints;
	}

	/**
	 * @param {string} book
	 * @param {object} bookData
	 * @param {Actor} actor
	 */
	constructor(book, bookData, actor) {
		this.id = book;
		this.raw = bookData;
		this.actor = actor;

		// console.log(bookData);

		for (let i = 9; i >= 0; i--) {
			const ld = new SpellLevelData(i, bookData.spells[`spell${i}`], bookData);
			this.spells.level[i] = ld;
			ld.higher = this.spells.level[i + 1];
		}
	}

	/** @param {ItemSpellPF} spell */
	addSpell(spell) {
		this.spells.all.push(spell);
		const clamp = Math.clamp ?? Math.clamped;
		const level = clamp(spell.level, 0, 9);
		if (Number.isFinite(level)) {
			const levelData = this.spells.level[level];
			levelData.addSpell(spell);
		}
	}
}
