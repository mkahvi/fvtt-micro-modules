const CFG = {
	module: 'mkah-pf1-buff-activator',
	FLAGS: {
		TOGGLE: 'buffToggle',
		OLD_ACTIVATION_DICT_KEY: 'activateBuff',
	},
	COLORS: {
		main: 'color:purple',
		number: 'color:mediumpurple',
		label: 'color:goldenrod',
		unset: 'color:unset',
	}
};

CFG.templateSource = `modules/${CFG.module}/buff-selector.hbs`;
Hooks.once('setup', () => getTemplate(CFG.templateSource).then(t => CFG.template = t));

const matchName = (name0, name1) => name0.localeCompare(name1, undefined, { ignorePunctuation: true, sensitivity: 'base', usage: 'search' }) === 0;

/**
 * Overcomplicated mess.
 *
 * @param {ItemAction} action
 */
function getActivationConfig(action) {
	/** @type {Item} */
	const item = action.item;
	const bcfg = item?.getFlag('world', CFG.FLAGS.TOGGLE)?.[action.id];

	const activateAll = new Set(bcfg?.activate ?? []),
		deactivateAll = new Set(bcfg?.deactivate ?? []),
		activateIds = new Set(),
		deactivateIds = new Set(),
		activateNames = [],
		deactivateNames = [],
		allIds = new Set(),
		allNames = [],
		actor = item.actor;

	function sortActivation(name, activate) {
		const id = /^#(?<id>[\da-z]{16})$/i.exec(name)?.groups.id;
		if (id) {
			allIds.add(id);
			activate ? activateIds.add(id) : deactivateIds.add(id);
		}
		else {
			allNames.push(name);
			activate ? activateNames.push(name) : deactivateNames.push(name);
		}
	}

	if (activateAll.size == 0) {
		// Backwards compatibility
		const oname = item.getItemDictionaryFlag(CFG.FLAGS.OLD_ACTIVATION_DICT_KEY);
		if (oname?.length >= 3) sortActivation(oname, true);
	}

	activateAll.forEach(i => sortActivation(i, true));
	deactivateAll.forEach(i => sortActivation(i, false));

	const activateBuffs = [],
		deactivateBuffs = [],
		allBuffs = actor?.itemTypes.buff?.filter(i => {
			const { id, name } = i;
			if (deactivateIds.has(id))
				deactivateBuffs.push(i);
			else if (activateIds.has(id))
				activateBuffs.push(i);
			else if (deactivateNames.some(n => matchName(name, n)))
				deactivateBuffs.push(i);
			else if (activateNames.some(n => matchName(name, n)))
				activateBuffs.push(i);
			else
				return false;
			return true;
		}) ?? [];

	return {
		all: {
			ids: allIds,
			names: allNames,
		},
		activate: {
			all: activateAll,
			ids: activateIds,
			names: activateNames
		},
		deactivate: {
			all: deactivateAll,
			ids: deactivateIds,
			names: deactivateNames
		},
		buffs: {
			all: allBuffs,
			activate: activateBuffs,
			deactivate: deactivateBuffs
		}
	};
}

function getRelevantBuff(actor, search) {

}

/**
 * React to item activation.
 *
 * Only `postAttack` event in case the activation is cancelled.
 *
 * @param {ActionUse} app
 */
async function itemActivation(app) {
	const { action, shared, actor, item } = app;
	const { all, activate, deactivate, buffs } = getActivationConfig(action);

	if ((buffs.all?.length ?? 0) == 0) {
		// console.warn('No buffs found for', action.id, { shared, all, activate, deactivate, buffs });
		return;
	}

	const C = CFG.COLORS;

	if (!actor) // should be impossible
		return console.warn(`%cACTIVATOR%c | Ignored | %c${item.name}%c has no parent actor`,
			C.main, C.unset, C.label, C.unset);

	const updates = [],
		updateIds = new Set(), // To make sure no buff is toggled multiple times
		updatePath = 'system.active';

	const generateUpdate = (buff, enable) => {
		const id = buff.id;
		if (!updateIds.has(id)) {
			if (buff.isActive !== enable)
				updates.push({ _id: id, [updatePath]: enable })
			updateIds.add(id);
		}
	}

	// De-activate
	Array.from(deactivate.ids).forEach(buffId => {
		const buff = actor.items.get(buffId);
		if (buff) generateUpdate(buff, false);
	});
	if (deactivate.names.length) {
		buffs.deactivate.filter(b => deactivate.names.some(ab => matchName(b.name, ab)))
			.forEach(b => generateUpdate(b, false));
	}

	// Activate
	Array.from(activate.ids).forEach(buffId => {
		const buff = actor.items.get(buffId);
		if (buff) generateUpdate(buff, true);
	});
	if (activate.names.length) {
		buffs.activate.filter(b => activate.names.some(ab => matchName(b.name, ab)))
			.forEach(b => generateUpdate(b, true));
	}

	// Perform actual update
	if (updates.length > 0) {
		console.log(`%cACTIVATOR%c | Triggered | %c${item.name}%c [%c${item.id}%c] -> %c${action.name}%c [%c${action.id}%c] updates:`,
			C.main, C.unset, C.label, C.unset, C.id, C.unset, C.label, C.unset, C.id, C.unset,
			updates.reduce((all, cur) => {
				all[cur._id] = cur['system.active'];
				return all;
			}, {}));
		actor.updateEmbeddedDocuments('Item', updates);
	}
	else {
		console.log(`%cACTIVATOR%c | Triggered | %c${item.name}%c [%c${item.id}%c] -> %c${action.name}%c [%c${action.id}%c] no updates.`,
			C.main, C.unset, C.label, C.unset, C.id, C.unset, C.label, C.unset, C.id, C.unset);
	}
}

/**
 * @param {ItemSheet} sheet
 * @param {JQuery} html
 * @param options
 */
async function injectBuffSelector(sheet, [html], options) {
	const item = options.item ?? sheet.item;
	if (!item) return;

	const activation = html.querySelector('select[name="activation.type"],select[name="activation.unchained.type"]');
	if (!activation) return;

	const actionId = sheet.action?.id ?? 'default';
	const action = item.actions.get(actionId);
	const { activate, deactivate, buffs } = getActivationConfig(action);

	async function setActivationBuff(ev, value) {
		ev.preventDefault();
		ev.stopPropagation();

		const nv = value ?? ev.target.value.trim();
		if (nv?.length > 0) {
			const update = {
				[`${actionId}.activate`]: [nv]
			};
			if (deactivate.all.has(nv)) update[`${actionId}.-=deactivate`] = null;
			return item.setFlag('world', CFG.FLAGS.TOGGLE, update)
				.then(_ => console.log(`ACTIVATOR | Enabled | "${item.name}" now activates "${nv}"`));
		}
		else {
			return item.update({ [`flags.world.${CFG.FLAGS.TOGGLE}.${actionId}.-=activate`]: null })
				.then(_ => console.log(`ACTIVATOR | Disabled | "${item.name}" no longer has buff activation.`));
		}
	}

	async function setDeActivationBuff(ev, value) {
		ev.preventDefault();
		ev.stopPropagation();

		const nv = value ?? ev.target.value.trim();
		if (nv?.length > 0) {
			const update = {
				[`${actionId}.deactivate`]: [nv]
			};
			if (activate.all.has(nv)) update[`${actionId}.-=activate`] = null;
			return item.setFlag('world', CFG.FLAGS.TOGGLE, update)
				.then(_ => console.log(`ACTIVATOR | Enabled | "${item.name}" now de-activates "${nv}"`));
		}
		else {
			return item.update({ [`flags.world.${CFG.FLAGS.TOGGLE}.${actionId}.-=deactivate`]: null })
				.then(_ => console.log(`ACTIVATOR | Disabled | "${item.name}" no longer has buff de-activation.`));
		}
	}

	const allBuffs = sheet.actor?.itemTypes.buff ?? [];

	allBuffs.sort((a, b) => a.name?.localeCompare(b.name, undefined, { ignorePunctuation: true, sensitivity: 'base', usage: 'sort' }));

	const getId = (match) => /^#(?<id>[a-zA-Z\d]{16})/.exec(match)?.groups.id;

	const activateFirst = activate.all.first(),
		activateId = getId(activateFirst),
		activateItem = activateId ? allBuffs.find(i => i.id === activateId) : null,
		deactivateFirst = deactivate.all.first(),
		deactivateId = getId(deactivateFirst),
		deactivateItem = deactivateId ? allBuffs.find(i => i.id === deactivateId) : null;

	const templateData = {
		uuid: item.uuid,
		buffs: allBuffs,
		isAction: !!item?.hasAction,
		activate: activate.all.first(),
		activateItem,
		deactivate: deactivate.all.first(),
		deactivateItem
	};

	const d = document.createElement('bee');
	d.innerHTML = CFG.template(templateData, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true });

	if (sheet.isEditable) {
		d.querySelector('input.activate-buff')?.addEventListener('change', setActivationBuff);
		d.querySelector('input.deactivate-buff')?.addEventListener('change', setDeActivationBuff);

		d.querySelectorAll('action[data-action="unlink"]').forEach(el => {
			const group = el.dataset.group;
			if (group === 'activate') el.addEventListener('click', ev => setActivationBuff(ev, ''));
			else if (group === 'deactivate') el.addEventListener('click', ev => setDeActivationBuff(ev, ''));
		});
	}
	else {
		d.querySelectorAll('input').forEach(el => {
			el.disabled = true;
			el.readOnly = true;
		});
	}

	activation.closest('.form-group').after(...d.childNodes);
}

Hooks.on('pf1PreDisplayActionUse', itemActivation);

Hooks.on('renderItemSheet', injectBuffSelector);
Hooks.on('renderItemActionSheet', injectBuffSelector);

let itemHintsAPI;

/**
 * @param {Actor} actor
 * @param {Item} item
 * @param {object} data
 * @returns {undefined|Hint[]}
 */
function itemHintsHandler(actor, item, data) {
	const flag = item.getItemDictionaryFlag(CFG.FLAGS.OLD_ACTIVATION_DICT_KEY);
	if (!flag) return;

	const cls = itemHintsAPI.HintClass;
	const bh = cls.create('Buff', ['buff-activator'], { hint: flag });
	return [bh];
}

function itemHintsRegistration() {
	const itemHintsModule = game.modules.get('mkah-pf1-item-hints');
	if (itemHintsModule?.active) {
		itemHintsAPI = itemHintsModule.api;
		itemHintsAPI.addHandler(itemHintsHandler);
	}
}

Hooks.once('ready', itemHintsRegistration);
