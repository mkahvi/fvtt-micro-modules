# Container Contents for Pathfinder 1e

![Supported Foundry Versions](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fversion%3Fstyle%3Dflat%26url%3Dhttps%3A%2F%2Fgitlab.com%2Fmkahvi%2Ffvtt-micro-modules%2F-%2Fraw%2Fmaster%2Fpf1-container-contents%2Fmodule.json)

Displays container contents at the end of the expanded container description.

Clicking item name opens the item sheet directly.

![Screencap](./container.png)

✅ It's decent. Not as good as it could be, but it does its job.

## Known problems

- Long item lists likely render very long.

## Install

Manifest URL: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/master/pf1-container-contents/module.json>

### PF1 v9 and older

Manual install only: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/c2d50de2525e0de591ebbfe87127bdce84e7dd7d/pf1-container-contents/pf1-container-contents.zip>

### Foundry v9 and older

Manual install only: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/blob/a4f98e2330458cd208fe72dc91c605a9c9d84e17/pf1-container-contents/pf1-container-contents.zip>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
