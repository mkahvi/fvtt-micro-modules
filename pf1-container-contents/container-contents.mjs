const CFG = { id: 'pf1-container-contents' };

const cssPropKey = 'module-container-contents';

const sheetId = '.pf1.sheet,.pf1alt.sheet'; // base sheet and alt sheet
const templateSource = `modules/${CFG.id}/container.hbs`;
let template;
Hooks.once('setup', () => {
	getTemplate(templateSource).then(t => template = t);
});

async function openItemSheet(el, container) {
	if (!container) return;

	const itemId = el.dataset.itemId ?? el.parentNode.dataset.itemId;
	const item = container.items.get(itemId);
	if (!item) return void ui.notifications.warn('Item not found');

	item.sheet.render(true, { focus: true });
}

/**
 * @param {Item} cnt
 * @param {Element} node
 */
const addContents = (cnt, node) => {
	const templateData = {
		id: cnt.id,
		hasItems: cnt.items?.contents.length > 0,
		items: cnt.items.map(item => ({
			id: item.id,
			name: item.name,
			uuid: item.uuid,
			quantity: item.system.quantity,
		})),
	};

	const d = document.createElement('div');
	d.innerHTML = template(templateData, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true });
	const dd = d.querySelector('div');
	node.append(dd);

	dd.addEventListener('contextmenu', ev => {
		const _el = ev.target;
		const el = _el.dataset.itemId ? _el : _el.parentNode;
		if (!el) return;
		ev.preventDefault();
		ev.stopPropagation();
		openItemSheet(el, cnt);
	});
};

/**
 * @param {HTMLElement} html
 * @param {Actor} actor
 * @param {Collection<Item>} items
 */
function renderSheetEvent(html, actor, items) {
	const handleElement = (summary, parent) => {
		if (summary.querySelector(`.${cssPropKey}`)) return; // already have it
		const cnt = items.get(parent.dataset.itemId);
		if (!cnt) return;

		addContents(cnt, summary, actor, items);
	};

	/**
	 * @param {MutationRecord[]} mutList
	 * @param {MutationObserver} _observer
	 */
	function mutationCallback(mutList, _observer) {
		function mutHandler(mut) {
			/** @type HTMLElement */
			const node = mut.addedNodes[0];
			const el = mut.target;
			if (!node.classList.contains('item-summary')) return;

			handleElement(node, el);
		}

		Array.from(mutList).filter(m => m.addedNodes.length).forEach(mutHandler);
	}

	const mutationConfig = { subtree: false, childList: true, attributes: false };
	const observer = new MutationObserver(mutationCallback);

	const iEls = html.querySelectorAll('.item-list .item');

	// TODO: Add single observer, filter inside mutation events
	for (const iEl of iEls) {
		const id = iEl.dataset.itemId;
		if (!id) continue; // Required by some custom sheets
		const item = items.get(id);
		if (!item) continue;

		const nEl = iEl.querySelector('.item-name');
		if (!nEl) continue;

		if (item.type === 'container') {
			observer.observe(iEl, mutationConfig);

			// Fill in already open descriptions
			const summary = iEl.querySelector('.item-summary');
			if (summary) handleElement(summary, iEl);
		}
	}
}

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 */
function renderActorSheetEventProxy(sheet, [html]) {
	const actor = sheet.actor;
	renderSheetEvent(html, actor, actor.items);
}

/**
 *
 * @param {ItemSheet} sheet
 * @param {JQuery<HTMLElement>} html
 */
function renderItemSheetEventProxy(sheet, [html]) {
	const item = sheet.item;
	if (item.type !== 'container') return;
	if (!(sheet instanceof pf1.applications.item.ItemSheetPF)) return;
	renderSheetEvent(html, item.actor, item.items);
}

Hooks.once('ready', () => {
	Hooks.on('renderActorSheetPF', renderActorSheetEventProxy);
	Hooks.on('renderItemSheet', renderItemSheetEventProxy);
	console.log('CONTAINER CONTENTS |', game.modules.get(CFG.id).version, '| READY!');
});
