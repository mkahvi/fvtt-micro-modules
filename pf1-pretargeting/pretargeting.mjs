// Pretargeting

const MODULE_ID = 'pf1-pretargeting';

const ignoredRanges = ['none', 'personal'];

const targetPrompt = (resolve) => {
	const title = game.i18n.localize('PF1Pretargeting.Prompt.Title'),
		hint = game.i18n.localize('PF1Pretargeting.Prompt.Hint');
	const dialog = new Dialog(
		{
			title,
			content: `<div class="flexcol"><h3>${title}</h3><p>${hint}</p></div>`,
			buttons: {
				skip: {
					label: game.i18n.localize('PF1Pretargeting.Prompt.Skip'),
					callback: () => resolve(true),
				},
				cancel: {
					label: game.i18n.localize('PF1Pretargeting.Prompt.Cancel'),
					callback: () =>resolve(false)
				}
			},
			default: 'skip',
			close: () => resolve(true),
		},
		{
			jQuery: false,
			top: 100,
			width: 260,
			resizable: true,
			classes: ['dialog', 'pretargeting']
		}
	);
	dialog.render(true, { focus: true });
	return dialog;
}

async function preTarget(wrapped) {
	const rv = await wrapped.call(this);
	if (rv > 0) return rv;

	const action = this.action;
	const targets = game.user.targets;
	if (action && targets.size == 0) {
		let dialog, hook;
		const ru = action.data.range.units;
		if (action.hasAttack && !ignoredRanges.includes(ru)) {
			// Get open windows
			const windows = Object.values(ui.windows).filter((x) => !!x.minimize && !x._minimized);

			try {
				// Minimize open windows
				await Promise.allSettled(windows.map((x) => x.minimize()));
				// Prompt for target
				const result = await new Promise(resolve => {
					dialog = targetPrompt(resolve);
					hook = Hooks.on('targetToken', (user, token, selected) => {
						if (user.id === game.user.id) {
							if (selected) resolve(true);
						}
					});
				});
				// Skipped
				if (result === false) {
					console.log('PRETARGETING | ❌ Action cancelled in target selection.');
					return 40100;
				}
			}
			catch (err) {
				console.error(err);
			}
			finally {
				// Restore minimized windows
				windows.map((x) => x.maximize());
				Hooks.off('targetToken', hook);
				if (dialog?.rendered) dialog?.close();
			}
		}
	}

	return 0;
}

const hookPretargeting = () => {
	/* global libWrapper */
	libWrapper.register(MODULE_ID, 'pf1.actionUse.ActionUse.prototype.checkRequirements', preTarget, 'MIXED');
};

Hooks.once('ready', hookPretargeting);
