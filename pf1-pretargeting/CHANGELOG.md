# Change Log

## 0.1.1.1

- Fix target selection not working with multi-action items.

## 0.1.1

- Minimize & restore open applications when selecting target (thanks to @claudekennilol)

## 0.1.0.1

- Fix target selection not appearing if specific action was not triggered (e.g. any single action item or shift-clicking activation).

## 0.1 Initial
