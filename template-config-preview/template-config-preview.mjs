/**
 * @param {MeasuredTemplateDocument} tpl
 */
const resetTemplate = (tpl) => {
	tpl.reset();
	tpl.object.refresh();
};

/**
 * @param {MeasuredTemplateConfig} app
 * @param {JQuery} html
 */
const previewHandler = (app, [html]) => {
	const tpl = app.object;

	const refreshPreview = (name, value) => {
		if (foundry.utils.hasProperty(tpl, name)) {
			tpl._modPreview = true;
			foundry.utils.setProperty(tpl, name, value);
			tpl.object.refresh();
		}
	};

	/**
	 * @param {Event} ev
	 */
	const changeEvent = (ev) => {
		/** @type {Element} */
		const el = ev.target;
		const name = el.name || el.dataset.edit;
		let value = el.type == 'checkbox' ? el.checked : el.value;

		if (value.length == 0) value = null;
		else if (value[0] === '#') { /* NOP */ }
		else if (name == 't') { /* NOP */ }
		else if (name == 'hidden') value = Boolean(value);
		else value = Number(value);

		const actual = foundry.utils.getProperty(tpl._source, name);
		el.classList.toggle('preview-active', actual !== value);
		if (actual !== value)
			el.dataset.tooltip = `Actual Value: ${actual}`;
		else
			el.removeAttribute('data-tooltip');

		// TODO: Clone styling between color input/button

		refreshPreview(name, value);
	};

	html.querySelectorAll('input,select')
		.forEach(el => el.addEventListener('change', changeEvent));
};

/**
 * Prevent unnecessary reset.
 *
 * @param {MeasuredTemplateDocument} tpl
 */
const cancelPreview = (tpl) => delete tpl._modPreview;

/**
 * @param {MeasuredTemplateConfig} app
 */
const cleanupPreview = (app) => {
	const tpl = app.object;
	if (tpl._modPreview)
		resetTemplate(tpl);
};

Hooks.on('renderMeasuredTemplateConfig', previewHandler);
Hooks.on('closeMeasuredTemplateConfig', cleanupPreview);
Hooks.on('updateMeasuredTemplate', cancelPreview);
