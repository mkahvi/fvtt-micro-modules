const CFG = {
	id: 'pf1-hidden-buffs',
};

function prepareItems(wrapped, context) {
	wrapped(context);

	try {
		if (game.user.isGM) return;

		for (const section of Object.values(context.buffs)) {
			if (section.items?.length) {
				section.items = section.items.filter(id => id.document.getFlag(CFG.id, 'hidden') !== true);
			}
		}
	}
	catch (err) {
		console.error(err);
	}
}

/**
 * @param {ItemSheet} sheet
 * @param {JQuery<HTMLElement>} html
 */
function renderItemSheet(sheet, [html]) {
	if (!game.user.isGM) return;

	const item = sheet.document;
	if (item.type !== 'buff') return;

	const adv = html.querySelector('.tab[data-tab="advanced"]');
	if (!adv) return;

	const div = document.createElement('div');

	div.innerHTML = `<div class="flexcol form-group hidden-buffs">
		<label class="checkbox">
			<i class="fa-regular fa-eye-slash"></i>
			<input type="checkbox" name="flags.pf1-hidden-buffs.hidden">
			Hide From Players (on Actor Sheet)
		</label>
	</div>`;

	const cb = div.querySelector('input');
	cb.checked = item.getFlag(CFG.id, 'hidden') ?? false;

	if (!sheet.isEditable) {
		cb.readOnly = true;
		cb.disabled = true;
	}

	adv.prepend(div);
}

Hooks.on('setup', () => {
	/* global libWrapper */
	libWrapper.register(CFG.id, 'pf1.applications.actor.ActorSheetPF.prototype._prepareItems', prepareItems, libWrapper.WRAPPER);
});

Hooks.on('renderItemSheet', renderItemSheet);

// Item hints bonus
Hooks.once('ready', () => {
	let api;
	const customHandler = (_actor, item) => {
		if (item.type !== 'buff') return;
		if (item.getFlag(CFG.id, 'hidden') !== true) return;

		return [api.HintClass.create('Label', ['css-selector'], {
			hint: 'Hidden From Players',
			icon: 'fa-solid fa-eye-slash',
		})];
	};

	const ih = game.modules.get('mkah-pf1-item-hints');
	if (ih?.active) {
		api = ih.api;
		ih.api.addHandler(customHandler);
	}
});
