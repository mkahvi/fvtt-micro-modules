# Alt Words of Power

These have nothing to do with PF1 rules, be it optional, third-party, or otherwise.

Implements:

- Actors
  - Wordcaster
- Items
  - Word spell
  - Word of Power

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## License

This software is distributed under the [MIT license](./LICENSE).
