const CFG = {
	id: 'pf1-wop',
	baseDC: 5,
};

const wopcasterid = `${CFG.id}.wop`;
const wopspellid = `${CFG.id}.spell`;
const wopwordid = `${CFG.id}.word`;

class WordCasterSheet extends ActorSheet {
	static get defaultOptions() {
		const options = super.defaultOptions;
		return {
			...options,
			template: `modules/${CFG.id}/templates/caster-sheet.hbs`,
			classes: [...options.classes, 'wop', 'caster'],
			dragDrop: [{ dragSelector: '.item-list .item[data-item-id]', dropSelector: 'form' }]
		}
	}

	async getData() {
		const actor = this.actor;
		const actorData = actor.system;
		const context = {
			...this.actor,
			abilities: [
				{ id: 'fitness', label: 'Fitness' },
				{ id: 'coordination', label: 'Coordination' },
				{ id: 'toughness', label: 'Toughness' },
			],
			attributes: [
				{ id: 'attack', label: 'Attack' },
				{ id: 'defense', label: 'Defense' },
				{ id: 'resistance', label: 'Resistance' },
			],
			magic: [
				{ id: 'concentration', label: 'Concentration' },
				{ id: 'negation', label: 'Negation' },
				{ id: 'capacity', label: 'Capacity' },
			],
		}

		for (const abl of context.abilities)
			abl.data = actorData.abilities[abl.id];

		for (const attr of context.attributes)
			attr.data = actorData.attributes[attr.id];

		for (const mag of context.magic)
			mag.data = actorData.magic[mag.id];

		context.itemTypes = actor.itemTypes;
		context.words = context.itemTypes[wopwordid] ?? [];
		context.spells = context.itemTypes[wopspellid] ?? [];

		const sortItems = (a, b) => (a.sort || 0) - (b.sort || 0);

		context.words.sort(sortItems);
		context.spells.sort(sortItems);

		await Promise.all(context.spells.map(spl => spl.system.prepareDynamicData()));

		context.enrichedNotes = await TextEditor.enrichHTML(actorData.details.notes);

		return context;
	}

	/**
	 * @param {Event} event
	 */
	_onAction(event) {
		event.preventDefault();

		const el = event.currentTarget;
		const action = el.dataset.action;
		const itemId = el.closest('.item[data-item-id]')?.dataset.itemId;
		const item = this.actor.items.get(itemId);
		console.log(action, item);

		switch (action) {
			case 'cast':
				item.use();
				break;
			case 'create':
				this._onCreateItem(el.dataset.type);
				break;
			case 'delete':
				this._onDeleteItem(item);
				break;
		}
	}

	_onCreateItem(type) {

	}

	_onDeleteItem(item) {
		const subtype = item.subTypeLabel;

		Dialog.confirm({
			title: 'Confirm Delete',
			content: `<h3>Delete Item?</h3><p><b>${subtype}</b>: ${item.name}</p><p>ID: <code>${item.id}</code></p><hr>`,
			yes: () => item.delete(),
			no: () => null,
		},
		{
			jQuery: false,
			rejectClose: false,
			width: 280,
		});
	}

	_onOpenItem(event) {
		event.preventDefault();

		const itemId = event.currentTarget.dataset.itemId;
		this.actor.items.get(itemId).sheet.render(true);
	}

	activateListeners(jq) {
		super.activateListeners(jq);

		const html = this.form;

		html.querySelectorAll('a.action')
			.forEach(el => el.addEventListener('click', this._onAction.bind(this)));

		html.querySelectorAll('.item[data-item-id]')
			.forEach(el => el.addEventListener('contextmenu', this._onOpenItem.bind(this)));
	}
}

class SpellSheet extends ItemSheet {
	static get defaultOptions() {
		const options = super.defaultOptions;
		return {
			...options,
			template: `modules/${CFG.id}/templates/spell-sheet.hbs`,
			classes: [...options.classes, 'wop', 'word-spell'],
			dragDrop: [{ dropSelector: 'form', dragSelector: 'form .item-list .item' }]
		}
	}

	_canDragDrop() {
		return this.isEditable;
	}

	_canDragStart() {
		return this.isEditable;
	}

	/**
	 *
	 * @param {DragEvent} event
	 */
	_onDragStart(event) {
		const data = {
			type: 'Item',
			index: event.target.dataset.index,
			action: 'sort'
		};
		event.dataTransfer.setData('text/plain', JSON.stringify(data));
	}

	_onDrop(event) {
		const data = TextEditor.getDragEventData(event);
		if (data.type !== 'Item') return void ui.notifications.error('Non-item dropped, ignoring.');

		if (data.action === 'sort')
			this._onSortItem(event, data);
		else
			this._onDropItem(event, data);
	}

	_onSortItem(event, data) {
		let { index } = data;
		index = parseInt(index);

		let tidx = event.target.dataset.index || event.target.closest('.item[data-index]')?.dataset.index;
		tidx = parseInt(tidx);
		if (!Number.isSafeInteger(tidx)) return void ui.notifications.warn('Invalid drop location');
		if (tidx === index) return;

		const words = this.item.toObject().system.words ?? [];

		const old = words.findSplice((val, idx) => idx === index);
		words.splice(tidx, 0, old);

		this.item.update({ system: { words } });
	}

	async _onDropItem(event, data) {
		if (!this.item.isOwner) return false;
		const item = await Item.implementation.fromDropData(data);

		if (item.type !== wopwordid) {
			ui.notifications.error('Only spell words can be added.');
			return false;
		}

		// TODO: Handle sorting

		const words = this.item.toObject().system.words ?? [];

		words.push({
			uuid: item.uuid,
			count: 1,
			type: item.system.type,
			burden: { base: item.system.burden }
		});

		this.item.update({ system: { words } });
	}

	async getData() {
		const item = this.item,
			itemData = item.system;

		const actor = item.actor;

		await itemData.prepareDynamicData();

		return {
			...item,
			words: itemData.words,
			baseDC: actor?.system.magic.dc ?? CFG.baseDC,
			get dc() {
				return this.baseDC + this.system.burden.total;
			},
			types: game.i18n.translations.WordsOfPower.Words.Types,
			enrichedDesc: await TextEditor.enrichHTML(item.system.description),
		}
	}

	_onAction(event) {
		event.preventDefault();

		const action = event.target.dataset.action;
		const el = event.target.closest('[data-item-uuid]');
		const index = parseInt(el.dataset.index);

		if (!Number.isSafeInteger(index))
			return void ui.notifications.error('Valid index not found.');

		const words = this.item.toObject().system.words ?? [];

		switch (action) {
			case 'increase':
				words[index].count += 1;
				break;
			case 'decrease':
				if (words[index].count == 0) return;
				words[index].count -= 1;
				break;
			case 'delete':
				words.findSplice((v, idx) => idx === index);
				break;
		}

		this.item.update({ system: { words } });
	}

	activateListeners(jq) {
		super.activateListeners(jq);

		const html = this.form;

		html.querySelectorAll('a.action')
			.forEach(el => el.addEventListener('click', this._onAction.bind(this)));
	}
}

class WordSheet extends ItemSheet {
	static get defaultOptions() {
		const options = super.defaultOptions;
		return {
			...options,
			template: `modules/${CFG.id}/templates/word-sheet.hbs`,
			classes: [...options.classes, 'wop', 'spell-word'],
		}
	}

	async getData() {
		const item = this.item;

		return {
			...item,
			enrichedDesc: await TextEditor.enrichHTML(item.system.description),
			types: game.i18n.translations.WordsOfPower.Words.Types,
		}
	}
}

function defineAbilityScore(baseValue = 10) {
	return class AbilityModel extends foundry.abstract.DataModel {
		static defineSchema() {
			const fields = foundry.data.fields;
			return {
				value: new fields.NumberField({ integer: true, min: 0, initial: 0, nullable: false }),
			}
		}

		_initialize(...args) {
			super._initialize(...args);

			this.base = baseValue ?? 10;

			this.total = this.base + this.value;
			this.mod = this.total - 10;
		}
	}
}

function defineAttribute(baseValue = 0, abilities = []) {
	return class AttributeModel extends foundry.abstract.DataModel {
		static defineSchema() {
			const fields = foundry.data.fields;
			return {
				ranks: new fields.NumberField({ integer: true, min: 0, initial: 0, nullable: false }),
				bonus: new fields.NumberField({ integer: true, initial: 0, nullable: false }),
			}
		}

		_initialize(...args) {
			super._initialize(...args);

			this.abilityMults = abilities;
			this.abilities = 0;

			this.base = baseValue ?? 10;

			this.total = this.base + this.ranks + this.bonus;
		}
	}
}

class WordCasterModel extends foundry.abstract.TypeDataModel {
	static defineSchema() {
		const fields = foundry.data.fields;
		return {
			details: new fields.SchemaField({
				age: new fields.StringField(),
				pronouns: new fields.StringField(),
				hair: new fields.StringField(),
				eyes: new fields.StringField(),
				height: new fields.StringField(),
				weight: new fields.StringField(),
				notes: new fields.HTMLField(),
			}),
			abilities: new fields.SchemaField({
				fitness: new fields.EmbeddedDataField(defineAbilityScore(10)),
				coordination: new fields.EmbeddedDataField(defineAbilityScore(10)),
				toughness: new fields.EmbeddedDataField(defineAbilityScore(10)),
			}),
			vigor: new fields.NumberField({ min: 0, integer: true, nullable: false, initial: 3 }),
			tradition: new fields.StringField(),
			attributes: new fields.SchemaField({
				attack: new fields.EmbeddedDataField(defineAttribute(0, [['fitness', 2], ['coordination', 1]])),
				defense: new fields.EmbeddedDataField(defineAttribute(10, [['coordination', 1], ['toughness', 1]])),
				resistance: new fields.EmbeddedDataField(defineAttribute(0, [['toughness', 0.2]])),
			}),
			magic: new fields.SchemaField({
				concentration: new fields.EmbeddedDataField(defineAttribute(0)),
				negation: new fields.EmbeddedDataField(defineAttribute(10)),
				capacity: new fields.EmbeddedDataField(defineAttribute(10)),
			}),
		}
	}

	prepareDerivedData() {
		const abilities = this.abilities;

		for (const data of Object.values(this.attributes)) {
			for (const [attr, mult] of data.abilityMults) {
				const bonus = Math.floor(abilities[attr].mod * mult);
				data.abilities += bonus;
				data.total += bonus;
			}
		}

		this.magic.dc = CFG.baseDC - this.magic.concentration.total;
	}
}

// Not necessary with v9.5
class WordCaster extends pf1.documents.actor.ActorPF {
	prepareBaseData() {
		this._itemTypes = null;
	}

	prepareDerivedData() { }
	_prepareChanges() { }
	_updateExp() { }
	getRollData() { return {} }
}

// Not necessary with v9.5
class SpellWordItem extends pf1.documents.item.ItemPF {
	prepareDerivedData() { }

	getRollData() { return {} }

	get typeLabel() {
		return game.i18n.localize(`WordsOfPower.Words.Types.${this.system.type}`);
	}
}

class WordSpellItem extends pf1.documents.item.ItemPF {
	prepareDerivedData() { }

	getRollData() { return {} }

	async use(options = {}) {
		options = {
			...options,
			flavor: this.name,
			critical: 5,
		};

		const textData = {
			words: this.system.words,
			dc: this.system.dc,
			burden: this.system.burden.total,
			description: this.system.description?.length ? await TextEditor.enrichHTML(this.system.description) : null,
		};
		const chatTemplateData = {
			extraText: (await renderTemplate(`modules/${CFG.id}/templates/components.hbs`, textData)).trim(),
			hasExtraText: true,
		}

		const roll = new pf1.dice.D20RollPF('1d20', {}, options);

		const subject = { wop: 'spell' };
		roll.toMessage({}, { chatTemplateData, subject });
	}

	get typeLabel() {
		return `Spell`;
	}
}

class WordReferenceModel extends foundry.abstract.DataModel {
	static defineSchema() {
		const fields = foundry.data.fields;
		return {
			id: new fields.StringField({ blank: false, nullable: false, initial: () => randomID(8) }),
			uuid: new fields.StringField(),
			count: new fields.NumberField({ min: 0, integer: true }),
			type: new fields.StringField({ blank: false, nullable: false }),
			burden: new fields.SchemaField({
				base: new fields.NumberField({ min: 0, integer: true }),
			})
		};
	}

	_initialize(...args) {
		super._initialize(...args);

		this.item = null;

		this.prepareDerivedData();
	}

	prepareDerivedData() {
		this.burden.total = this.burden.base * this.count;
	}

	get name() {
		return this.item?.name;
	}

	get img() {
		return this.item?.img;
	}

	get valid() {
		return !!this.item;
	}

	get typeLabel() {
		return game.i18n.localize(`WordsOfPower.Words.Types.${this.type}`);
	}
}

class SpellModel extends foundry.abstract.TypeDataModel {
	static defineSchema() {
		const fields = foundry.data.fields;
		return {
			description: new fields.HTMLField(),
			burden: new fields.SchemaField({
				adjust: new fields.NumberField({ integer: true, initial: undefined, nullable: true }),
			}),
			words: new fields.ArrayField(new fields.EmbeddedDataField(WordReferenceModel)),
		};
	}

	prepareBaseData() {
		this.burden.total = 0;
	}

	prepareDerivedData() {
		for (const word of this.words) {
			const item = fromUuidSync(word.uuid);
			if (item instanceof Item) {
				word.item = item;
				word.prepareDerivedData();
			}
			else {
				fromUuid(word.uuid).then(item => {
					word.item = item;
					word.prepareDerivedData();
				});
			}
		}
	}

	async prepareDynamicData() {
		this.prepareBaseData();

		let burden = 0;
		for (const word of this.words) {
			if (!word.item) {
				const item = await fromUuid(word.uuid);
				word.item = item;
			}
			word.prepareDerivedData();
			burden += word.burden.total;
		}
		this.burden.total += burden;
	}

	get dc() {
		const baseDC = this.parent?.actor?.system.magic?.dc ?? CFG.baseDC;
		return baseDC + this.burden.total;
	}
}

class WordModel extends foundry.abstract.TypeDataModel {
	static defineSchema() {
		const fields = foundry.data.fields;
		return {
			type: new fields.StringField({ nullable: false, blank: false, initial: 'effect' }),
			description: new fields.HTMLField(),
			burden: new fields.NumberField({ integer: true, min: 0, initial: undefined, nullable: true }),
		};
	}
}

Hooks.on('preCreateItem', (item, data, context, user) => {
	if (!item.actor) return;
	if (![wopspellid, wopwordid].includes(item.type)) return;
	if (item.actor.type !== wopcasterid) {
		ui.notifications.error(`"${item.actor.type}" actor can't have word magic`);
		return false;
	}
});

Hooks.once('init', () => {
	CONFIG.Actor.dataModels[wopcasterid] = WordCasterModel;
	CONFIG.Item.dataModels[wopspellid] = SpellModel;
	CONFIG.Item.dataModels[wopwordid] = WordModel;

	Actors.registerSheet(CFG.id, WordCasterSheet, { label: 'Words of Power', types: [wopcasterid], makeDefault: true });
	Items.registerSheet(CFG.id, SpellSheet, { label: 'Word Spell', types: [wopspellid], makeDefault: true });
	Items.registerSheet(CFG.id, WordSheet, { label: 'Spell Word', types: [wopwordid], makeDefault: true });

	// Do not provide custom document (following not necessary with v9.5)
	CONFIG.Actor.documentClasses[wopcasterid] = WordCaster;
	CONFIG.Item.documentClasses[wopspellid] = WordSpellItem;
	CONFIG.Item.documentClasses[wopwordid] = SpellWordItem;
});
