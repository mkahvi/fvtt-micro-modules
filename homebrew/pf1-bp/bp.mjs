const flagId = 'buildPoints';
const flagNamespace = 'world';
const flagPath = `flags.${flagNamespace}.${flagId}`;

function getBuildPoints(actor) {
	const feats = actor.itemTypes.feat
		.map(f => ({ name: f.name, ...getFeatInfo(f) }))
		.filter(f => f.cost != 0);
		// .filter(f => f.total != 0);

	// const bpTotal = feats.reduce((total, bpf) => total + bpf.total, 0);
	const bpTotal = feats.reduce((total, bpf) => total + bpf.cost, 0);

	return { total: bpTotal, count: feats.length, items: feats };
}

class BPTrackerModel extends foundry.abstract.TypeDataModel {
	static defineSchema() {
		const fields = foundry.data.fields;

		return {
			actors: new fields.ArrayField(new fields.SchemaField({
				id: new fields.StringField({ required: true, blank: false }),
				sort: new fields.NumberField({ integer: true, initial: 0 }),
			})),
		};
	}

	prepareDerivedData() {
		for (const info of this.actors) {
			info.document = game.actors.get(info.id);
		}
	}

	addActor(actor) {
		if (this.actors.find(a => a.id === actor.id)) return false;

		const sort = Math.max(0, ...this.actors.map(a => a.sort ?? 0)) + 1_000;
		console.log(sort);

		const actors = this.toObject().actors;
		actors.push({ id: actor.id, sort });

		console.debug('BP Tracker | Tracking:', actor.name);

		return this.parent.update({ system: { actors } });
	}

	removeActor(actorId) {
		const actors = this.toObject().actors.filter(a => a.id !== actorId);

		console.debug('BP Tracker | Untracking:', actorId, game.actors.get(actorId)?.name);

		return this.parent.update({ system: { actors } });
	}

	sortActor(actor, newPosition) {
		if (!actor || !newPosition) return false;
		if (actor.id == newPosition) return false;

		const actors = this.toObject().actors;

		const reArrange = () => {
			actors.sort((a, b) => (a.sort ?? 0) - (b.sort ?? 0));
			actors.forEach((actor, index) => actor.sort = (index + 1) * 1_000);
		};

		reArrange(); // Arrange to ensure sorting order and even spread

		const actorInfo = actors.find(a => a.id === actor.id);
		const targetInfo = actors.find(a => a.id === newPosition);
		if (!actorInfo || !targetInfo) return false;

		if (targetInfo.sort > actorInfo.sort)
			actorInfo.sort = targetInfo.sort + 100;
		else
			actorInfo.sort = targetInfo.sort - 100;

		reArrange(); // Arrange again for the new order

		this.parent.update({ system: { actors } });
	}
}

class BPTracker extends ActorSheet {
	get title() { return 'Build Point Tracker: ' + this.actor.name; }

	get template() { return 'modules/pf1-bp/templates/tracker.hbs'; }

	static get defaultOptions() {
		const options = super.defaultOptions;
		return {
			...options,
			classes: [...options.classes, 'actor', 'build-point-tracker'],
			dragDrop: [{ dropSelector: 'form', dragSelector: 'li.actor' }],
			width: 660,
			height: 600,
		};
	}

	getData() {
		const actors = this.actor.system.actors.sort((a, b) => a.sort - b.sort);

		for (const info of actors) {
			const actor = info.document;
			info.name = actor.name;
			info.img = actor.img;
			info.points = getBuildPoints(actor);

			if (this.appId) actor.apps[this.appId] = this;
		}

		return { actors };
	}

	async _onDropActor(event, data) {
		if (!this.isEditable) return false;

		const actor = fromUuidSync(data.uuid);
		if (!(actor instanceof Actor)) return void ui.notifications.error('Received non-actor data!');

		const actors = this.actor.toObject().system.actors;

		const old = actors.find(a => a.id === actor.id);
		if (old) {
			return this.actor.system.sortActor(actor, event.target.closest('[data-actor-id]')?.dataset.actorId);
		}
		else {
			return this.actor.system.addActor(actor);
		}
	}

	_onDragStart(event) {
		const actorId = event.target.dataset.actorId;

		const dragData = {
			type: 'Actor',
			uuid: this.actor.system.actors.find(a => a.id === actorId)?.document.uuid,
		};

		event.dataTransfer.setData('text/plain', JSON.stringify(dragData));
	}

	async close(...args) {
		const actors = this.actor.system.actors.map(a => a.document);
		for (const actor of actors) {
			delete actor.apps[this.appId];
		}

		return super.close(...args);
	}

	/**
	 *
	 * @param {JQuery<HTMLElement>} jq
	 */
	activateListeners(jq) {
		super.activateListeners(jq);

		const html = jq[0];

		for (const el of html.querySelectorAll('a[data-action]')) {
			el.addEventListener('click', this._onAction.bind(this));
		}

		for (const el of html.querySelectorAll('.actor[data-actor-id]')) {
			el.addEventListener('contextmenu', this._onActorSheet.bind(this));
		}
	}

	/**
	 * @param {Event} event
	 */
	_onAction(event) {
		event.preventDefault();

		const action = event.target.dataset.action;
		const actorId = event.target.closest('.actor[data-actor-id]')?.dataset.actorId;

		switch (action) {
			case 'remove': {
				return this.actor.system.removeActor(actorId);
			}
		}
	}

	_onActorSheet(event) {
		event.preventDefault();

		const actorId = event.target.closest('.actor[data-actor-id]')?.dataset.actorId;

		game.actors.get(actorId)?.sheet.render(true);
	}
}

function getFeatInfo(item) {
	const bp = item.getFlag(flagNamespace, flagId);

	/*
	let mult = 0;
	if (item.system.uses?.per) {
		// V11 infinite max uses
		if (!item.system.uses?.maxFormula)
			mult = item.system.uses?.value;
		// V10 and older max resource
		else
			mult = item.system.uses?.max;
	}
	else {
		// Feat Levels module
		mult = item.getFlag('world', 'level');
	}

	// Assume 1 when not explicitly defined
	mult ??= 1;
	*/

	return {
		cost: bp ?? 0,
		// mult,
		// total: (bp || 0) * (mult || 0),
	};
}

/**
 * @param {ActorSheet} sheet
 * @param {JQuery<HTMLElement>} html
 */
function renderActorSheet(sheet, [html]) {
	const actor = sheet.actor;
	// Only normal actors can have BP
	if (!['character', 'npc'].includes(actor.type)) return;

	const bpEl = document.createElement('div');
	bpEl.classList.add('build-points');

	const { total: bpTotal, count: bpFeatCount, items: feats } = getBuildPoints(actor);

	bpEl.innerHTML = `<label>BP</label><span>${bpTotal}</span>`;

	// TODO: Custom tooltip handler to avoid bloating the HTML (does it matter tho?)
	// bpEl.dataset.tooltip = feats.map(f => `<label>${f.name}</label> <span class='cost'>${f.cost}</span> × <span class='multiplier'>${f.mult}</span> = <span class='total'>${f.total}</span>`).join('<br>');
	bpEl.dataset.tooltip = feats.map(f => `<label>${f.name}</label> <span class='cost'>${f.cost}</span>`).join('<br>');
	bpEl.dataset.tooltipClass = 'build-points-details';

	const summary = html.querySelector('.tab.summary');
	summary?.append(bpEl);

	const featsBPTotal = document.createElement('div');
	featsBPTotal.classList.add('build-points');
	featsBPTotal.innerHTML = `<label>BP</label> <span class='total'>${bpTotal}</span>`;

	const features = html.querySelector('.tab.features');
	features?.querySelector('.extra-feats')?.append(featsBPTotal);
}

/**
 * @param {ItemSheet} sheet
 * @param {JQuery<HTMLElement>} html
 */
function renderItemSheet(sheet, [html]) {
	const item = sheet.document;

	// Only feats can have BP
	if (item.type !== 'feat') return;

	const header = html.querySelector('header.sheet-header');
	if (!header) return;

	const bits = header.querySelector('ul.summary');
	if (!bits) return;

	const liEl = document.createElement('li');
	liEl.classList.add('build-points');

	// Label
	const labelEl = document.createElement('label');
	labelEl.textContent = 'BP';

	// Input
	const inputEl = document.createElement('input');
	inputEl.placeholder = 'n/a';
	inputEl.type = 'number';
	inputEl.min = 0;
	inputEl.step = 1;
	inputEl.name = flagPath;

	const { cost, mult, total } = getFeatInfo(item);
	if (cost) inputEl.value = cost;

	// Total
	const totalEl = document.createElement('span');

	if (mult > 1) {
		totalEl.innerHTML = `× <b>${mult}</b> = <b>${total}</b>`;
		totalEl.classList.add('total');
	}

	liEl.append(labelEl, inputEl, totalEl);
	bits.append(liEl);
}

Hooks.on('renderActorSheet', renderActorSheet);
Hooks.on('renderItemSheet', renderItemSheet);

// Item Hints tag
function customHandler(api, actor, item) {
	if (item.type !== 'feat') return;

	const { cost } = getFeatInfo(item);
	if (cost) {
		const hint = api.HintClass.create(`${cost} BP`, ['build-points'], {
			// hint: 'text', // tooltip
			// icon: 'fa-solid fa-star', // font awesome icon
		});
		return [hint]; // Array of hints
	}
};

Hooks.once('ready', () => {
	const ih = game.modules.get('mkah-pf1-item-hints');
	if (ih.active) {
		ih.api.addHandler(customHandler.bind(null, ih.api));
	}
});

Hooks.once('init', () => {
	CONFIG.Actor.dataModels['pf1-bp.tracker'] = BPTrackerModel;

	Actors.registerSheet('pf1-bp', BPTracker, {
		label: 'Build Point Tracker',
		types: ['pf1-bp.tracker'],
		makeDefault: true,
	});
});
