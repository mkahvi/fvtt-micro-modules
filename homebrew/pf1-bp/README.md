# Build Points for Pathfinder 1e

Simple BP tracking and assignment interface.

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## License

This software is distributed under the [MIT license](./LICENSE).
