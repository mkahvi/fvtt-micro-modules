const buffTypes = ['temp'];

function restEvent(actor, _opts, _updateData, itemUpdateData) {
	const buffs = actor.items
		.filter(b => b.type === 'buff' && b.isActive && buffTypes.includes(b.subType));

	const buffIds = [];
	const updates = buffs.map(b => {
		buffIds.push(b.id);
		return { _id: b.id, 'data.active': false };
	});

	if (updates.length === 0) return;

	updates.forEach(b => {
		const ou = itemUpdateData.find(i => buffIds.includes(i._id));
		if (ou) foundry.utils.mergeObject(ou, b);
		else itemUpdateData.push(b);
	});
}

Hooks.on('actorRest', restEvent);
