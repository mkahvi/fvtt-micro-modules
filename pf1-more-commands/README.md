# More Chat Commands for Pathfinder 1e

![Supported Foundry Versions](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fversion%3Fstyle%3Dflat%26url%3Dhttps%3A%2F%2Fgitlab.com%2Fmkahvi%2Ffvtt-micro-modules%2F-%2Fraw%2Fmaster%2Fpf1-more-commands%2Fmodule.json)

Adds number of additional PF1 specific chat commands.

See also system agnostic [more commands](../more-commands/)

✅ It's decent.

## Commands

| Command | Arguments | Description | Example |
|:--- |:--- |:--- |:--- |
| `/init` | n/a | Roll initiative for selected tokens, adding them to combat. | `/init` |
| `/health` | =?Number | Adjusts health of selected tokens by defined amount. `reset` can be used as optional argument for expected results. | `/health -5t` |
| `/hp` | ... | Alias for `/health` | `/hp reset` |
| `/save` | [save ID] | Rolls defined saving throw with selected tokens. | `/save ref` |
| `/skill` | [skill ID] | Rolls the skill associated with the skill key for all selected tokens. | `/skill per`<br>`/skill crf.alchemy` |
| `/ability` | [ability ID] | Rolls ability test. | `/ability str` |

Most commands fall back to using configured character if no token(s) are selected.

## Install

Manifest URL: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/master/pf1-more-commands/module.json>

### Foundry V9 and older

Manual install only: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/blob/b9f67f2d88994e8d07b5c9b8e85e28a688415034/pf1-more-commands/pf1-more-commands.zip>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
