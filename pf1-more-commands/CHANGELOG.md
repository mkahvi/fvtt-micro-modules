# Change Log

## 0.12.0.1

- Maintenance

## 0.12

- Added: Support for subskills.
  - Subskills do not need to include the middle `.subSkills` part, `/skill crf.alchemy` is enough.

## 0.11

- Added: `/ability` command.

## 0.10.0.1

- Fix: Disable ghost condition toggling commands on unsupported versions.

## 0.10.0

- All system agnostic features moved to generic [more commands](../more-commands/) module.

## 0.9.1

- `/hp` now supports special `reset` keyword for resetting health information.

## 0.9

- `/torch` can be used without arguments to toggle it.
- Added `/sight` (with `/vision` alias) for toggling sight the same way as `/torch`.
- Limit actor updates to known types (for v11 support)
- Health adjustments clamp maximum HP correctly.
- Support for older Foundry versions than 10 dropped.

## 0.8

- Added `/health` command taking token HP adjust value as first parameter and any string starting with `t` or `n` as second parameter which can be run-in with the number to target temporary HP or nonlethal damage.
  - Includes `/hp` as alias.
  - e.g. `/hp -5t`, `/hp 20 temp`, `/hp 15 nonlethal`
  - NL damage overflows to normal health as expected, damage to temporary HP overflows to normal health.
  - Note: `/hp -5` will cause 5 actual damage, skipping temporary HP. However, `/hp -5t` will cause it to behave as per normal damage.

## 0.7

- Added `/combat` command with optional on/enter/add/off/leave/remove argument.

## 0.6

- Added `/torch` command with optional on/off/true/false argument.

## 0.5

- v10 compatibility

## 0.4

- Added disposition commands: `/friendly`, `/ally`, `/neutral`, `/enemy`, and `/hostile`.

## 0.3

- Fixed command matching.
- Added `/condition` & `/cond`

## 0.2

- Added `/init`

## 0.1 Initial
