const CFG = { module: 'mkah-pf1-more-commands' };

// UTILITY

const supportedActorTypes = ['character', 'npc'];

const getTokens = () => canvas.tokens.controlled;
const getActors = (tokens) => (tokens ?? getTokens()).map(t => t.actor).filter(a => supportedActorTypes.includes(a?.type)) ?? [game.user.character];

// COMMANDS

/**
 * @param {"ref"|"fort"|"wil"} saveId
 * @param _chatData
 */
function rollSave(saveId, _chatData) {
	const actors = getActors();

	if (actors.length) {
		if (Object.keys(pf1.config.savingThrows).includes(saveId)) {
			actors.forEach(actor => actor.rollSavingThrow(saveId, { skipPrompt: true }));
		}
		else {
			ui.notifications?.error(`Invalid saving throw ID: "${saveId}"`);
		}
	}
	else {
		ui.notifications?.error('No tokens selected to roll saves for.');
	}
}

function rollSkill(fullSkillId, _chatData) {
	const actors = getActors();

	const parts = fullSkillId.split('.');
	const skillId = parts.at(0);
	const subSkillId = parts.length > 1 ? parts.at(-1) : null;

	if (actors.length) {
		if (Object.keys(pf1.config.skills).includes(skillId)) {
			const sklId = subSkillId ? `${skillId}.subSkills.${subSkillId}` : skillId;
			actors.forEach(actor => actor.rollSkill(sklId, { skipDialog: true }));
		}
		else
			ui.notifications?.error(`Invalid skill ID: "${skillId}"`);
	}
	else
		ui.notifications?.error('No tokens selected to roll saves for.');
}

function rollAbility(abilityId, _chatData) {
	const actors = getActors();

	if (actors.length) {
		if (Object.keys(pf1.config.abilities).includes(abilityId))
			actors.forEach(actor => actor.rollAbilityTest(abilityId, { skipDialog: true }));
		else
			ui.notifications?.error(`Invalid ability ID: "${abilityId}"`);
	}
	else
		ui.notifications?.error('No tokens selected to roll saves for.');
}

const Health = {
	normal: Symbol('normal'),
	nonlethal: Symbol('nonlethal'),
	tempoary: Symbol('temporary'),
};

/**
 * @param {Actor[]} actors
 * @param {number} value
 * @param {symbol} type
 * @param {object} options
 * @param {boolean} options.isExact
 */
const alterHpForActors = (actors, value, type, { isExact = false } = {}) => {
	const promises = [];
	for (const actor of actors) {
		const hp = actor?.system.attributes.hp;
		const updateData = {};
		switch (type) {
			case 'reset':
				if (hp.value !== hp.max)
					updateData['system.attributes.hp.value'] = hp.max;
				if (hp.temp !== 0)
					updateData['system.attributes.hp.temp'] = 0;
				if (hp.nonlethal !== 0)
					updateData['system.attributes.hp.nonlethal'] = 0;
				break;
			case Health.normal: {
				if (!isExact) value = hp.value + value;
				updateData['system.attributes.hp.value'] = Math.min(value, hp.max);
				break;
			}
			case Health.nonlethal: {
				if (isExact)
					updateData['system.attributes.hp.nonlethal'] = value;
				else {
					const nnl = hp.nonlethal + value;
					updateData['system.attributes.hp.nonlethal'] = Math.min(nnl, hp.max);
					// Overflow to normal health
					const overflow = nnl > hp.max ? nnl - hp.max : 0;
					if (overflow != 0) updateData['system.attributes.hp.value'] = Math.min(hp.value + -overflow, hp.max);
				}
				break;
			}
			case Health.tempoary: {
				if (isExact)
					updateData['system.attributes.hp.temp'] = value;
				else {
					const nthp = hp.temp + value;
					updateData['system.attributes.hp.temp'] = Math.max(0, nthp);
					// Overflow to normal health when reducing
					const overflow = nthp < 0 ? nthp : 0;
					if (overflow != 0) updateData['system.attributes.hp.value'] = Math.min(hp.value + nthp);
				}
				break;
			}
		}

		if (!foundry.utils.isEmpty(updateData)) {
			promises.push(actor.update(updateData));
		}
	}

	return promises;
};

function alterHealth(args, _chatData) {
	const actors = getActors();
	if (actors.length == 0) return ui.notifications.warn('Please select one or more tokens to alter health of.');

	const re = /^(?<mod>=)?\s*(?<value>[-+]?\d+|reset)\s*(?<type>\w+)?$/i.exec(args);
	const { mod, value, type } = re?.groups ?? {};
	if (value === undefined) return ui.notifications.warn('No valid value given for health alteration.');

	const isExact = mod === '=';
	const nValue = value === 'reset' ? Infinity : parseFloat(value);

	const htype = (() => {
		if (value === 'reset')
			return 'reset';
		else if (/^[Tt]/.test(type))
			return Health.tempoary;
		else if (/^[Nn]/.test(type))
			return Health.nonlethal;
		else
			return Health.normal;
	})();

	if (Number.isNaN(nValue)) return ui.notifications.warn(`Invalid value "${value}"`);

	const promises = alterHpForActors(actors, nValue, htype, { isExact });

	Promise.allSettled(promises)
		.then(_ => ui.notifications.info(`Adjusted health by ${nValue} for ${promises.length} actors`));
}

function rollInitiative(_args, _chatData) {
	const tokens = getTokens();

	// This is bad on older version of PF1 than 0.80.8 due to skipDialog option not existing.

	if (tokens.length) {
		const firstToken = tokens[0];
		const tokenLayer = firstToken.layer;
		const oldCombatants = tokens.reduce((arr, t) => {
			if (t.combatant) arr.push(t.combatant);
			return arr;
		}, []);

		tokenLayer.toggleCombat(true) // toggle combat state for selected tokens
			.then(newCombatants => {
				const combatants = [...oldCombatants, ...newCombatants];
				if (combatants.length === 0) return ui.notifications?.warn('Failed to add combatants.');
				return combatants[0].combat.rollInitiative(combatants.map(c => c.id), { skipDialog: true });
			});
	}
	else
		ui.notifications?.error('No tokens selected to roll into initiative.');
}

// TODO
function toggleFeature(command, chatData) {
	const actors = getActors();
	if (actors.length === 0) return ui.notifications?.error('No tokens selected to toggle feature for.');

	const re = /(?<toggle>[-+])(?<feature>.*)/.exec(command);
	if (!re) return;

	const toggle = re.groups.toggle,
		state = toggle === '+' ? true : toggle === '-' ? false : undefined,
		search = re.groups.feature;
}

// MAPPING

const COMMANDS = {
	save: rollSave,
	skill: rollSkill,
	hp: alterHealth,
	health: alterHealth,
	init: rollInitiative,
	ability: rollAbility,
	// toggle: toggleFeature,
};

// HOOK IT IN

/**
 * @param {ChatLog} log
 * @param {string} data
 * @param chatData
 */
function moreCommands(log, data, chatData) {
	if (data[0] !== '/') return;

	const re = /^\/(?<exec>\w+)\b\s*(?<args>.*)?$/.exec(data);
	// console.log(data, re);
	if (!re) return;

	const command = re.groups?.exec,
		args = re.groups?.args;

	const cfn = COMMANDS[command];
	if (!cfn) return;

	try {
		cfn(args?.trim(), chatData);
		return false;
	}
	catch (error) {
		console.error({ command, args }, error);
	}
}

Hooks.on('chatMessage', moreCommands);

// Register API
Hooks.once('init', () => game.modules.get(CFG.module).api = { commands: COMMANDS });
