# Token Scaling Config for Pathfinder 1e

![Supported Foundry Versions](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fversion%3Fstyle%3Dflat%26url%3Dhttps%3A%2F%2Fgitlab.com%2Fmkahvi%2Ffvtt-micro-modules%2F-%2Fraw%2Fmaster%2Fpf1-token-scaling%2Fmodule.json)

Allows configuring the default token sizes as defined by PF1.

✅ Works great.

## Install

Manifest URL: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/master/pf1-token-scaling/module.json>

Foundry v9 needs manual install: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/98886dd92c8fbb796c7615492eb4a12998a56639/pf1-token-scaling/pf1-token-scaling.zip?inline=false>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
