const SETTINGS = {
	module: 'mkah-pf1-token-scaling',
	scaling: 'scaling',
};

let original;

class Scale {
	h;
	w;
	scale;
	constructor(h, w, r) {
		this.h = h;
		this.w = w;
		this.scale = r;
	}

	static create(h, w, r) {
		return new Scale(h, w, r);
	}

	static stale() {
		return new Scale();
	}
}

class ScalingConfigData {
	fine = Scale.stale();
	dim = Scale.stale();
	tiny = Scale.stale();
	sm = Scale.stale();
	med = Scale.stale();
	lg = Scale.stale();
	huge = Scale.stale();
	grg = Scale.stale();
	col = Scale.stale();

	static get default() {
		return new ScalingConfigData();
	}

	static get minified() {
		const c = new ScalingConfigData();
		console.log({ original });
		Object.entries(original).forEach(([size, sd]) => {
			c[size].h = sd.h;
			c[size].w = sd.w;
			c[size].scale = sd.scale;
		});
		c.fine.scale = 0.2;
		c.dim.scale = 0.35;
		c.tiny.scale = 0.5;
		c.sm.scale = 0.7;
		return c;
	}
}

function getConfig() {
	const scaling = game.settings.get(SETTINGS.module, SETTINGS.scaling);
	const virtual = deepClone(original);
	for (const [size, data] of Object.entries(virtual)) {
		for (const vkey of Object.keys(data)) {
			const value = scaling[size][vkey];
			if (value === undefined) continue;
			virtual[size][vkey] = value;
		}
	}
	const merged = foundry.utils.mergeObject(ScalingConfigData.default, virtual);
	// console.log('getConfig:', { merged, scaling, virtual });
	return merged;
}

const setConfig = (config) => game.settings.set(SETTINGS.module, SETTINGS.scaling, config);

const applyChanges = () => Object.assign(CONFIG.PF1.tokenSizes, getConfig());

class ScalingConfigDialog extends FormApplication {
	config;
	constructor(...args) {
		super(...args);
		this.options.classes.push(SETTINGS.module, 'token-scaling-config');
		this.config = getConfig();
	}

	static get defaultOptions() {
		return foundry.utils.mergeObject(super.defaultOptions, {
			id: `${SETTINGS.module}-config-dialog`,
			title: 'Token Scaling Configuration',
			width: 320,
			height: 'auto',
			submitOnClose: false,
			closeOnSubmit: false,
			resizable: false,
		});
	}

	get template() {
		return `modules/${SETTINGS.module}/config.hbs`;
	}

	getData() {
		const data = super.getData();
		data.config = this.config;
		return data;
	}

	/**
	 * @param {Event} event
	 * @param {object} formData
	 */
	_updateObject(event, formData) {
		const newConfig = Object.assign(getConfig(), expandObject(formData));
		this.config = newConfig;
		// console.log('updateObject:', { newConfig, formData, old: getConfig() });
		return setConfig(newConfig)
			.then(_ => {
				ui.notifications.info('Token scaling configuration saved!');
				applyChanges();
				this.render(true);
			});
	}

	/**
	 * @param {JQuery} jq
	 */
	activateListeners(jq) {
		super.activateListeners(jq);

		const app = this;

		const html = jq[0];

		html.querySelector('button[type="reset"]')
			?.addEventListener('click', function reset(ev) {
				ev.preventDefault();
				ev.stopPropagation();

				app.config = deepClone(original);
				app.render(true);
				ui.notifications.info('Scaling reset but not saved yet.');
			});

		html.querySelector('button.reset.minify')
			?.addEventListener('click', function minify(ev) {
				ev.preventDefault();
				ev.stopPropagation();

				app.config = ScalingConfigData.minified;
				app.render(true);
				ui.notifications.info('Scaling minified but not saved yet.');
			});
	}
}

const postInit = () => {
	game.settings.register(
		SETTINGS.module, SETTINGS.scaling,
		{
			type: Object,
			default: ScalingConfigData.default,
			scope: 'world',
			config: false,
			onChange: _ => applyChanges(),
		},
	);

	game.settings.registerMenu(
		SETTINGS.module, SETTINGS.scaling,
		{
			id: `${SETTINGS.module}-config`,
			name: 'Configure',
			label: 'Token Scaling',
			hint: 'Configure default token scaling.',
			icon: 'fas fa-ruler-combined', // button icon
			type: ScalingConfigDialog,
			restricted: true,
		},
	);

	original = deepClone(CONFIG.PF1.tokenSizes);

	applyChanges();
	// console.log({ conf: getConfig(), original });
};

// Hooks.once('pf1.postInit', postInit); // 0.81.3 and before
Hooks.once('pf1PostInit', postInit); // 0.82.0 and onward
