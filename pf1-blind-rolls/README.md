# Blind Rolls for Pathfinder 1

![Supported Foundry Versions](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fversion%3Fstyle%3Dflat%26url%3Dhttps%3A%2F%2Fgitlab.com%2Fmkahvi%2Ffvtt-micro-modules%2F-%2Fraw%2Fmaster%2Fpf1-blind-rolls%2Fmodule.json)

Forces blind roll mode for specified skills, regardless of anything else.

Blind roll skills default to Stealth, Disguise, Disable Device and Perception. This can be configured.

Actor sheet shows little icon with skills that have this behaviour forced.

## Install

Manifest URL: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/master/pf1-blind-rolls/module.json>

For older than PF1 0.82.5: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/06a16808be7ee94adec044346e836b6fc21d34ef/pf1-blind-rolls/pf1-blind-rolls.zip?inline=false>

For older than PF1 0.78.14: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/bc1f9e2f60675f5c7439e2f5e99ca78f392cd182/pf1-blind-rolls/pf1-blind-rolls.zip?inline=false> (manual install only)

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
