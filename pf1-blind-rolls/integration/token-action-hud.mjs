import { getBlindSkills } from '../blind-rolls.mjs';

/**
 * @param {Application} app
 * @param {JQuery} html
 * @param {object} options
 */
const signalBlindSkills = (app, [html], options) => {
	const dom = document.getElementById('tah-group-skills');

	const skillConfig = getBlindSkills();
	const allSkills = new Set(skillConfig.total);

	dom.querySelectorAll('.tah-action button[value]').forEach(el => {
		const data = JSON.parse(el.value);
		const skillId = data.actionId;
		if (allSkills.has(skillId)) {
			const secretIcon = document.createElement('i');
			secretIcon.classList.add('blind-skill', 'far', 'fa-eye-slash');
			el.append(secretIcon);
		}
	});
}

export const init = () => {
	Hooks.on('renderTokenActionHud', signalBlindSkills)
	console.log('Blind Rolls | Module Integration | Token Action HUD | Initialized');
}
