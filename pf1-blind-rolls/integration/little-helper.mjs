import { getBlindSkills } from '../blind-rolls.mjs';

/**
 * @param {ActorSheet} app
 * @param {JQuery} html
 */
const signalBlindSkillsInQuickSkills = (app, [html]) => {
	const qs = html.querySelector('.tab.summary .lil-quick-skills');
	if (!qs) return;

	qs.classList.add('blind-rolls-layout-adjust');

	const skills = getBlindSkills();

	qs.querySelector('.skill-list')
		?.querySelectorAll('[data-skill-id]')
		.forEach(el => {
			const skillId = el.dataset.skillId.replace(/\.subSkills/, '');
			const secretIcon = document.createElement('i');
			secretIcon.classList.add('blind-skill');
			if (skills.total.includes(skillId)) {
				secretIcon.classList.add('far', 'fa-eye-slash');
				secretIcon.dataset.tooltip = 'BlindRolls.IconHint';
			}
			else // Ensure columns in layout
				secretIcon.classList.add('padding');
			el.append(secretIcon);
		});
}

export const init = () => {
	Hooks.on('renderActorSheet', signalBlindSkillsInQuickSkills)
	console.log('Blind Rolls | Module Integration | Little Helper | Initialized');
}
