export const CFG = {
	id: 'pf1-blind-rolls',
	SETTINGS: { secrets: 'secrets', custom: 'custom' },
	default: ['ste', 'dis', 'dev', 'per'],
};

/**
 * @param {string[]} skills Fallback list
 * @returns {string[]}
 */
export const getBlindSkills = (skills) => ({
	skills: skills ?? game.settings.get(CFG.id, CFG.SETTINGS.secrets) ?? [],
	custom: game.settings.get(CFG.id, CFG.SETTINGS.custom) ?? [],
	get total() { return [...this.skills, ...this.custom]; },
});

class SkillConfig extends FormApplication {
	constructor() {
		super();

		this.settings = getBlindSkills();
	}

	get title() { return game.i18n.localize('BlindRolls.Settings.Title'); }

	get template() { return `modules/${CFG.id}/skill-config.hbs`; }

	static get defaultOptions() {
		const _default = super.defaultOptions;
		return {
			..._default,
			classes: [..._default.classes, 'blind-rolls'],
			height: 640,
			width: 460,
			resizable: true,
			submitOnClose: false,
			submitOnChange: false,
			closeOnSubmit: true,
		};
	}

	getData() {
		const settings = this.settings;

		const skills = [];
		Object.entries(CONFIG.PF1.skills).forEach(([key, label]) => {
			skills.push({
				key,
				label,
				blind: settings.skills.includes(key),
			});
		});

		skills.sort((a, b) => a.label.localeCompare(b.label));

		const custom = settings.custom.join(', ');

		return {
			skills,
			custom,
		};
	}

	/**
	 * @param {JQuery} jq
	 */
	activateListeners(jq) {
		super.activateListeners(jq);

		const html = jq[0];

		html.querySelector('[type="reset"]')?.addEventListener('click', ev => {
			ev.preventDefault();

			this.settings = {
				skills: CFG.default,
				custom: [],
			};

			this.render();

			ui.notifications.info(game.i18n.localize('BlindRolls.Settings.ResetNotify'));
		});
	}

	_updateObject(event, formData) {
		formData = expandObject(formData);
		const skills = Object
			.entries(formData.skills)
			.filter(([_, enabled]) => enabled)
			.map(([key]) => key);

		const custom = formData.custom
			.split(',')
			.map(s => s.replace('.subSkills', '').trim())
			.filter(f => f.length);

		game.settings.set(CFG.id, CFG.SETTINGS.secrets, skills);
		game.settings.set(CFG.id, CFG.SETTINGS.custom, custom);
	}
}

/**
 * Roll dialog handler to improve user experience with better communication of what's happening.
 *
 * @param {Dialog} app
 * @param {JQuery} html
 * @param {object} options
 */
const rollDialogHandler = (app, [html], options = {}) => {
	// Subject available with 0.78.14 (0.8.8) and newer
	// Subject is no longer available with 0.82.2, breaking this.
	// console.log({ options: opts });
	const skillId = app.options.subject?.skill?.replace('.subSkills', '');
	// console.log({ skillId });
	if (!skillId) return;
	const blindSkills = getBlindSkills();
	if (!blindSkills.total.includes(skillId)) return;

	const rollmode = html.querySelector('select[name="rollMode"]');
	rollmode.value = 'blindroll';
	rollmode.disabled = true;

	// Add hidden input to deal with disabled select
	const fin = document.createElement('input');
	fin.value = 'blindroll';
	fin.type = 'hidden';
	fin.name = 'rollMode';
	rollmode.after(fin);
};

/**
 * Ensure skill roll chat message is blind
 *
 * @param {ChatMessage} cm
 */
const preCreateChatMessageHandler = (cm) => {
	const skillId = cm.getFlag('pf1', 'subject')?.skill?.replace('.subSkills', '');
	if (!skillId) return;

	const blindSkills = getBlindSkills();
	if (!blindSkills.total.includes(skillId)) return;

	const updateData = {
		blind: true,
		whisper: ChatMessage.getWhisperRecipients('GM').map(u => u.id),
	};

	// Force blind roll
	if (game.release.generation >= 10)
		cm.updateSource(updateData);
	else
		cm.data.update(updateData);
};

const registerSettings = () => {
	game.settings.register(CFG.id, CFG.SETTINGS.secrets, {
		scope: 'world',
		type: Array,
		default: CFG.default,
		config: false,
		onChange: value => {
			const valid = Object.keys(CONFIG.PF1.skills);
			const skills = getBlindSkills(value);
			const invalid = skills.filter(skl => !valid.includes(skl));
			if (invalid.length) ui.notifications?.warn(`Invalid secret skill keys: ${invalid.join(', ')}`, { permanent: true });
		},
	});

	game.settings.register(CFG.id, CFG.SETTINGS.custom, {
		scope: 'world',
		type: Array,
		default: [],
		config: false,
	});

	game.settings.registerMenu(CFG.id, CFG.SETTINGS.secrets, {
		label: 'BlindRolls.Settings.Title',
		type: SkillConfig,
		restricted: true,
	});
};

const migrateSettings = () => {
	if (!game.user.isGM) return;

	const oldCfg = game.settings.get(CFG.id, CFG.SETTINGS.secrets);
	if (oldCfg.length === 1) {
		const newsettings = oldCfg[0].split(',').filter(f => f.length).sort();
		game.settings.set(CFG.id, CFG.SETTINGS.secrets, newsettings)
			.then(() => console.log('Blind Rolls setting migrated', { old: oldCfg, new: newsettings }));
	}
};

/**
 * Display icon on skills that will blind roll
 *
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 */
const signalBlindRolls = (sheet, [html], options) => {
	const blindskills = getBlindSkills();
	if (blindskills.total.length === 0) return;

	const tab = html.querySelector('.tab.skills');

	tab?.querySelectorAll('li[data-skill]').forEach(el => {
		const id = el.dataset.skill;
		if (!blindskills.total.includes(id)) return;

		const nameEl = el.querySelector('.skill-name');

		const secretIcon = document.createElement('i');
		secretIcon.classList.add('blind-skill', 'far', 'fa-eye-slash');
		secretIcon.dataset.tooltip = 'BlindRolls.IconHint';

		if (CFG.v10) {
			const targetEl = nameEl.querySelector('.controls');
			targetEl?.prepend(secretIcon);
		}
		else {
			const padding = document.createElement('span');
			padding.classList.add('blind-skill-padding');
			nameEl.append(padding, secretIcon);
		}
	});
};

const moduleSupport = () => {
	if (game.modules.get('token-action-hud-core')?.active)
		import('./integration/token-action-hud.mjs').then(m => m.init());

	if (game.modules.get('koboldworks-pf1-little-helper')?.active)
		import('./integration/little-helper.mjs').then(m => m.init());
};

// Hooks.on('pf1.preRoll', preRollHandler);
Hooks.on('preCreateChatMessage', preCreateChatMessageHandler);
Hooks.on('renderDialog', rollDialogHandler);
Hooks.once('init', registerSettings);
Hooks.once('ready', migrateSettings);

Hooks.on('renderActorSheet', signalBlindRolls);

Hooks.once('ready', moduleSupport);
Hooks.once('setup', () => {
	CFG.v10 = foundry.utils.isNewerVersion(game.system.version, '9.6');
});
