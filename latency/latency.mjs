import { CFG } from './module/common.mjs';
import { Pinger } from './module/pinger.mjs';

// V10 compatibility
function getDocData(doc) {
	/* global isNewerVersion */
	const isnewer = foundry.utils?.isNewerVersion ?? isNewerVersion;
	CFG.v10 ??= isnewer(game.version, '10');
	return CFG.v10 ? doc : doc.data;
}

Hooks.once('init', () => {
	game.settings.register(
		CFG.id,
		CFG.SETTING.intervalKey,
		{
			name: 'Ping interval (seconds)',
			scope: 'client',
			type: Number,
			default: 15,
			range: { min: 5, max: 300, step: 5 },
			config: true,
		},
	);

	game.settings.register(
		CFG.id,
		CFG.SETTING.debugKey,
		{
			name: 'Debug',
			hint: 'Enables additional logging.',
			scope: 'client',
			type: Boolean,
			default: false,
			config: true,
		},
	);
});

let pinger;

/**
 * @returns {{lastSample: TimeSample, median: number}}
 */
function getUserLatency() {
	return {
		last: pinger.history.last,
		median: pinger.history.median,
	};
}

Hooks.on('renderPlayerList', async () => pinger?.refresh());

Hooks.once('ready', () => {
	pinger = new Pinger();

	const mod = game.modules.get(CFG.id);

	// API
	mod.api = { get: getUserLatency };

	// delay ping start by # seconds
	setTimeout(() => pinger.start(), 2000);

	const md = getDocData(mod);
	console.log(`LATENCY | ${md.version} | READY`);
});
