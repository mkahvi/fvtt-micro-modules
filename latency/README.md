# Latency

⚠️ OBSOLETE STARTING WITH FOUNDRY V13 ⚠️

![Supported Foundry Versions](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fversion%3Fstyle%3Dflat%26url%3Dhttps%3A%2F%2Fgitlab.com%2Fmkahvi%2Ffvtt-micro-modules%2F-%2Fraw%2Fmaster%2Flatency%2Fmodule.json)

Adds small latency display to user list.

![Screencap](./ping.png)

✅ Recommended for general use

## Technical

The pinging is accomplished by repeated no-cache HEAD fetch requests to the game server. These should be as lightweight as possible (about 300 bytes per ping) without requiring any special support from anything (which _would_ lower the overhead, but is not guaranteed to work, or be accessible via JS).

## API

Via `game.modules.get('mkah-latency').api`

`api.get()` returns object with `last` and `median` properties for current user.

## Install

Manifest URL: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/master/latency/module.json>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT license](./LICENSE).

## Credits

Inspired by the [Response Times](https://foundryvtt.com/packages/response-time) module.
