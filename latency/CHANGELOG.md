# Change Log

## 0.3.1.1

- Fix: Sample array sorting was incorrect, causing best, worst and median values to display incorrectly.

## 0.3.1

- Users now do early quick double ping to get basic stats going.
- Fix: Other user tooltip stats were showing current user stats.
- Fix: Desync was always undefined.

## 0.3

- Latency display has tooltip now.
- Pinging now works via websocket (fetch() based logic removed)

## 0.2.1

- Bundling via esbuild
