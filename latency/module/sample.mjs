export class TimeSample {
	ms;
	stamp;
	delta;
	constructor(ms, stamp, delta) {
		this.ms = ms;
		this.stamp = stamp;
		this.delta = delta;
	}
}
