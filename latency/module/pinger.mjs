import { CFG } from './common.mjs';
import { History } from './history.mjs';
import { TimeSample } from './sample.mjs';
import { PingHUD } from './hud.mjs';

class UserSample {
	sample = new TimeSample(0, 0, 0);
	median = 0;
	timeoutId = null;
}

export class Pinger {
	socketFlag = `module.${CFG.id}`;

	interval = 15_000;
	staleTimeout = 30_000;

	tolerance = 0.5; // tolerance percentage
	toleranceMax = 200; // tolerance max ms
	toleranceMin = 50; // tolerance min ms
	errorMargin = 0; // assumed error margin

	pingId = -1;
	updaterId = -1;

	dirty = false; // track if anything has actually changed

	hud;

	history = new History();

	/** @type {Object<string,UserSample>} */
	lastSocketSample = {};

	constructor() {
		this.refresh();

		const clamp = Math.clamp ?? Math.clamped;

		this.interval = clamp(game.settings.get(CFG.id, CFG.SETTING.intervalKey), 5, 300) * 1_000;
		this.staleTimeout = this.interval * 12;

		console.group('LATENCY');
		try {
			console.log('Pinging interval:', this.interval / 1_000, 's');
			// console.log('Socket interval:', this.exInterval, 'ms');
			console.log('Stale timeout:', this.staleTimeout / 1_000, 's');
			console.log('Socket flag:', this.socketFlag);
		}
		finally {
			console.groupEnd();
		}

		game.socket.on(this.socketFlag, this.socket.bind(this));
		game.socket.on('userActivity', this.userActivity.bind(this));
	}

	slowUser(uid) {
		const lastMsg = this.lastSocketSample[uid]?.sample.stamp ?? NaN;
		if (this.hud.stale(uid, lastMsg)) {
			if (game.settings.get(CFG.id, CFG.SETTING.debugKey)) {
				const ago = Math.floor((Date.now() - lastMsg) / 100) / 10;
				console.warn('LATENCY | No socket data from user for a while:',
					game.users.get(uid)?.name, uid, '- last:', ago, `seconds ago (${new Date(lastMsg).toISOString()})`);
			}
		}
	}

	staleTest(uid) {
		const userSample = this.lastSocketSample[uid];
		if (!userSample) return;
		if (userSample.timeoutId !== null) {
			clearTimeout(userSample.timeoutId);
			userSample.timeoutId = null;
		}
		if (!userSample) return; // TODO: Make this better failure
		userSample.timeoutId = setTimeout(() => this.slowUser(uid), this.staleTimeout);
	}

	/**
	 * Receive socket data.
	 *
	 * @param {object} data
	 * @param {string} user User ID
	 */
	socket(data, user) {
		const now = Date.now();
		if (game.settings.get(CFG.id, CFG.SETTING.debugKey)) console.log('LATENCY | SOCKET DATA |', data, user);
		// if (data.user === game.user.id) return; // redundant?
		this.dirty = true;
		this.lastSocketSample[user] ??= new UserSample();
		try {
			const ts = new TimeSample(data.ms, data.stamp, data.delta);
			ts.diff = data.ping - now;

			this.hud?.update(ts, data.median, user);

			const lastSample = this.lastSocketSample[user];
			lastSample.sample = ts;
			lastSample.median = data.median;
			lastSample.best = data.best;
			lastSample.worst = data.worst;
			this.staleTest(user);
		}
		catch (err) {
			console.error('LATENCY |', err);
		}
	}

	userActivity(userId, data) {
		if (data.active === true) {
			// Nop
		}
		else if (data.active === false) {
			this.slowUser(userId);
		}
	}

	refresh() {
		this.hud = new PingHUD(this);
		game.users.forEach(u => {
			if (u.active) {
				const ls = this.lastSocketSample[u.id];
				if (ls) this.hud.update(ls.sample, ls.median, u.id);
				else this.lastSocketSample[u.id] = new UserSample();
			}
			else {
				if (u.id in this.lastSocketSample)
					delete this.lastSocketSample[u.id];
			}
		});
	}

	start() {
		if (game.settings.get(CFG.id, CFG.SETTING.debugKey)) console.log('LATENCY | Starting');

		this.pingId = setInterval(() => this.ping(), this.interval);
		this.updaterId = setInterval(() => this.updateUI(), 500);

		// Quick initial ping
		this.ping();

		// Quick second ping
		if (this.interval > 4_000) setTimeout(() => this.ping(), 2_000);
	}

	stop() {
		if (game.settings.get(CFG.id, CFG.SETTING.debugKey)) console.log('LATENCY | Stopping');

		clearTimeout(this.pingId);
		clearTimeout(this.updaterId);
		clearTimeout(this.shareId);

		this.hud.dead();
	}

	error(response) {
		console.warn('LATENCY |', response);
	}

	pingStart = Date.now();
	pingEnd = 0;
	pingTime = 0;
	lastSample = new TimeSample(0, 0, 0);

	pingCount = 0;
	ongoingPing = 0;

	ping() {
		const debug = game.settings.get(CFG.id, CFG.SETTING.debugKey);

		const stamp = Date.now();
		if (this.pinging) {
			if (debug) console.log('LATENCY | Interval reached; Ping ongoing.');
			const ms = stamp - this.pingStart;
			const delta = ms - this.history.last.ms;
			this.ongoingPing = delta;
			if (delta > 500) {
				const ts = new TimeSample(ms, stamp, delta);
				this.hud.update(ts, this.history.median, game.user.id, true);
				// TOOD: adjust interval if this keeps happening "consistently"?
			}
			// Add warning once ping increases too high
			if (delta > 10_000) {
				// 10s ping is bad
			}
			return;
		}
		else {
			this.pingStart = stamp;
		}

		this.pinging = true;

		const last = this.history.last;

		const msgData = {
			ping: this.pingStart,
			ms: last.ms,
			stamp: last.stamp,
			delta: last.delta,
			best: this.history.best,
			median: this.history.median,
			worst: this.history.worst,
		};

		game.socket.volatile.emit(this.socketFlag, msgData, {}, this.pong.bind(this));
	}

	pong() {
		this.pinging = false;
		this.pingEnd = Date.now();
		this.pingTime = this.pingEnd - this.pingStart - this.errorMargin;
		this.history.push(this.pingTime, this.pingEnd);
		this.dirty = true;
	}

	report() {
		const worst = this.history.worst,
			best = this.history.best,
			median = this.history.median,
			last = this.history.last;

		const ongoing = this.pinging ? this.ongoingPing : 0;

		console.group('LATENCY');
		try {
			console.log('Data:', this.history.samples);
			console.log('Samples:', this.history.samples.map(m => m.ms));
			console.log('Sorted:', this.history.sorted);
			if (ongoing) console.log('Ongoing:', ongoing, 'ms');
		}
		finally {
			console.groupEnd();
		}

		ui.notifications.info(`Latency – best: ${best} ms, <b>median: ${median} ms</b>, worst: ${worst} ms, last: ${last.ms} ms`);

		if (ongoing > 0)
			ui.notifications.warn(`Latency – ongoing unresolved ping for ${ongoing} ms`);
	}

	updating = false;
	async updateUI() {
		if (!this.dirty) return;
		if (this.updating) return;
		this.dirty = false;

		this.updating = true;
		try {
			// const last = this.history.last;
			this.hud.update(this.history.last, this.history.median);
		}
		finally {
			this.updating = false;
		}
	}
}
