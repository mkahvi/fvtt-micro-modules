export class PingHUD {
	/**
	 * DOM node for #players
	 */
	playerhud;
	userHud = {};
	pinger;

	constructor(pinger) {
		this.pinger = pinger;
		// console.log('LATENCY | Constructing HUD');
		this.playerhud = document.getElementById('players');
		const p = this;
		game.users.filter(u => u.active)
			.forEach(u => {
				const pe = p.getUserHUDElement(u.id);
				if (pe) p.addLatencyElement(pe, u.id);
			});
	}

	getUserHUDElement(uid) {
		return this.playerhud.querySelector(`[data-user-id="${uid}"]`);
	}

	_latencyClick(_dom) {
		this.pinger.report();
	}

	/**
	 * @param {string} uid User ID
	 */
	_tooltipContent(uid) {
		const s = this.userHud[uid];

		const ongoing = this.pinger.pinging ? this.pinger.ongoingPing : 0;

		const rv = [];
		if (game.user.id === uid) {
			const history = this.pinger.history;
			const worst = history.worst,
				best = history.best,
				median = history.median,
				last = history.last;

			rv.push(`Best: ${best} ms`, `Median: ${median} ms`, `Worst: ${worst} ms`, `Last: ${last?.ms} ms`);
		}
		else {
			const lastMsg = this.pinger.lastSocketSample[uid] ?? {};
			const { best, median, last, worst, sample } = lastMsg;
			rv.push(`Best: ${best} ms`, `Median: ${median} ms`, `Worst: ${worst} ms`, `Last: ${sample?.ms} ms`);
			if (sample) {
				const now = Date.now();
				const stamp = new Date(sample.stamp);
				const delta = Math.round((now - sample.stamp) / 1_000);
				const padded = (t) => `0${t}`.slice(-2);
				const hh = padded(stamp.getHours()),
					mm = padded(stamp.getMinutes()),
					ss = padded(stamp.getSeconds());

				rv.push(`Desync: ${sample.diff} ms`);
				rv.push('', 'Last message:', `${hh}:${mm}:${ss} (${delta}s ago)`);
			}
		}

		if (ongoing > 0) rv.push(`Ongoing unresolved ping for ${ongoing} ms`);

		return rv.join('<br>');
	}

	addLatencyElement(node, uid) {
		const s = document.createElement('span');
		s.classList.add('latency-time');
		s.textContent = '? ms';
		this.userHud[uid] = s;
		node.append(s);
		s.addEventListener('click', (d) => this._latencyClick(d), { passive: true });

		s.addEventListener('mouseover', async () => {
			const content = this._tooltipContent(uid);
			game.tooltip.activate(s, { text: content, direction: 'UP', cssClass: 'latency-tooltip' });
		}, { passive: true });

		s.addEventListener('mouseout', () => game.tooltip.deactivate(), { passive: true });
		return s;
	}

	update(sample, median, uid, intermediate = false) {
		let s = this.userHud[uid ?? game.user.id];
		const uEl = this.getUserHUDElement(uid);
		if (!s) {
			if (uEl) s = this.addLatencyElement(uEl, uid); // HACK
			return;
		}
		const { delta, ms: msReal } = sample;
		const ms = Math.max(msReal, median) || msReal;
		// ▼▲ ⇩⇧
		// ⇓ ⇑ ≅
		const ad = Math.abs(delta);
		const roughlySame = ad <= 25 || ad < (ms * 0.1);
		const symbol = roughlySame ? '≅' : delta > 0 ? '⇑' : '⇓';
		s.innerHTML = `${symbol} <b>${ms}</b> ms`;

		if (intermediate) {
			if (msReal > 5_000 && !s.classList.contains('dead')) s.classList.add('dead');
		}
		else {
			s.classList.remove('stale');
			s.removeAttribute('title');
		}
	}

	/**
	 * @param uid User ID
	 * @param lastMsg
	 * @returns true if newly stale
	 */
	stale(uid, lastMsg) {
		const s = this.userHud[uid];
		if (!s) return false;
		if (s.classList.contains('stale')) return false;
		s.classList.add('stale');
		s.title = 'Last message: ' + new Date(lastMsg).toTimeString();
		return true;
	}

	dead() {
		Object.entries(this.userHud).forEach(ux => {
			ux[1].classList.add('dead');
		});
	}
}
