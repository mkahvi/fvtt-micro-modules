import { TimeSample } from './sample.mjs';

export class History {
	samples;
	lastSample;
	#sorted;

	constructor(sampleSize = 10) {
		this.resize(sampleSize);
	}

	resize(sampleSize) {
		const clamp = Math.clamp ?? Math.clamped;
		sampleSize = clamp(sampleSize, 2, 100);
		if (sampleSize % 2 === 0) sampleSize++; // Ensure easy median is always there.
		this.samples = new Array(sampleSize).fill(new TimeSample(Infinity, 0, 0));
		this.lastSample = this.samples[0];
	}

	push(ms, stamp) {
		const delta = ms - this.samples[this.samples.length - 1].ms;
		const sample = new TimeSample(ms, stamp, delta);
		this.lastSample = sample;
		this.samples.push(sample);
		this.samples.shift();
		this.#sorted = null;
		return sample;
	}

	get sorted() {
		this.#sorted ??= this.samples.map(s => s.ms).filter(ms => Number.isFinite(ms)).sort((a, b) => a - b);
		return this.#sorted;
	}

	get worst() {
		return this.sorted.at(-1);
	}

	get best() {
		return this.sorted[0];
	}

	get median() {
		const sorted = this.sorted;
		return sorted[Math.floor(sorted.length / 2)];
	}

	get last() {
		return this.samples.at(-1);
	}
}
