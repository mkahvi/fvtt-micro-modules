# Change Log

## 1.0.1.2

- Foundry v12 compatibility

## 1.0.1.1

- Change: Reduced debug output.

## 1.0.1

- Refactoring

## 1.0.0.4

- Fix: Only PCs get XP tracked now.
- Fix: Better ownership testing.

## 1.0.0.3

- Fix: Actor owners did not see the XP cards.

## 1.0.0.2

- Fix: Ignore actor imports

## 1.0.0.1

- Ignore actors in compendiums.

## 1.0.0

- Initial
