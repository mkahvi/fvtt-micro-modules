const MODULE_ID = 'pf1-xp-monitor';

const CFG = /** @type {const} */ ({
	SETTINGS: {
		gracePeriod: 'gracePeriod',
		interveningMessages: 'interveningMessages',
	},
	COLOR: {
		main: 'color:cornflowerblue',
		number: 'color:mediumpurple',
		unset: 'color:unset',
	},
});

let notMyExperienceCounter = 5000;
let lastExperienceMsgId = null;

/**
 * Generate experience card contents based on delta.
 *
 * @param actor
 * @param delta
 */
async function createCardContents(actor, delta) {
	const nextLvlXp = actor.system.details.xp?.max ?? 0,
		curXp = actor.system.details.xp?.value ?? 0,
		neededXp = nextLvlXp - curXp;

	const templateData = {
		delta,
		neededXp,
	};

	return renderTemplate(`modules/${MODULE_ID}/card.hbs`, templateData);
};

async function printExpCard(actor, delta) {
	const msgData = {
		content: await createCardContents(actor, delta),
		flags: { [MODULE_ID]: { delta } },
		speaker: ChatMessage.getSpeaker({ actor }),
		user: game.user.id,
		type: CONST.CHAT_MESSAGE_TYPES.OOC,
		whisper: [],
	};

	// Collect all GMs and owning users
	const levels = CONST.DOCUMENT_OWNERSHIP_LEVELS ?? CONST.DOCUMENT_PERMISSION_LEVELS;

	const users = game.users
		.filter(user => actor.testUserPermission(user, levels.OWNER))
		.map(u => u.id);

	// Whisper to above users
	msgData.whisper = Array.from(users);

	return ChatMessage.create(msgData);
};

async function updateLastCard(actor, cm, delta) {
	if (!cm) return printExpCard(actor, delta);

	const sameActor = actor ? cm?.speaker?.actor === actor.id : false;
	if (!sameActor) { // shouldn't happen
		console.error('%cExperience%c | Last card isn\'t for us!', CFG.COLOR.main, CFG.COLOR.unset);
		return printExpCard(actor, delta);
	}

	delta += cm.getFlag(MODULE_ID, 'delta') ?? 0;

	const msgData = {
		flags: { [MODULE_ID]: { delta } },
		content: await createCardContents(actor, delta),
	};

	lastExperienceMsgId = cm.id;
	return cm.update(msgData);
};

/**
 * @param {ChatMessage} cm
 * @param {JQuery} html
 */
function renderChatMessageEvent(cm, [html]) {
	const acc = cm.getFlag(MODULE_ID, 'delta');
	if (acc === undefined)
		return void notMyExperienceCounter++;

	if (cm.isAuthor) {
		notMyExperienceCounter = 0;
		lastExperienceMsgId = cm.id;
	}
	else
		notMyExperienceCounter++;

	const wt = html.querySelector('.whisper-to');
	if (wt) wt.style.display = 'none';
	html.style.borderColor = null;
	html.classList.add(MODULE_ID);
};

/**
 * @param {Actor} actor
 * @param {object} changed
 * @param {object} context
 */
function preUpdateActorEvent(actor, changed, context) {
	// Ignore non-system updates
	if (!changed.system) return;
	// Ignore imports
	if (context.diff === false && context.recursive === false) return;
	// Ignore packs
	if (actor.pack) return;

	// Only PCs
	if (actor.type !== 'character') return;

	const newXp = changed.system?.details?.xp?.value;
	if (newXp === undefined) return;

	const oldXp = actor.system.details.xp.value ?? 0;
	const delta = (newXp ?? 0) - (oldXp ?? 0);
	if (delta == 0) return;

	context._expMonitorDelta = delta;
};

/**
 * @param {Actor} actor Actor
 * @param {object} delta  Transaction details
 */
function finalizeTransaction(actor, delta) {
	const cm = game.messages.get(lastExperienceMsgId),
		lastStamp = cm?.timestamp ?? 0,
		timeSince = Date.now() - lastStamp,
		sameActor = actor ? cm?.speaker?.actor === actor.id : false,
		grace = game.settings.get(MODULE_ID, CFG.SETTINGS.gracePeriod),
		maxDistance = game.settings.get(MODULE_ID, CFG.SETTINGS.interveningMessages);

	console.log('%cExperience%c | Transaction:', CFG.COLOR.main, CFG.COLOR.unset, delta);

	if (!sameActor || notMyExperienceCounter > maxDistance || timeSince > grace * 1_000) {
		printExpCard(actor, delta);
	}
	else {
		if (sameActor && grace > 0) updateLastCard(actor, cm, delta);
		else printExpCard(actor, delta);
	}
};

/**
 * @param {Actor} actor
 * @param _update
 * @param _context
 * @param context
 * @param {string} userId
 */
function updateActorEvent(actor, _update, context, userId) {
	// Only triggering user should handle things for simplicity
	if (game.user.id !== userId) return;
	// Ignore non-delta
	const delta = context._expMonitorDelta ?? 0;
	if (delta == 0) return;

	finalizeTransaction(actor, delta);
};

Hooks.on('renderChatMessage', renderChatMessageEvent);

Hooks.on('updateActor', updateActorEvent);
Hooks.on('preUpdateActor', preUpdateActorEvent);

Hooks.once('init', function registerSettings() {
	game.settings.register(
		MODULE_ID,
		CFG.SETTINGS.gracePeriod,
		{
			name: 'Grace period',
			hint: 'Period (in seconds) for how long recent experience message can be updated with new total. Reduces log spamming. 0 disables.',
			type: Number,
			default: 60,
			range: { min: 0, max: 300, step: 5 },
			scope: 'world',
			config: true,
		},
	);

	game.settings.register(
		MODULE_ID,
		CFG.SETTINGS.interveningMessages,
		{
			name: 'Max intervening messages',
			hint: 'New experience message is printed instead of updating regardless of the passed time if there\'s this many messages since.',
			type: Number,
			default: 3,
			range: { min: 0, max: 20, step: 1 },
			scope: 'world',
			config: true,
		},
	);
});
