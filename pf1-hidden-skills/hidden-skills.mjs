const MODULE_ID = 'mkah-pf1-hidden-skills';

const getSystemData = (doc) => game.release.generation >= 10 ? doc.system : doc.data.data;

let origSkills,
	origBgSkills,
	origASkills;

function hideSkillTargets(original) {
	const rv = original();
	const hiddenSkills = getSkills();
	return rv.filter(s => !hiddenSkills.includes(s.split('.', 2)[1]));
}

function getSkills(value) {
	if (!(value?.length > 0))
		value = game.settings.get(MODULE_ID, 'hidden');

	return value?.split(',').map(i => i.trim()).filter(i => i?.length > 0) ?? []
}

function changeSkills(value) {
	const hidden = getSkills(value);
	const badSkills = [];

	CONFIG.PF1.skills = duplicate(origSkills);
	CONFIG.PF1.arbitrarySkills = duplicate(origASkills).filter(s => !hidden.includes(s));
	CONFIG.PF1.backgroundSkills = duplicate(origBgSkills).filter(s => !hidden.includes(s));

	const trulyHidden = [];

	for (const sk of Object.keys(CONFIG.PF1.skills))
		CONFIG.PF1.skills[sk] = game.i18n.localize(CONFIG.PF1.skills[sk]);

	for (const sk of hidden) {
		if (sk in CONFIG.PF1.skills) {
			delete CONFIG.PF1.skills[sk];
			trulyHidden.push(sk);
		}
		else
			badSkills.push(sk);
	}

	if (trulyHidden.length > 0)
		console.log('HIDDEN SKILLS | Actually hidden:', trulyHidden);
	if (badSkills.length > 0)
		console.warn('HIDDEN SKILLS | Malformed skills:', badSkills);
}

Hooks.once('init', () => {
	origSkills = duplicate(CONFIG.PF1.skills);
	origASkills = duplicate(CONFIG.PF1.arbitrarySkills);
	origBgSkills = duplicate(CONFIG.PF1.backgroundSkills);

	game.settings.register(
		MODULE_ID,
		'hidden',
		{
			name: 'Hidden skill IDs',
			hint: 'List of skill IDs to hide separated by commas (e.g. "acr,per,sen")<br>Refresh is needed to unhide skills.<br>Character sheets need to be re-opened for hidden skills to update.',
			type: String,
			default: '',
			config: true,
			scope: 'world',
			onChange: changeSkills
		}
	);

	/* global libWrapper */
	libWrapper.register(MODULE_ID, 'game.pf1.entities.ActorPF.prototype._skillTargets', hideSkillTargets);

	changeSkills();
});

function renderActorSheetEvent(sheet, jq, _data) {
	const skills = getSkills();
	if (skills.length === 0) return;

	for (const sk of jq.find('li.skill,li.sub-skill')) {
		if (skills.includes(sk.dataset.skill) || skills.includes(sk.dataset.mainSkill))
			sk.remove();
	}
}

Hooks.on('renderActorSheet', renderActorSheetEvent);

/** @returns {Promise} */
function refundSkills(actor) {
	const skills = getSkills();
	let ranksFreed = 0;
	const updateData = {};
	for (const [key, value] of Object.entries(getSystemData(actor).skills)) {
		if (skills.includes(key)) {
			if (value.rank != null) {
				ranksFreed += value.rank;
				updateData[`data.skills.${key}.rank`] = null;
			}

			if (value.subSkills) {
				for (const [key2, value2] of Object.entries(value.subSkills)) {
					if (value2.rank != null) {
						ranksFreed += value2.rank;
						updateData[`data.skills.${key}.subSkills.${key2}.rank`] = null;
					}
				}
			}
		}
	}

	if (Object.keys(updateData).length > 0) {
		console.log('Freeing', ranksFreed, 'skill ranks.');
		return actor.update(updateData);
	}
	else {
		console.log('No hidden skills found.');
	}
}

Hooks.once('ready', () => {
	game.modules.get(MODULE_ID).api = { refundSkills };
});
