/**
 * @param {JQuery} html
 */
function removeDamageButtons(_, [html]) {
	// Regular buttons, e.g. from /damage 3d6
	html.querySelectorAll('[data-action="applyDamage"]')
		.forEach(el => el.closest('.chat-message[data-message-id] .card-button-group')?.remove());

	// Inline actions (0.80.15 and newer)
	html.querySelectorAll('.inline-action[data-action="applyDamage"]')
		.forEach(el => el?.remove());
}

Hooks.on('renderChatMessage', removeDamageButtons);
