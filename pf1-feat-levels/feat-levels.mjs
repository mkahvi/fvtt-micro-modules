const createLevelInput = (item) => {
	const level = item?.getFlag('world', 'level');

	const ld = document.createElement('input');
	ld.type = 'number';
	ld.step = '1';
	ld.classList.add('feat-level');
	if (level !== undefined)
		ld.value = level;
	else
		ld.placeholder = '0';

	ld.dataset.tooltip = 'PF1.Level';

	// Save value on change
	ld.addEventListener('change', ev => editItemLevel(ev, item));
	// Select value on click
	ld.addEventListener('click', ev => ev.target.select(), { passive: true });
	// Block wheeling
	ld.addEventListener('wheel', ev => ev.preventDefault());

	return ld;
};

/**
 * @param {Event} event
 * @param {Item} item
 */
const editItemLevel = (event, item) => {
	event.preventDefault();

	const value = event.target.value;
	if (value.length == 0)
		item.unsetFlag('world', 'level');
	else {
		const newLevel = parseInt(value);
		const oldLevel = item.getFlag('world', 'level');
		if (oldLevel === newLevel) return;
		item.setFlag('world', 'level', newLevel);
	}
};

/**
 * @param {ItemSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param _options
 */
const injectLevelConfig = (sheet, [html], _options) => {
	const item = sheet.item;
	if (item.type !== 'feat') return;

	const summary = html.querySelector('header .header-details ul.summary');
	if (!summary) return;

	const li = document.createElement('li');

	const ld = createLevelInput(item);

	const level = item?.getFlag('world', 'level') ?? 0;
	ld.dataset.tooltip = `@item.level : ${level}`;

	const ll = document.createElement('label');
	ll.textContent = game.i18n.localize('PF1.Level');

	li.append(ll, ld);
	summary.append(li);
};

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 * @param _options
 */
const injectLevelDisplay = (sheet, [html], _options) => {
	const actor = sheet.actor;

	const tab = html.querySelector('.tab.features[data-tab]');
	if (!tab) return; // Probably odd sheet
	tab.querySelectorAll('.item[data-item-id]').forEach(el => {
		const itemId = el.dataset.itemId;
		const item = actor.items.get(itemId);
		if (['template'].includes(item.type)) return;

		const uses = el.querySelector('.item-uses');
		if (!uses) return;
		if (uses.childElementCount > 0) return;

		const ld = createLevelInput(item);

		uses?.append(ld);
	});
};

/**
 * Inject feat level as `@item.level`
 *
 * @param {Item|Actor|ItemAction} doc
 * @param {object} result
 */
function getRollDataWithFeatLevel(doc, result) {
	if (doc instanceof Item && doc.type === 'feat') {
		const level = doc.getFlag('world', 'level');
		result.item ??= {};
		result.item.level = level;
	}
}

Hooks.on('renderActorSheet', injectLevelDisplay);
Hooks.on('renderItemSheet', injectLevelConfig);
Hooks.on('pf1GetRollData', getRollDataWithFeatLevel);
