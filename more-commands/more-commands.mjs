const MODULE_ID = 'mana-more-commands';

const randomNum = () => foundry.dice.MersenneTwister.random();

const snapToGrid = (point) => {
	// TODO: Do something else on gridless/hex
	return canvas?.grid?.getSnappedPoint(point, { mode: CONST.GRID_SNAPPING_MODES.CORNER });
};

/**
 * Shuffles array in place.
 *
 * Slow due to double randomization.
 *
 * @param {Array} arr
 * @returns {Array}
 */
function shuffle(arr) {
	const narr = [];
	while (arr.length) {
		const idx = Math.floor(randomNum() * arr.length);
		narr.push(...arr.splice(idx, 1));
	}
	while (narr.length) {
		const idx = Math.floor(randomNum() * narr.length);
		arr.push(...narr.splice(idx, 1));
	}
	return arr;
}

/**
 *
 * @param {PlaceableObject[]} tokens
 * @returns {Point}
 */
function getCentralPosition(tokens) {
	let left = Infinity, right = -Infinity, top = Infinity, bottom = -Infinity;

	for (const token of tokens) {
		const { x, y } = token.center;
		if (x < left) left = x;
		if (x > right) right = x;
		if (y < top) top = y;
		if (y > bottom) bottom = y;
	}

	const x = (right + left) / 2;
	const y = (top + bottom) / 2;

	return snapToGrid({ x, y });
}

/**
 * Get snapped center of a point.
 *
 * @param {Point} point
 * @returns {Point}
 */
function getCellCenter({ x, y } = {}) {
	const gridSizePxHalf = canvas.scene.grid.size / 2;
	return snapToGrid({ x: x + gridSizePxHalf, y: y + gridSizePxHalf });
}

/**
 * @param {Point} p0
 * @param {Point} p1
 * @returns
 */
function getDistance(p0, p1) {
	return new Ray(p0, p1).distance;
}

/**
 * Returns tokens within certain distance from origin
 *
 * @param {Point} origin
 * @param {number} distance
 * @param {Token[]} tokens
 * @param debug
 * @returns {Token[]}
 */
function getTokensWithinRange(origin, distance, tokens, debug = true) {
	const baseErrorMargin = canvas.scene.grid.size / 2;
	distance ||= baseErrorMargin;
	tokens ||= canvas.scene.tokens.map(t => t.object);
	return tokens.filter(t => {
		const td = getDistance(origin, t.center);
		const errorMargin = Math.max(t.w, t.h) / 2;
		const testDistance = (distance + errorMargin);
		return td <= testDistance;
	});
}

function testCollision(origin, target) {
	return CONFIG.Canvas.polygonBackends.move.testCollision(origin, target, { mode: 'any', type: 'move' });
}

const getTokens = () => canvas.tokens.controlled;
const getCombatants = (tokens) => (tokens ?? getTokens()).map(t => t.document?.combatant);
const getActors = (tokens) => (tokens ?? getTokens()).map(t => t.actor) ?? [game.user.character];

const gmGuard = (command) => {
	const gm = game.user.isGM;
	if (!gm) ui.notifications?.warn(`/${command} requires GM rights.`);
	return gm;
};

// COMMANDS

function hideToken(_args, _chatData) {
	if (!gmGuard('hide')) return;

	const tokens = getTokens();
	if (tokens.length === 0) ui.notifications?.error('No tokens selected to hide.');

	const scene = canvas?.scene;

	const updates = [];
	tokens.forEach(t => !t.document.hidden ? updates.push({ _id: t.id, hidden: true }) : undefined);

	if (updates.length)
		scene?.updateEmbeddedDocuments('Token', updates).then(_ => ui.notifications?.info(`${updates.length} tokens hidden.`));
	else
		ui.notifications.error('All selected tokens already hidden.');
}

function revealToken(_args, _chatData) {
	if (!gmGuard('reveal')) return;

	const tokens = getTokens();

	if (tokens.length === 0) ui.notifications?.error('No tokens selected to hide.');

	const scene = canvas?.scene;

	const updates = [];
	tokens.forEach(t => t.document.hidden ? updates.push({ _id: t.id, hidden: false }) : undefined);

	if (updates.length)
		scene?.updateEmbeddedDocuments('Token', updates).then(_ => ui.notifications?.info(`${updates.length} tokens revealed.`));
	else
		ui.notifications.error('All selected tokens already revealed.');
}

function quantumDeath(dead = true) {
	const tokens = getTokens();
	if (tokens.length === 0) return ui.notifications?.error(`No tokens selected to ${dead ? 'kill' : 'revive'}.`);

	const p = [];

	// TODO: Support multiple combats?
	const combat = game.combat;
	if (combat) {
		const updates = tokens
			.filter(t => t.combatant)
			.map(t => ({ _id: t.combatant.id, defeated: dead }));

		if (updates.length)
			p.push(combat.updateEmbeddedDocuments('Combatant', updates));
	}

	const status = CONFIG.statusEffects
		.find(ae => ae.id === CONFIG.specialStatusEffects.DEFEATED);

	// Disable death marker on tokens
	for (const token of tokens) {
		const effect = token.actor && status ? status : CONFIG.controlIcons.defeated;
		p.push(token.toggleEffect(effect, { active: dead, overlay: true }));
	}

	Promise.all(p).then(_ => ui.notifications?.info(`${tokens.length} actor(s) ${dead ? 'killed' : 'revived'}.`));
	return true;
}

function killToken(_args, _chatData) {
	if (!gmGuard('kill')) return;
	quantumDeath(true);
}

function reviveToken(_args, _chatData) {
	if (!gmGuard('revive')) return;
	quantumDeath(false);
}

function toggleCombat(args, chatData) {
	let forceState = null;
	switch (args?.toLowerCase()) {
		case 'start':
		case 'add':
		case 'enter':
		case 'on': forceState = true; break;
		case 'leave':
		case 'remove':
		case 'off': forceState = false; break;
	}

	const tokens = getTokens();
	if (tokens.length) {
		const someInCombat = tokens.some(t => t.inCombat),
			someUnCombat = tokens.some(t => !t.inCombat),
			newState = forceState ?? !!someUnCombat;
		const oldCombat = someInCombat ? tokens.find(t => t.inCombat)?.combatant.combat : null;

		const layer = tokens[0].layer;
		layer.toggleCombat(newState, oldCombat);
	}
	else
		ui.notifications?.error('No tokens selected to toggle combat with.');
}

function toggleCondition(command, chatData) {
	const actors = getActors();
	if (actors.length === 0) return ui.notifications?.error('No tokens selected to toggle condition for.');
	if (!command) return ui.notifications?.error('Missing arguments.');

	const re = /(?<toggle>[-+])?(?<condition>.*)/.exec(command);
	if (!re) return;

	const toggle = re.groups.toggle,
		state = toggle === '+' ? true : toggle === '-' ? false : undefined,
		search = re.groups.condition;

	// if (toggle === undefined) return ui.notifications?.warn('Indeterminate target state');

	// TODO: Support fuzzier matching
	const matches = [...CONFIG.statusEffects.map(s => s.id)]
		.filter(c => c.startsWith(search));

	const condition = CONFIG.statusEffects.find(s => matches.includes(s.id));

	if (matches.length > 1 && !condition) return ui.notifications?.warn('Which? ' + matches.join(', ') + '?');
	if (matches.length === 0) return ui.notifications?.warn(`Condition not recognized "${search}"`);

	const aeData = {
		name: game.i18n.localize(condition.label),
		icon: condition.icon,
		statuses: [condition.id],
	};

	const updates = [];
	for (const actor of actors) {
		if (!actor.statuses.has(condition.id)) {
			if (state === false) continue;
			updates.push(actor.createEmbeddedDocuments('ActiveEffect', [aeData]));
		}
		else {
			if (state === true) continue;
			const old = actor.appliedEffects.filter(ae => ae.statuses.has(condition.id));
			if (old.length === 0) continue;
			updates.push(actor.deleteEmbeddedDocuments('ActiveEffect', old.map(o => o.id)));
		}
	}

	Promise.allSettled(updates).then(_ => console.log(`Condition "${condition.id}" set:`, state ?? 'toggle'));
}

const dispositionMap = {
	secret: CONST.TOKEN_DISPOSITIONS.SECRET,
	friendly: CONST.TOKEN_DISPOSITIONS.FRIENDLY,
	neutral: CONST.TOKEN_DISPOSITIONS.NEUTRAL,
	hostile: CONST.TOKEN_DISPOSITIONS.HOSTILE,
};

/**
 * @param {"friendly"|"neutral"|"hostile"|"secret"} disposition
 */
function setDisposition(disposition) {
	const targetDisposition = dispositionMap[disposition];
	if (targetDisposition === undefined) throw new Error(`Invalid disposition: ${disposition}`);

	const tokens = getTokens();
	const updates = [];
	for (const token of tokens) {
		if (token.disposition !== targetDisposition) {
			updates.push({ _id: token.id, disposition: targetDisposition });
		}
	}

	if (updates.length) {
		canvas.scene.updateEmbeddedDocuments('Token', updates)
			.then(_ => ui.notifications?.info(`${tokens.length} token(s) set to ${disposition} disposition.`));
	}
}

function toggleTorch(args) {
	const tokens = getTokens();

	let targetState;
	if (['true', 'on', '1'].includes(args)) targetState = true;
	else if (['false', 'off', '0'].includes(args)) targetState = false;

	const updates = [];
	for (const token of tokens) {
		const doc = token.document;
		const _targetState = targetState ?? doc.light.bright === 0;
		if (doc.light.bright > 0 && _targetState !== true) {
			updates.push({ _id: token.id, 'light.bright': 0, 'light.dim': 0 });
		}
		else if (doc.light.bright == 0 && _targetState !== false) {
			updates.push({ _id: token.id, 'light.bright': 20, 'light.dim': 40 });
		}
	}
	if (updates.length) {
		canvas.scene.updateEmbeddedDocuments('Token', updates)
			.then(_ => ui.notifications.info(`Torch toggled for ${updates.length} tokens.`));
	}
}

function toggleSight(args) {
	const tokens = getTokens();
	let targetState;
	if (['true', 'on', '1'].includes(args)) targetState = true;
	else if (['false', 'off', '0'].includes(args)) targetState = false;
	const updates = [];
	for (const token of tokens) {
		const doc = token.document;
		const _targetState = targetState ?? !doc.sight.enabled;
		if (doc.sight.enabled !== _targetState) {
			updates.push({ _id: token.id, 'sight.enabled': _targetState });
		}
	}
	if (updates.length) {
		canvas.scene.updateEmbeddedDocuments('Token', updates)
			.then(_ => ui.notifications.info(`Toggled sight for ${updates.length} tokens.`));
	}
}

function stackTokens(args) {
	const tokens = getTokens();
	if (tokens.length == 0) return void ui.notifications.error('Please select some tokens first.');

	const fastUpdate = args ? /\bfa/gi.test(args) : false;
	const mouse = args ? /\b(?:mo|po)/gi.test(args) : false;

	let target;
	if (mouse) {
		const { x, y } = canvas.mousePosition ?? {};
		target = snapToGrid({ x, y });
	}
	else {
		let minX = Infinity, maxX = -Infinity, minY = Infinity, maxY = -Infinity;
		const gridSizePx = canvas.scene.grid.size;
		tokens.forEach(t => {
			if (t.x < minX) minX = t.x;
			const tmx = t.x + ((t.document.width - 1) * gridSizePx);
			if (tmx > maxX) maxX = tmx;
			if (t.y < minY) minY = t.y;
			const tmy = t.y + ((t.document.height - 1) * gridSizePx);
			if (tmy > maxY) maxY = tmy;
		});

		// All tokens already at same location
		if (tokens.every(t => tokens[0].x === t.x && tokens[0].y === t.y)) return;

		const midX = minX + (maxX - minX) / 2,
			midY = minY + (maxY - minY) / 2;

		target = snapToGrid({ x: midX - 1, y: midY - 1 });
	}

	const updates = tokens.map(t => ({ _id: t.id, x: target.x, y: target.y }));

	const context = {};
	if (fastUpdate) context.animate = false;

	canvas.scene.updateEmbeddedDocuments('Token', updates, context);
}

function unstackTokens(args) {
	const tokens = getTokens();
	if (tokens.length == 0) return void ui.notifications.error('Please select some tokens first.');

	const fastUpdate = args ? /\bfa/gi.test(args) : false;
	const checkWalls = args ? !/\bnowa/gi.test(args) : true;

	const gridSize = canvas.scene.grid.distance;
	const gridSizePx = canvas.scene.grid.size;

	// Find starting position
	const origin = getCentralPosition(tokens);

	// Establish center
	const td = new MeasuredTemplateDocument({ _id: foundry.utils.randomID(), t: 'circle', x: origin.x, y: origin.y, distance: 0.0001 }, { parent: canvas.scene });
	const t = new MeasuredTemplate(td);
	t._applyRenderFlags({ refreshShape: true }); // fill in t.shape

	function assessTokenSpace(t) {
		const wr = t.document.width,
			hr = t.document.height;

		const squares = [];

		for (let x0 = 0; x0 <= (wr - 1); x0++) {
			for (let y0 = 0; y0 <= (hr - 1); y0++) {
				squares.push({ x: t.x + x0 * gridSizePx, y: t.y + y0 * gridSizePx });
			}
		}

		return squares;
	}

	const usedSquares = new Set();
	const interferingTokens = new Set(getTokensWithinRange(origin, gridSizePx * 15, canvas.scene.tokens.map(t => t.object)));
	// Tokens to be moved do not count as interference
	for (const t of tokens) interferingTokens.delete(t);

	// Consume squares for all interfering tokens
	for (const t of interferingTokens) {
		assessTokenSpace(t)
			.map(({ x, y }) => `${x},${y}`)
			.forEach(uid => usedSquares.add(uid));
	}

	/**
	 * @param {Point} pt0 - Square center
	 * @returns {boolean}
	 */
	function testSquare(pt0) {
		const uid = `${pt0.x},${pt0.y}`;
		if (usedSquares.has(uid)) return false;
		if (checkWalls && testCollision(getCellCenter(origin), getCellCenter(pt0))) {
			usedSquares.add(uid);
			return false;
		}

		return true;
	}

	function findSpace({ x, y } = {}, wr, hr) {
		const minX = x - ((wr - 1) * gridSizePx),
			maxX = x + ((wr - 1) * gridSizePx),
			minY = y - ((hr - 1) * gridSizePx),
			maxY = y + ((hr - 1) * gridSizePx);

		// Generate grid and check which squares are available
		const cells = new Array(wr * 2 - 1).fill(null);
		cells.forEach((_, i) => cells[i] = new Array(hr * 2 - 1).fill(null));

		let xo = 0;
		for (let x0 = minX; x0 <= maxX; x0 += gridSizePx) {
			let yo = 0;
			for (let y0 = minY; y0 <= maxY; y0 += gridSizePx) {
				cells[xo][yo] = {};
				const cell = cells[xo][yo];
				cell.x = x0;
				cell.y = y0;
				cell.available = testSquare(cell);
				yo += 1;
			}
			xo += 1;
		}

		// Test if token fits.
		function testCell(x0, y0) {
			const squares = [];
			for (let ix = 0; ix < wr; ix++) {
				for (let iy = 0; iy < hr; iy++) {
					const cell1 = cells[x0 + ix][y0 + iy];
					if (!cell1.available) return null;
					squares.push({ x: cell1.x, y: cell1.y });
				}
			}
			return squares;
		}

		const possible = new Set();

		for (let x0 = 0; x0 < (cells.length - (wr - 1)); x0++) {
			for (let y0 = 0; y0 < (cells[x0].length - (hr - 1)); y0++) {
				//
				const cell = cells[x0][y0];
				if (!cell.available) continue;

				const ssquares = testCell(x0, y0);
				if (ssquares?.length) {
					cell.squares = ssquares;
					possible.add(cell);
				}
			}
		}

		return possible;
	}

	const updates = [];

	let increment = 0;
	while (tokens.length) {
		t._applyRenderFlags({ refreshShape: true }); // Required to get size adjusts to update squares
		const squares = t._getGridHighlightPositions();

		const available = [];
		for (const square of squares) {
			if (testSquare(square))
				available.push(square);
		}

		if (available.length > 0) {
			shuffle(available);

			while (available.length) {
				if (tokens.length === 0) break;
				const dest = available.shift();
				const destc = getCellCenter(dest);

				for (let i0 = 0; i0 < tokens.length; i0++) {
					const mt = tokens.shift();
					// TODO: Handle large tokens
					const isLarge = Math.max(mt.w, mt.h) > gridSizePx;
					if (isLarge) {
						const sets = Array.from(findSpace(dest, mt.document.width, mt.document.height));
						if (sets.length === 0) {
							// No valid space, push token back
							// console.log('Couldn\'t fit', mt.name);
							tokens.push(mt);
							continue;
						}

						let sdest = sets[0];
						if (sets.length > 1) {
							sets.forEach(set => {
								set.center = {
									x: set.x + mt.w / 2,
									y: set.y + mt.h / 2,
								};
								set.distance = getDistance(destc, set.center);
							});

							// Sort by distance
							sets.sort((a, b) => a.distance - b.distance);
							// Randomly choose from all available positions
							// const first = sets[0];
							// sets = sets.filter(s => s.distance === first.distance);
							const offset = Math.floor(randomNum() * sets.length);
							sdest = sets.at(offset);
						}

						for (const cell of sdest.squares)
							usedSquares.add(`${cell.x},${cell.y}`);
						updates.push({ _id: mt.document.id, x: sdest.x, y: sdest.y });

						break;
					}
					else {
						const uid = `${dest.x},${dest.y}`;
						if (!usedSquares.has(uid)) {
							updates.push({ _id: mt.document.id, x: dest.x, y: dest.y });
							usedSquares.add(uid);
						}
						else {
							tokens.push(mt);
						}
						break;
					}
				}
			}
		}

		if (tokens.length == 0) break;

		increment += 1;
		if (increment > 15)
			return void ui.notifications.error('Could not find enough space!!!');

		td.updateSource({ distance: increment * gridSize });
	}

	const context = {};
	if (fastUpdate) context.animate = false;

	canvas.scene.updateEmbeddedDocuments('Token', updates, context);
}

// MAPPING

const COMMANDS = {
	hide: hideToken,
	reveal: revealToken,
	kill: killToken,
	revive: reviveToken,
	combat: toggleCombat,
	condition: toggleCondition,
	cond: toggleCondition,
	neutral: setDisposition.bind(null, 'neutral'),
	hostile: setDisposition.bind(null, 'hostile'),
	enemy: setDisposition.bind(null, 'hostile'),
	friendly: setDisposition.bind(null, 'friendly'),
	ally: setDisposition.bind(null, 'friendly'),
	secret: setDisposition.bind(null, 'secret'),
	torch: toggleTorch,
	sight: toggleSight,
	vision: toggleSight, // alias
	// toggle: toggleFeature,
	stack: stackTokens,
	unstack: unstackTokens,
};

// HOOK IT IN

/**
 * @param {ChatLog} log
 * @param {string} data
 * @param chatData
 */
function moreCommands(log, data, chatData) {
	if (data[0] !== '/') return;

	const re = /^\/(?<exec>\w+)\b\s*(?<args>.*)?$/.exec(data);
	// console.log(data, re);
	if (!re) return;

	const command = re.groups?.exec,
		args = re.groups?.args;

	const cfn = COMMANDS[command];
	if (!cfn) return;

	try {
		cfn(args?.trim(), chatData);
		return false;
	}
	catch (error) {
		console.error({ command, args }, error);
	}
}

Hooks.on('chatMessage', moreCommands);

// Register API
Hooks.once('ready', () => game.modules.get(MODULE_ID).api = { commands: COMMANDS });
