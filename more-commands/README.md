# More Chat Commands

![Supported Foundry Versions](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fversion%3Fstyle%3Dflat%26url%3Dhttps%3A%2F%2Fgitlab.com%2Fmkahvi%2Ffvtt-micro-modules%2F-%2Fraw%2Fmaster%2Fmore-commands%2Fmodule.json)

Adds number of additional chat commands.

This is offshoot from the [PF1 More Commands](../pf1-more-commands/) module.

✅ It's decent.

## Commands

| Command | Arguments | Description | Example |
|:--- |:--- |:--- |:--- |
| `/cond` | [-+?][condition ID] | Sets condition state. Condition name does not need to be full, only unique. If there's no - or + preceding the condition, it is toggled. | `/cond +daze`<br>`/cond -sl`<br>`/cond sta` |
| `/friendly` | n/a | Set disposition. | |
| `/ally` | n/a | Set disposition. | |
| `/neutral` | n/a | Set disposition. | |
| `/enemy` | n/a | Set disposition. | |
| `/hostile` | n/a | Set disposition. | |
| `/secret` | n/a | Set disposition. | |
| `/hide` | n/a | Hides selected tokens. | `/hide` |
| `/reveal` | n/a | Reveals selected tokens | `/reveal` |
| `/stack` | `fast`, `pointer` | Stack tokens to singular location. `fast` disabled animation. ||
| `/unstack` | `fast`, `nowalls` | Unstack tokens. `nowalls` ignores walls. ||
| `/combat` | on/off | Toggle combat state of selected tokens. | `/combat on` |
| `/kill` | n/a | Marks selected tokens as defeated. | `/kill` |
| `/revive` | n/a | Removes defeated marks on selected tokens. | `/revive` |
| `/torch` | [on/off] | Adds basic torch light source to selected tokens. Toggles by default, optional argument forces it one way or another. | `/torch` |
| `/sight` | [on/off] | Toggles token vision. | `/sight off` |
| `/vision` | [on/off] | Alias for `/sight`. | `/vision` |

Most commands fall back to using configured character if no token(s) are selected.

## Install

Manifest URL: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/master/more-commands/module.json>

Foundry v11 and older needs manual install: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/00d871a6a38d5bb8717980074a23a221dab53e83/more-commands/more-commands.zip>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
