# Change Log

## 0.2.0.2

- Fix: Incorrect use of PF1 code when system agnostic code is needed.

## 0.2.0.1

- Fix: Removed a deprecation warning

## 0.2.0

- Fixed: `/unstack` no longer worked.
- Change: `/stack` now stacks in middle of the selected mass of tokens instead of picking arbitrary token among selected.
- Foundry v12 compatibility. Support for older removed.

## 0.1.0.2

- Fix: Condition toggling did not function correctly.

## 0.1.0 Initial
