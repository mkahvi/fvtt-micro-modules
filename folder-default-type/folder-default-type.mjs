const MODULE_ID = 'folder-default-type',
	FLAG_ID = 'default';

/**
 * @param {FolderConfig} app
 * @param {JQuery} html
 * @param {Object} options
 */
const injectDefaultTypeSelector = (app, [html], options) => {
	const folder = app.document;
	const docType = folder.type;
	const types = game.template[docType]?.types ?? [];
	if (types.length == 0) return;
	const defaultType = folder.getFlag(MODULE_ID, FLAG_ID);

	const d = document.createElement('div');
	d.classList.add('form-group');
	const label = document.createElement('label');
	label.textContent = game.i18n.localize('FolderDefaultType');
	const select = document.createElement('select');
	select.name = `flags.${MODULE_ID}.${FLAG_ID}`;
	types.forEach(type => {
		const choice = document.createElement('option');
		choice.value = type;
		choice.textContent = game.i18n.localize(CONFIG[docType]?.typeLabels[type]) || type;
		if (type === defaultType) choice.selected = true;
		select.append(choice);
	});
	d.append(label, select);

	html.querySelector('button[type=submit]')?.before(d);

	// Fix dialog size
	if (html.classList.contains('window-app'))
		html.style.height = 'auto';
}

/**
 * @param {Dialog} app
 * @param {JQuery} html
 * @param {Object} options
 */
const selectDefaultType = (app, [html], options) => {
	// No better identifier than this
	const isCreateDialog = !!html.querySelector('form#document-create');
	if (!isCreateDialog) return;

	const typeSelect = html.querySelector('[name=type]');
	const folderSelect = html.querySelector('[name=folder]');
	if (!typeSelect || !folderSelect) return; // sanity check

	const changeType = () => {
		const folderId = folderSelect.value;
		const folder = game.folders?.get(folderId);
		const defaultType = folder?.getFlag(MODULE_ID, FLAG_ID);
		if (defaultType) typeSelect.value = defaultType;
	}

	// React to folder changes
	folderSelect.addEventListener('change', changeType);

	changeType(); // Set initial type
}

Hooks.on('renderFolderConfig', injectDefaultTypeSelector);

Hooks.on('renderDialog', selectDefaultType);
