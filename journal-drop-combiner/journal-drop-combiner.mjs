const MODULE_ID = 'journal-drop-combiner';

/**
 * @param {Function} wrapped
 * @param {Event} event
 */
async function handleJournalDrop(wrapped, event) {
	const { type, uuid } = TextEditor.getDragEventData(event);
	if (type == 'JournalEntry') {
		const journal = await fromUuid(uuid);
		const pages = journal.pages.map(p => p.toObject());
		return this.document.createEmbeddedDocuments('JournalEntryPage', pages, { keepId: true });
	}

	wrapped.call(this, event);
}

Hooks.once('init', () => {
	/* global libWrapper  */
	libWrapper.register(MODULE_ID, 'JournalSheet.prototype._onDrop', handleJournalDrop, libWrapper.MIXED);
});
