import { CFG } from './module/common.mjs';
import { PartyExpDialog } from './module/party-exp-dialog.mjs';
import { PartyConfig } from './module/party-config.mjs';
import { CRExpCalculator } from './module/cr-exp-calculator.mjs';

/**
 * Register settings
 */
function registerSettings() {
	game.settings.register(
		CFG.module,
		CFG.SETTINGS.party,
		{
			type: Object,
			default: PartyConfig.default,
			scope: 'world',
			config: false,
			onChange: () => PartyExpDialog.rerender(null, null)
		}
	);

	// Transparency
	game.settings.register(
		CFG.module,
		CFG.SETTINGS.party,
		{
			// GM only? Full disclosure? Partial?
		}
	);
}

function addPartyExpButton(dir, jq, _data) {
	if (!game.user.isGM) return;

	const button = document.createElement('button');
	const buttonIcon = document.createElement('i');
	buttonIcon.classList.add('fas', 'fa-graduation-cap');
	button.append(
		buttonIcon,
		document.createTextNode('Party Experience')
	);
	button.id = 'mkah-party-experience-button';

	jq.find('footer.action-buttons').append(button);
	button.addEventListener('click', PartyExpDialog.open);
}

function updateActorEvent(actor, data, _options, _userId) {
	const xpC = data.data?.details?.xp?.value;
	const lvC = data.data?.details?.level?.value;
	if (xpC !== undefined || lvC !== undefined) PartyExpDialog.rerender(actor.id);
}

Hooks.once('init', registerSettings);
Hooks.on('renderActorDirectory', addPartyExpButton);
Hooks.once('ready', () => {
	game.modules.get(CFG.module).api = {
		partyXpDialog: PartyExpDialog.open,
		crCalculator: CRExpCalculator.open,
	};

	// PartyExpDialog.open();
	// CRExpCalculator.open();

	if (!game.user.isGM) return;
	Hooks.on('updateActor', updateActorEvent);
});
