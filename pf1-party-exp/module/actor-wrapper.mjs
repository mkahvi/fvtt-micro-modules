import { maxPrecision } from './common.mjs';
import { getSystemData } from './compat.mjs';

export class ActorWrapper {
	actor;

	constructor(actor) {
		this.actor = actor;
	}

	get id() {
		return this.actor.id;
	}

	get name() {
		return this.actor.name;
	}

	get img() {
		return this.actor.img;
	}

	get level() {
		return getSystemData(this.actor)?.details.level.value ?? -1;
	}

	get owner() {
		const users = Object.keys(game.release.generation >= 10 ? this.actor.ownership : this.actor.data.permission)
			.filter(uid => uid !== 'default')
			.map(uid => game.users.get(uid))
			.filter(user => !user.isGM)
			.filter(user => user ? this.actor.testUserPermission(user, 'OWNER') : false);
		return users[0];
	}

	get exp() {
		return getSystemData(this.actor)?.details.xp.value ?? -1;
	}

	get pct() {
		return maxPrecision(this.progress / this.delta * 100, 2);
	}

	get progress() {
		return this.exp - this.leveling.prior;
	}

	get delta() {
		const l = this.leveling;
		return l.next - l.prior;
	}

	get leveling() {
		// Experience bar
		const prior = this.actor.getLevelExp(this.level - 1 || 0),
			next = this.actor.getLevelExp(this.level || 1);

		return { prior, next };
	}

	get missing() {
		return this.leveling.next - this.exp;
	}

	get ready() {
		return this.missing <= 0;
	}
}
