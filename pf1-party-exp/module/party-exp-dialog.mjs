import { CFG, getPartyCfg, setPartyCfg } from './common.mjs';
import { ActorWrapper } from './actor-wrapper.mjs';
import { CRExpCalculator } from './cr-exp-calculator.mjs';

export class PartyExpDialog extends FormApplication {
	actors = new Set();
	partyData = [];
	config;
	lastAddXP = 0;

	// Debounced rerender function
	_rerender;

	constructor() {
		super();

		this._rerender = foundry.utils.debounce(() => this.render(true), 250);

		this.config = getPartyCfg();
		for (const actorId of this.config.members)
			this._addActorData(actorId);
	}

	get template() {
		return `modules/${CFG.module}/template/party-exp.hbs`;
	}

	getData() {
		const data = super.getData();

		data.editable = game.user.isGM;

		data.members = this.partyData;

		if (data.members.length) {
			const highest = new ActorWrapper();
			for (const m of data.members) {
				if (m.exp > highest.exp)
					highest.actor = m.actor;
				else if (m.exp == highest.exp) {
					// Same XP but higher level
					if (m.level > highest.level)
						highest.actor = m.actor;
				}
			}
			data.highest = highest;
			data.ready = highest.exp >= highest.leveling.next;
			data.lastAddXP = this.lastAddXP;

			for (const m of data.members)
				m.xpMismatch = m.exp !== highest.exp;
		}
		else {
			data.lastAddXP = 0;
			data.ready = false;
		}

		return data;
	}

	static get defaultOptions() {
		return foundry.utils.mergeObject(super.defaultOptions,
			{
				id: 'mkah-party-experience-dialog',
				title: 'Party Experience',
				dragDrop: [{ dragSelector: '.actor-list[data-actor-id]', dropSelector: null }],
				width: 600,
				top: 160,
				height: 'auto',
				resizable: true,
				submitOnClose: false,
				closeOnSubmit: false,
			},
		);
	}

	static open() {
		console.log('Opening Party XP Dialog');
		const app = Object.values(ui.windows).find(w => w instanceof PartyExpDialog);
		if (app) {
			app.bringToTop();
			return app;
		}
		else {
			const napp = new PartyExpDialog();
			napp.render(true);
			return napp;
		}
	}

	static rerender(app, actorId) {
		if (!app) app = Object.values(ui.windows).find(w => w instanceof PartyExpDialog);
		if (app) {
			const m = actorId !== null ? app.partyData?.find(d => d.id === actorId) : true;
			if (m) app._rerender();
		}
	}

	async _deleteActor(actorId) {
		this.actors.delete(actorId);
		this.partyData = this.partyData.filter(d => d.id !== actorId);

		const party = getPartyCfg();
		party.members = party.members.filter(uid => uid !== actorId);
		await setPartyCfg(party);
		PartyExpDialog.rerender(this, null);
	}

	_addActorData(actorId) {
		const actor = game.actors.get(actorId);
		if (!actor) return;
		this.actors.add(actorId);
		this.partyData.push(new ActorWrapper(actor));
		return actor;
	}

	async _addActor(actorId) {
		console.log('_addActor:', { actorId });
		if (this.actors.has(actorId)) return;
		this._addActorData(actorId);
		const party = getPartyCfg();
		party.members.push(actorId);
		await setPartyCfg(party);
		PartyExpDialog.rerender(this, actorId);
	}

	async _onDrop(event) {
		let data;
		try {
			data = JSON.parse(event.dataTransfer.getData('text/plain'));
		}
		catch (err) {
			console.error(err);
			return false;
		}

		if (data.type === 'Actor')
			return this._addActor(data.id);
	}

	async createChatMessage(text) {
		return ChatMessage.create({
			content: text,
			rollMode: CONST.DICE_ROLL_MODES.PRIVATE,
			whisper: ChatMessage.getWhisperRecipients('GM'),
		});
	}

	async addXP(xp) {
		if (xp == 0) return;
		console.log('Adding XP:', xp);
		this.lastAddXP = xp;
		const updates = [];
		for (const m of this.partyData) {
			updates.push({
				'data.details.xp.value': m.exp + xp,
				_id: m.actor.id,
			});
		}
		if (updates.length) {
			await Actor.updateDocuments(updates);
			this.createChatMessage(`Add XP: ${xp}`);
		}
	}

	async setXP(xp) {
		if (xp < 0) return;
		console.log('Setting XP:', xp);
		const updates = [];
		for (const m of this.partyData) {
			if (m.exp == xp) continue;
			updates.push({
				'data.details.xp.value': xp,
				_id: m.actor.id,
			});
		}
		if (updates.length) {
			await Actor.updateDocuments(updates);
			this.createChatMessage(`Set XP: ${xp}`);
		}
	}

	async _onButton(ev) {
		ev.preventDefault();
		ev.stopPropagation();
		const el = ev.target;
		el.disabled = true;
		const form = el.closest('form');
		const formData = new FormDataExtended(form).toObject();
		const action = el.dataset.action;

		switch (action) {
			case 'add-xp': {
				const xp = formData['add-xp'];
				console.log('Add XP:', xp);

				await this.addXP(xp);
				break;
			}
			case 'set-xp': {
				const xp = formData['set-xp'];
				const result = await Dialog.confirm({
					title: 'Are you sure you want to set XP?',
					content: `Setting XP of all to ${xp}`,
					defaultYes: true,
				});
				if (result) {
					console.log('Set XP:', xp);
					await this.setXP(xp);
				}
				break;
			}
			case 'calculate-cr': {
				new CRExpCalculator().render(true);
				break;
			}
			default: {
				console.log('Unrecognized button:', action);
				break;
			}
		}
		el.disabled = false;
		PartyExpDialog.rerender(this, null);
	}

	_openSheet(ev) {
		ev.preventDefault();
		let el = ev.target;
		if (!el.dataset.actorId) el = el.closest('[data-actor-id]');
		const aid = el.dataset.actorId;
		console.log('Opening sheet:', aid);
		game.actors.get(aid)?.sheet.render(true);
	}

	_onAction(ev) {
		console.log('ACTION!');
		const action = ev.target.dataset.action;
		switch (action) {
			case 'delete': {
				const elp = ev.target.closest('[data-actor-id]');
				const aid = elp.dataset.actorId;
				if (aid) this._deleteActor(aid);
				break;
			}
			default:
				console.warn('Unreconized action!', action);
				break;
		}
	}

	activateListeners(jq) {
		super.activateListeners(jq);

		jq.find('action').on('click', this._onAction.bind(this));
		jq.find('button').on('click', this._onButton.bind(this));
		jq.find('[data-actor-id]').on('contextmenu', this._openSheet.bind(this));
	}
}
