// Reference: https://aonprd.com/Rules.aspx?Name=Building%20an%20Adventure&Category=Basics%20from%20the%20Core%20Rulebook

const CROffsetToLabelMap = {
	0: '1/2',
	'-1': '1/3',
	'-2': '1/4',
	'-3': '1/6',
	'-4': '1/8',
};

const CRXPMap = {
	'-4': { total: 50, abstract: { small: 15, normal: 15, huge: 10 } }, // 1/8
	'-3': { total: 65, abstract: { small: 20, normal: 15, huge: 10 } }, // 1/6
	'-2': { total: 100, abstract: { small: 35, normal: 25, huge: 15 } }, // 1/4
	'-1': { total: 135, abstract: { small: 45, normal: 35, huge: 25 } }, // 1/3
	0: { total: 200, abstract: { small: 65, normal: 50, huge: 35 } }, // 1/2

	1: { total: 400, abstract: { small: 135, normal: 100, huge: 65 } },
	2: { total: 600, abstract: { small: 200, normal: 150, huge: 100 } },
	3: { total: 800, abstract: { small: 265, normal: 200, huge: 135 } },
	4: { total: 1_200, abstract: { small: 400, normal: 300, huge: 200 } },
	5: { total: 1_600, abstract: { small: 535, normal: 400, huge: 265 } },
	6: { total: 2_400, abstract: { small: 800, normal: 600, huge: 400 } },
	7: { total: 3_200, abstract: { small: 1_070, normal: 800, huge: 535 } },
	8: { total: 4_800, abstract: { small: 1_600, normal: 1_200, huge: 800 } },
	9: { total: 6_400, abstract: { small: 2_130, normal: 1_600, huge: 1_070 } },
	10: { total: 9_600, abstract: { small: 3_200, normal: 2_400, huge: 1_600 } },
	11: { total: 12_800, abstract: { small: 4_270, normal: 3_200, huge: 2_130 } },
	12: { total: 19_200, abstract: { small: 6_400, normal: 4_800, huge: 3_200 } },
	13: { total: 25_600, abstract: { small: 8_530, normal: 6_400, huge: 4_270 } },
	14: { total: 38_400, abstract: { small: 12_800, normal: 9_600, huge: 6_400 } },
	15: { total: 51_200, abstract: { small: 17_100, normal: 12_800, huge: 8_530 } },
	16: { total: 76_800, abstract: { small: 25_600, normal: 19_200, huge: 12_800 } },
	17: { total: 102_400, abstract: { small: 34_100, normal: 25_600, huge: 17_100 } },
	18: { total: 153_600, abstract: { small: 51_200, normal: 38_400, huge: 25_600 } },
	19: { total: 204_800, abstract: { small: 68_300, normal: 51_200, huge: 34_100 } },
	20: { total: 307_200, abstract: { small: 102_000, normal: 76_800, huge: 51_200 } },
	21: { total: 409_600, abstract: { small: 137_000, normal: 102_400, huge: 68_300 } },
	22: { total: 614_400, abstract: { small: 205_000, normal: 153_600, huge: 102_400 } },
	23: { total: 819_200, abstract: { small: 273_000, normal: 204_800, huge: 137_000 } },
	24: { total: 1_228_800, abstract: { small: 410_000, normal: 307_200, huge: 204_800 } },
	25: { total: 1_638_400, abstract: { small: 546_000, normal: 409_600, huge: 273_000 } },
}

export const CRMap = () => {
	return Object.keys(CRXPMap).map(k => Number(k)).sort((a, b) => a - b).reduce((rv, k) => {
		rv.push({ value: k, label: String(CROffsetToLabelMap[`${k}`] ?? k) });
		return rv;
	}, []);
};

export class CRXP {
	/**
	 * @param {Object} options
	 * @param {Number} options.pc PC class levels
	 * @param {Number} options.npc NPC class levels
	 * @param {Number} options.hd Racial hit dice
	 * @returns {Number} CR offset
	 */
	static getCR({ base = 0, pc = 0, npc = 0, hd = 0, gear = 0, env = 0, adjust = 0, key = true } = {}) {
		console.log(arguments);
		let cr = base,
			halfPC = 0,
			fullPC = 0;

		// Flat adjusts to CR
		const adjTotal = adjust + gear + env;

		// PC levels
		if (base < -50 && hd == 0 && pc > 0 && npc == 0) {
			// PC only
			cr += pc - 1;
			fullPC = pc - 1;
		}
		else {
			// Mixed
			fullPC = key ? pc : Math.max(0, pc - Math.max(0, cr));
			halfPC = pc - fullPC;
			cr += fullPC + halfPC / 2;
		}

		// NPC levels
		if (pc == 0 && hd == 0 && npc > 0) {
			// NPC only
			cr += npc - 2;
		}
		else {
			// Mixed
			cr += npc / 2;
		}

		// TODO: Racial HD??

		cr += adjTotal;

		if (base == -100) cr -= base; // adjust fake CR

		console.log('DEBUG:', { cr, hd, pc: { total: pc, full: fullPC, half: halfPC, key }, npc, adjust: { gear, env, other: adjust } });

		return cr;
	}

	/**
	 *
	 * @param {Number} crOffset
	 * @returns {Object} Object containing experience information
	 */
	static convertCRtoXP(crOffset) {
		return duplicate(CRXPMap[crOffset] ?? { total: 0 });
	}

	/**
	 *
	 * @param {Number} crOffset
	 * @returns {String}
	 */
	static getCRLabel(crOffset) {
		return String(CROffsetToLabelMap[crOffset] ?? crOffset);
	}
}
