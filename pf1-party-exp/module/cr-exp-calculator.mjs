import { CFG } from './common.mjs';
import { CRXP, CRMap as CRArray } from './cr-data.mjs';

export class CRExpCalculator extends FormApplication {
	_base = -100;
	_pc = 0;
	_key = true;
	_npc = 0;
	_hd = 0;
	_gear = 0;
	_env = 0;
	_other = 0;
	_finalCR = 0;
	_xp = 0;
	_abstractXP = false;

	constructor(data) {
		super();
	}

	get template() {
		return `modules/${CFG.module}/template/cr-calculator.hbs`;
	}

	static get defaultOptions() {
		return foundry.utils.mergeObject(super.defaultOptions,
			{
				id: 'mkah-party-exp-cr-calculator',
				title: 'CR Experience Calculator',
				width: 230,
				top: 120,
				left: 220,
				height: 'auto',
				resizable: true,
				submitOnClose: false,
				closeOnSubmit: false,
				submitOnChange: true,
			},
		);
	}

	getData() {
		const data = super.getData();

		data.crArray = CRArray();
		data.crMap = data.crArray.reduce((m, c) => {
			m[`${c.value}`] = c.label;
			return m;
		}, {});

		data.piecemeal = {
			base: this._base,
			pc: this._pc,
			key: this._key,
			npc: this._npc,
			hd: this._hd,
			gear: this._gear,
			env: this._env,
			other: this._other,
		};

		data.finalCR = this._finalCR;
		data.xp = this._xp;
		data.abstractXP = this._abstractXP;

		// console.log(duplicate(data));
		return data;
	}

	_updateObject(event, data) {
		// console.log('_updateObject:', data);

		this._base = data['base-cr'] ?? this._base;
		this._hd = data['racial'] ?? this._hd;
		this._pc = data['pc-class'] ?? this._pc;
		this._key = data['key-class'] ?? this._key;
		this._npc = data['npc-class'] ?? this._npc;
		this._gear = data['gear'] ?? this._gear;
		this._env = data['env'] ?? this._env;
		this._other = data['other'] ?? this._other;
		this._abstractXP = data['abstract-xp'] ?? this._abstractXP;

		const cr = CRXP.getCR({
			base: this._base,
			pc: this._pc,
			npc: this._npc,
			hd: this._hd,
			gear: this._gear,
			env: this._env,
			adjust: this._other,
			key: this._key,
		});

		this._finalCR = cr;

		const xp = CRXP.convertCRtoXP(cr);

		this._xp = xp.total;

		this.render(true);
	}

	activateListeners(jq) {
		super.activateListeners(jq);
	}

	static open(data) {
		console.log('Opening CR calculator');
		let app = Object.values(ui.windows).find(w => w instanceof CRExpCalculator);
		app ??= new CRExpCalculator(data);
		app.render(true, { focus: true });
		return app;
	}
}
