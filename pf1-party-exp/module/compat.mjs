export const getSystemData = (doc) => game.release.generation >= 10 ? doc?.system : doc?.data.data;
