import { PartyConfig } from './party-config.mjs';

/**
 * Configuration
 */
export const CFG = {
	module: 'mkah-pf1-party-exp',
	SETTINGS: { party: 'party' },
};

export const maxPrecision = function (num, decimalPlaces = 0, type = 'round') {
	const p = Math.pow(10, decimalPlaces || 0),
		n = num * p * (1 + Number.EPSILON);
	return Math[type](n) / p;
};

export function getPartyCfg() {
	const _default = PartyConfig.default;
	const _settings = game.settings.get(CFG.module, CFG.SETTINGS.party);
	return foundry.utils.mergeObject(_default, _settings);
}

export function setPartyCfg(data) {
	return game.settings.set(CFG.module, CFG.SETTINGS.party, data);
}
