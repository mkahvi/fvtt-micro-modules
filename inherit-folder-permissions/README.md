# Inherit Folder Permissions

![Supported Foundry Versions](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fversion%3Fstyle%3Dflat%26url%3Dhttps%3A%2F%2Fgitlab.com%2Fmkahvi%2Ffvtt-micro-modules%2F-%2Fraw%2Fmaster%2Finherit-folder-permissions%2Fmodule.json)

Allows setting default permissions for items inside a folder.

Configure by editing a folder.

☐ It's okay.

## Limitations

Sub-folders are unaffected.

## Install

Manifest URL: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/master/inherit-folder-permissions/module.json>

Foundry v9 needs manual install via: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/74876456a543fc8fc800ac8b02347f94d5ef5fec/inherit-folder-permissions/inherit-folder-permissions.zip?inline=false>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
