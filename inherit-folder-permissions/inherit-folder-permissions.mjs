const CFG = {
	module: 'inherit-folder-permissions',
	folderTemplate: null,
	COLORS: {
		main: 'color:mediumaquamarine;',
		label: 'color:mediumseagreen;',
		number: 'color:mediumpurple;',
		id: 'color:darkseagreen;',
		unset: 'color:unset;'
	}
};

Hooks.once('setup', () => getTemplate(`modules/${CFG.module}/folder-config.hbs`).then(t => CFG.folderTemplate = t));

/**
 * @param {String} node
 * @param {String} text
 * @param {String[]} classes
 * @param {Object} options
 * @returns {Element}
 */
export function createNode(node = 'span', text = null, classes = []) {
	const n = document.createElement(node);
	if (text) n.textContent = text;
	if (classes.length) n.classList.add(...classes.filter(c => !!c));
	return n;
}

/**
 * Configuration dialog for per user permissions.
 */
class InheritanceControl extends DocumentSheet {
	get template() {
		return `modules/${CFG.module}/inheritance-control.hbs`;
	}

	static get defaultOptions() {
		const _default = super.defaultOptions;
		return {
			..._default,
			classes: [..._default.classes, CFG.module, 'inheritance-control']
		}
	}

	getData() {
		const _default = super.getData();

		const enabled = this.document.getFlag(CFG.module, 'inherit');
		const permissions = this.document.getFlag(CFG.module, 'permissions') ?? {};

		const users = Object.entries(permissions).reduce((users, [uid, data]) => {
			users[uid] = { id: uid, user: game.users.get(uid), level: data.level ?? -1 };
			return users;
		}, {});

		const defaultUser = users.default ?? { level: -1 };
		game.users.forEach(usr => {
			if (usr.isGM)
				delete users[usr.id];
			else if (users[usr.id] == undefined)
				users[usr.id] = { user: usr, level: -1 };
		});
		delete users.default;
		return {
			..._default,
			enabled,
			users,
			count: Object.keys(users).length,
			permissions: {
				'-1': 'Unconfigured',
				0: 'None',
				1: 'Limited',
				2: 'Observer',
				3: 'Owner'
			},
			defaultUser,
		}
	}

	/**
	 * @param {JQuery} jq
	 */
	activateListeners(jq) {
		super.activateListeners(jq);
		const html = jq[0];

		html.querySelector('button[type="reset"]')?.addEventListener('click', ev => {
			ev.preventDefault();
			ev.stopPropagation();

			this.document.unsetFlag(CFG.module, 'permissions');
		});
	}
}

/**
 * Handler for folder configuration dialog.
 *
 * @param {FolderConfig} fc
 * @param {JQuery} html
 * @param {Object} options
 */
function folderConfig(fc, [html], options) {
	const folder = fc.document;
	if (folder.id == null) return; // Temporary folder

	const templateData = {
		folderId: folder.id,
		inherit: folder.getFlag(CFG.module, 'inherit') ?? false
	};

	const content = document.createElement('div');
	content.innerHTML = CFG.folderTemplate(templateData);

	const permButton = content.querySelector('button');

	permButton.addEventListener('click', ev => {
		ev.preventDefault();
		ev.stopPropagation();

		const rect = ev.target.getBoundingClientRect();

		new InheritanceControl(folder)
			.render(true, { focus: true, top: rect.bottom + 8, left: rect.left - 200 });
	});

	/**
	 * @param {Event} ev
	 */
	async function toggleInheritance(ev) {
		ev.preventDefault();
		ev.stopPropagation();

		const inherit = folder.getFlag(CFG.module, 'inherit') ?? false;
		if (!inherit) folder.setFlag(CFG.module, 'inherit', true);
		else {
			folder.update({ [`flags.-=${CFG.module}`]: null });
			Object.values(folder.apps)
				.find(app => app instanceof InheritanceControl && app.document.id == folder.id)
				?.close();
		}
	}

	content.querySelector('input[type="checkbox"]')
		.addEventListener('change', toggleInheritance);

	html.querySelector('button[type="submit"]')
		.before(...content.children);
}

function preCreateOrUpdateDoc(doc, updateData) {
	const inSidebar = ['items', 'journal', 'tables'].includes(doc.collectionName);
	if (!inSidebar) return;

	// Check if this is created as member of a folder already
	const folderId = updateData.folder;
	if (!folderId) {
		// console.warn('Document not placed inside a folder');
		return;
	}

	// These are likely unnecessary
	if (doc.actor) return; // Ignore updates to documents in actors
	if (doc.pack) return; // Ignore updates to documents in packs

	const folder = game.folders.get(folderId);
	// Check if we have configuration for this.
	if (!folder?.getFlag(CFG.module, 'inherit')) return;

	const perms = folder.getFlag(CFG.module, 'permissions');
	if (!perms) return;

	const permissions = Object.entries(perms).reduce((users, [id, data]) => {
		const level = data.level;
		if (Math.clamped(level, 0, 3) !== level) return users; // Sanity check
		users[id] = level;
		return users;
	}, {});

	if (foundry.utils.isEmpty(permissions)) return;

	const C = CFG.COLORS;
	console.log(`%cInherit Folder Permissions%c | %c${doc.name}%c [%c${doc.id}%c]; in folder %c${folder.name}%c [%c${folder.id}%c]`,
		C.main, C.unset, C.label, C.unset, C.id, C.unset, C.label, C.unset, C.id, C.unset, { permissions });

	doc.updateSource({ ownership: permissions });
}

Hooks.on('renderFolderConfig', folderConfig);

Hooks.on('preCreateItem', preCreateOrUpdateDoc);
Hooks.on('preCreateActor', preCreateOrUpdateDoc);
Hooks.on('preCreateJournal', preCreateOrUpdateDoc);
Hooks.on('preCreateRollTable', preCreateOrUpdateDoc);

Hooks.on('preUpateItem', preCreateOrUpdateDoc);
Hooks.on('preUpdateItem', preCreateOrUpdateDoc);
Hooks.on('preUpdateJournal', preCreateOrUpdateDoc);
Hooks.on('preUpdateRollTable', preCreateOrUpdateDoc);

Hooks.once('ready', () => {
	const mod = game.modules.get(CFG.module);
	console.log(`%cInherit Folder Permissions%c | %c${mod.version}%c | Ready!`,
		CFG.COLORS.main, CFG.COLORS.unset, CFG.COLORS.number, CFG.COLORS.unset);
});
