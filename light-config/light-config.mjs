/**
 * @param {AmbientLightConfig} app
 * @param {JQuery<HTMLElement>} param1
 */
function renderLightConfig(app, [html]) {
	const els = html.querySelectorAll('input[type="range"]');
	for (const el of els) {
		if (!el) continue;
		const label = el.nextElementSibling;
		if (label.classList.contains('range-value')) {
			label.style.display = 'none';
			const i = document.createElement('input');
			i.type = 'number';
			i.classList.add('mana-light-config');
			i.dataset.for = name;
			i.value = el.valueAsNumber;
			const max = Number(el.max) || Infinity;
			const min = Number(el.min) || 0;
			const step = Number(el.step) || 1;
			i.max = max;
			i.min = min;
			i.step = step;
			el.addEventListener('input', (ev) => i.value = Math.clamped(Number(ev.target.value) || 0, min, max)
				, { passive: true });
			i.addEventListener('input', (ev) => {
				const value = ev.target.valueAsNumber || 0;
				const cvalue = Math.clamped(value, min, max)
				el.value = cvalue;
				if (value !== cvalue) ev.target.value = cvalue;
			}, { passive: true });
			el.after(i);
		}
	}
}

Hooks.on('renderAmbientLightConfig', renderLightConfig);
