const MODULE_ID = 'pf1-disable-encumbrance';

const getSystemData = (doc) => game.release.generation >= 10 ? doc.system : doc.data.data;

// Re-implementation of most of what PF1 itself does.
function disableEncumbrance(wrapped) {
	wrapped.call(this);

	const actorData = getSystemData(this),
		encumbrance = actorData.attributes.encumbrance;

	encumbrance.level = 0; // No encumbrance. This should cover speed penalty.

	// Return no active penalties.
	return {
		acp: 0,
		maxDexBonus: null,
	};
}

Hooks.once('init', () => {
	/* global libWrapper */
	libWrapper.register(MODULE_ID, 'game.pf1.documents.ActorPF.prototype._computeEncumbrance', disableEncumbrance, libWrapper.WRAPPER);
});

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 */
const hideEncumbrance = (sheet, [html]) => {
	const footer = html.querySelector('.tab.inventory .tab-footer');
	footer?.querySelector('.encumbrance')
		?.parentElement?.remove();
	footer?.querySelector('.carry-bonus')
		?.remove();
}

Hooks.on('renderActorSheetPF', hideEncumbrance);
