/* global libWrapper */

(() => {
	const MODULE_ID = 'mkah-loading';

	// Stream view behaves weird with all these
	const createNode = (node, text) => {
		const n = document.createElement(node);
		n.textContent = text;
		return n;
	}

	let l = {
		el: document.createElement('div'),
		label: document.createElement('h3'),
		hint: document.createElement('label'),
		components: document.createElement('div'),
		pre: {
			socket: createNode('label', 'WebSocket'),
			world: createNode('label', 'World Data'),
			game: createNode('label', 'Game'),
		},
		init: {
			hook: createNode('label', 'Hook Call (System & Modules)'),
			i18n: createNode('label', 'Translations'),
		},
		setup: {
			hook: createNode('label', 'Hook Call (System & Modules)'),
			sidebar: createNode('label', 'Sidebar'),
			// notifications: createNode('label', 'Notifications'),
			// navigation: createNode('label', 'Navigation'),
			// players: createNode('label', 'Player List'),
			// hotbar: createNode('label', 'Hotbar'),
			// pause: createNode('label', 'Pause State'),
			// combat: createNode('label', 'Combat Tracker'),
			// actors: createNode('label', 'Actor List'),
			// cards: createNode('label', 'Card List'),
			// items: createNode('label', 'Item List'),
			// journals: createNode('label', 'Journal List'),
			// scenes: createNode('label', 'Scene List'),
			// rolltable: createNode('label', 'RollTable List'),
			chatlog: createNode('label', 'Chat Log'),
			fonts: createNode('label', 'Fonts'),
			separator0: createNode('span', ' | '),
			textures: createNode('label', 'Textures'),
			// scene: createNode('label', 'Scene'),
			// fog: createNode('label', 'Fog'),
			canvas: createNode('label', 'Canvas'),
			// hud: createNode('label', 'HUD'),
			separator1: createNode('span', ' | '),
			lighting: createNode('label', 'Lighting'),
			perception: createNode('label', 'Perception'),
		},
		ready: {
			hook: createNode('label', 'Hook Call (System & Modules)'),
		},
		setLabel: function (text, step) {
			if (!this.label) return;
			this.label.textContent = `(${step}/3) ${text}`;
			const C = this.C;
			console.log(`%cLOADING%c | (%c${step}%c/%c3%c) ${text}`,
				C.label, C.unset, C.number, C.unset, C.number, C.unset);
		},
		setHint: function (text) {
			if (!this.hint) return;
			this.hint.textContent = text;
		},
		C: {
			label: 'color:mediumseagreen',
			unset: 'color:unset',
			number: 'color:goldenrod',
		},
		endLoader: () => {
			libWrapper.unregister(MODULE_ID, 'Hooks.callAll');

			setTimeout(() => {
			// Cleanup here since system.ready is not guaranteed to run
				l.el?.classList.remove('active');
				setTimeout(() => {
					l.el.remove();
					l = undefined;

					// Remove self from stylesheets, too.
					document.head.querySelector('link[href="modules/mkah-loading/loading.css"]')?.remove();
					document.head.querySelector('script[src="modules/mkah-loading/loading.mjs"]')?.remove();
				}, 500);
			}, 1500);
		}
	};

	l.components?.classList.add('components');
	l.setLabel('Pre-Init', 0);
	l.setHint('Loading core Foundry, initializing websocket, loading game system, modules and base static data.');
	l.components.replaceChildren(...Object.values(l.pre));

	l.el.id = 'mana-base-loading-progress';
	l.el.append(l.label, l.hint, l.components);

	document.body.append(l.el);

	l.el?.classList.add('active');

	/* global Game */
	const origGameCreate = Game.create;
	Game.create = async function (...args) {
		l.pre.game?.classList.add('processing');
		const rv = await origGameCreate.call(this, ...args);
		l.pre.game?.classList.add('done');
		Game.create = origGameCreate;
		return rv;
	}

	const origGameConnect = Game.connect;
	Game.connect = async function (...args) {
		l.pre.socket?.classList.add('processing');
		const rv = await origGameConnect.call(this, ...args);
		l.pre.socket?.classList.add('done');
		Game.connect = origGameConnect;
		return rv;
	}

	const origGameGetData = Game.getData;
	Game.getData = async function (...args) {
		l.pre.world?.classList.add('processing');
		const rv = await origGameGetData.call(this, ...args);
		l.pre.world?.classList.add('done');
		Game.connect = origGameGetData;
		return rv;
	}

	async function gameSetup(wrapped, ...args) {
		libWrapper.register(MODULE_ID, 'FontConfig._loadFonts', fontLoading, libWrapper.WRAPPER);
		// libWrapper.register(MODULE_ID, 'Canvas.prototype.initialize', canvasInit, libWrapper.WRAPPER);
		libWrapper.register(MODULE_ID, 'Game.prototype.initializeCanvas', gameCanvasInit, libWrapper.WRAPPER);
		// libWrapper.register(MODULE_ID, 'Canvas.prototype.draw', sceneDraw, libWrapper.WRAPPER);
		libWrapper.register(MODULE_ID, 'TextureLoader.loadSceneTextures', textureLoading, libWrapper.WRAPPER);
		// libWrapper.register(MODULE_ID, 'PerceptionManager.prototype.initialize', perceptionInit, libWrapper.WRAPPER);
		// libWrapper.register(MODULE_ID, 'FogManager.prototype.initialize', fogInit, libWrapper.WRAPPER);

		// NOTES:Something happens at end of canvasReady, causing page rendering to freeze up.

		const rv = await wrapped.call(this, ...args);
		l?.endLoader();
		libWrapper.unregister(MODULE_ID, 'Game.prototype.setupGame');
		return rv;
	}

	// FontConfig._loadFonts
	const fontLoading = (wrapped, ...args) => {
		l?.setup?.fonts?.classList.add('processing');
		const rv = wrapped.call(this, ...args);
		l?.setup?.fonts?.classList.add('done');
		libWrapper.unregister(MODULE_ID, 'FontConfig._loadFonts');
		return rv;
	}

	Hooks.once('init', () => {
		if (game.view === 'stream')
			return void l?.endLoader();

		libWrapper.register(MODULE_ID, 'Game.prototype.setupGame', gameSetup, libWrapper.WRAPPER);
	});

	const hookStart = (hook) => {
		if (!l) return;
		l[hook].hook?.classList.add('processing');
		switch (hook) {
			case 'init': {
				l?.setLabel('Foundry Init', 1);
				l?.setHint('Initializing settings, class extensions and overrides, and reacting to active configuration.');
				break;
			}
			case 'setup': {
				l.setLabel('Foundry Setup', 2);
				l.setHint('Setting up documents, UI applications and canvas.');
				break;
			}
			case 'ready': {
				l.setLabel('Foundry Ready', 3);
				l.setHint('Foundry has finished loading. Game system and modules may still be processing data.');
				break;
			}
		}
		l.components.replaceChildren(...Object.values(l[hook]));
	}

	const hookEnd = (hook) => {
		l?.[hook].hook?.classList.add('done');
		switch (hook) {
			case 'init': {
				break;
			}
			case 'setup': {
				break;
			}
			case 'ready': {
				break;
			}
		}
	}

	function hooksListener(wrapped, hook, ...args) {
		const coreHook = ['init', 'ready', 'setup'].includes(hook);
		if (coreHook) hookStart(hook);
		const rv = wrapped.call(this, hook, ...args);
		if (coreHook) hookEnd(hook);
		return rv;
	}

	async function i18nInit(wrapped, ...args) {
		l.init.i18n?.classList.add('processing');
		const rv = await wrapped.call(this, ...args);
		l.init.i18n?.classList.add('done');
		libWrapper.unregister(MODULE_ID, 'Localization.prototype.initialize');
		return rv;
	}

	Hooks.once('libWrapper.Ready', () => {
		libWrapper.register(MODULE_ID, 'Hooks.callAll', hooksListener, libWrapper.WRAPPER);
		libWrapper.register(MODULE_ID, 'Localization.prototype.initialize', i18nInit, libWrapper.WRAPPER);
	});

	// Game.initializeCanvas
	async function gameCanvasInit(wrapped, ...args) {
		l?.setup?.canvas?.classList.add('processing');
		const rv = await wrapped.call(this, ...args);
		l?.setup?.canvas?.classList.add('done');
		libWrapper.unregister(MODULE_ID, 'Game.prototype.initializeCanvas');
		return rv;
	}

	// TextureLoader.loadSceneTextures(scene)
	const textureLoading = async (wrapped, ...args) => {
		l?.setup?.textures?.classList.add('processing');
		const rv = await wrapped.call(this, ...args);
		l?.setup?.textures?.classList.add('done');
		libWrapper.unregister(MODULE_ID, 'TextureLoader.loadSceneTextures');
		return rv;
	}

	// PerceptionManager.initialize()
	function perceptionInit(wrapped, ...args) {
		const rv = wrapped.call(this, ...args);
		console.log('PERCEPTION!!!');
		return rv;
	}

	// FogManager.initialize()
	async function fogInit(wrapped, ...args) {
		l?.setup?.fog?.classList.add('processing');
		const rv = await wrapped.call(this, ...args);
		l?.setup?.fog?.classList.add('done');
		libWrapper.unregister(MODULE_ID, 'FogManager.prototype.initialize');
		return rv;
	}

	Hooks.once('renderSidebar', async () => l.setup?.sidebar?.classList.add('done'));
	Hooks.once('renderNotifications', async () => l.setup?.notifications?.classList.add('done'));
	Hooks.once('renderSceneNavigation', async () => l.setup?.navigation?.classList.add('done'));
	Hooks.once('renderPlayerList', async () => l.setup?.players?.classList.add('done'));
	Hooks.once('renderHotbar', async () => l.setup?.hotbar?.classList.add('done'));
	Hooks.once('renderPause', async () => l.setup?.pause?.classList.add('done'));
	Hooks.once('renderCombatTracker', async () => l.setup?.combat?.classList.add('done'));
	Hooks.once('renderActorDirectory', async () => l.setup?.actors?.classList.add('done'));
	Hooks.once('renderCardsDirectory', async () => l.setup?.cards?.classList.add('done'));
	Hooks.once('renderItemDirectory', async () => l.setup?.items?.classList.add('done'));
	Hooks.once('renderJournalDirectory', async () => l.setup?.journals?.classList.add('done'));
	Hooks.once('renderSceneDirectory', async () => l.setup?.scenes?.classList.add('done'));
	Hooks.once('renderRollTableDirectory', async () => l.setup?.rolltable?.classList.add('done'));

	Hooks.once('renderChatLog', async () => l.setup?.chatlog?.classList.add('done'));

	Hooks.once('canvasReady', async () => {
		// For some reason this change can hang for a while before it takes effect
		l?.setup?.canvas?.classList.add('done');
	});

	Hooks.once('renderHeadsUpDisplay', async () => l?.setup?.hud?.classList.add('done'));

	// These two can happen delayed
	Hooks.once('lightingRefresh', async () => l?.setup?.lighting?.classList.add('done'));
	Hooks.once('sightRefresh', async () => l?.setup?.perception?.classList.add('done'));
})();
