const getSystemData = (doc) => game.release.generation >= 10 ? doc.system : doc.data.data;

// Copies from Little Helper
function createNode(node = 'span', text = '', classes = [], { title = null, isHTML = false } = {}) {
	const n = document.createElement(node);
	if (isHTML) n.innerHTML = text;
	else n.textContent = text;
	if (classes.length) n.classList.add(...classes);
	if (title) n.title = title;
	return n;
}

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} jq
 * @param {Object} options
 */
function injectSpellRanges(sheet, jq, options) {
	const actor = sheet.actor;
	const html = jq[0];
	const tab = html.querySelector('.tab.spellbook');
	if (!tab) return;

	const actorData = getSystemData(actor);

	const spellranges = createNode('div', null, ['flexrow', 'spell-ranges']);

	const sr = 25 + Math.floor(actorData.cl / 2) * 5,
		src = Math.floor(sr / 5),
		mr = 100 + 10 * actorData.cl,
		mrc = Math.floor(mr / 5),
		lr = 400 + 40 * actorData.cl,
		lrc = Math.floor(lr / 5);

	spellranges.append(
		createNode('label', 'Short'),
		createNode('span', `${sr} ft.`, ['range', 'short-range', 'exact']),
		createNode('span', `[${src} sq.]`, ['range', 'short-range', 'cells']),
		createNode('label', 'Medium'),
		createNode('span', `${mr} ft.`, ['range', 'medium-range', 'exact']),
		createNode('span', `[${mrc} sq.]`, ['range', 'medium-range', 'cells']),
		createNode('label', 'Long'),
		createNode('span', `${lr} ft.`, ['range', 'long-range', 'exact']),
		createNode('span', `[${lrc} sq.]`, ['range', 'long-range', 'cells']),
	);

	tab.querySelector('ol')?.before(spellranges);
}

Hooks.on('renderActorSheet', injectSpellRanges);
