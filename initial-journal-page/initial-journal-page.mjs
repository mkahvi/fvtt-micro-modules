const MODULE_ID = 'initial-journal-page',
	PAGE_NAME_ID = 'name';

Hooks.on('preCreateJournalEntry', function addInitialPage(journal, data, options) {
	if (data.pages?.length > 0) return;

	const name = game.settings.get(MODULE_ID, PAGE_NAME_ID) || 'Initial'

	const page = new JournalEntryPage({ name, type: 'text', sort: 100000 });
	journal.updateSource({ pages: [page.toObject()] });
});

Hooks.once('init', () => {
	game.settings.register(MODULE_ID, PAGE_NAME_ID, {
		name: 'Initial name',
		type: String,
		default: 'Initial',
		scope: 'client',
		config: true,
	});
});
