# Micro Modules for Foundry VTT

These are various extremely tiny modules that do one very specific thing that I feel does not warrant proper module release on their own.

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

All of these software projects are distributed under the [MIT license](./LICENSE).
