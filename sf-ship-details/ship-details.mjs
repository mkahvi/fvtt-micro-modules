/**
 * @param {String} node
 * @param {String|null} text
 * @param {String[]} classes
 * @param {Object} options
 * @param {String|null} options.title
 * @param {Boolean} options.isHTML
 * @param {Element[]} options.children
 * @param {Object} [options.data] Object mapping for data-${key}=${value} values.
 * @param {Object} [options.attr] Object mapping for arbitrary attributes.
 * @returns {Element}
 */
export function createNode(node = 'span', text = null, classes = [], { title = null, isHTML = false, children = [], data, attr } = {}) {
	const n = document.createElement(node);
	if (text) {
		if (isHTML) n.innerHTML = text;
		else n.textContent = text;
	}
	if (classes.length) n.classList.add(...classes.filter(c => !!c));
	if (title) n.title = title;
	if (children.length) n.append(...children.filter(c => !!c));
	if (data) {
		for (const [key, value] of Object.entries(data))
			n.setAttribute(`data-${key}`, value);
	}
	if (attr) {
		for (const [key, value] of Object.entries(attr)) {
			if (value !== undefined)
				n.setAttribute(key, value);
		}
	}
	return n;
}

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param {Object} options
 */
function enrichShipSheet(sheet, [html], options) {
	const actor = sheet.actor;
	if (actor.type !== 'starship') return;

	const rd = actor.getRollData();

	const dTab = html.querySelector('.tab.details');

	// Clarify complement
	const cSep = dTab?.querySelector('.attributes .attribute .sep');
	if (cSep) {
		const min = cSep?.previousElementSibling;
		const max = cSep?.nextElementSibling;
		min?.prepend(createNode('span', 'Min', ['x-min-max-label', 'min']));
		max?.prepend(createNode('span', 'Max', ['x-min-max-label', 'max']));
	}

	const dSec = createNode('div', null, ['flexcol', 'x-extra-details']);

	// Add hack DC
	const t = createNode('h3', 'Extra Details', ['form-header']);
	dSec.append(t);

	const hackDC = createNode('div', null, ['flexrow', 'hack-dc-field']);
	const tier = rd.details.tier;
	hackDC.append(
		createNode('label', 'Hack base DC:', ['label']),
		createNode('span', `${15 + Math.floor(tier * 1.5)}`, ['hack-dc', 'value'], { title: 'DC = 15 + Tier × 1.5' })
	);
	dSec.append(hackDC);

	dTab?.append(dSec);
}

Hooks.on('renderActorSheet', enrichShipSheet);
