# Delay Markers

![Supported Foundry Versions](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fversion%3Fstyle%3Dflat%26url%3Dhttps%3A%2F%2Fgitlab.com%2Fmkahvi%2Ffvtt-micro-modules%2F-%2Fraw%2Fmaster%2Fdelay-markers%2Fmodule.json)

Adds ability to mark turns as delayed in combat tracker context menu.

The delay marker is automatically removed when the combatant's initiative changes, so manual undelay is not necessary.

The marker adds an icon to the combatant and shifts their name to be right aligned to easily see they're delaying.

The context menu has options for resuming right now, which is initiative just before current combatant, or right after them.

## Screenshot

![Screenshot](./screencap.png)

## Install

Manifest URL: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/master/delay-markers/module.json>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
