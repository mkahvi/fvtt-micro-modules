const CFG = {
	id: 'delay-markers',
	FLAGS: {
		scope: 'world',
		delay: 'delayedTurn',
	},
};

function getDelayFlag(cid) {
	const c = game.combat.combatants.get(cid);
	return c?.getFlag(CFG.FLAGS.scope, CFG.FLAGS.delay) == true;
}

function notCurrentTurn(cid) {
	return game.combat.combatant.id !== cid;
}

function toggleCombatantDelay(cid) {
	const combatant = game.combat.combatants.get(cid);
	const delayed = combatant?.getFlag(CFG.FLAGS.scope, CFG.FLAGS.delay);
	if (delayed == true)
		combatant.unsetFlag(CFG.FLAGS.scope, CFG.FLAGS.delay);
	else
		combatant.setFlag(CFG.FLAGS.scope, CFG.FLAGS.delay, true);
}

/**
 * @param {string} cid - Combatant ID
 * @param {boolean} rightNow - Resume right now.
 */
async function resumeCombatant(cid, rightNow) {
	const combat = game.combat;
	const combatant = combat.combatants.get(cid);
	const initiative = rightNow ? api.resume.now(cid) : api.resume.after(cid);

	if (initiative === null) return combatant.unsetFlag(CFG.FLAGS.scope, CFG.FLAGS.delay);

	const context = {};
	if (rightNow) context.turnEvents = false; // Tell Foundry to not adjust the turn-order

	await combatant.update({ initiative }, context);
}

function resumeInitiative(cid, rightNow) {
	const combat = game.combat;
	const resuming = combat.combatants.get(cid);
	const current = combat.combatant;
	if (current == resuming) return null; // Resume as self
	const idx = combat.turns.findIndex(c => c === current);
	const currentInit = current.initiative ?? 0;

	let other = null;
	// Before
	if (rightNow) {
		// First, shortcircuit
		if (idx == 0) return currentInit + 1;
		other = combat.turns[idx - 1];
	}
	// After
	else {
		// Last, shortcircuit
		if (idx == combat.turns.length - 1) return currentInit - 1;
		other = combat.turns[idx + 1];
	}

	if (other === current) return null; // Resuming as self

	const diff = (other.initiative - current.initiative) / 2;
	return current.initiative + diff;
}

/**
 * @param {*} _
 * @param {object[]} entries
 */
function registerContextMenuOptions(_, entries) {
	if (!game.user.isGM) return;

	entries.unshift(
		{
			name: 'DelayMarker.Delay',
			icon: '<i class="fas fa-pause-circle"></i>',
			condition: ([li]) => !getDelayFlag(li.dataset.combatantId),
			callback: ([li]) => toggleCombatantDelay(li.dataset.combatantId),
		},
		{
			name: 'DelayMarker.Undelay',
			icon: '<i class="fas fa-play-circle"></i>',
			condition: ([li]) => !!getDelayFlag(li.dataset.combatantId),
			callback: ([li]) => toggleCombatantDelay(li.dataset.combatantId),
		},
		{
			name: 'DelayMarker.ResumeNow',
			icon: '<i class="fas fa-play-circle"></i>',
			condition: ([li]) => !!getDelayFlag(li.dataset.combatantId) && notCurrentTurn(li.dataset.combatantId),
			callback: ([li]) => resumeCombatant(li.dataset.combatantId, true),
		},
		{
			name: 'DelayMarker.ResumeAfter',
			icon: '<i class="fas fa-play-circle"></i>',
			condition: ([li]) => !!getDelayFlag(li.dataset.combatantId) && notCurrentTurn(li.dataset.combatantId),
			callback: ([li]) => resumeCombatant(li.dataset.combatantId, false),
		},
	);
}

/**
 * preUpdateCombatant
 * Broken in v9, so only v10 code is here
 *
 * @param {Combatant} combatant
 * @param {object} update
 * @param {object} options
 * @param {string} userId
 */
function finishDelay(combatant, update, options, userId) {
	// Unset delayed turn if initiative is updated.
	if (update.initiative !== undefined && combatant.getFlag(CFG.FLAGS.scope, CFG.FLAGS.delay) == true)
		foundry.utils.setProperty(update, `flags.${CFG.FLAGS.scope}.-=${CFG.FLAGS.delay}`, null);
}

/**
 * updateCombatant
 * For v9 to do the same as above in secondary update instead.
 *
 * @param {Combatant} combatant
 * @param {object} update
 * @param {object} options
 * @param {string} userId
 */
function finishDelayForV9(combatant, update, options, userId) {
	if (game.user.id !== userId) return;

	if (update.initiative !== undefined && combatant.getFlag(CFG.FLAGS.scope, CFG.FLAGS.delay) == true)
		combatant.unsetFlag(CFG.FLAGS.scope, CFG.FLAGS.delay);
}

/**
 * @param {CombatTracker} tracker
 * @param {JQuery} html
 * @param {object} options
 */
function renderDelayedTurn(tracker, [html], options) {
	const list = html.querySelector('ol.directory-list');
	const combat = tracker.viewed;

	const levels = CONST.DOCUMENT_OWNERSHIP_LEVELS ?? CONST.DOCUMENT_PERMISSION_LEVELS;

	list?.querySelectorAll('li.combatant.actor[data-combatant-id]').forEach(li => {
		const cid = li.dataset.combatantId;
		const c = combat.combatants.get(cid);

		if (!c?.testUserPermission(game.user, levels.OBSERVER)) return;

		const delayed = c.getFlag(CFG.FLAGS.scope, CFG.FLAGS.delay);
		if (delayed) {
			li.classList.add('delayed-turn');
		}
	});
}

Hooks.on('renderCombatTracker', renderDelayedTurn);
Hooks.on('getCombatTrackerEntryContext', registerContextMenuOptions);
Hooks.once('setup', () => {
	if (game.release.generation >= 10)
		Hooks.on('preUpdateCombatant', finishDelay);
	else
		Hooks.on('updateCombatant', finishDelayForV9); // v9 has buggy preUpdateCombatant
});

const api = {
	resume: {
		now: (cid) => resumeInitiative(cid, true),
		after: (cid) => resumeInitiative(cid, false),
	},
};

Hooks.once('ready', () => {
	game.modules.get(CFG.id).api = api;
});
