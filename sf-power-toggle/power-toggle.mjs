// Add power ⚡ toggle to ship systems

const getSystemData = (doc) => game.release.generation >= 10 ? doc.system : doc.data.data;

/**
 * @param {Event} ev
 * @param {Item} item
 */
function togglePower(ev, item) {
	ev.stopPropagation();
	ev.preventDefault();

	return item.update({ 'data.isPowered': !getSystemData(item).isPowered });
}

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param {Object} options
 */
function addPowerToggle(sheet, [html], options) {
	const actor = sheet.actor;
	if (actor.type !== 'starship') return;

	/**
	 * @param {Element} el
	 */
	function handleSystem(el) {
		const item = actor.items.get(el.dataset.itemId);
		if (!item) return; // Something bad

		// Components that have no meaningful power toggle function
		if (['starshipFrame', 'starshipPowerCore', 'actorResource'].includes(item.type)) return;

		const itemData = getSystemData(item);
		const isPowered = itemData.isPowered ?? false;
		const pcu = itemData.pcu || 0;

		// Should systems with zero PCU be ignored?

		const pt = document.createElement('a');
		pt.classList.add('item-control', 'item-power', 'mod-power-toggle');
		pt.title = `Toggle Power\nPCU: ${pcu}`;
		pt.textContent = '⚡';
		el.classList.add(isPowered ? 'mod-power-toggle-powered' : 'mod-power-toggle-unpowered');

		if (sheet.isEditable)
			pt.addEventListener('click', ev => togglePower(ev, item));

		el.querySelector('.item-controls')?.prepend(pt);
	}

	function findSystems(selector) {
		const tab = html.querySelector(selector);
		const items = tab?.querySelectorAll('.inventory-list .item[data-item-id]');
		items.forEach(handleSystem);
	}

	findSystems('.tab.weapons');
	findSystems('.tab.features');
}

Hooks.on('renderActorSheet', addPowerToggle);
