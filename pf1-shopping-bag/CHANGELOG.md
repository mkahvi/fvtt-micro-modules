# Change Log

## 1.1.0

- New: Spell support
- Fix: Item quantity handling was faulty in places.
- Fix: Delete Stacks option was not respected.

## 1.0 Initial
