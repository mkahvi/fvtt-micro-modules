/**
 * TODO: Adjust wand charges.
 * TODO: Enhanced items
 */

const API = {
	cost: {
		scribing: (level) => Math.max(5, Math.pow(level, 2) * 10),
	}
};

const CFG = {
	id: 'pf1-shopping-bag',
	SETTINGS: {
		preferGold: 'preferGold',
		deleteStack: 'deleteStack',
	},
};

function closeItem(item) {
	Object.values(item?.apps ?? {}).forEach(app => app.close?.());
}

function resetItem(item) {
	item.reset();
	item.render();
}

const numberFormat = (num, sign = false) => {
	if (!sign) return num;
	return num < 0 ? `${num}` : `+${num}`;
}

/**
 * @readonly
 * @enum {number}
 */
const GOLD = {
	IS_ALL: 2,
	IS_SACRED: 1,
	IS_TRUE: 0
};

/**
 * @returns {GOLD} - Gold preference
 */
const getGoldPreference = () => {
	switch (game.settings.get(CFG.id, CFG.SETTINGS.preferGold)) {
		case 'only': return GOLD.IS_ALL;
		case 'prefer': return GOLD.IS_SACRED;
		default:
		case 'none': return GOLD.IS_TRUE;
	}
}

const currencies = [{ id: 'pp', label: 'PP', small: 'pp' }, { id: 'gp', label: 'GP', small: 'gp' }, { id: 'sp', label: 'SP', small: 'sp' }, { id: 'cp', label: 'CP', small: 'cp' }];

class Currency {
	total = 0;
	cp = 0;
	sp = 0;
	gp = 0;
	pp = 0;

	/**
	 * @param {Item|object} [init] - Initial data
	 * @param {object} [options] - Additional options
	 * @param {boolean} [options.full] - Full value
	 * @param {GOLD} [options.preferGold] - Gold preference
	 * @param options.sign
	 */
	constructor(init, { full = false, preferGold = GOLD.IS_TRUE, sign = false } = {}) {
		Object.defineProperties(this, {
			preferGold: {
				value: preferGold,
			},
			sign: {
				value: sign,
			}
		});

		const valOpts = { inLowestDenomination: true };
		if (full) valOpts.sellValue = 1;

		if (init instanceof Item) {
			this.total = init?.getValue(valOpts) || 0;
			this.splitCurrency();
		}
		else if (init) {
			const { cp, sp, gp, pp } = init;
			this.cp = cp || 0;
			this.sp = sp || 0;
			this.gp = gp || 0;
			if (this.preferGold > GOLD.IS_TRUE) this.gp += (pp || 0) * 10
			else this.pp = pp || 0;
			this.calculateTotal();
		}
	}

	/**
	 * Split all currencies.
	 */
	splitCurrency() {
		const isNegative = this.total < 0;
		// Flip to make the math work
		this.total = Math.abs(this.total);

		this.sp = Math.floor(this.total / 10);
		this.cp = this.total - (this.sp * 10);
		this.gp = Math.floor(this.sp / 10);
		this.sp = this.sp - (this.gp * 10);

		if (this.preferGold == GOLD.IS_TRUE) {
			this.pp = Math.floor(this.gp / 10);
			this.gp = this.gp - (this.pp * 10);
		}

		// Flip back
		if (isNegative) {
			this.pp = -this.pp;
			this.gp = -this.gp;
			this.sp = -this.sp;
			this.cp = -this.cp;
			this.total = -this.total;
		}
	}

	/**
	 * Consolidate all currencies to copper.
	 */
	consolidate() {
		this.cp += this.sp * 10;
		this.cp += this.gp * 100;
		this.cp += this.pp * 1000;
		this.sp = this.gp = this.pp = 0;
		this.total = this.cp;
	}

	calculateTotal() {
		this.total = this.cp + this.sp * 10 + this.gp * 100 + this.pp * 1000;
	}

	/**
	 * @param {Currency} p
	 */
	add({ pp = 0, gp = 0, sp = 0, cp = 0 } = {}) {
		if (this.preferGold > GOLD.IS_TRUE)
			this.gp += (pp || 0) * 10;
		else
			this.pp += (pp || 0);

		this.gp += (gp || 0);
		this.sp += (sp || 0);
		this.cp += (cp || 0);

		this.calculateTotal();

		return this;
	}

	subtract({ pp = 0, gp = 0, sp = 0, cp = 0 } = {}) {
		if (pp > 0) this.pp -= pp;
		if (gp > 0) this.gp -= gp;
		if (sp > 0) this.sp -= sp;
		if (cp > 0) this.cp -= cp;

		this.payDeficiencies();

		this.calculateTotal();

		return this;
	}

	/**
	 * Adjust currencies if they're negative.
	 * TODO: Make this megasmart.
	 *
	 * @returns {this}
	 */
	payDeficiencies() {
		// COPPER
		while (this.cp < 0) {
			if (this.sp >= 1) {
				this.sp -= 1;
				this.cp += 10;
			}
			else if (this.gp >= 1) {
				this.gp -= 1;
				this.cp += 100;
				if (this.cp > 0)
					this.redistribute({ cp: 100 });
			}
			else if (this.pp >= 1) {
				this.pp -= 1;
				this.cp += 1_000;
				if (this.cp > 0)
					this.redistribute({ cp: 1000 });
			}
			else
				return this;
		}

		// SILVER
		while (this.sp < 0) {
			if (this.cp >= 10) {
				this.cp -= 10;
				this.sp += 1;
			}
			else if (this.gp >= 1) {
				this.gp -= 1;
				this.sp += 10;
			}
			else if (this.pp >= 1) {
				this.pp -= 1;
				this.sp += 100;
				if (this.sp > 0)
					this.redistribute({ sp: 100 });
			}
			else
				return this;
		}

		// GOLD
		while (this.gp < 0) {
			if (this.sp >= 10) {
				this.sp -= 10;
				this.gp += 1;
			}
			else if (this.cp >= 100) {
				this.cp -= 100;
				this.gp += 1;
			}
			else if (this.pp >= 1) {
				this.pp -= 1;
				this.gp += 10;
			}
			else
				return this;
		}

		// PLATINUM
		while (this.pp < 0) {
			if (this.gp >= 10) {
				this.gp -= 10;
				this.pp += 1;
			}
			else if (this.sp >= 100) {
				this.sp -= 100;
				this.pp += 1;
			}
			else if (this.cp >= 1_000) {
				this.cp -= 1000;
				this.pp += 1;
			}
			else
				return this;
		}

		return this;
	}

	/**
	 * Redistribute coins to higher currencies.
	 *
	 * @param {{pp:number,gp:number,sp:number,cp:number}} maximums - Maximum number of coins to redistribute.
	 */
	redistribute({ gp = 0, sp = 0, cp = 0 } = {}) {
		// COPPER
		if (cp > 0) {
			cp = Math.min(this.cp, cp);
			const pp = Math.floor(cp / 1_000);
			cp -= pp * 1_000;
			this.pp += pp;
			this.cp += pp * 1_000;
			const gp = Math.floor(cp / 100);
			cp -= gp * 100;
			this.gp += gp;
			this.cp -= gp * 100;
			const sp = Math.floor(cp / 10);
			cp -= sp * 10;
			this.sp += sp;
			this.cp -= sp * 10;
		}
		// SILVER
		if (sp > 0) {
			sp = Math.min(this.sp, sp);
			const pp = Math.floor(sp / 100);
			sp -= pp * 100;
			this.pp += pp;
			this.sp -= pp * 100;
			const gp = Math.floor(sp / 10);
			sp -= gp * 10;
			this.gp += gp;
			this.sp -= gp * 10;
		}
		// GOLD
		if (gp > 0) {
			gp = Math.min(this.gp, gp);
			const pp = Math.floor(gp / 10);
			gp -= pp * 10;
			this.pp += pp;
			this.gp -= gp * 10;
		}
	}

	/**
	 * @type {boolean} True if the pile has negative currencies.
	 */
	get isDeficient() {
		return this.pp < 0 || this.gp < 0 || this.sp < 0 || this.cp < 0;
	}

	toString() {
		if (this.preferGold === GOLD.IS_ALL) {
			return `${this.total / 100}g`;
		}

		const labels = ['p', 'g', 's', 'c'];
		const values = [this.pp, this.gp, this.sp, this.cp];
		const output = [];
		values.forEach((quantity, index) => {
			if (quantity != 0)
				output.push(`${numberFormat(quantity, this.sign)}${labels[index]}`);
		});
		return output.length ? output.join(' ') : 'n/a';
	}

	get parts() {
		if (this.preferGold === GOLD.IS_ALL) {
			return [{ ...currencies[1], quantity: this.total / 100 }];
		}

		const output = [];
		for (const curr of currencies) {
			const quantity = this[curr.id];
			output.push({ ...curr, quantity });
		}

		return output;
	}

	/**
	 * Clone new equivalent instance.
	 *
	 * @returns {Currency}
	 */
	clone() {
		return new this.constructor({ total: this.total, pp: this.pp, gp: this.gp, sp: this.sp, cp: this.cp, sign: this.sign, preferGold: this.preferGold });
	}

	toObject() {
		return { pp: this.pp, gp: this.gp, sp: this.sp, cp: this.cp };
	}
}

class ItemData {
	/** @type {Item} */
	item;

	/** @type {Item} */
	original;

	/**	@type {string} */
	img;

	/**	@type {string} */
	name;

	/**	@type {string} */
	get uuid() {
		const uuid = this.original.uuid;
		if (this.isScribing)
			return 'Scribe.' + uuid;
		if (this.item.type === 'consumable')
			return this.item.subType.capitalize() + '.' + uuid;
		return uuid;
	}

	/**	@type {number} */
	get quantity() { return this.item.system.quantity || 0; }

	get originalQuantity() {
		if (!this.original.isPhysical) return 1;
		return this.original.system.quantity || 0;
	}

	get isScribing() { return this.item.type === 'loot' && this.item.subType === 'misc' && this.original.type === 'spell'; }

	get hasQuantity() { return this.item.isPhysical ?? false; }

	/**
	 * @param {object} [options] - Additional options passed to {@link Currency}
	 * @param {boolean} [options.full=false]
	 * @param {GOLD} [options.preferGold=GOLD.IS_TRUE]
	 * @returns {this}
	 */
	prepare({ full = false, preferGold = GOLD.IS_TRUE } = {}) {
		this.item.reset();
		this.value = new Currency(this.item, { full, preferGold });
		return this;
	}

	constructor(item, original) {
		this.original = original ?? item;
		this.item = new Item.implementation(item.toObject());
		this.img = item.img;
		this.name = item.name;
	}
}

async function createScribeItem(item, itemData, books) {
	const book = books.find(b => b.key === itemData.book);
	const price = API.cost.scribing(book?.level ?? itemData.level ?? 1);

	const templateData = {
		link: item.link,
		level: itemData.sl,
		book: book.name || 'Unknown',
	};

	return new Item.implementation({
		name: game.i18n.format('ShoppingBag.Scribing', { name: item.name }),
		type: 'loot',
		img: 'icons/commodities/materials/feather-blue-glowing.webp',
		system: {
			subType: 'misc',
			quantity: 1,
			price,
			identified: true,
			description: {
				value: await renderTemplate(`modules/${CFG.id}/scribe-desc.hbs`, templateData),
			}
		},
		flags: {
			pf1: {
				spell: {
					book: book.key,
				}
			}
		}
	}).toObject();
}

const createConsumable = (item, itemData, type, books) => {
	if (type === 'scribe') return createScribeItem(item, itemData, books);
	const data = { ...item.toObject(), ...itemData };
	return CONFIG.Item.documentClasses.spell.toConsumable(data, type);
}

async function toConsumableDialog(item, actor) {
	const itemData = item.toObject();

	const learnedAt = item.system?.learnedAt?.class ?? {};
	const books = Object.entries(actor.system.attributes?.spells?.spellbooks ?? {})
		.filter(([_, book]) => book.inUse && ['prepared', 'hybrid'].includes(book.spellPreparationMode))
		.map(([key, book]) => ({ key, name: book.label, level: learnedAt[book.class] ?? item.system.level ?? 1 }));

	const [sl, cl] = CONFIG.Item.documentClasses.spell.getMinimumCasterLevelBySpellData(itemData);

	const getFormData = (html) => {
		const formData = expandObject(new FormDataExtended(html.querySelector('form')).object);
		formData.sl ??= 1;
		formData.cl ??= 1;
		// NaN check here to allow SL 0
		if (Number.isNaN(formData.sl)) formData.sl = 1;
		return formData;
	};

	const buttons = {
		potion: {
			icon: '<i class="fas fa-prescription-bottle"></i>',
			label: game.i18n.localize('PF1.CreateItemPotion'),
			callback: (html) => createConsumable(item, getFormData(html), 'potion'),
		},
		scroll: {
			icon: '<i class="fas fa-scroll"></i>',
			label: game.i18n.localize('PF1.CreateItemScroll'),
			callback: (html) => createConsumable(item, getFormData(html), 'scroll'),
		},
		wand: {
			icon: '<i class="fas fa-magic"></i>',
			label: game.i18n.localize('PF1.CreateItemWand'),
			callback: (html) => createConsumable(item, getFormData(html), 'wand'),
		},
		scribe: {
			icon: '<i class="fas fa-hand-sparkles"></i>',
			label: game.i18n.localize('ShoppingBag.Scribe'),
			callback: (html) => createConsumable(item, getFormData(html), 'scribe', books),
		},
	};

	if (books.length === 0)
		delete buttons.scribe;

	return Dialog.wait({
		title: game.i18n.format('PF1.CreateItemForSpell', { name: itemData.name }),
		content: await renderTemplate(`modules/${CFG.id}/create-dialog.hbs`, { name: item.name, sl, cl, books }),
		buttons,
		close: () => null,
		default: 'potion',
	},
	{
		classes: [...Dialog.defaultOptions.classes, 'shopping-bag', 'spell-handler'],
		jQuery: false,
		rejectClose: false,
		itemData,
	});
}

class ShoppingBag extends FormApplication {
	/** @type {Actor} */
	actor;

	/**	@type {Collection<string,ItemData>} */
	sales = new Collection();

	/**	@type {Collection<string,ItemData>} */
	purchases = new Collection();

	/**	@type {Currency} */
	remainingtotal;
	/** @type {Currency} */
	final;

	/**	@type {Currency} */
	diff;

	/**	@type {Currency} */
	wealth;

	/**	@type {Currency} */
	cost;

	constructor(actor) {
		super();
		this.actor = actor;

		this.registerApp();
	}

	registerApp() {
		this.actor.apps[this.id] = this;
	}

	unregisterApp() {
		delete this.actor.apps[this.id];
	}

	close(...args) {
		this.unregisterApp();
		super.close(...args);
	}

	get id() {
		return 'shopping-bag-' + this.actor.uuid;
	}

	get title() {
		return game.i18n.format('ShoppingBag.Title', { actor: this.actor.name });
	}

	get template() {
		return `modules/${CFG.id}/shopping-bag.hbs`;
	}

	static get defaultOptions() {
		const options = super.defaultOptions;
		return {
			...options,
			dragDrop: [{ dragSelector: null, dropSelector: 'form' }],
			classes: [...options.classes, 'shopping-bag'],
			scrollY: ['.item-list'],
			submitOnClose: false,
			submitOnChange: true,
			closeOnSubmit: false,
			height: 540,
			width: 920,
			resizable: true,
		};
	}

	_canDragDrop() {
		return true;
	}

	_onDrop(event) {
		event.preventDefault();
		const data = TextEditor.getDragEventData(event);

		if (data.containerId)
			return void ui.notifications.error('ShoppingBag.Error.NoContainerItems', { localize: true });

		switch (data.type) {
			case 'Item':
				this._onDropItem(event, data);
				break;
			case 'Folder':
				this._onDropFolder(event, data);
				break;
			default:
				ui.notifications.error(`Invalid drop data type "${data.type}"`);
		}
	}

	async _onDropItem(event, data) {
		let item = await Item.implementation.fromDropData(data);
		if (item.system.quantity <= 0 && !item.actor) item.updateSource({ 'system.quantity': 1 });

		// TODO: Handle spells

		const collection = item.actor === this.actor ? this.sales : this.purchases;

		const original = item;
		let uuid = item.uuid;

		if (item.isPhysical) { /* NOP */ }
		else if (!item.actor && item.type === 'spell') {
			let spellcruft = await toConsumableDialog(item, this.actor);
			if (!spellcruft) return;
			spellcruft = new Item.implementation(spellcruft);
			if (spellcruft.type === 'loot')
				collection.set('Scribe.' + uuid, new ItemData(spellcruft, original));
			else {
				item = spellcruft;
				uuid = item.system.subType.capitalize() + '.' + uuid;
			}

			if (item.type === 'spell') {
				item.updateSource({ 'system.spellbook': spellcruft.getFlag('pf1', 'spell')?.book || 'primary' });
			}
		}
		else if (item.actor)
			return void ui.notifications.warn('ShoppingBag.Error.NoNonPhysicalActor', { localize: true });
		else
			return void ui.notifications.warn('ShoppingBag.Error.NoNonPhysical', { localize: true });

		// TODO: Handle containers

		collection.set(uuid, new ItemData(item, original));

		this.render();
	}

	async _onDropFolder(event, data) {
		const folder = await Folder.implementation.fromDropData(data);
		if (folder.type !== 'Item') return;

		for (let item of folder.contents) {
			if (!(item instanceof Item)) item = await fromUuid(item.uuid);
			this.purchases.set(item.uuid, new ItemData(item));
		}

		this.render();
	}

	async getData() {
		const preferGold = getGoldPreference();
		const opts = { preferGold };

		const sales = this.sales
				.map(/** @param {ItemData} s */ s => s.prepare(opts)),
			purchases = this.purchases
				.map(/** @param {ItemData} p */ p => p.prepare({ ...opts, full: true }));

		const actorData = this.actor.system;

		// Calculate total value of sales
		const sale = new Currency(null, opts);
		sales.forEach(p => sale.add(p.value));
		// Calculate total value of purchases
		const cost = new Currency(null, opts);
		purchases.forEach(p => cost.add(p.value));
		// Calculate actor wealth
		const wealth = new Currency(null);
		const wealthAlt = new Currency(null);
		for (const { id } of currencies) {
			wealth.add({ [id]: (actorData.currency[id] || 0) });
			wealthAlt.add({ [id]: actorData.altCurrency[id] || 0 });
		}
		// TODO: Add options for handling weightless currency
		// wealth.add(wealthAlt);

		// Difference in wealth
		const diff = new Currency(null, { ...opts, sign: true });
		diff.total += sale.total;
		diff.total -= cost.total;
		diff.splitCurrency();

		const final = new Currency(null);
		final.add(wealth);
		final.add(diff);
		final.splitCurrency();

		this.remainingtotal = final;
		this.diff = diff;
		this.wealth = wealth;
		this.cost = cost;
		this.final = wealth.clone().add(sale).subtract(cost);

		const isValid = final.total >= 0 && (sales.some(i => i.quantity > 0) || purchases.some(i => i.quantity > 0));

		const context = {
			wealth,
			sale,
			cost,
			diff,
			final: this.final,
			// Items
			sales,
			purchases,
			currencies,
			isValid,
		};

		return context;
	}

	#queuedRender = false;
	render(...args) {
		if (this._state === Application.RENDER_STATES.RENDERING) this.#queuedRender = true;
		else super.render(...args);
	}

	async _render(...args) {
		await super._render(...args);

		// Handle queued render
		if (this.#queuedRender) {
			this.#queuedRender = false;
			this.render();
		}
	}

	activateListeners(jq) {
		super.activateListeners(jq);

		const [html] = jq;

		html.addEventListener('click', this._onClick.bind(this));

		html.addEventListener('contextmenu', this._onRightClick.bind(this));

		html.addEventListener('click', this._onButtonClick.bind(this));
	}

	/**
	 * @param {Event} event
	 */
	_onClick(event) {
		let el = event.target;
		if (el.tagName === 'I') el = el.parentElement;
		const isCommand = el.matches('.controls > a');
		if (!isCommand) return;

		event.preventDefault();

		const itemEl = el.closest('[data-uuid]');
		const uuid = itemEl?.dataset.uuid?.trim();
		const category = itemEl?.dataset.category?.trim();
		const isPurchase = category === 'purchase';
		const isBulk = !!event.shiftKey;
		const isBulkAlt = !!event.ctrlKey;

		let adjust = 1;
		if (isBulk) adjust *= 10;
		if (isBulkAlt) adjust *= 5;

		const collection = isPurchase ? this.purchases : this.sales;
		switch (el.dataset.action) {
			case 'increase': {
				/** @type {ItemData} */
				const itemData = collection.get(uuid, { strict: true });
				let newQuantity = Math.max(0, itemData.quantity + adjust);
				if (itemData.isScribing && newQuantity > 1) newQuantity = 1;

				if (!isPurchase) {
					const max = itemData.originalQuantity;
					if (newQuantity > max) newQuantity = max;
				}
				itemData.item.updateSource({ 'system.quantity': newQuantity });
				resetItem(itemData.item);
				break;
			}
			case 'decrease': {
				const itemData = collection.get(uuid, { strict: true });
				itemData.item.updateSource({ 'system.quantity': Math.max(0, itemData.quantity - adjust) });
				resetItem(itemData.item);
				break;
			}
			case 'remove': {
				/** @type {ItemData} */
				const itemData = collection.get(uuid, { strict: true });
				collection.delete(uuid);
				// Remove spell if scribe is removed
				if (uuid.startsWith('Scribe.')) {
					const scribeUuid = uuid.replace(/^Scribe\./, '');
					const scribing = collection.get(scribeUuid, { strict: true });
					collection.delete(scribeUuid);
					scribing.item.sheet.close();
				}
				// Remove scribe is spell is removed
				else if (itemData.item.type === 'spell') {
					const scribeUuid = 'Scribe.' + uuid;
					const scribe = collection.get(scribeUuid, { strict: true });
					if (scribe) closeItem(scribe.item);
					collection.delete(scribeUuid);
				}
				closeItem(itemData.item);
				break;
			}
			case 'browse-compendium':
				pf1.applications.compendiums.items.render(true);
				break;
		}

		this.render();
	}

	/**
	 * @param {Event} event
	 */
	_onButtonClick(event) {
		let el = event.target;
		if (el.tagName === 'I') el = el.parentElement;
		const isButton = el.matches('.buttons > button');
		if (!isButton) return;

		event.preventDefault();

		const action = el.dataset.action;

		switch (action) {
			case 'commit':
				this._commitTransaction();
				break;
		}
	}

	async _commitTransaction() {
		console.log('COMMIT TRANSACTION!');
		this.unregisterApp(); // To avoid re-renders during transaction

		this.form.classList.add('disabled');

		await this._generateCard();

		const actor = this.actor;

		// Reduce money
		const newWealth = this.final;
		if (newWealth.total != 0) {
			const actorUpdate = { system: { currency: newWealth.toObject() } };
			// await actor.update(actorUpdate);
			console.log('Transaction | Wealth:', actorUpdate);
			await actor.update(actorUpdate);
		}

		/** @type {ItemData[]} */
		const sales = this.sales;
		if (sales.size) {
			const itemUpdates = [];
			const deleteIds = [];

			const deleteStacks = game.settings.get(CFG.id, CFG.SETTINGS.deleteStack);

			for (const data of sales) {
				if (data.quantity == 0) continue;
				const remaining = Math.max(0, data.originalQuantity - data.quantity);
				if (remaining == 0 && deleteStacks)
					deleteIds.push(data.original.id);
				else {
					itemUpdates.push({
						_id: data.original.id,
						system: { quantity: remaining }
					});
				}
			}
			console.log('Transaction | Delete:', deleteIds);
			console.log('Transaction | Update:', itemUpdates);

			if (itemUpdates.length)
				await actor.updateEmbeddedDocuments('Item', itemUpdates);
			if (deleteIds.length)
				await actor.deleteEmbeddedDocuments('Item', deleteIds);
		}

		const purchases = this.purchases;
		if (purchases.size) {
			const newItems = purchases.map(p => p.item.toObject());
			console.log('Transaction | New:', newItems);
			await actor.createEmbeddedDocuments('Item', newItems);
		}

		this.close();
	}

	async _generateCard() {
		const templateData = {
			sales: this.sales.filter(i => i.quantity > 0),
			purchases: this.purchases.filter(i => i.quantity > 0),
			currency: this.diff.parts,
		};

		const data = {
			content: await renderTemplate(`modules/${CFG.id}/shopping-card.hbs`, templateData),
			speaker: {
				actor: this.actor,
			}
		};

		ChatMessage.applyRollMode(data, CONST.DICE_ROLL_MODES.PRIVATE);
		return ChatMessage.create(data);
	}

	_onRightClick(event) {
		let el = event.target;
		if (el.tagName === 'A') el = el.parentElement;
		if (el.tagName === 'SPAN') el = el.parentElement;
		const isItem = el.matches('.item[data-uuid]');
		if (!isItem) return;

		const uuid = el.dataset.uuid;
		const collection = el.dataset.category === 'purchase' ? this.purchases : this.sales;
		const itemData = collection.get(uuid, { strict: true });
		itemData.item.apps[this.id] = this;
		itemData.item.sheet.render(true, { focus: true });
	}

	_updateObject(event) {
		event.preventDefault();

		console.log('Sales:', this.sales);
		console.log('Purchases:', this.purchases);
		console.log('Remaining:', this.remainingtotal);
	}

	/**
	 * @param {Actor} actor
	 * @param {Event} event
	 * @returns {ShoppingBag}
	 */
	static open(actor, event) {
		event.preventDefault();
		event.stopPropagation();

		const el = event.currentTarget;
		const rect = el.getBoundingClientRect();

		const app = new this(actor);
		app.render(true, { focus: true, top: rect.bottom + 20 });
		return app;
	}
}

function registerSettings() {
	game.settings.register(CFG.id, CFG.SETTINGS.preferGold, {
		name: 'ShoppingBag.Settings.PreferGold.Label',
		hint: 'ShoppingBag.Settings.PreferGold.Hint',
		type: String,
		default: 'none',
		choices: {
			none: 'ShoppingBag.Settings.PreferGold.No',
			prefer: 'ShoppingBag.Settings.PreferGold.Little',
			only: 'ShoppingBag.Settings.PreferGold.Lot'
		},
		onChange: () => {
			Object.values(ui.windows)
				.filter(app => app instanceof ShoppingBag)
				.forEach(app => app.render());
		},
		config: true,
		scope: 'client',
	});

	game.settings.register(CFG.id, CFG.SETTINGS.deleteStack, {
		name: 'ShoppingBag.Settings.DeleteStack.Label',
		hint: 'ShoppingBag.Settings.DeleteStack.Hint',
		type: Boolean,
		default: false,
		scope: 'client',
		config: true,
	});
}

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 */
function addShoppingBagButton(sheet, [html]) {
	const currency = html.querySelector('.tab.inventory .currency');
	if (!currency) return;

	const bag = document.createElement('a');
	bag.innerHTML = '<i class="fa-solid fa-bag-shopping"></i>';
	bag.classList.add('shopping-bag', 'action');
	bag.dataset.tooltip = 'ShoppingBag.Tooltip';
	currency.append(bag);

	const bagspacer = document.createElement('a');
	bagspacer.classList.add('shopping-bag-spacer');
	currency.nextElementSibling?.append(bagspacer);

	bag.addEventListener('click', ev => ShoppingBag.open(sheet.actor, ev));
}

function registerAPI() {
	game.modules.get(CFG.id).api = API;
}

Hooks.on('renderActorSheet', addShoppingBagButton);
Hooks.once('init', registerSettings);
Hooks.once('init', registerAPI);
