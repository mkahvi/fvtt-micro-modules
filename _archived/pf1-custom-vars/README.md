# Mana's Custom Variables for Pathfinder 1

![Supported Foundry Versions](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fversion%3Fstyle%3Dflat%26url%3Dhttps%3A%2F%2Fgitlab.com%2Fmkahvi%2Ffvtt-micro-modules%2F-%2Fraw%2Fmaster%2Fpf1-custom-vars%2Fmodule.json)

Allows defining custom variables for roll data in actors sheet settings tab.

Variables are accessible in formulas via `@var.VARNAME`.

![Config](./img/screencaps/config.png)

✅ Pretty good alternative to weird resource naming and using excess items for such.

## API

You can reset custom variables for an actor with the following:

```js
game.modules.get('mkah-pf1-custom-vars').api.resetActor(actor);
```

You can get the current variables simply with getFlag:

```js
actor.getFlag('mkah-pf1-custom-vars', 'vars');
```

Macros can alter the flags with actor.update or with setFlag:

```js
actor.update({ [`flags.mkah-pf1-custom-vars.vars.${varName}.formula`]: "1 + 2 + 3 + @quack" });
```

## Install

Manifest URL: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/master/pf1-custom-vars/module.json>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
