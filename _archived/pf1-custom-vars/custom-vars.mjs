const CFG = { id: 'mkah-pf1-custom-vars' };

let hookId;

/* global RollPF:readonly */

const templatePath = `modules/${CFG.id}/editor.hbs`;
/** @type {HandlebarsTemplateDelegate} */
let template;
Hooks.once('setup', () => getTemplate(templatePath).then(t => template = t));

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 */
function insertCustomVarEditor(sheet, [html]) {
	const actor = sheet.actor;
	if (!['character', 'npc'].includes(actor?.type)) return;

	const abl = html.querySelector('.tab.settings .ability-scores');
	if (!abl) return; // Alt sheet or something

	const rollData = actor.getRollData();

	const original = actor.getFlag(CFG.id, 'vars') ?? {};

	const variables = deepClone(original);
	for (const [key, value] of Object.entries(variables)) {
		value.error = { slug: false, formula: false };
		if (value.formula === '') value.error.formula = true;
		if (key === '' || key !== key.slugify({ strict: true })) delete variables[key];
	}

	async function attemptSave() {
		if (!sheet.isEditable) return;

		// Short circuit for deleting all vars
		if (Object.keys(variables).length == 0) {
			console.log('No variables left, cleaning up...');
			return actor.unsetFlag(CFG.id, 'vars');
		}

		let canSave = true;
		for (const v of Object.values(variables)) {
			canSave = !(v.error.slug || v.error.formula);
			if (v.error.slug) ui.notification.warn(`Bad slug: ${v.slug}`);
			if (v.formula.length === 0) {
				ui.notifications.info(`'${v.slug}' requires a formula to finish saving.`);
				canSave = false;
			}
			if (v.error.formula) ui.notifications.warn(`Bad formula: ${v.formula}`);
			if (!canSave) break;
		}

		if (canSave) {
			// ui.notifications.info("Can save variables");
			const vars = duplicate(variables);
			for (const v of Object.values(vars))
				delete v.error;

			const slugs = Object.keys(vars), oldslugs = Object.keys(original);
			// const diff = diffObject(original, vars);

			const deleteSlugs = [];
			for (const s of oldslugs) {
				if (!slugs.includes(s)) deleteSlugs.push(s);
			}
			if (deleteSlugs.length) {
				console.log('Custom Vars | Deleting:', deleteSlugs);
				const flat = flattenObject(vars);
				for (const s of deleteSlugs)
					flat[`-=${s}`] = null;
				const update = flattenObject({ [`flags.${CFG.id}.vars`]: flat });
				await actor.update(update);
			}
			else {
				await actor.setFlag(CFG.id, 'vars', vars);
			}
			console.log('Custom Vars saved:', vars);
		}
		else {
			ui.notifications.warn('Can NOT save variables');
		}
	}

	/**
	 * @param {Event} ev
	 */
	function deleteVariable(ev) {
		ev.preventDefault();
		const el = ev.target;
		if (el.dataset.disabled) return;
		const p = ev.target.closest('.custom-var'),
			id = p.dataset.variableId;
		if (id) delete variables[id];
		p.remove();

		attemptSave();
	}

	async function validateFormula(formula) {
		if (formula.length === 0) {
			ui.notifications.warn('Empty formula');
			return { result: false, total: 0 };
		}

		const roll = RollPF.safeRoll(formula, rollData);
		if (roll.err) {
			console.error(roll.err);
			ui.notifications.error(roll.err);
			return { result: false, total: 0 };
		}

		return { result: true, total: roll.total };
	}

	/**
	 * @param {Event} ev
	 */
	async function changeSlug(ev) {
		ev.preventDefault();
		const el = ev.target;

		const value = el.value,
			slug = value.slugify({ strict: true }).replace(/^\d+/, '');
		if (value !== slug) el.value = slug;

		const p = el.closest('.custom-var'),
			old = p.dataset.variableId;

		if (slug === '' || Object.keys(variables).includes(slug)) {
			el.classList.add('error');
			if (old) variables[old].error.slug = true;
			console.warn('Bad slug:', { slug, source: value, old });
			ui.notifications.warn(`Bad slug: ${slug}`);
			return;
		}

		el.classList.remove('error');

		const fel = p.querySelector('.formula');

		const data = old ? variables[old] : {
			formula: '',
			error: { slug: false, formula: false }
		};
		if (old) delete variables[old];
		else data.formula = fel?.value ?? '';

		data.error.slug = false;
		variables[slug] = data;
		p.dataset.variableId = slug;

		if (variables[slug].error.formula || data.formula.length === 0) {
			console.warn('Bad or missing formula');
			fel.classList.add('error');
			return;
		}
		else if (!old) {
			const { result, total } = await validateFormula(data.formula);
			if (!result) {
				console.warn('Bad formula:', data.formula, { total, result });
				fel.classList.add('error');
				return;
			}
			p.querySelector('.value').textContent = `${total}`;
		}

		attemptSave();
	}

	/**
	 * @param {Event} ev
	 */
	async function changeFormula(ev) {
		ev.preventDefault();
		const el = ev.target;

		const formula = el.value.trim(),
			p = el.closest('.custom-var'),
			id = p.dataset.variableId,
			old = variables[id];

		const { result, total } = await validateFormula(formula);

		if (!result) {
			el.setCustomValidity('Valid formula required');
			el.reportValidity();
			if (old) old.error.formula = true;
			return;
		}

		p.querySelector('.value').textContent = `${total}`;

		if (!old) {
			const slugEl = p.querySelector('.slug');
			slugEl.setCustomValidity('Variable slug required');
			slugEl.reportValidity();
			return;
		}

		el.classList.remove('error');

		old.formula = formula;
		old.error.formula = false;

		attemptSave();
	}

	const vars = [];
	for (const [key, v] of Object.entries(variables))
		vars.push({ slug: key, formula: v.formula });

	// Add empty var
	vars.push({ slug: '', formula: '', builtIn: true });

	const tdata = { vars, isEditable: sheet.isEditable };
	const t = template(tdata, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true });

	const div = document.createElement('div');
	div.innerHTML = t;
	const f = div.querySelector('.formulas');

	// Validate formulas
	for (const [k, v] of Object.entries(variables)) {
		const p = f.querySelector(`[data-variable-id="${k}"]`);
		if (!p) {
			// TODO: Delete this bad data?
			continue;
		}
		validateFormula(v.formula).then(rv => {
			if (rv.result)
				p.querySelector('.value').textContent = `${rv.total}`;
			else
				p.querySelector('.formula').classList.add('error');
		});
	}

	if (sheet.isEditable) {
		div.querySelectorAll('[data-action]')
			.forEach(el => {
				const action = el.dataset.action;
				switch (action) {
					case 'delete': el.addEventListener('click', deleteVariable); break;
					// case 'add': el.addEventListener('click', addFormulaClick); break;
				}
			});

		div.querySelectorAll('input')
			.forEach(el => {
				const it = el.dataset.customVarInput;
				switch (it) {
					case 'slug': el.addEventListener('change', changeSlug); break;
					case 'formula': el.addEventListener('change', changeFormula); break;
				}
			});
	}
	else {
		div.querySelectorAll('input')
			.forEach(el => el.readOnly = true);
	}

	abl.after(...div.childNodes);
}

function reOrderRollDataHooks() {
	if (Hooks.events['pf1GetRollData'].length == 1) return;
	const old = Hooks.events['pf1GetRollData'].findSplice(h => h.id == hookId);
	if (old) Hooks.events['pf1GetRollData'].push(old);
}

/**
 * @param {Actor|Item} doc
 * @param {object} result
 * @param {boolean} refresh
 */
function getRollDataWithCustomVars(doc, result) {
	// if (!refresh) return; // skipping on non-refresh causes the data to be lost
	if (!(doc instanceof Actor)) return; // don't do anything with items

	const vars = doc.getFlag(CFG.id, 'vars') ?? {};
	result.var = {};

	try {
		for (const [key, entry] of Object.entries(vars)) {
			entry.total ??= RollPF.safeRoll(entry.formula, result).total;
			result.var[key] = entry.total;
		}
	}
	catch (err) {
		console.error(err);
	}
}

hookId = Hooks.on('pf1GetRollData', getRollDataWithCustomVars);
Hooks.on('renderActorSheetPF', insertCustomVarEditor);
Hooks.once('ready', () => {
	game.modules.get(CFG.id).api = {
		resetActor: (actor) => actor.unsetFlag(CFG.id, 'vars')
	}

	reOrderRollDataHooks();
});
