# Change Log

## 0.5.0

- Fix: PF1 v9 compatibility.

## 0.4.0.1

- Fix errors with unsupported sheets.

## 0.4.0

- Foundry v10 and PF1 0.82.x compatibility

## 0.3.0

- Transiton to HBS templating.
- Fix for unlinked actors gaining `slug` and `formula` keys in their actorData.

## 0.2.1.1

- Fix for uneditable sheets.

## 0.2.1

- API: `resetActor(actor)` for clearing custom vars.

## 0.2

- Foundry v9 compatibility
- PF1 compatibilty update

## 0.1 Initial
