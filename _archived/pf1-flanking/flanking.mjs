const getSystemData = (doc) => game.release.generation >= 10 ? doc.system : doc.data.data;
const getDocData = (doc) => game.release.generation >= 10 ? doc : doc.data;

// Adapted from: https://github.com/davidfig/intersects/blob/master/box-box.js (License: MIT)
function boxIntersection(box1, box2) {
	return box1.x < box2.x + box2.w && box1.x + box1.w > box2.x && box1.y < box2.y + box2.h && box1.y + box1.h > box2.y;
}

// Get tokens within rectangle.
function getTokensWithin(box, { ignoreIds = [] } = { }) {
	return canvas.tokens.placeables.filter(t => {
		if (!t.visible) return false;
		if (ignoreIds.includes(t.id)) return false;
		return boxIntersection(box, t);
	});
}

let searchBoxG = null;

/**
 * @param {Token} token
 * @param {boolean} hovering
 */
function hoverTokenEvent(token, hovering) {
	if (!hovering) {
		searchBoxG.clear();
		return;
	}

	const scene = token.scene;

	const grid = getDocData(scene).size;

	const adjacentBox = {
		h: token.h + grid * 2,
		w: token.w + grid * 2,
		x: token.position.x - grid,
		y: token.position.y - grid,
	};

	// Visual
	searchBoxG.lineStyle(3, 0xBAFF00, 0.7)
		.drawRect(adjacentBox.x, adjacentBox.y, adjacentBox.w, adjacentBox.h);

	// Actual search
	const tokens = getTokensWithin(adjacentBox, { ignoreIds: [token.id] });

	if (tokens.length === 0) {
		console.log('No tokens in search box, bailing.');
		return;
	}

	console.log('Tokens within search box:', tokens.map(t => t.name));
}

Hooks.on('hoverToken', hoverTokenEvent);

Hooks.on('canvasReady', () => {
	searchBoxG = new PIXI.Graphics();
	canvas.controls.addChild(searchBoxG);
});

function clearCachedData(actor) {
	// delete actor.___flanking;
}

Hooks.on('preUpdateActor', clearCachedData);

/**
 * Returns minimum and maximum range of all valid attacks for the actor.
 *
 * @returns {object}
 */
function getThreatenedArea() {
	const min = 0;
	let max = 5;
	for (const atk of this.itemTypes.attack) {
		if (!atk.hasRange) continue;
		// Ignore ranged and spells
		if (['rwak', 'msak', 'rsak', 'rcman'].includes(getSystemData(atk).actionType)) continue;

		// if (rd.minUnits === "") {}
		console.log(atk.name, atk.range);

		max = Math.max(max, atk.range);
	}

	console.log('Threatened area:', { min, max });

	return { min, max };
}

Hooks.once('ready', () => {
	game.pf1.entities.ActorPF.prototype.getThreatenedArea = getThreatenedArea;
});
