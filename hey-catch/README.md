# Hey, Catch!

![Supported Foundry Versions](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fversion%3Fstyle%3Dflat%26url%3Dhttps%3A%2F%2Fgitlab.com%2Fmkahvi%2Ffvtt-micro-modules%2F-%2Fraw%2Fmaster%2Fhey-catch%2Fmodule.json)

Allows drag & dropping items onto tokens.

Basic safeguards are included. No adding items to stacked tokens, tho optionally you can enable trying to add to the topmost token. Also dropping items to the same actor are stopped.

You can also disable the basic missed drop notification in case you use a module that makes such drops valid.

✅ Recommended for general use

## Install

Manifest URL: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/master/hey-catch/module.json>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
