const CFG = {
	module: 'hey-catch',
	SETTINGS: {
		missedDrop: 'missedDrop',
		pilemaster: 'pilemaster'
	}
};

const getDocData = (doc) => game.release.generation >= 10 ? doc : doc.data;

function sortTokens(a, b) {
	if (game.release.generation >= 11)
		return b.mesh.sort - a.mesh.sort;
	else
		return b.zIndex - a.zIndex;
}

/**
 * @param {Actor} actor
 * @param {Item} item
 */
function addItem(actor, item) {
	if (item)
		return actor.createEmbeddedDocuments('Item', [item]);
	else
		return ui.notifications.warn('HeyCatch.NoItemData', { localize: true });
}

/**
 * Find token(s) at location
 * Reference used: PlaceablesLayer.selectObjects
 *
 * @param {number} x
 * @param {number} y
 * @returns {Token[]}
 */
function findTokens(x, y) {
	const controllable = canvas.tokens.placeables.filter(obj => obj.visible && obj.actor && obj.control instanceof Function);
	const v10 = game.release.generation >= 10;
	return controllable.filter(obj => {
		const w = v10 ? obj.w : obj.width,
			h = v10 ? obj.h : obj.height;
		return Number.between(x, obj.x, obj.x + w) && Number.between(y, obj.y, obj.y + h);
	});
}

const sortTargets = (targets, sourceActor, sourceToken) => targets
	.sort(sortTokens)
	.reduce((arr, t) => {
		if (sourceToken && sourceToken === t.id)
			ui.notifications.warn('HeyCatch.SelfDrop', { localize: true });
		else if (sourceActor && getDocData(t).actorLink && t.actor.id === sourceActor)
			ui.notifications.warn('HeyCatch.SelfDrop', { localize: true });
		else
			arr.push(t);
		return arr;
	}, []);

/**
 * Find valid drop targets near x/y coords
 *
 * @param {number} x
 * @param {number} y
 * @param {object} options
 * @param {string} options.sourceActor Actor ID
 * @param {string} options.sourceToken Token ID
 * @returns
 */
function findDropTarget(x, y, { sourceActor, sourceToken } = {}) {
	const found = findTokens(x, y);

	if (found.length === 0) {
		// This might be intentional in case of other modules allowing dropping items onto bare canvas.
		const missed = game.settings.get(CFG.module, CFG.SETTINGS.missedDrop);
		if (missed) ui.notifications.warn('HeyCatch.NoDropTarget', { localize: true });
		return;
	}

	// console.log('Dropped On:', token.name, token);

	const targets = sortTargets(found, sourceActor, sourceToken);

	if (targets.length === 0)
		return void ui.notifications.warn('HeyCatch.NoActor', { localize: true });

	const target = targets[0];

	// Make sure only one token is there to avoid mistakes
	if (targets.length > 1) {
		const allowStacked = game.settings.get(CFG.module, CFG.SETTINGS.pilemaster);
		if (allowStacked) {
			// Allow "topmost" for drop
			const v11 = game.release.generation >= 11;
			if ((v11 && target.document.sort == 0) || (!v11 && target.zIndex == 0))
				return void ui.notifications.warn('HeyCatch.NoTopmost', { localize: true });
		}
		else
			return void ui.notifications?.warn('HeyCatch.NoStacked', { localize: true });
	}

	return target?.actor;
}

/**
 * v10 compatible drop handler
 *
 * @param {Canvas} canvas
 * @param {object} dropData
 */
function catchDrop(canvas, dropData) {
	const { type, x, y, uuid } = dropData;
	if (type !== 'Item') return;
	const item = fromUuidSync(uuid);

	let getItem = async () => item;
	if (item instanceof Item) {
		// NOP
	}
	else if (item._id !== undefined && item.pack?.length) {
		const { pack: packId, _id: itemId } = item;
		console.log({ packId, itemId });
		getItem = async () => game.packs.get(packId).getDocument(itemId);
	}
	else {
		console.error('Hey, Catch! | Weird Drop:', dropData);
		return void ui.notifications.error('HeyCatch.UnknownError', { localize: true, console: false });
	}

	const sourceActor = item?.actor?.id,
		sourceToken = /(?:^|\.)Token\.(?<id>[a-zA-Z\d]{16})(?:\.|$)/.exec(uuid)?.groups.id;

	const actor = findDropTarget(x, y, { sourceActor, sourceToken });
	if (!actor) return;

	// TODO: Could add support for asking GM to validate drop onto unowned actor?
	if (!actor?.isOwner) return void ui.notifications.warn('HeyCatch.NotOwner', { localize: true });

	getItem().then(i => addItem(actor, i));
	return false; // Block other modules handling this
}

/**
 * v9 and older drop handler
 *
 * @param {Canvas} canvas
 * @param {object} dropData
 */
function catchDropOld(canvas, dropData) {
	const { id: itemId, x, y, pack: packId, type, actorId, sceneId, tokenId, data: itemData } = dropData;
	if (type !== 'Item') return;

	let getItem, sourceActor, sourceToken;

	if (packId) {
		// Compendium
		getItem = async function getItemFromPack() {
			const pack = game.packs.get(packId);
			return pack.getDocument(itemId).then(i => i?.toObject());
		}
	}
	else if (itemId) {
		// Items Directory
		getItem = async function getItemFromDirectory() {
			return game.items.get(itemId)?.toObject();
		}
	}
	else if (actorId) {
		// From actor
		if (tokenId) {
			const scene = game.scenes.get(sceneId);
			sourceToken = scene?.tokens.get(tokenId);
			sourceActor = sourceToken?.actor;
		}
		else {
			sourceActor = game.actors.get(actorId);
		}

		getItem = async function getItemFromActor() {
			// return sourceActor.items.get(itemId);
			return itemData;
		}

		// console.log('SourceActor:', sourceActor, sourceToken);
	}
	else {
		console.error('Weird drop:', dropData);
		return void ui.notifications.error('HeyCatch.UnknownError', { localize: true, console: false });
	}

	const actor = findDropTarget(x, y, { sourceActor, sourceToken });
	if (!actor) return;

	// TODO: Could add support for asking GM to validate drop onto unowned actor?
	if (!actor?.isOwner) return void ui.notifications.warn('HeyCatch.NotOwner', { localize: true });

	getItem().then(item => addItem(actor, item));
	return false; // Block other modules handling this
}

Hooks.once('init', () => {
	Hooks.on('dropCanvasData', game.release.version >= 10 ? catchDrop : catchDropOld);

	game.settings.register(CFG.module, CFG.SETTINGS.missedDrop, {
		name: 'HeyCatch.MissedDrop',
		hint: 'HeyCatch.MissedDropHint',
		type: Boolean,
		default: true,
		scope: 'world',
		config: true,
	});

	// TODO: Allow option to add the item to _all_ of the actors in the stack?
	game.settings.register(CFG.module, CFG.SETTINGS.pilemaster, {
		name: 'HeyCatch.AllowStacks',
		hint: 'HeyCatch.AllowStacksHint',
		type: Boolean,
		default: false,
		scope: 'client',
		config: true,
	});
});
