/**
 * @param {SceneConfig} app
 * @param {JQuery<HTMLElement>} param1
 * @param {object} options
 */
function renderSceneConfig(app, [html], options) {
	console.log('SCENE CONFIG', options);

	const scene = app.document;

	const labels = {
		dimensions: 'SCENES.Dimensions',
		units: 'SceneConfig.Units',
		width: 'Width',
		height: 'Height',
	};

	let oldX, oldY;

	Object.entries(labels).forEach(([key, value]) => labels[key] = game.i18n.localize(value));

	const div = document.createElement('div');
	div.innerHTML = `
	<div class="form-group mana-scene-config">
		<label>
			${labels.dimensions}
			<span class="units">(${labels.units})</span>
		</label>
		<div class="form-fields">
			<label>${labels.width}</label>
			<input type="number" step="1" min="1" class="grid-size-x" placeholder="X">
			<label>${labels.height}</label>
			<input type="number" step="1" min="1" class="grid-size-y" placeholder="Y">
		</div>
	</div>
	`;

	const tab = html.querySelector('.tab[data-tab="grid"]');
	const pxEl = tab.querySelector('input[name="grid.size"]');
	const getSize = () => pxEl.valueAsNumber;

	const xCEl = div.querySelector('input.grid-size-x');
	const yCEl = div.querySelector('input.grid-size-y');

	const xPxEl = tab.querySelector('input[name="width"]');
	const yPxEl = tab.querySelector('input[name="height"]');

	function changeXCells(ev) {
		const x = ev.target.valueAsNumber;
		const size = getSize();
		xPxEl.value = x * size;
		if (app.linkedDimensions) {
			const y = Math.round(yCEl.valueAsNumber * (x / oldX));
			yCEl.value = y;
			yPxEl.value = y * size;
			oldY = y;
		}
		oldX = x;
	}
	function changeYCells(ev) {
		const y = ev.target.valueAsNumber;
		yPxEl.value = y * getSize();
		if (app.linkedDimensions) {
			const x = Math.round(xCEl.valueAsNumber * (y / oldY));
			xCEl.value = x;
			xPxEl.value = x * size;
			oldX = x;
		}
		oldY = y;
	}
	function changeXPixels(ev) {
		const x = ev.target.valueAsNumber;
		xCEl.value = x / getSize();
	}
	function changeYPixels(ev) {
		const y = ev.target.valueAsNumber;
		yCEl.value = y / getSize();
	}

	function changeGridSize(ev) {
		const size = ev.target.valueAsNumber;
		xPxEl.value = xCEl.valueAsNumber * size;
		yPxEl.value = yCEl.valueAsNumber * size;
	}

	const size = scene.grid.size;
	oldX = scene.width / size;
	oldY = scene.height / size;
	xCEl.value = oldX;
	yCEl.value = oldY;

	pxEl.addEventListener('change', changeGridSize);
	xCEl.addEventListener('change', changeXCells);
	yCEl.addEventListener('change', changeYCells);
	xPxEl.addEventListener('change', changeXPixels);
	yPxEl.addEventListener('change', changeYPixels);

	const gsEl = pxEl.closest('.form-group');

	gsEl.after(...div.children);
}

Hooks.on('renderSceneConfig', renderSceneConfig);
