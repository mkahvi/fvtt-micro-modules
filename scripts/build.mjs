import path from 'path';
import fs from 'fs';
import Bun from 'bun';

const ignore = [
	/^node_modules$/,
	/^stub$/,
	/^\.git$/,
	/^\.vscode$/,
	/^scripts$/,
];

function readManifest(folder) {
	try {
		const packData = fs.readFileSync(path.resolve(folder, 'module.json'));
		const packJSON = JSON.parse(packData);
		const [mainFile] = packJSON.esmodules;
		return [packJSON.title, mainFile];
	}
	catch (err) {
		return [];
	}
}

async function build(folder, unbundled, bundled) {
	console.log(folder);
	const res = await Bun.build({
		entryPoints: [path.resolve(folder, unbundled)],
		naming: '[dir]/[name].min.mjs',
		outdir: folder,
		format: 'esm',
		sourcemap: 'linked',
		minify: {
			whitespace: true,
			identifiers: false,
			syntax: true,
		},
	});

	if (res.success)
		console.log('> size:', Math.floor(res.outputs[0].size / 1_000), 'kB');
	else
		console.error(res);
}

// build();

const folders = fs.readdirSync('.', { withFileTypes: true, encoding: 'utf-8' }).reduce((folders, entry) => {
	if (entry.isDirectory()) {
		for (const ign of ignore) {
			const re = ign.exec(entry.name);
			if (re) return folders;
		}
		folders.push(entry.name);
	}
	return folders;
}, []);

for (const folder of folders) {
	const [title, bundled] = readManifest(folder);
	if (!bundled || !title) continue;
	if (!/\.min\./.test(bundled)) continue;
	const unbundled = bundled.replace(/\.min\./, '.');

	await build(folder, unbundled, bundled);
}
