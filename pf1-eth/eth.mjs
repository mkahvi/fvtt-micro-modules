const CFG = {
	id: 'pf1-eth',
	SETTINGS: {
		defenses: 'defenses',
	}
}

async function promptValue(title, value = 0, token) {
	return Dialog.wait({
		title: game.i18n.format('EnhancedTokenHUD.EditTitle', { type: title }),
		content: `<input type="number" value="${value}"><hr>`,
		buttons: {
			reset: {
				label: game.i18n.localize('EnhancedTokenHUD.Reset'),
				icon: '<i class="fa-solid fa-rotate-left"></i>',
				callback: () => false,
			},
			save: {
				label: game.i18n.localize('EnhancedTokenHUD.Save'),
				icon: '<i class="fa-solid fa-floppy-disk"></i>',
				callback: (html) => html.querySelector('input').valueAsNumber,
			}
		},
		default: 'save',
		close: () => null,
	},
	{
		width: 280,
		jQuery: false,
		classes: [...Dialog.defaultOptions.classes, 'pf1-eth', 'get-number'],
	});
}

/**
 * @param {Event} event
 * @param {TokenDocument} token
 * @param {TokenHUD} app
 */
async function onClick(event, token, app) {
	event.preventDefault();

	const { type } = event.target.dataset;

	const isPcp = type === 'perception';
	const isSnk = type === 'stealth';

	let label;
	if (isPcp) {
		label = game.i18n.localize('EnhancedTokenHUD.Perception');
	}
	else if (isSnk) {
		label = game.i18n.localize('EnhancedTokenHUD.Stealth');
	}
	else return;

	const check = token.getFlag('pf1-eth', `checks.${type}`);

	const buttons = {
		reset: {
			label: game.i18n.localize('EnhancedTokenHUD.Reset'),
			icon: '<i class="fa-solid fa-rotate-left"></i>',
			callback: () => false,
		},
		set: {
			label: game.i18n.localize('EnhancedTokenHUD.Adjust'),
			icon: '<i class="fa-solid fa-pen-nib"></i>',
			callback: () => promptValue(label, check?.result, token),
		}
	};

	if (!game.user.isGM) delete buttons.set;
	if (!check) delete buttons.reset;

	if (Object.keys(buttons).length === 0) return;

	const result = await Dialog.wait({
		title: `${label} – ${token.name}`,
		content: game.i18n.localize('EnhancedTokenHUD.Prompt'),
		buttons,
		close: () => null,
		default: 'reset',
	},
	{
		width: 360,
		jQuery: false,
		classes: [...Dialog.defaultOptions.classes, 'pf1-eth', 'adjust-check'],
	});

	if (result === null) return;
	if (result !== false && !Number.isFinite(result)) return void console.log('ETH | Invalid value from prompt:', result);

	switch (type) {
		case 'perception':
			if (result === false)
				token.unsetFlag('pf1-eth', 'checks.perception');
			else
				token.setFlag('pf1-eth', 'checks.perception.result', result);
			break;
		case 'stealth':
			if (result === false)
				token.unsetFlag('pf1-eth', 'checks.stealth');
			else
				token.setFlag('pf1-eth', 'checks.stealth.result', result);
			break;
	}
}

/**
 * @param {TokenHUD} app
 * @param {JQuery<HTMLElement>} html
 * @param {object} options
 */
function enhanceHUD(app, [html], options) {
	const left = html.querySelector('.col.left');

	const token = app.object.document;
	const actor = token.actor;
	if (!actor) return;

	if (!actor.testUserPermission(game.user, 'OBSERVER')) return;

	const isGM = game.user.isGM;

	const checks = token.getFlag('pf1-eth', 'checks') ?? {};
	const pcpCheck = (checks?.perception?.visible || isGM) ? (checks?.perception?.result ?? null) : null;
	const snkCheck = (checks?.stealth?.visible || isGM) ? (checks?.stealth?.result ?? null) : null;
	const attribs = actor.system.attributes ?? {};
	const acs = attribs?.ac ?? {};
	const acValue = acs.normal?.total ?? null;
	const tacValue = acs.touch?.total ?? null;
	const cmdValue = attribs.cmd?.total ?? null;
	const dexDenied = (acs.flatFooted?.total ?? acValue) - acValue;

	const div = document.createElement('div');
	div.classList.add('pf1-enhanced-hud');

	function createEntry(type, selector, icon, value = null, signed = false, tooltip) {
		const sh = document.createElement('div');
		sh.classList.add('control-icon', selector);
		if (signed && Number.isFinite(value)) value = Intl.NumberFormat('nu', { signDisplay: 'always' }).format(value);
		sh.innerHTML = `<i class="fa-solid fa-fw fa-${icon}"></i><span class='value'>${value ?? '– '}</span>`;
		sh.dataset.tooltip = tooltip;
		sh.dataset.type = type;
		return sh;
	}

	if (game.settings.get(CFG.id, CFG.SETTINGS.defenses)) {
		const ac = createEntry('armor', 'normal-armor', 'shield-halved', acValue, false, 'EnhancedTokenHUD.Armor');
		const tac = createEntry('touch', 'touch-armor', 'hand', tacValue, false, 'EnhancedTokenHUD.Touch');
		const cmd = createEntry('cmd', 'maneuver-defense', 'hands-bubbles', cmdValue, false, 'EnhancedTokenHUD.CMD');

		const dex = createEntry('ff', 'dex-denied', 'user-injured', dexDenied, true, 'EnhancedTokenHUD.DexDenied');

		div.append(ac, tac, cmd, dex);
	}

	const pcp = createEntry('perception', 'perception', 'eye', pcpCheck, false, 'EnhancedTokenHUD.Perception');
	const snk = createEntry('stealth', 'stealth', 'user-ninja', snkCheck, false, 'EnhancedTokenHUD.Stealth');

	div.append(pcp, snk);

	div.addEventListener('click', event => onClick(event, token, app));

	left.append(div);
}

/**
 * @param {ChatMessage} cm
 * @param {object} context
 * @param {string} userId
 */
function onChatMessage(cm, context, userId) {
	if (!game.users.activeGM?.isSelf) return;

	const actor = ChatMessage.getSpeakerActor(cm.speaker);
	if (!actor?.isOwner) return;

	const subject = cm.getFlag('pf1', 'subject');
	if (!['per', 'ste'].includes(subject?.skill)) return;
	const isPcp = subject.skill === 'per';
	const isSnk = subject.skill === 'ste';

	let token = actor.token ?? game.scenes.get(cm.speaker?.scene)?.tokens.get(cm.speaker?.token);
	if (!token) {
		const tokens = actor?.getActiveTokens(true, true) ?? [];
		if (tokens.length > 1) return void console.log('ETH | Too many tokens');
		token = tokens[0];
	}
	if (!token) return void console.log('ETH | No token found!');

	const result = cm.rolls[0]?.total;
	if (!Number.isFinite(result)) return void console.log('ETH | Invalid check result:', result);

	const visible = !cm.blind;

	const data = {};
	if (isPcp) data.perception = { result, visible };
	if (isSnk) data.stealth = { result, visible };

	token.setFlag('pf1-eth', 'checks', data);
}

function hoverToken(token, hovering) {
	if (!token.actor?.isOwner) return;
}

function registerSettings() {
	game.settings.register(CFG.id, CFG.SETTINGS.defenses, {
		name: 'EnhancedTokenHUD.DisplayDefenses',
		type: Boolean,
		default: true,
		config: true
	});
}

Hooks.on('renderTokenHUD', enhanceHUD);
Hooks.on('init', registerSettings);

Hooks.on('createChatMessage', onChatMessage);

// Hooks.on('hoverToken', hoverToken);
