# Enhanced Token HUD for for Pathfinder 1e

![Supported Foundry Versions](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fversion%3Fstyle%3Dflat%26url%3Dhttps%3A%2F%2Fgitlab.com%2Fmkahvi%2Ffvtt-micro-modules%2F-%2Fraw%2Fmaster%2Fpf1-eth%2Fmodule.json)

Adds display of the following to token HUD:

- Last perception check
- Last stealth check
- Optional:
  - Current values of AC, Touch AC, and CMD and dex denied modifier

Clicking the perception/stealth display prompts for resetting it or setting it to some specific value (only by GM).

## Screencaps

![HUD](./hud.png)

## API

None provided

### Example macros

**Reset selected tokens**

```js
canvas.tokens.controlled.map(t => t.document)
  .forEach(t => t.unsetFlag("pf1-eth", "checks"));
```

## Install

Manifest URL: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/master/pf1-eth/module.json>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
