const CFG = {
	module: 'pf1-quick-actions',
	SETTINGS: {
		noDefault: 'noDefault',
		contextEdit: 'contextEdit',
	},
};

// Re-implement system function
const getSkipActionPrompt = function () {
	return (
		game.settings.get('pf1', 'skipActionDialogs') && !pf1.skipConfirmPrompt ||
		!game.settings.get('pf1', 'skipActionDialogs') && pf1.skipConfirmPrompt
	);
};

/**
 * @param {string} node
 * @param {string | null} text
 * @param {string[]} classes
 * @param {object} options
 * @param {string | null} options.title
 * @param {boolean} options.isHTML
 * @param {Element[]} options.children
 * @param {object} [options.data] Object mapping for data-${key}=${value} values.
 * @param {object} [options.attr] Object mapping for arbitrary attributes.
 * @param {string} [options.id]
 * @returns {Element}
 */
export function createNode(node = 'span', text = null, classes = [], { title = null, isHTML = false, children = [], data, attr, id } = {}) {
	const n = document.createElement(node);
	if (text) {
		if (isHTML) n.innerHTML = text;
		else n.textContent = text;
	}
	if (id) n.id = id;
	if (classes.length) n.classList.add(...classes.filter(c => !!c));
	if (title) n.title = title;
	if (children.length) n.append(...children.filter(c => !!c));
	if (data) {
		for (const [key, value] of Object.entries(data))
			n.setAttribute(`data-${key}`, value);
	}
	if (attr) {
		for (const [key, value] of Object.entries(attr)) {
			if (value !== undefined)
				n.setAttribute(key, value);
		}
	}
	return n;
}

/**
 * @param {Element} parent
 * @param {Element} tip
 */
function setTooltipPosition(parent, tip) {
	const p = parent.getBoundingClientRect();
	const t = tip.getBoundingClientRect();
	const top = p.bottom - t.height;
	const left = p.right - t.width;
	tip.style.cssText += `top:${top}px;left:${left}px;`;
}

/**
 * @param {ItemAction} action
 * @param {string} labelOverride
 * @returns {HTMLElement}
 */
function makeActionElement(action, labelOverride) {
	return createNode('li', null, ['action'], {
		children: [
			createNode('span', null, ['image'], {
				children: [createNode('img', null, [], { attr: { src: action.img } })],
			}),
			createNode('span', labelOverride || action.name, ['name']),
		],
		data: {
			'action-id': action.id,
		},
	});
}

// Temporarily hide the tooltip
function hideTooltip(tip) {
	tip.classList.add('fading');
	setTimeout(() => tip.remove(), 270);
}

/**
 * @param {Event} ev
 * @param {Item} item
 * @param {Element} el
 */
function createTooltip(ev, item, el) {
	const actions = item.actions;
	if (!actions) return;

	const localize = game.i18n.localize.bind(game.i18n);

	const actionEls = actions.map(a => makeActionElement(a));

	if (!game.settings.get(CFG.module, CFG.SETTINGS.noDefault)) {
		actionEls.push(
			createNode('hr', null, ['span-2']),
			makeActionElement(item.defaultAction, localize('PF1.Default')),
		);
	}

	const tip = createNode('aside', null, ['action-quick-selection'], {
		// id: 'action-quick-selection',
		children: [
			createNode('h2', localize('PF1.ActionPlural')), createNode('ul', null, ['action-list'], {
				children: actionEls,
			}),
		],
	});

	// document.getElementById('action-quick-selection');
	document.body.append(tip);

	setTooltipPosition(el, tip);

	// Move out listener
	tip.addEventListener('mouseleave', () => hideTooltip(tip), { passive: true, once: true });

	tip.addEventListener('contextmenu', (ev) => {
		ev.preventDefault();
		ev.stopPropagation();

		if (game.settings.get(CFG.module, CFG.SETTINGS.contextEdit)) {
			const el = ev.target.closest('.action-list .action[data-action-id]');
			const actionId = el?.dataset.actionId;
			const aid = item.actions.get(actionId);
			if (!aid) return void console.warn(`Could not find action by ID "${actionId}"`);
			const asheet = aid.sheet ?? new game.pf1.applications.ItemActionSheet(aid);
			asheet?.render(true, { focus: true });
		}

		hideTooltip(tip);
	}, { once: true });

	tip.addEventListener('click', ev => {
		ev.preventDefault();
		ev.stopPropagation();

		const el = ev.target.closest('.action-list .action[data-action-id]');

		hideTooltip(tip);

		const actionId = el?.dataset.actionId;
		const action = item.actions.get(actionId);
		if (!action) return void console.warn(`Could not find action by ID "${actionId}"`);
		action.use({ skipDialog: getSkipActionPrompt() });
	}, { once: true });
}

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} jq
 * @param {object} options
 */
function injectActionHoverListeners(sheet, [html], options) {
	const actor = sheet.actor;

	// const tip = createNode('span', null, ['action-quick-selection']);
	// html.append(tip);

	html.querySelectorAll('.item-list .item[data-item-id]')
		.forEach(el => {
			const item = actor.items.get(el.dataset.itemId);
			if (!(item?.system.actions?.length > 1)) return;
			if (['class', 'container', 'loot'].includes(item.type)) return;

			const actionEl = el.querySelector('.item-detail.item-actions');
			if (!actionEl) return;

			actionEl.addEventListener('mouseenter', (ev) => createTooltip(ev, item, actionEl), { passive: true });
		});
}

Hooks.on('renderActorSheet', injectActionHoverListeners);

Hooks.once('init', function registerSettings() {
	game.settings.register(
		CFG.module,
		CFG.SETTINGS.noDefault,
		{
			name: 'PF1QuickActions.Settings.NoDefault.Label',
			hint: 'PF1QuickActions.Settings.NoDefault.Hint',
			type: Boolean,
			scope: 'client',
			default: false,
			config: true,
		},
	);

	game.settings.register(
		CFG.module,
		CFG.SETTINGS.contextEdit,
		{
			name: 'PF1QuickActions.Settings.ContextEdit.Label',
			hint: 'PF1QuickActions.Settings.ContextEdit.Hint',
			type: Boolean,
			scope: 'client',
			default: false,
			config: true,
		},
	);
});
