# Change Log

## 0.4.1.1

- Fix: Actions could not be used.

## 0.4.1

- ⚠️ PF1v10 compatibility, minimum required version increased to v10.

## 0.4

- Change: Tooltip handling redesigned.

## 0.3

- ⚠️ PF1v9 compatibility, minimum required version increased to v9.

## 0.2.0.2

- Fix: Skip action prompt.

## 0.2

- Foundry v10 compatibility

## 0.1 Initial
