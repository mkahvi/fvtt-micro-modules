/**
 * @param {DragEvent} event
 */
function startDrag(event) {
	const uuid = event.target.dataset.uuid;
	const doc = fromUuidSync(uuid);
	const dragData = doc.toDragData();

	// Include entire data if shift is held
	if (event.shiftKey && !('data' in dragData)) {
		dragData.data = doc.toObject();
	}

	event.dataTransfer.setData('text/plain', JSON.stringify(dragData));
}

function createMarker(doc) {
	const mark = document.createElement('div');
	mark.classList.add('link-drag-mark');
	mark.innerHTML = '<i class="fa-solid fa-passport fa-fw"></i>';
	mark.dataset.tooltip = game.i18n.localize('ItemDragMark.Tooltip');
	mark.dataset.uuid = doc.uuid;
	mark.draggable = true;
	mark.addEventListener('dragstart', startDrag);
	return mark;
}

/**
 * @param {ItemSheet} sheet
 * @param {JQuery<HTMLElement>} html
 */
function addSheetDragMark(sheet, [html]) {
	if (!html.classList.contains('sheet')) return;
	const mark = createMarker(sheet.object);
	html.append(mark);
}

/**
 * @param {ItemSheet} sheet
 * @param {JQuery<HTMLElement>} html
 */
function addJournalPageDragMark(sheet, [html]) {
	const el = html.closest('.window-content .journal-entry-content');
	if (el) {
		const mark = createMarker(sheet.object);
		el.append(mark);
	}
}

Hooks.on('renderItemSheet', addSheetDragMark);
Hooks.on('renderActorSheet', addSheetDragMark);

Hooks.on('renderJournalSheet', addSheetDragMark);
Hooks.on('renderJournalPageSheet', addJournalPageDragMark);

Hooks.on('renderCardsConfig', addSheetDragMark);

Hooks.on('renderRollTableConfig', addSheetDragMark);

// Hooks.on('renderSceneConfig', addSheetDragMark); // Not particularly meaningful
