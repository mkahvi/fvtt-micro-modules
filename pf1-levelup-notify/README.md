# Pathfinder 1 Levelup Notify

![Supported Foundry Versions](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fversion%3Fstyle%3Dflat%26url%3Dhttps%3A%2F%2Fgitlab.com%2Fmkahvi%2Ffvtt-micro-modules%2F-%2Fraw%2Fmaster%2Fpf1-levelup-notify%2Fmodule.json)

Notifies the GM in chat about available level-up and renders an icon on tokens also.

![Chat Card](./img/screencaps/chatcard.png)

![Token](./img/screencaps/token.png)

☐ Can be great for GMs who themselves level up NPCs with experience tracking. Otherwise not that useful.

## Configuration

Not available.

## Limitations

None known.

## Install

Manifest URL: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/master/pf1-levelup-notify/module.json>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).

## Credits

[Hades symbol](https://game-icons.net/1x1/delapouite/hades-symbol.html) icon  
by Delapouite under CC BY 3.0  
from [game-icons.net](https://game-icons.net/1x1/delapouite/hades-symbol.html)
