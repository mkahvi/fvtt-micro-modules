const CFG = { module: 'mkah-pf1-levelup-notify' };

const getSystemData = (doc) => game.release.generation >= 10 ? doc.system : doc.data.data;
const getDocData = (doc) => game.release.generation >= 10 ? doc : doc.data;
const getPrefix = () => game.release.generation >= 10 ? 'system' : 'data';

const templatePath = `modules/${CFG.module}/levelup-notify.hbs`;
let template;
Hooks.once('ready', async () => getTemplate(templatePath).then(t => template = t));

const lastNotify = {};

const levelupImg = `modules/${CFG.module}/img/hades.svg`;

let debug = false; // updated when rendering icons

function hasIcon(token) {
	return token.children.find(i => i.levelUpMarker);
}

async function setIcon(token) {
	if (hasIcon(token)) return;
	const img = await loadTexture(levelupImg);
	const size = canvas.grid.size;
	img.orig = { height: size, width: size, x: size / 2, y: size / 2 };
	const sprite = new PIXI.Sprite(img);
	sprite.anchor.set(0.5); // center
	sprite.scale.set(getDocData(token).height * 0.75);
	const ch = token.addChild(sprite);
	ch.x = token.w * 0.92;
	ch.y = token.h * 0.76;

	// easy identification
	img.levelUpMarker = true;
	ch.levelUpMarker = true;
}

async function removeIcon(token) {
	// console.log("REMOVING ICON:",token.actor.name);
	const icon = hasIcon(token);
	if (icon) token.removeChild(icon);
}

function canLevelUp(actor, currentLevel, { quiet = false } = {}) {
	if (!actor) return;
	const actorData = getSystemData(actor);
	const level = currentLevel ?? actorData.attributes.hd.total;
	const nextLevelXp = actor.getLevelExp(level);
	if (!quiet && debug) console.warn('canLevelUp?:', actor.name, actorData.details.xp.value, '>=', nextLevelXp, 'to', level + 1, actorData.details.xp.value >= nextLevelXp);
	return actorData.details.xp.value >= nextLevelXp;
}

// eslint-disable-next-line no-shadow
function renderLevelupIcon(canvas, actor, currentLevel) {
	if (!game.user.isGM) return;
	debug = game.modules.get(CFG.module).debug;

	const tokens = canvas.tokens.placeables
		.filter(t => getDocData(t).actorLink); // linked tokens

	for (const t of tokens) {
		if (actor && getDocData(t).actorId !== actor.id) continue;

		if (canLevelUp(t.actor, currentLevel)) setIcon(t);
		else removeIcon(t);
	}
}

function cardPrinter(actor, data) {
	if (lastNotify[actor.id]?.level === data.upToLevel) return; // spam blocker
	lastNotify[actor.id] = { level: data.upToLevel }

	data.levelupMessage = game.i18n.format('MKAh.LevelUpNotify.Ready', { from: data.level, to: data.upToLevel })

	const cm = ChatMessage.create({
		content: template(data, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true }),
		rollMode: 'selfroll',
		whisper: [game.user.id],
		speaker: ChatMessage.getSpeaker({ actor: actor }),
	});

	return cm;
}

function checkLevelUpReadiness(actor, currentLevel) {
	if (canLevelUp(actor, currentLevel)) {
		const actorData = getSystemData(actor);
		const lv = actorData.details.level.value;
		const xp = duplicate(actorData.details.xp);

		let levelUps = 1;
		let upToLevel = lv - 1, nextLevelXp, lastXp;
		do {
			nextLevelXp = actor.getLevelExp(++upToLevel);
			if (lastXp === nextLevelXp || levelUps > 100) {
				console.warn('Infinite Loop?', upToLevel, xp.value, '>=', nextLevelXp);
				break;
			}
			lastXp = nextLevelXp;
			levelUps++;
		}
		while (xp.value >= nextLevelXp);

		cardPrinter(actor, {
			actor: actor,
			token: game.release.generation >= 10 ? actor.prototypeToken : actor.data.token,
			level: lv,
			upToLevel,
			levelUps,
			xp: {
				current: xp.value,
				required: xp.max,
				over: xp.value - xp.max,
			}
		});
	}

	renderLevelupIcon(canvas, actor, currentLevel);
}

function levelUpFinished(actor, newLv) {
	renderLevelupIcon(canvas, actor, newLv);
}

function updateActorEvent(actor, data, _options, _userId) {
	if (!game.user.isGM) return;

	if (data[getPrefix()]?.details?.xp?.value !== undefined)
		checkLevelUpReadiness(actor);
}

function updateClassEvent(actor, item, oldLv, newLv) {
	if (!game.user.isGM) return;

	canLevelUp(actor, newLv) ?
		checkLevelUpReadiness(actor, newLv) :
		levelUpFinished(actor, newLv);
}

// For rendering icons on scene change
Hooks.on('canvasReady', renderLevelupIcon);

// For detecting changes
Hooks.on('updateActor', updateActorEvent);
Hooks.on('pf1.classLevelChange', updateClassEvent);

Hooks.once('init', () => {
	game.modules.get(CFG.module).debug = false;
});
