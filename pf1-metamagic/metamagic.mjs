const MODULE_ID = 'pf1-metamagic',
	MAXIMIZE_FLAG_ID = 'maximize';

const template_path = `modules/${MODULE_ID}/config.hbs`;
let template;
Hooks.once('setup', () => getTemplate(template_path).then(t => template = t));

const getState = (state) => {
	switch (state) {
		case 'true': return true;
		case 'false': return false;
		default: return undefined;
	}
}

const hookInputs = (im, { item, action, isActionSheet }) => {
	im.addEventListener('change', (ev) => {
		ev.preventDefault();
		ev.stopPropagation();
		im.indeterminate = false;
		im.readOnly = true;
		im.disabled = true;

		const f = item.getFlag(MODULE_ID, MAXIMIZE_FLAG_ID) ?? {};
		const state = getState(ev.target.value);
		console.log('Maximize:', state === undefined ? 'Default' : (state ? 'Yes' : 'No'));

		if (state === undefined)
			im.indeterminate = true;

		if (isActionSheet) {
			if (state !== undefined) {
				f.actions ??= {};
				f.actions[action.id] = state;
				item.setFlag(MODULE_ID, MAXIMIZE_FLAG_ID, duplicate(f));
			}
			else {
				item.unsetFlag(MODULE_ID, `${MAXIMIZE_FLAG_ID}.actions.${action.id}`);
			}
		}
		else {
			if (state !== undefined)
				item.setFlag(MODULE_ID, `${MAXIMIZE_FLAG_ID}.default`, state);
			else
				item.unsetFlag(MODULE_ID, `${MAXIMIZE_FLAG_ID}.default`);
		}
	});

	im?.addEventListener('contextmenu', (ev) => {
		ev.preventDefault();
		ev.stopPropagation();
		const f = item.getFlag(MODULE_ID, MAXIMIZE_FLAG_ID) ?? {};
		if (isActionSheet && f.actions?.[action?.id] === undefined) return;
		if (!isActionSheet && f.default === undefined) return;

		im.readOnly = true;
		im.disabled = true;

		console.log('Maximize: Default');

		if (isActionSheet) {
			im.indeterminate = true;
			item.unsetFlag(MODULE_ID, `${MAXIMIZE_FLAG_ID}.actions.${action.id}`);
		}
		else {
			im.indeterminate = true;
			item.unsetFlag(MODULE_ID, `${MAXIMIZE_FLAG_ID}.default`);
		}
	});
}

/**
 * @param {ItemSheet|ItemActionSheet} sheet
 * @param {JQuery} html
 * @param {Object} options
 */
const injectMaximizeToggle = (sheet, [html], options) => {
	if (!template) return; // HBS template hasn't loaded yet

	const isEditable = sheet.isEditable,
		isActionSheet = sheet instanceof pf1.applications.component.ItemActionSheet;

	// Ignore items with no actions
	if (!isActionSheet && sheet.document.system.actions === undefined) return;

	const tabId = isActionSheet ? 'misc' : 'advanced';
	const tab = html.querySelector(`.tab[data-tab=${tabId}]`);
	if (!tab) return;

	const action = sheet.action,
		item = action?.item ?? sheet.document,
		isItem = !action;

	const fm = item.getFlag(MODULE_ID, MAXIMIZE_FLAG_ID) ?? {};
	const maximizeAction = fm.actions?.[action?.id],
		maximizeItem = fm.default,
		maximize = maximizeAction ?? maximizeItem;

	const maxOptions = {
		true: 'Enabled',
		false: 'Disabled',
		inherit: 'Default/Inherit'
	};

	const getActive = () => {
		if (maximize === true) return 'true';
		if (maximize === false) return 'false';
		return 'inherit';
	}

	const templateData = {
		maximize: maximize ?? false,
		indeterminate: maximize === undefined,
		options: maxOptions,
		active: getActive(),
		isItem,
	};

	const d = document.createElement('div');
	d.innerHTML = template(templateData, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true });

	const im = d.querySelector('select.maximize');
	if (isEditable) hookInputs(im, { item, action, isActionSheet });

	if (isActionSheet && maximizeAction === undefined)
		im.indeterminate = true;
	else if (maximize === undefined)
		im.indeterminate = true;

	if (!isEditable) {
		d.querySelectorAll('input').forEach(el => {
			el.readOnly = true;
			el.disabled = true;
		});
	}

	tab.append(...d.childNodes);
}

async function doMaximize() {
	const maximizeDamage = async (dmg) => {
		if (!dmg) return;
		dmg.roll = await dmg.roll.clone().evaluate({ maximize: true, async: false });
	}

	const tag = game.i18n.localize('Metamagic.Maximize.Tag');
	const td = this.shared.templateData;
	if (!td.hasProperties)
		td.properties.push({ header: 'Understudy', value: [tag] });
	else
		td.properties[0].value.push(tag);

	const promises = [];
	for (const attack of this.shared.attacks) {
		for (const damage of attack.chatAttack.damageRows) {
			promises.push(maximizeDamage(damage.normal));
			promises.push(maximizeDamage(damage.crit));
		}
	}

	await Promise.allSettled(promises);
}

async function testMaximize(wrapped, ...args) {
	const { action, item } = this.shared;
	const fm = item.getFlag(MODULE_ID, MAXIMIZE_FLAG_ID) ?? {};
	const maximize = fm.actions?.[action?.id] ?? fm.default ?? false;
	if (maximize) await doMaximize.call(this);
	return wrapped.call(this, ...args);
}

Hooks.once('ready', () => {
	/* global libWrapper */
	libWrapper.register(MODULE_ID, 'pf1.actionUse.ActionUse.prototype.postMessage', testMaximize, libWrapper.WRAPPER);

	Hooks.on('renderItemActionSheet', injectMaximizeToggle);
	Hooks.on('renderItemSheet', injectMaximizeToggle);
});
