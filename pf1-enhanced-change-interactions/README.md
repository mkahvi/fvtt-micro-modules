# Enhanced Change Interactions for Pathfinder 1e (Proof of Concept)

Proof of concept for enhanced interactions with changes.

Intended to be integrated into the system with v10.

Adds:

- Duplicate button to Changes
- Drag & drop functionality to changes

## Install

Manifest URL: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/master/pf1-enhanced-change-interactions/module.json>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT license](./LICENSE).
