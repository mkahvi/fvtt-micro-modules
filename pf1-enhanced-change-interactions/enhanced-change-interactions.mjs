function renderItemSheet(sheet, [html]) {
	const item = sheet.document;

	const tab = html.querySelector('.tab[data-tab="changes"]');
	if (!tab) return;

	const list = tab.querySelector('.item-list.changes');

	function onChangeDrop(ev) {
		let dropData;
		try {
			dropData = JSON.parse(event.dataTransfer.getData('text/plain'));
		}
		catch (e) {
			return false;
		}

		if (dropData.type !== 'pf1ItemChange') return false;
		ev.preventDefault();
		ev.stopPropagation();

		delete dropData.data._id;
		pf1.components.ItemChange.create([dropData.data], { parent: item });
	}

	tab.addEventListener('drop', onChangeDrop);

	function duplicateChange(ev) {
		const p = this.closest('.change[data-change]');

		const oldChange = item.changes.get(p.dataset.change);
		if (!oldChange) return void ui.notifications.warn(`Change with ID "${p.dataset.change}" not found!`);

		const datad = foundry.utils.deepClone(oldChange.data);
		delete datad._id;
		pf1.components.ItemChange.create([datad], { parent: item });
	}

	function onDragStart(ev) {
		ev.stopPropagation();

		const changeId = this.dataset.change;
		const oldc = item.changes.get(changeId);
		if (!oldc) return ev.preventDefault();

		const dragData = {
			type: 'pf1ItemChange',
			data: {
				...oldc.data
			}
		};

		event.dataTransfer.setData('text/plain', JSON.stringify(dragData));
	}

	list.querySelectorAll('li.change[data-change]').forEach(el => {
		el.draggable = true;

		const a = document.createElement('a');
		a.classList.add('item-control', 'change-control', 'duplicate-change');
		a.innerHTML = '<i class="fa-solid fa-copy"></i>';
		a.dataset.tooltip = 'Duplicate Change';
		el.querySelector('.item-controls').prepend(a);
		a.addEventListener('click', duplicateChange);

		el.addEventListener('dragstart', onDragStart);
	});
}

Hooks.on('renderItemSheet', renderItemSheet);
