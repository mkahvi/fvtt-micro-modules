const getSystemData = (doc) => game.release.generation >= 10 ? doc.system : doc.data.data;
const getPrefix = () => game.release.generation >= 10 ? 'system' : 'data';

function preUpdateActor(actor, updateData, _options, _userId) {
	if (!updateData[getPrefix()]?.altCurrency) return;
	if (!['character', 'npc'].includes(actor.type)) return;

	// Push weightless currency to weighted

	const currency = foundry.utils.mergeObject(getSystemData(actor).currency, updateData[getPrefix()]?.currency ?? {}, { inplace: false });
	const altCurrency = updateData[getPrefix()].altCurrency;
	for (const [type, coins] of Object.entries(altCurrency)) {
		if (coins > 0) {
			const update = {
				[getPrefix()]: {
					currency: { [type]: currency[type] + coins },
					altCurrency: { [type]: 0 },
				},
			};
			if (game.release.generation >= 10)
				actor.updateSource(update);
			else
				actor.data.update(update);

			delete altCurrency[type];
		}
	}
}

async function convertActorCurrency(actor, { silent = false } = {}) {
	if (!actor.isOwner) return;

	const actorData = getSystemData(actor),
		currency = duplicate(actorData.currency),
		altCurrency = duplicate(actorData.altCurrency);

	const updateData = {};
	let coinsTotal = 0;
	const prefix = getPrefix();
	for (const [type, weightlessCoins] of Object.entries(altCurrency)) {
		coinsTotal += weightlessCoins;
		if (weightlessCoins > 0) {
			updateData[prefix] ??= { currency: {}, altCurrency: {} };
			updateData[prefix].currency[type] = currency[type] + weightlessCoins;
			updateData[prefix].altCurrency[type] = 0;
		}
	}

	if (coinsTotal > 0) {
		if (!silent) console.log('CURRENCY CONVERSION:', updateData);
		return actor.update(updateData);
	}
	else {
		if (!silent) console.log('NO WEIGHTLESS CURRENCY FOUND');
	}
}

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 */
function onRenderActorSheet(sheet, [html]) {
	const actor = sheet.actor;
	if (!['character', 'npc'].includes(actor.type)) return;

	const inventory = html.querySelector('.tab.inventory[data-tab="inventory"]');
	if (!inventory) return; // console.warn('Could not find inventory tab');

	const weightlessCurrency = inventory.querySelector(`[name="${getPrefix()}.altCurrency.gp"]`)?.parentElement;
	if (!weightlessCurrency) return;// console.warn("No weightless currency on sheet");
	weightlessCurrency.remove();

	if (actor.__noWeightLessCurrency !== true) {
		convertActorCurrency(actor, { silent: true })
			.then(_ => actor.__noWeightLessCurrency = true);
	}
}

// Hooks.on('preUpdateActor', preUpdateActor);
Hooks.on('renderActorSheet', onRenderActorSheet);

Hooks.once('ready', function registerAPI() {
	game.modules.get('pf1-no-weightless-currency').api = { convert: convertActorCurrency };
});
