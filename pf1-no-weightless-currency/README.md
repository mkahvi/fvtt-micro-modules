# No Weightless Currency for Pathfinder 1

![Supported Foundry Versions](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fversion%3Fstyle%3Dflat%26url%3Dhttps%3A%2F%2Fgitlab.com%2Fmkahvi%2Ffvtt-micro-modules%2F-%2Fraw%2Fmaster%2Fpf1-no-weightless-currency%2Fmodule.json)

Simple weightless currency removal module.

- Weightless currency is removed from the character sheet.
- Weightless currency is converted to weighted when added to characters.

  ✅ Does what it means to.

## API

Simple API function is exposed to ensure actor has no weightless currency, moving all of it to weighted currency.

```js
game.modules.get('pf1-no-weightless-currency').api.convert(actor);
```

## Install

Manifest URL: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/master/pf1-no-weightless-currency/module.json>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
