# Smart New Token Control

![Supported Foundry Versions](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fversion%3Fstyle%3Dflat%26url%3Dhttps%3A%2F%2Fgitlab.com%2Fmkahvi%2Ffvtt-micro-modules%2F-%2Fraw%2Fmaster%2Fsmart-new-token-control%2Fmodule.json)

Replaces default new token behaviour where it takes control and pans to new token no matter what.

By default if a new owned token is placed, the new token gains control and is panned to, regardless what the player is doing.

This module makes it so that panning occurs only if no token is controlled.

## Install

Manifest URL: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/master/smart-new-token-control/module.json>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).

## Credits

Imitation of [DF Chat Enhancements](https://foundryvtt.com/packages/df-chat-enhance)'s sub-feature that does the same. However I had no desire for the rest of the module.
