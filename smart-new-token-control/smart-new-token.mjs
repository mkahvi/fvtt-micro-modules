const MODULE_ID = 'smart-new-token-control';

/**
 * Mostly copy from foundry.js to maintain normal behaviour
 *
 * Synchronized with Foundry 10.291
 */
function smartTokenOnCreate(wrapped, options, userId) {
	this.draw().then(() => {
		const refreshVision = this.document.sight.enabled && this.observer;
		const refreshLighting = this.emitsLight;
		if (refreshVision || refreshLighting) {
			this.updateSource({ defer: true });
			canvas.perception.update({ refreshVision, refreshLighting }, true);
		}

		// Assume token control section; only deviation from core
		if (!game.user.isGM && this.isOwner && !this.document.hidden) {
			// Deviate from normal behaviour by not always taking control & panning
			if (game.canvas?.tokens.controlled.length > 0)
				console.log('Blocked control&pan to new owned token');
			else
				this.control({ pan: true });
		}

		if (!refreshVision) canvas.effects.visibility.restrictVisibility();
	});
}

Hooks.once('init', () => {
	console.log('SMART NEW TOKEN CONTROL | Overriding Token#_onCreate');
	/* global libWrapper */
	libWrapper.register(MODULE_ID, 'Token.prototype._onCreate', smartTokenOnCreate, 'OVERRIDE');
});
