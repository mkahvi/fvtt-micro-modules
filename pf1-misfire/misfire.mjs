const MODULE_ID = 'pf1-misfire';
const misfireFlag = 'misfire',
	gunTrainingFlag = 'misfireBrokenMitigation';

/* global pf1Misfire */
globalThis.pf1Misfire = /** @type {const} */ ({
	broken: 4,
	nonproficient: 4,
	guntraining: 2,
});

function limitPrecision(number, decimals = 2) {
	const mult = Math.pow(10, decimals);
	return Math.floor(number * mult) / mult;
}

function getMisfireInfo(action, { item, actor } = {}) {
	item ??= action.item;
	actor ??= item?.actor;

	if (!action && !item) return;

	const amisfire = action?.data.ammo?.misfire ?? null;
	const imisfire = item?.system.ammo?.misfire ?? null;
	const misfire = amisfire ?? imisfire;

	if (misfire === null) return null;

	const isProficient = item.isProficient ?? true;

	const isBroken = item.isBroken ?? false;
	const hasBrokenMitigation = isBroken ? actor?.hasItemBooleanFlag?.(gunTrainingFlag) ?? false : false;

	return {
		base: misfire,
		nonproficiency: {
			active: !isProficient,
			get value() { return this.active ? pf1Misfire.nonproficient : 0; },
		},
		broken: {
			active: isBroken,
			get value() { return this.active ? pf1Misfire.broken : 0; },
		},
		mitigation: {
			active: hasBrokenMitigation,
			get value() { return this.active ? pf1Misfire.guntraining : 0; },
		},
		get isModified() {
			return this.nonproficiency.active || this.broken.active;
		},
		get total() {
			return this.base + this.nonproficiency.value + Math.max(0, this.broken.value - this.mitigation.value);
		},
	};
}

function setTooltip(el, info) {
	const breakdown = [
		`<span>${info.total}</span>`,
		'<label>Threshold</label>',
		'<hr>',
		`<span>${info.base}</span>`,
		'<label>Base</label>',
	];

	if (info.nonproficiency.active) breakdown.push(`<span>+${info.nonproficiency.value}</span>`, '<label>Nonproficient</label>');
	if (info.broken.active) {
		breakdown.push(`<span>+${info.broken.value}</span>`, '<label>Broken</label>');
		if (info.mitigation.active) breakdown.push(`<span>-${info.mitigation.value}</span>`, '<label>Gun training</label>');
	}
	el.dataset.tooltip = breakdown.join('');
	el.dataset.tooltipClass = 'pf1 misfire';
}

/**
 * @param {Item} item
 */
async function migrateItem(item) {
	const misfire = item.getItemDictionaryFlag('misfire');
	if (misfire === undefined) return null;

	const updateData = {
		system: {
			flags: {
				dictionary: {
					'-=misfire': null,
				},
			},
		},
	};

	if (Number.isFinite(misfire) && !Number.isFinite(item.system.ammo?.misfire))
		updateData.system.ammo = { misfire };

	return item.update(updateData);
}

async function migrateData() {
	const activeGM = game.users.activeGM;
	if (activeGM) {
		if (!activeGM?.isSelf) return;
	}

	const migration = game.settings.get(MODULE_ID, 'migration');
	const mod = game.modules.get(MODULE_ID);
	if (migration === mod.version || foundry.utils.isNewerVersion(migration, mod.version)) return void console.debug('Misfire | Migration | Not needed');

	const actors = game.actors.filter(a => a.isOwner);
	let pct10 = Math.max(25, Math.floor(actors.length / 10));
	if (actors.length < 50) pct10 = 100;
	let i = 0, migrated = 0;
	for (const actor of actors) {
		i += 1;
		if (i % pct10 === 0) console.log('Misfire | Migrating', limitPrecision((i / actors.length) * 100, 1), '% |', i, 'out of', actors.length);
		for (const item of actor.items) {
			const rv = await migrateItem(item);
			if (rv) migrated += 1;
		}
	}

	console.log('Misfire | Migrating | Unlinked Tokens...');
	for (const scene of game.scenes) {
		for (const token of scene.tokens) {
			if (!token.actorId) continue;
			if (token.isLinked || token.actorLink) continue;
			const actor = token.actor;
			if (!actor) continue;
			if (!actor.isOwner) continue;
			for (const item of actor.items) {
				const rv = await migrateItem(item);
				if (rv) migrated += 1;
			}
		}
	}

	console.log('Misfire | Migration Completed |', migrated, 'Item(s) Updated');
	if (game.users.activeGM?.isSelf) game.settings.set(MODULE_ID, 'migration', mod.version);
}

/**
 * @param {ChatMessage} cm
 * @param {JQuery} html
 */
function injectMisfireInChatMessage(cm, [html]) {
	const item = cm.itemSource;

	const metadata = cm.system;
	if (!metadata) return;

	const attackData = metadata.rolls?.attacks;
	if (!attackData) return;

	if (!item?.testUserPermission(game.user, 'OBSERVER')) return;

	const action = item.actions.get(metadata.actionId);
	const mInfo = getMisfireInfo(action, { item });
	if (!mInfo) return;

	const misfire = mInfo.total;

	if (!(misfire > 0)) return;

	let misfireCount = 0;
	for (const atkData of Object.values(attackData)) {
		// Assumes first term is the roll. Doesn't work for fortune/misfortune
		const atk = Roll.fromData(atkData.attack);

		const natRoll = atk.dice?.[0]?.total; // Assume first roll is relevant
		if (natRoll === undefined) continue;
		if (natRoll <= misfire) misfireCount++;
	}

	if (misfireCount == 0) return;

	// Update chat card
	const footer = html.querySelector('.card-footer');
	if (!footer) return;

	const tags = footer.querySelectorAll('.gm-sensitive div');
	let tagL;
	if (tags) {
		tagL = tags[tags.length - 1];
	}
	else {
		// Hacky solution in case there's no tags.
		const tagDiv = document.createElement('div');
		tagDiv.classList.add('flexcol', 'property-group', 'gm-sensitive');
		const tagGroup = document.createElement('label');
		tagGroup.textContent = 'Missing Info'; // TODO: i18n
		tagL = document.createElement('div');
		tagL.classList.add('flexrow');
		footer.append(tagDiv);
	}

	const tag = document.createElement('span');
	tag.classList.add('tag', 'misfire');
	tag.textContent = game.i18n.format('MKAh.Misfire.Tag', { count: misfireCount });

	setTooltip(tag, mInfo);

	tagL.append(tag);
}

Hooks.once('init', () => {
	game.settings.register(MODULE_ID, 'migration', {
		type: String,
		default: '0.1.0',
		config: false,
		scope: 'world',
	});
});

// ItemAction.misfire adjustment
function misfireWrapper(wrapped) {
	const rv = wrapped();

	const misfire = getMisfireInfo(this);
	if (!misfire) return rv;

	console.log(rv, misfire.total, misfire);
	return misfire.total;
}

/**
 *
 * @param {ItemSheet} app
 * @param {JQuery<HTMLElement>} html
 */
function injectMisfireSheetDisplay(app, [html]) {
	const input = html.querySelector('input[name="ammo.misfire"],input[name="system.ammo.misfire"]');
	if (!input) return;

	const action = app.action;
	const item = app.item ?? action.item;

	const mInfo = getMisfireInfo(action, { item });
	if (!mInfo?.isModified) return;

	const l = document.createElement('label');
	l.classList.add('misfire-adjust');
	l.classList.toggle('broken', mInfo.broken.active);
	l.innerHTML = `<i class="fa-solid fa-burst"></i> ${mInfo.total}`;

	setTooltip(l, mInfo);

	input.after(l);
}

// Hooks.once('init', () => CONFIG.debug.modMkMisfire = false); // not implemented

// ----- HOOKS and delayed processing of old messages
// Hacky fix for: https://gitlab.com/Furyspark/foundryvtt-pathfinder1/-/issues/782

Hooks.on('renderItemActionSheet', injectMisfireSheetDisplay);
Hooks.on('renderItemSheet', injectMisfireSheetDisplay);

Hooks.on('renderChatMessage', injectMisfireInChatMessage);

Hooks.once('pf1PostReady', () => migrateData());

Hooks.once('libWrapper.Ready', () => {
	/* global libWrapper */
	libWrapper.register(MODULE_ID, 'pf1.components.ItemAction.prototype.misfire', misfireWrapper, libWrapper.WRAPPER);
});
