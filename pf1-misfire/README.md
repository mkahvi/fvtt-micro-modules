# Misfire for Pathfinder 1e

![Supported Foundry Versions](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fversion%3Fstyle%3Dflat%26url%3Dhttps%3A%2F%2Fgitlab.com%2Fmkahvi%2Ffvtt-micro-modules%2F-%2Fraw%2Fmaster%2Fpf1-misfire%2Fmodule.json)

Adds misfire configuration to weapons and printing info on the attack message.

Features:
- Broken increases misfire range
- Nonproficiency increases misfire range
- Gun training reduces misfire range of broken weapons

This module is being phased out. Minimal extra features are provided anymore.

## Configuration

Gun training is handled by detecting `misfireBrokenMitigation` boolean flag. This boolean flag can be in any enabled item/feature/buff.  
Rules reference: <https://aonprd.com/Rules.aspx?Name=Firearms&Category=Mastering%20Combat>

Proficiency penalty to misfire only cares about the shooter. Actual rules say this is determined by the person who loads the weapon. I deemed another character loading the gun too obscure case to care however.

## Screencaps

![Item Sheet](img/misfire-item.png)

![Action Sheet](img/misfire-action.png)

![Tooltip](img/misfire-tooltip.png)

## Compatibility

No issues known.

## Install

Manifest URL: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/master/pf1-misfire/module.json>

### Latest for PF1v9

Manual install only: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/1ebbeb7673a4a5a3541d7025f6b19302845f839c/pf1-misfire/pf1-misfire.zip>

### Latest for Foundry v9

Manual install only: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/e6fed14ea56087fce13a76b28c66ecf4f0b3c3ac/pf1-misfire/pf1-misfire.zip?inline=false>

### Latest for Foundry 0.7.x

Manual install only: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/961c918d73284ef42e04d1c5e8bd179a94fc55a1/pf1-misfire/module.zip?inline=false>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
