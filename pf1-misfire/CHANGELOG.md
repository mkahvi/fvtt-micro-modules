# Change Log

## 0.6.1

- Proper support for PF1 v10

## 0.6.0

- Migration of misfire dictionary flags used by the module to the actual misfire field in the weapons.
- Minimum required PF1 version increased to v10

## 0.5.0

- Module ID changed, please uninstall the old one!
- PF1v9 compatibility

## 0.4.0

- PF1 0.82.2 compatibility
- Fix: The specified value "undefined" cannot be parsed, or is out of range.

## 0.3.1

- Improved layout

## 0.3.0

- Foundry v10 compatibility

## 0.2.2.2 Fix for cards with missing item

## 0.2.2.1 Remove debug logging

## 0.2.2

- Chat message parsing no longer unnecessarily tests cm.itemSource.hasAttack
- PF1 0.80.25 compatibility. No per-attack misfire chance for now.

## 0.2.1

- Proficiency is taken into consideration for misfire.
- Tooltip added for the misfire tag.

## 0.2.0.1 Remove debug logging

## 0.2.0

- Easy configuration interface.
- Broken condition support.

## 0.1.5.2 Maintennace update

## 0.1.5.1 Foundry 0.7.x fix

## 0.1.5 Foundry 0.7 compatibility

## 0.1.4.1 Hotfix for Foundry 0.8 compatibility

## 0.1.4

- Foundry 0.8 compatibility

## 0.1.3.1

- Fix for PF1 issue [#782](https://gitlab.com/foundryvtt_pathfinder1e/foundryvtt-pathfinder1/-/issues/782)
- Last version to support Founry 0.7

## 0.1.3

- Fix: Permission checking

## 0.1.2

- Fix misfire tag display if holder element is missing.

## 0.1.1

- Check permission on source item rather than card
- Better checks
- Show notification only on last possible spot instead of all.

## 0.1.0 Initial release
