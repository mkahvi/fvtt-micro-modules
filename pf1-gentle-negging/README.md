# Gentle Negging for Pathfinder 1e

![Supported Foundry Versions](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fversion%3Fstyle%3Dflat%26url%3Dhttps%3A%2F%2Fgitlab.com%2Fmkahvi%2Ffvtt-micro-modules%2F-%2Fraw%2Fmaster%2Fpf1-gentle-negging%2Fmodule.json)

Multiclassing friendly negative levels, spreading them one by one to each class instead of all to everything.

## Example

Take for example multiclassed character:

> Barbarian 3, Rogue 4, Cleric 2

With 3 negative levels (with this module) they become:

> Barbarian 2, Rogue 2, Cleric 2

Instead of RAW:

> Barbarian 0, Rogue 1, Cleric 0

Which compared to single class character of same level goes from `9` to `6`, not `9` to `1`.

And that's on top of all other problems multiclassed characters have.

## Install

Manifest URL: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/master/pf1-gentle-negging/module.json>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
