Hooks.on('pf1GetRollData', function gentleNeg(actor, result) {
	if (!result.classes) return;

	const negLevels = result.attributes.energyDrain ?? 0;
	if (!(negLevels > 0)) return;

	const classes = Object.values(result.classes);
	classes.forEach(cls => cls.level = cls.unlevel); // Reset what system did

	// Apply negative levels
	for (let i = 0; i < negLevels; i++) {
		classes.sort((a, b) => b.level - a.level);
		const cls = classes[0];
		cls.level = Math.max(0, cls.level - 1);
	}
});
