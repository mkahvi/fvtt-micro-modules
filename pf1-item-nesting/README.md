# Item Nesting for Pathfinder 1e (Proof of Concept)

![Supported Foundry Versions](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fversion%3Fstyle%3Dflat%26url%3Dhttps%3A%2F%2Fgitlab.com%2Fmkahvi%2Ffvtt-micro-modules%2F-%2Fraw%2Fmaster%2Fpf1-item-nesting%2Fmodule.json)

Allows nesting of items.

In use after configuring:  
![Nested](./img/screencaps/nested-items.png)

Configuration is done in advanced tab of each item:  
![Config](./img/screencaps/parent-selection.png)

✅ The functionality is somewhat limited, but very helpful for organization.

## Configuration

Module settings includes a toggle to actively protect against bad nesting. Intended to be used to salvage bad actors where you somehow managed a loop in parent-child relations, not for active use.

## API

```js
const api = game.modules.get('pf1-item-nesting').api;
const valid = await api.validate.item(item);
const {loops, missing, valid} = await api.validate.actor(actor);
```

Both validation functions accept second boolean parameter for automatically removing bad nesting configurations (loops or missing parents).

## Install

Requires PF1v9 or newer.

Manifest URL: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/master/pf1-item-nesting/module.json>

### For pre-PF1v9

Manual install: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/31f2b507229901e67244f14807c0dec6a7668338/pf1-poc-nested-abilities/pf1-poc-nested-abilities.zip>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
