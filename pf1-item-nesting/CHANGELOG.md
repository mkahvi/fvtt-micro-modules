# Change Log

## 1.1.0

- New: Allow editing nesting in items outside of actors. Be careful with this!
- Change: API enhancements
- Change: Module ID changed from `mkah-pf1-poc-nested-abilities` to `pf1-item-nesting`

## 1.0.0

- Change: Bumped version
- Fix: Removed debug output

## 0.7.0

- Change: `groupBy` dictionary flag is no longer used for controlling nesting. Migration provided to convert the old.

## 0.6.3.2

- Fix: Odd behaviour with imports or similar document updates.

## 0.6.3.1

- Fix: Nesting certain items did not work correctly.
- Fix: Nesting spells past level boundaries was allowed.

## 0.6.3

- New: Adjust grouping of other items when grouping tag changes. [#42]

## 0.6.2

- PF1v9 compatibility.

## 0.6.1

- Fix parent item selection erroring.
- Add translation support.
- Linking by tag only requires parent item has tag (previously both needed it).
- Parent editor has more standard layout.

## 0.6.0

- Styling fixes
- Foundry v10 compatibility

## 0.5.1.1

- Fix for configuration being interactive when sheet isn't editable.
