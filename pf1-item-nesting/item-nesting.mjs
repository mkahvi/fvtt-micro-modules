const CFG = /** @type {const} */ ({
	id: 'pf1-item-nesting',
	SETTINGS: {
		guard: 'guard',
		offActor: 'offActor',
	},
	path: 'flags.world.nesting.groupBy',
});

const template = `modules/${CFG.id}/nesting-config.hbs`;
Hooks.once('setup', () => loadTemplates([template]));

class ItemData {
	/**
	 * @type {string}
	 */
	get id() { return this.item.id; }
	/**
	 * @type {ItemPF}
	 */
	item;
	/**
	 * @type {string}
	 */
	get tag() { return this.item.system.tag; }

	link = { id: null, tag: null };

	/**
	 * @type {Element}
	 */
	element;

	/**
	 * Children, if any.
	 *
	 * @type {ItemData[]}
	 */
	children = [];

	/**
	 * @type {ItemData|undefined}
	 */
	#parent;

	set parent(v) {
		if (v === this) throw Error('Attempting to set self as parent');
		if (this.hasParent(v)) throw Error('Attempting to create infinite loop');
		this.#parent = v;
	}

	get parent() {
		return this.#parent;
	}

	/**
	 * @param {ItemData} child
	 * @returns {boolean}
	 */
	hasParent(child) {
		if (this.parent) {
			if (this.parent === child) return true;
			return this.parent.hasParent(child);
		}
		return false;
	}

	/**
	 * Collection this parent belongs in, if any.
	 *
	 * @type {ItemData[]}
	 */
	get collection() {
		return this.parent?.children;
	}

	/**
	 * Sublist element
	 *
	 * @type {Element}
	 */
	sub;

	/**
	 * @type {number}
	 */
	get depth() {
		if (this.#_depth === undefined) {
			let depth = 0;
			let sitem = this;
			const visited = new Set();
			while (sitem.parent) {
				if (visited.has(sitem.parent.id)) {
					console.error('Infinite loop', this);
					return null;
				}
				visited.add(sitem.parent.id);
				depth++;
				sitem = sitem.parent;
			}
			this.#_depth = depth;
		}
		return this.#_depth;
	}

	/**
	 * @type {number}
	 */
	#_depth;

	constructor(item, element) {
		this.item = item;
		const { tag, id } = getGrouping(item);
		this.link = { id, tag };
		this.element = element;
	}
}

/**
 * @param {Item} item
 * @returns {Promise<Item>}
 */
const unGroup = (item) => item.update({ 'flags.world.nesting.-=groupBy': null });
/**
 *
 * @param {Item} item - Item to group
 * @param {string|object} opts
 * @returns {Promise<Item>} - Return value of item.update()
 */
const groupBy = (item, opts) => {
	if (typeof opts === 'string') opts = { id: opts };
	const { id, tag } = opts;
	return item.update({ [CFG.path]: id ? `#${id}` : tag });
};

/**
 * @param {Item} item
 * @returns {string|undefined}
 */
const getGroup = (item) => item.flags.world?.nesting?.groupBy;

/**
 * @param {string} raw
 * @returns {{id:string, tag:string}}
 */
function parseParentString(raw) {
	const isId = raw?.[0] === '#',
		tag = isId ? undefined : raw,
		id = isId ? raw.substring(1) : undefined;
	return { id, tag, found: id !== undefined || tag !== undefined };
}

/**
 * Returns item ID or tag ID for what this item should be nested under.
 *
 * @param {ItemPF} item
 * @returns {{id:string, tag:string}}
 */
const getGrouping = (item) => parseParentString(getGroup(item));

function getResourceItem(actor, tag) {
	const actorData = actor.system;
	const resource = actorData.resources[tag];
	if (resource) {
		return resource.item ?? actor.items.get(resource._id);
	}
	return actor.items.find(i => i.system.tag === tag);
}

/**
 * @param {object} p
 * @param {ActorPF} p.actor
 * @param {string | undefined} p.id Item ID
 * @param {string | undefined} p.tag Tag ID
 */
function getItemByIdOrTag({ actor, id, tag } = {}) {
	if (!actor) return null;
	if (id) return actor.items.get(id);
	if (tag) return getResourceItem(actor, tag);
	return null;
}

/**
 * @param {ItemPF} item
 * @param {string | undefined} conflictingTags
 * @param {string | undefined} conflictingIds
 * @param {number} _depth internal
 * @returns {boolean}
 */
function hasConflictingNesting(item, conflictingTags = [], conflictingIds = [], _depth = 0) {
	const actor = item.actor;

	if (_depth > 10) {
		console.warn('Nesting infinite loop detection sank', _depth, 'items deep, ending search early and treating as loop.');
		return true; // Assume something is bad
	}

	const { tag, id, found } = getGrouping(item);
	if (!found) return false;

	const itemId = item.id, itemTag = item.system.tag || pf1.utils.createTag(item.name);

	conflictingIds.push(itemId);
	if (itemTag) conflictingTags.push(itemTag);

	if (tag !== undefined && conflictingTags.includes(tag)) return true;
	if (id !== undefined && conflictingIds.includes(id)) return true;

	const parent = getItemByIdOrTag({ actor, id, tag });
	if (!parent) return false;
	return hasConflictingNesting(parent, conflictingTags, conflictingIds, _depth + 1);
}

/**
 * @param {ItemPF} item
 * @param {string | undefined} conflictingGroupId
 * @param {string | undefined} conflictingParentId
 */
function couldHaveConflictingNesting(item, { tag, id } = {}) {
	const actor = item.actor;
	const parent = getItemByIdOrTag({ actor, id, tag });
	if (!parent) return false;
	const conflictingTags = [],
		conflictingIds = [];
	const itemId = item.id, itemTag = item.system.tag || pf1.utils.createTag(item.name);
	conflictingIds.push(itemId);
	if (itemTag) conflictingTags.push(itemTag);
	return hasConflictingNesting(parent, conflictingTags, conflictingIds, 1);
}

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 */
function nestActorItems(sheet, [html]) {
	const actor = sheet.actor;

	const guard = game.settings.get(CFG.id, CFG.SETTINGS.guard);

	// Search for item lists
	const lists = html.querySelectorAll('.item-list');
	if (!lists) return void console.debug('ITEM NESTING | NO LIST FOUND');

	for (const list of lists) {
		// reverse ensures order is maintained when re-inserting to new location
		const itemEls = list.querySelectorAll('li.item');
		/** @type {Collection<string,ItemData>} */
		const items = new Collection();
		/** @type {Collection<string,string>} */
		const itemTags = new Collection();

		const children = new Set();

		// Build list
		for (const itemEl of itemEls) {
			const itemId = itemEl.dataset.itemId;
			const item = actor.items.get(itemId);
			if (!item) continue;

			const itemData = new ItemData(item, itemEl);
			items.set(itemId, itemData);
			const tag = itemData.tag;
			if (tag) itemTags.set(tag, itemId);

			if (itemData.link.id || itemData.link.tag) {
				if (guard) {
					itemData.looped ??= hasConflictingNesting(item);
					if (guard && itemData.looped) {
						console.warn('Infinite loop detected:', item.name, item.id, itemData.link);
						ui.notifications?.warn(`Looped nesting detected in "${item.name}" [${item.id}]`, { permanent: true });
						return;
					}
				}

				children.add(item.id);
			}
		}

		// No child relations found
		if (children.size == 0) continue;

		const itemParents = new Collection();

		let childCount = 0;

		for (const itemId of children) {
			const item = items.get(itemId);
			// Map grouping ID into item ID
			item.link.id ??= itemTags.get(item.link.tag);
			// Map parent ID to item data
			const parent = item.parent = items.get(item.link.id);
			if (parent) {
				if (!itemParents.has(parent.id)) {
					itemParents.set(parent.id, parent);
					parent.children = [];
				}
				parent.children.push(item.id);
				childCount++;
			}
		}

		// No parents for children found
		if (childCount == 0) continue;

		if (itemParents.size === 0) continue; // Should be impossible with above early exists.

		/** @type {ItemData[]} */
		const parents = [...itemParents]
			// Sort parents by depth so deepest are handled first.
			.sort((a, b) => a.depth - b.depth);

		for (const parent of parents) {
			if (!parent.sub) {
				// Generate <ul>
				parent.sub = document.createElement('ul');
				parent.sub.classList.add('nesting-children');
				parent.element.classList.add('nesting-parent');
				parent.element.after(parent.sub);
			}

			for (const child of parent.children) {
				// Add item to parent's <ul>
				const item = items.get(child);
				const el = item.element;
				parent.sub.append(el);
				el.classList.add('nesting-child');
			}
		}
	}
}

function sameSubtype(item0, item1) {
	// Spells compare against level only
	if (item0.type === 'spell')
		return item0.system.level === item1.system.level;
	// Otherwise if not any of these, allow nesting always
	if (!['feat', 'attack', 'buff', 'loot'].includes(item0.type)) return true;
	// Check subtype for the rest
	return item0.subType === item1.subType;
}

/**
 * @param {ItemSheet} sheet
 * @param {JQuery} html
 * @param {object} data
 */
function injectNestingConfig(sheet, [html], data) {
	const allowEdit = !(!game.settings.get(CFG.id, CFG.SETTINGS.offActor) && !sheet.actor);

	const item = data.item,
		itemId = item.id,
		type = item.type,
		actor = data.item.parent,
		actorId = actor?.id;

	// const parentByTag = item.system.tag !== undefined;
	// if (!parentByTag) return; // Do only for feats

	function mapItem(item) {
		if (!item) return null;
		return {
			id: item.id,
			tag: item.system.tag || pf1.utils.createTag(item.name),
			name: item.name,
			img: item.img,
		};
	}

	const items = actor?.itemTypes[type]
		?.filter(i => i.id !== itemId && sameSubtype(i, item)) // same subtype and not self
		.map(mapItem) ?? [];

	// Get advanced tab container by homing into the tags.
	const advanced = html.querySelector('.tab[data-tab="advanced"] .tags')?.parentNode;
	if (!advanced) return;

	const listId = `${actorId}-${itemId}-poc-nested-abilities-parents`;

	const okIcon = ['good-tag', 'fas', 'fa-check-circle'];
	const badIcon = ['bad-tag', 'fas', 'fa-exclamation-triangle'];

	const { id, tag } = getGrouping(item);
	if (!allowEdit && !id && !tag) return;

	const templateData = { listId, link: { id, tag }, items, ignoreId: item.id, ignoreTag: item.system.tag, actor };
	templateData.link.parent = mapItem(getItemByIdOrTag({ actor, id, tag }));

	const d = document.createElement('div');
	d.innerHTML = Handlebars.partials[template](templateData, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true });
	if (!sheet.isEditable || !allowEdit) {
		d.querySelectorAll('input').forEach(el => {
			el.disabled = true;
			el.readOnly = true;
		});
	}

	const inputBoxByTag = d.querySelector('input.by-tag'),
		inputBoxById = d.querySelector('input.by-id');
	const validatorbyTag = d.querySelector('validator.by-tag i'),
		validatorbyId = d.querySelector('validator.by-id i');

	advanced.append(...d.childNodes);

	const validate = (validator, value, isTag = true) => {
		if (!validator) return true;

		// Remove old classes to clear icons
		validator.removeAttribute('class');

		// Check if parent exists
		const found = items.find(i => isTag ? i.tag === value : i.id === value) !== undefined;

		// Check if this causes a loop
		let looped = false;
		if (found) {
			const p = {};
			if (isTag) p.tag = value;
			else p.id = value;
			looped = couldHaveConflictingNesting(item, p);
			if (looped) ui.notifications?.error('Defined nesting causes a loop');
		}

		// Add icons
		if (found && !looped) validator.classList.add(...okIcon);
		else {
			validator.classList.add(...badIcon);
			validator.dataset.tooltip = 'PF1ItemNesting.Warning.NotFound';
		}

		return found && !looped;
	};

	if (tag) validate(validatorbyTag, tag, true);
	if (id) validate(validatorbyId, id, false);

	function prepareInputEvent(event) {
		event.stopPropagation();
		event.preventDefault();
		return event.target.value;
	}

	inputBoxByTag.addEventListener('change', event => {
		const newVal = prepareInputEvent(event);
		if (newVal.length) {
			const valid = validate(validatorbyId, newVal, true);
			if (valid) groupBy(item, { tag: newVal }); // Should refresh
		}
		else unGroup(item);
	});

	inputBoxById?.addEventListener('change', event => {
		const newVal = prepareInputEvent(event);
		if (newVal.length) {
			const valid = validate(validatorbyTag, newVal, false);
			if (valid) groupBy(item, { id: newVal }); // Should refresh
		}
		else unGroup(item);
	});
}

async function publicItemValidator(item, autoremove = false) {
	const actor = item.actor;
	if (!actor) return false;
	const looped = hasConflictingNesting(item);
	if (looped && autoremove) await unGroup(item);
	return !looped;
}

/**
 * @param {Actor} actor
 * @param {boolean} autoremove
 * @returns {object} {loops:number, missing:number}
 */
async function publicActorValidator(actor, autoremove = false) {
	if (!actor.isOwner) return;
	let loops = 0, missing = 0;
	for (const item of actor.items) {
		const { id, tag, found } = getGrouping(item);
		if (!found) continue;
		const parent = getItemByIdOrTag({ actor, id, tag });
		if (!parent) {
			missing++;
			console.warn('Parent missing:', item.name, item.id);
			if (autoremove) await unGroup(item);
		}
		else {
			const looped = hasConflictingNesting(item);
			if (looped) {
				loops++;
				console.warn('Looped item:', item.name, item.id);
				if (autoremove) await unGroup(item);
			}
		}
	}

	return { loops, missing, valid: loops == 0 && missing == 0 };
}

// Track ID changes
function preUpdateItem(item, changed, context) {
	if (context.recursive === false || context.diff === false) return;

	if (!changed.name && !changed.system?.tag && changed.system?.useCustomTag === undefined) return;

	const actor = item.actor;
	if (!actor) return;

	const oldTag = item._source.system.tag || pf1.utils.createTag(item.name);
	context.__exNestingTag = oldTag;
}

// Update other items that were relying on old ID
async function updateItem(item, changed, context, userId) {
	if (context.recursive === false || context.diff === false) return;

	if (game.user.id !== userId) return;
	const oldTag = context.__exNestingTag;
	if (!oldTag) return;
	const newTagId = item.system.tag || pf1.utils.createTag(changed.name || item.name);
	if (newTagId === oldTag) return;
	const actor = item.actor;
	if (!actor) return;

	console.debug('Tag changed:', oldTag, '=>', newTagId);

	const items = (actor.itemTypes[item.type] ?? [])
		.filter(i => getGrouping(i)?.tag === oldTag);

	if (items.length == 0) return;

	for (const item of items)
		await groupBy(item, newTagId);
}

Hooks.once('init', () => {
	game.settings.register(CFG.id, CFG.SETTINGS.guard, {
		name: 'PF1ItemNesting.Settings.ActiveGuards',
		hint: 'PF1ItemNesting.Settings.ActiveGuardsHint',
		type: Boolean,
		default: false,
		scope: 'client',
		config: true,
	});

	game.settings.register(CFG.id, CFG.SETTINGS.offActor, {
		name: 'PF1ItemNesting.Settings.OffActor',
		hint: 'PF1ItemNesting.Settings.OffActorHint',
		type: Boolean,
		default: false,
		scope: 'world',
		config: true,
	});

	game.settings.register(CFG.id, 'migration', {
		type: String,
		default: '0.6.0',
		scope: 'world',
		config: false,
	});
});

function limitPrecision(number, decimals = 2) {
	const mult = Math.pow(10, decimals);
	return Math.floor(number * mult) / mult;
}

async function migrate() {
	if (game.users.activeGM) {
		if (!game.users.activeGM.isSelf) return;
	}

	const migration = game.settings.get(CFG.id, 'migration');
	const mod = game.modules.get(CFG.id);
	if (migration === mod.version || isNewerVersion(migration, mod.version)) return void console.debug('Nesting 🪺 | Migration | Not needed');

	const actors = game.actors.filter(a => a.isOwner);
	let pct10 = Math.max(25, Math.floor(actors.length / 10));
	if (actors.length < 50) pct10 = 100;
	let i = 0, migrated = 0;
	for (const actor of actors) {
		i += 1;
		if (i % pct10 === 0) console.log('Nesting 🪺 | Migration |', limitPrecision((i / actors.length) * 100, 1), '% |', i, 'out of', actors.length);

		for (const item of actor.items) {
			const group = item.getItemDictionaryFlag('groupBy');
			if (group) {
				migrated += 1;
				await item.removeItemDictionaryFlag('groupBy', { render: false });
				await item.update({ [CFG.path]: group });
			}
		}
	}

	console.log('Nesting 🪺 | Migration | Complete |', migrated, 'Items Adjusted');

	if (game.users.activeGM?.isSelf) game.settings.set(CFG.id, 'migration', mod.version);
}

// pf1PostReady is fired after system migration is done
Hooks.once('pf1PostReady', () => migrate());

// Register API
Hooks.once('ready', () => {
	game.modules.get(CFG.id).api = {
		validate: {
			item: publicItemValidator,
			actor: publicActorValidator,
		},
		migrate,
		ungroup: unGroup,
		group: groupBy,
		get: getGrouping,
		getParent: (item) => {
			const actor = item.actor;
			const { id, tag, found } = getGrouping(item);
			if (!found) return null;
			return getItemByIdOrTag({ actor, id, tag });
		},
		isLooped: (item) => hasConflictingNesting(item),
	};
});

Hooks.on('renderActorSheet', nestActorItems);
Hooks.on('renderItemSheetPF', injectNestingConfig);
Hooks.on('preUpdateItem', preUpdateItem);
Hooks.on('updateItem', updateItem);
