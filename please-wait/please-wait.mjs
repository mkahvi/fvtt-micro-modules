const CFG = {
	module: 'please-wait',
	socketFlag: undefined,
	gracePeriod: 30_000,
	element: undefined,
	ident: undefined,
	animDelay: 900,
};

function utcOffset() {
	return new Date().getTimezoneOffset() * -60_000;
}

/**
 * @returns {number} Microseconds since epoch.
 */
function getUTC() {
	const d = new Date();
	const utc = d.getTime() + d.getTimezoneOffset() * 60_000;
	return utc;
}

function getUserFromData() {
	const data = game.data;
	const id = data.userId;
	const userData = id ? data.users?.find(u => u._id === id) : null;
	return {
		id,
		role: userData?.role ?? 1,
		name: userData?.name,
	};
}

const startTime = getUTC();

const inprogress = {};

export const maxPrecision = function (num, decimalPlaces = 0, type = 'round') {
	const p = Math.pow(10, decimalPlaces || 0),
		n = num * p * (1 + Number.EPSILON);
	return Math[type](n) / p;
};

function delayedRemoval(el, time) {
	setTimeout(_ => el.remove(), time);
}

function userCleanup(sid, report = true) {
	// console.log('Cleaning up after', sid);
	if (report) ui.notifications?.warn(`User [${sid}] loading in has either vanished or is having excessive trouble.`);
	inprogress[sid].element.classList.add('done');
	delayedRemoval(inprogress[sid].label, CFG.animDelay);
	delayedRemoval(inprogress[sid].element, CFG.animDelay);
	inprogress[sid].label = undefined;
	inprogress[sid].element = undefined;

	// Remove notification
	if (!inprogress || Object.keys(inprogress).length === 0) {
		CFG.element.classList.toggle('active-halt', false);
	}

	delete inprogress[sid];
}

function resetUserTimeout(sid, { renew = true, user = undefined } = {}) {
	clearTimeout(inprogress[sid].timer);
	if (renew) {
		inprogress[sid].timer = setTimeout(() => userCleanup(sid), CFG.gracePeriod);
		console.log('PLEASE WAIT | Waiting', CFG.gracePeriod / 1000, 'seconds for', user ? user : `SID:${sid}`, 'before giving up');
	}
}

function setupUser(data) {
	const sid = data.socketId;

	const element = document.createElement('div');
	element.dataset.socket = sid;
	element.classList.add('user');
	const label = document.createElement('h3');
	element.append(label);
	CFG.element.append(element);

	const ipdata = {
		id: data.user,
		state: 'init',
		lastSeen: getUTC(),
		reported: {
			start: data.start,
			emit: data.time,
		},
		element,
		label,
	};
	inprogress[data.socketId] = ipdata;
	return ipdata;
}

function loadingInProgress(data) {
	console.log('PLEASE WAIT | data received:', data.user, data.state, data);
	const passedTime = (data.time - data.start) / 1_000;
	const sid = data.socketId;
	const date = new Date(data.start + utcOffset());
	// console.log('TIME.NOW:', Date.now());
	// console.log('STARTED.UTC:', new Date(data.start).getTime());
	// console.log('UTC.OFFSET:', utcOffset());
	// console.log('STARTED.LOCAL:', new Date(data.start + utcOffset()).getTime());

	switch (data.state) {
		case 'init': {
			CFG.element.classList.toggle('active-halt', true);

			const o = ['User'];
			const user = game.users.get(data.user);
			const userName = user?.name;
			if (userName) o.push(`"${userName}"`);
			else o.push(`[SID:${sid}]`);
			o.push('is loading [INIT] – started at:');
			o.push(date.toTimeInputString());

			const ipdata = setupUser(data);
			const output = o.join(' ');
			ipdata.label.textContent = output;
			// ui.notifications?.warn(output);
			console.log('PLEASE WAIT |', output);

			resetUserTimeout(data.socketId, { user: user.id });
			break;
		}
		case 'setup': {
			if (!inprogress[data.socketId]) setupUser(data);

			inprogress[data.socketId].state = 'setup';
			inprogress[data.socketId].reported.emit = data.time;

			const o = ['User'];
			const user = game.users.get(data?.user);
			const userName = user.name;
			if (userName) o.push(`"${userName}"`);
			else o.push(`[SID:${sid}]`);
			o.push(`is loading [SETUP, reached after ${maxPrecision(passedTime, 2)}s]`);

			const label = inprogress[data.socketId].label;
			const output = o.join(' ');
			if (label) label.textContent = output;
			// ui.notifications?.warn(output);
			console.log('PLEASE WAIT |', output);

			// inprogress[data.socketId].label.textContent = `User [SID:${sid}] is loading [SETUP, reached after ${maxPrecision(passedTime, 2)}s]`;

			resetUserTimeout(data.socketId, { user: user?.id });
			break;
		}
		case 'ready': {
			if (!inprogress[data.socketId]) setupUser(data);

			const user = game.users.get(data.user);
			const username = user.name;
			const output = `User "${username}" has finished loading (in ${maxPrecision(passedTime, 2)}s - started at: ${date.toTimeInputString()})`;
			const label = inprogress[data.socketId].label;
			if (label) label.textContent = output;
			// ui.notifications?.info(output);
			console.log('PLEASE WAIT |', output);

			resetUserTimeout(data.socketId, { renew: false, user: user.id });
			userCleanup(data.socketId, false);
			break;
		}
		case 'reload': {
			// Uhh?
			// Since we can't discern the cause for page unloading (the assumed reload), showing anything here seems bad.
			// Otherwise it would show when people just log off.
			console.log('PLEASE WAIT | User unloaded:', data.user, game.users.get(data.user)?.name);
		}
	}
}

/**
 * @param {object} override
 * @returns {object}
 */
function generateBasicSocketData(override = {}) {
	const data = foundry.utils.mergeObject({
		socketId: game.socket.id,
		user: game.user?.id,
		time: getUTC(),
		start: startTime,
	}, override);

	if (!data.user) {
		data.user = CFG.ident?.user;
		data.cachedUser = true;
	}

	return data;
}

/**
 * Attempt to signal reload.
 *
 * @param {Event} event
 */
function reloading(event) {
	// TODO: How to detect it's a reload and not just unload?
	// Options:
	// - Listen to f5 and similar keypresses... bad solution
	// - Hope event has information on intent? ... probably not the case, but would be great.
	// console.log(event);
	// console.dir(event);
	const socketData = generateBasicSocketData({ state: 'reload' });
	// Refresh stored data
	localStorage.setItem(`module.please-wait.ident.${game.world.id}`, JSON.stringify({ user: game.user.id, world: game.world.id, timestamp: getUTC() }));
	game.socket.emit(CFG.socketFlag, socketData);
}

Hooks.once('init', () => {
	CFG.socketFlag = `module.${CFG.module}`;

	console.log('PLEASE WAIT | init', game.socket.id);

	const user = game.user ?? getUserFromData();
	if (user?.id) {
		console.log('PLEASE WAIT | User:', { user, socket: game.socket.id });
	}
	else {
		const pageReload = performance.navigation && performance.navigation.type == performance.navigation.TYPE_RELOAD;
		const ident = JSON.parse(localStorage.getItem(`module.please-wait.ident.${game.world.id}`) ?? {});
		// Check if we have an ident and navigation was a reload or within decent timeframe
		if (ident && (pageReload || ident.timestamp > getUTC() - 180_000)) {
			console.log('PLEASE WAIT | IDENT:', ident);
			CFG.ident = ident;
		}
		else {
			console.log('PLEASE WAIT | Stale identity ignored.');
		}
	}

	const socketData = generateBasicSocketData({ state: 'init', user: user?.id });
	game.socket.emit(CFG.socketFlag, socketData);

	window.addEventListener('beforeunload', reloading);

	// TODO: Start sending keep alive messages
});

Hooks.once('setup', () => {
	const user = game.user ?? getUserFromData();

	const socketData = generateBasicSocketData({ state: 'setup', user: user?.id });
	game.socket.volatile.emit(CFG.socketFlag, socketData);
	console.log('PLEASE WAIT | setup', { user, socket: game.socket.id });

	// For displaying other people loading.
	const div = document.createElement('div');
	div.id = 'please-wait';
	div.classList.add('loading');
	CFG.element = div;
	document.body.append(div);
});

Hooks.once('ready', () => {
	const socketData = generateBasicSocketData({ state: 'ready' });
	game.socket.emit(CFG.socketFlag, socketData);
	console.log('PLEASE WAIT | ready', game.socket.id);

	const time = (socketData.time - socketData.start) / 1_000;
	if (game.users?.filter(u => u.active).length > 1)
		ui.notifications?.info(`Reported load time to other users: ${maxPrecision(time, 2)}s`);

	// Set grace period for other users to be larger of default or 4 times our own.
	CFG.gracePeriod = Math.floor(Math.max(CFG.gracePeriod, time * 4));

	// Listen for others
	game.socket.on(CFG.socketFlag, loadingInProgress);

	// Save info so refresh reports the user name nice
	localStorage.setItem(`module.please-wait.ident.${game.world.id}`, JSON.stringify({ user: game.user.id, world: game.world.id, timestamp: getUTC() }));
});
