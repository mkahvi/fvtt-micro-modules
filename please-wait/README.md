# Please Wait

![Supported Foundry Versions](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fversion%3Fstyle%3Dflat%26url%3Dhttps%3A%2F%2Fgitlab.com%2Fmkahvi%2Ffvtt-micro-modules%2F-%2Fraw%2Fmaster%2Fplease-wait%2Fmodule.json)

Announces users in midst of loading.

Foundry does not currently (v11 and before at least) handle keeping users in sync if updates happen during their loading process. This attempts to mitigate the problems by announcing when people need to pause fiddling with things to ensure the user loads fully, removing the need for repeat reloads.

⚠️ Foundry v12 should make this module obsolete. ⚠️

## Flaws

- Early loading notifications may mislabel users if they have multiple accounts in the same world.

## Install

Manifest URL: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/master/please-wait/module.json>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
