# Change Log

## 1.0.1.3

- Fix: Holding alt while moving combatant would cause current turn to shift.

## 1.0.1.2

- Fix: With move turn disabled, current turn would shift incorrectly when moving current combatant.

## 1.0.1.1

- Fix: Re-ordering would fail without active combat (while setting it up). [#41]

## 1.0.1

- Fix: Re-ordering sometimes not refreshing combatant order.

## 1.0.0

- Foundry v10 compatibility
- Feature completeness confirmed

## 0.2.0

- Changed how initiative re-ordering works by default. Old behaviour can be restored in module settings.

## 0.1.1

- Foundry v9 compatibility confirmation

## 0.1.0.1

- Permission fix

## 0.1

- Initial release
