/**
 * BUGS:
 * - Highlight line does not swap on Alt press.
 */

const CFG = {
	module: 'mkah-reorder-initiative',
	SETTINGS: {
		moveTurn: 'moveTurn',
		fixTracker: 'fixTracker',
	},
};

const combatantMatch = '.combatant[data-combatant-id]';

let dragId, currentId, currentValidEl, entered = false;

function altEventHandler(ev) {
	const alt = ev.altKey;
	currentValidEl?.classList.remove(alt ? 'drag-hover-over' : 'drag-hover-under');
	currentValidEl?.classList.add('drag-hover', alt ? 'drag-hover-under' : 'drag-hover-over');
}

/**
 *
 * @param {string} combatantId
 * @param {number} initiative
 * @param {object} options
 * @param {number} options.adjust
 * @returns
 */
async function updateCombatant(combatantId, initiative, { adjust: diff = 0 } = {}) {
	if (!game.user.isGM) return;

	const combat = game.combats.viewed;

	const moveTurn = game.settings.get(CFG.module, CFG.SETTINGS.moveTurn);

	if (combat.combatant?.id === combatantId) {
		const turn = combat.turn;

		let newTurn = turn;
		if (moveTurn) {
			const turns = combat?.turns
				.map((c, idx) => ({ id: c.id, initiative: c.initiative }));
			turns.find(c => c.id === combatantId).initiative = initiative;
			turns.sort((a, b) => b.initiative - a.initiative);
			newTurn = turns.findIndex(c => c.id === combatantId);
		}
		else {
			// NOP
		}

		return combat.update({
			turn: newTurn,
			combatants: [{ _id: combatantId, initiative }],
		},
		{ _reorderReinit: true });
	}

	return combat.combatants.get(combatantId).update({ initiative });
}

/**
 * @param {Event} ev
 */
function dragStartEvent(ev) {
	const el = ev.target;
	/** @type {Element} */
	const c = el.matches(combatantMatch) ? el : el.closest(combatantMatch);
	if (!c) return false;

	const d = c.dataset;
	dragId = d.combatantId;
	currentId = d.combatantId;

	ev.stopPropagation(); // Dragging from image doesn't work without this for some reason.

	ev.dataTransfer.setData('text/plain', JSON.stringify(d));
	ev.dataTransfer.effectAllowed = 'move';

	ev.dataTransfer.setDragImage(c, c.clientLeft + 60, c.clientTop + 30);
	// ev.dataTransfer.setDragImage(c, c.offsetX + 60, c.offsetY + 30);

	c.classList.add('drag-source-active');

	document.body.addEventListener('dragend', () => {
		currentId = dragId = undefined;
		c.classList.remove('drag-source-active');
	}, { passive: true, once: true });

	return true;
}

function dragOverEvent(ev) {
	if (!dragId) return;

	const el = ev.target,
		c = el.matches(combatantMatch) ? el : el.closest(combatantMatch);

	if (!c) return false;
	currentValidEl = c;

	altEventHandler(ev);
}

/**
 * Purely for the bling
 *
 * @param {DragEvent} ev
 */
function activeDragEvent(ev) {
	if (!dragId) return;

	let curEl = ev.target;
	if (!curEl.matches(combatantMatch)) {
		const childEl = curEl.closest(combatantMatch);
		if (!childEl) {
			currentId = undefined;
			return;
		}
		curEl = childEl;
	}
	currentValidEl = curEl;
	entered = true;

	const targetId = curEl.dataset.combatantId;

	const allowed = dragId !== targetId;
	ev.dataTransfer.dropEffect = allowed ? 'move' : 'none';

	if (currentId === targetId) return;
	currentId = targetId;

	altEventHandler(ev);
}

/**
 * Purely for the bling
 *
 * @param {DragEvent} ev
 */
function dragLeaveEvent(ev) {
	if (!dragId) return;

	let curEl = ev.target;
	if (!curEl.matches(combatantMatch)) {
		const childEl = curEl.closest(combatantMatch);
		if (!childEl) return;
		curEl = childEl;
	}

	if (entered) currentValidEl = null;
	entered = false;

	const targetId = curEl.dataset.combatantId;

	if (currentId !== targetId || !this.contains(ev.relatedTarget))
		curEl.classList.remove('drag-hover', 'drag-hover-under', 'drag-hover-over');
}

/**
 * @param {DragEvent} ev
 */
function dropEvent(ev) {
	if (!dragId) return;

	const recordedId = dragId;
	dragId = undefined;

	const dropEl = ev.target,
		targetEl = dropEl.matches(combatantMatch) ? dropEl : dropEl.closest(combatantMatch),
		combatantId = targetEl?.dataset.combatantId;
	if (!combatantId) return;

	const alt = ev.altKey;

	targetEl.classList.remove('drag-hover', 'drag-hover-under', 'drag-hover-over');

	const combat = game.combat;
	if (!combat) return;

	let dropData;
	try {
		dropData = JSON.parse(ev.dataTransfer.getData('text/plain'));
	}
	catch (err) {
		return;
	}

	if (recordedId !== dropData?.combatantId) return; // Record of drop item mismatch

	const dropped = game.combat.combatants.get(dropData?.combatantId),
		// Combatant under drop location
		target = game.combat.combatants.get(combatantId);
	if (!dropped || !target) return;
	if (dropped.id === target.id) return; // dropped onto itself

	const adjacentEl = alt ? targetEl.nextElementSibling : targetEl.previousElementSibling,
		// Combatant adjacent on the side of the highlight bar
		adjacent = game.combat.combatants.get(adjacentEl?.dataset?.combatantId);
	if (adjacent && dropped.id === adjacent.id) return; // drop shift into its original spot

	// TODO: Add broader support to different initiative rules
	let initDiff = adjacent?.initiative !== undefined ? adjacent.initiative - target.initiative : 0;

	// Cap init adjustment to 1
	if (initDiff > 0)
		initDiff = Math.min(1, initDiff / 2);
	else if (initDiff < 0)
		initDiff = Math.max(-1, initDiff / 2);
	else
		initDiff = alt ? -1 : 1;

	updateCombatant(dropped.id, target.initiative + initDiff, { adjust: initDiff });
}

function onCombatUpdate(combat, _changed, context, _userId) {
	if (context._reorderReinit) {
		// Fix combat turns
		const fixTracker = game.settings.get(CFG.module, CFG.SETTINGS.fixTracker);
		if (fixTracker && combat === game.combats.viewed) {
			combat.setupTurns();
			ui.combat.initialize();
		}
	}
}

Hooks.once('init', () => {
	game.settings.register(CFG.module, CFG.SETTINGS.moveTurn, {
		name: 'InitiativeReorder.MoveTurn.Label',
		hint: 'InitiativeReorder.MoveTurn.Hint',
		type: Boolean,
		default: false,
		scope: 'world',
		config: true,
	});

	game.settings.register(CFG.module, CFG.SETTINGS.fixTracker, {
		name: 'InitiativeReorder.FixTracker.Label',
		hint: 'InitiativeReorder.FixTracker.Hint',
		type: Boolean,
		default: true,
		scope: 'world',
		config: true,
	});
});

function addDragHandles(tracker, jq, _) {
	if (!game.user.isGM) return;

	// Mark all combatants as draggable
	jq.find('li.combatant').addClass('draggable').attr('draggable', true);
}

/**
 * @param {CombatTracker} app
 * @param {JQuery} html
 * @param {object} options
 */
function hookCombatTracker(app, [html], options) {
	if (!game.user.isGM) return;

	const listing = html.querySelector('.directory-list');
	if (!listing) return console.error('User listing not found in combat tracker');

	listing.addEventListener('dragover', dragOverEvent, { passive: true });
	listing.addEventListener('dragleave', dragLeaveEvent, { passive: true });
	listing.addEventListener('dragenter', activeDragEvent, { passive: true });
	listing.addEventListener('drop', dropEvent, { passive: true });

	listing.addEventListener('dragstart', dragStartEvent, { passive: true });
}

Hooks.on('renderCombatTracker', addDragHandles);
Hooks.on('renderCombatTracker', hookCombatTracker);
Hooks.on('updateCombat', onCombatUpdate);
