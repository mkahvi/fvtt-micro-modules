const CFG = {
	module: 'mkah-pf1-archaic-damage-buttons',
	SETTINGS: {
		enabled: 'enabled',
		keepNew: 'keepNew',
		headers: 'headers',
	},
};

const signNum = new Intl.NumberFormat(undefined, { signDisplay: 'always' }).format;

const makeButton = (text, value, tags, classes = [], { isHalf = false, isFull = false, isCritical = false } = {}) => {
	const ab = document.createElement('a');
	ab.classList.add('inline-action');
	ab.dataset.value = value;
	ab.dataset.tags = tags;
	ab.dataset.action = 'applyDamage';
	ab.textContent = text;
	ab.classList.add('damage-button', ...classes);
	ab.dataset.type = isCritical ? 'critical' : 'normal';
	if (game.release.generation >= 10) {
		const delta = game.i18n.format('ArchaicDamageButtons.Health', { delta: `<span class='value'>${signNum(-value)}</span>` }),
			type = game.i18n.format(`ArchaicDamageButtons.${isCritical ? 'Critical' : 'Normal'}`, { percentage: `<span class='percentage'>${text}</span>` }),
			tt = `${type}<br>${delta}`;
		ab.dataset.tooltip = tt;
		ab.dataset.tooltipDirection = 'DOWN';
		ab.dataset.tooltipClass = 'archaic-buttons';
	}
	else {
		ab.title = game.i18n.format('ArchaicDamageButtons.Health', { delta: signNum(-value) });
	}
	return ab;
};

const getDocData = (doc) => game.release.generation >= 10 ? doc : doc?.data;

/**
 * @param {ChatMessage} cm
 * @param {JQuery} html
 */
function replaceDamageButtons(cm, [html]) {
	const enabled = game.settings.get(CFG.module, CFG.SETTINGS.enabled);
	if (!enabled) return;

	const cmData = getDocData(cm);

	// Check for first instance of damage
	const damage0 = cmData.flags?.pf1?.metadata?.rolls?.attacks?.[0]?.damage;
	const hasDamage0 = damage0?.[0] !== undefined;
	if (!hasDamage0) return;

	const cardContent = html.querySelector('.pf1.item-card');
	if (!cardContent) return;

	/*
	const { itemId, actionId, tokenUuid, actorId } = cardContent.dataset ?? {};

	const item = cm.itemSource,
		action = item.actions?.get(actionId);
	*/

	// const hasDamage1 = action?.hasDamage ?? item?.hasDamage;
	// if (!hasDamage1) return;

	const keepInline = game.settings.get(CFG.module, CFG.SETTINGS.keepNew),
		addHeaders = game.settings.get(CFG.module, CFG.SETTINGS.headers);

	cardContent?.querySelectorAll('.chat-attack').forEach(atk => {
		const table = atk.querySelector('table');
		if (!table) {
			// console.error('Missing Attack/Damage Table on attack card:', cm, atk);
			return;
		}
		const applyDamage = table?.querySelectorAll('th .inline-action[data-action="applyDamage"]');
		if (applyDamage.length === 0) return;
		// const parent = applyDamage[0].parentElement;
		const buttons = [];
		applyDamage?.forEach(dmg => {
			buttons.push({ value: dmg.dataset.value, tags: dmg.dataset.tags });
			if (!keepInline) dmg.remove();
		});
		// buttons.sort((a, b) => b.value - a.value); // They should already be in order
		const [normalFull, normalHalf, critFull, critHalf] = buttons;
		const damageButtons = document.createElement('div');
		damageButtons.classList.add('archaic-damage-buttons', 'card-buttons', 'card-button-group');

		function addButtons(container, full, half, { isCritical = false } = {}) {
			const applyFull = makeButton('100%', full.value, full.tags, ['apply-full', isCritical ? 'critical' : 'normal'], { isFull: true, isCritical }),
				applyHalf = makeButton('50%', half.value, half.tags, ['apply-half', isCritical ? 'critical' : 'normal'], { isHalf: true, isCritical });
			container.append(applyFull, applyHalf);
		}

		function generateHeader(text, css = []) {
			if (!addHeaders) return damageButtons;
			const header = document.createElement('div');
			header.classList.add('button-group', ...css);
			const label = document.createElement('label');
			label.classList.add('header');
			label.textContent = text;
			header.append(label);
			damageButtons.append(header);
			return header;
		}

		addButtons(generateHeader(game.i18n.localize('PF1.Normal'), ['normal']), normalFull, normalHalf);
		if (critFull) addButtons(generateHeader(game.i18n.localize('PF1.Critical'), ['critical']), critFull, critHalf, { isCritical: true });

		table.after(damageButtons);

		// Clicks are intercepted by .card-buttons listener and nothing special is needed for that here.
	});
}

Hooks.once('init', function archaicButtonsSetup() {
	game.settings.register(CFG.module, CFG.SETTINGS.enabled, {
		name: 'ArchaicDamageButtons.Settings.Enabled',
		default: true,
		type: Boolean,
		scope: 'client',
		config: true,
	});

	game.settings.register(CFG.module, CFG.SETTINGS.headers, {
		name: 'ArchaicDamageButtons.Settings.Headers',
		hint: 'ArchaicDamageButtons.Settings.HeadersHint',
		default: true,
		type: Boolean,
		scope: 'client',
		config: true,
	});

	game.settings.register(CFG.module, CFG.SETTINGS.keepNew, {
		name: 'ArchaicDamageButtons.Settings.KeepSystem',
		hint: 'ArchaicDamageButtons.Settings.KeepSystemHint',
		default: false,
		type: Boolean,
		scope: 'client',
		config: true,
	});
});

Hooks.on('renderChatMessage', replaceDamageButtons);
