# Change Log

## 1.3.0.1

- Fix: Conformity with system API.

## 1.3.0

- Change: Damage tooltip renders now down instead of up to not obscure damage breakdown.
- Style: Tooltip is now more colored.
- i18n: Full support

## 1.2.0

- Fix: Shift clicking damage buttons did not bring up apply damage dialog.
- Change: Buttons are now prettier(?)

## 1.1.0

- Change: Use Foundry tooltips on v10 instead of title attribute

## 1.0.0.1

- Fixed: Bad detection of no damage in some cards.

## 1.0.0

- Foundry v10 support

## 0.1.1.2

- Fixed: Add missing common identifiers for module compatibility.

## 0.1.1.1

- Fixed: Buttons for cards with missing item source.

## 0.1.1

- Fixed: Critical damage buttons were missing
- New: Hover tooltip on buttons state how much they change.

## 0.1.0 Initial
