# Accountant for Pathfinder 1e

![Supported Foundry Versions](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fversion%3Fstyle%3Dflat%26url%3Dhttps%3A%2F%2Fgitlab.com%2Fmkahvi%2Ffvtt-micro-modules%2F-%2Fraw%2Fmaster%2Fpf1-accountant%2Fmodule.json)

Prints changes to wealth to chat log.

Small grace period allows the old message to be updated with new total change before new card is printed.

By default shows exact currencies edited, but has option of turning all currencies into gold.

The cards are by default printed as whispers to all owning players (and GMs).

![Chat Card](./img/screencaps/chat-card.png)

  ✅ Recommended. I feel it's a bit toxic, but if people have habit of making mistakes (like myself), it's fine.

## Known issues

Money inside a container that is also inside another container will not be detected. The system does not provide useful diffs to such changes, or at least I have not figured such out.

## Install

Manifest URL: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/master/pf1-accountant/module.json>

For Foundry 0.7 you need to manually install older version: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/blob/8cd0c02ba5f1c1e8852d8c18b72c182127469665/pf1-accountant/module.zip>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT license](./LICENSE).
