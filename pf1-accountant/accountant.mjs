const MODULE_ID = 'pf1-accountant';

const CFG = {
	SETTINGS: {
		gracePeriod: 'gracePeriod',
		interveningMessages: 'interveningMessages',
		exactCurrency: 'exactCurrency',
	},
	COLOR: {
		main: 'color:goldenrod',
		number: 'color:mediumpurple',
		unset: 'color:unset',
	},
};

const signNum = (value) => value >= 0 ? `+${value}` : `${value}`;

const currencyTypes = ['pp', 'gp', 'sp', 'cp'];
let notMyAccountingCounter = 5000;
let lastAccountingMsgId = null;

class Currency {
	base = {};
	alt = {};
	delta = {};

	constructor(data) {
		currencyTypes.forEach(t => {
			this.base[t] = data.currency?.[t] ?? 0;
			this.alt[t] = data.altCurrency?.[t] ?? 0;
			this.delta[t] = 0;
		});
	}
}

/**
 * Generate currency card contents based on currency deltas.
 *
 * @param {object} deltas
 */
const createCardContents = (deltas) => {
	// TODO: Support pf1.config.currency configuration
	// const cp = pf1.utils.currency.merge(deltas)
	// const gp = pf1.utils.currency.convert(cp);
	const cp = deltas.pp * 1_000 + deltas.gp * 100 + deltas.sp * 10 + deltas.cp;
	const gp = Math.floor(cp) / 100;

	const anyChange = Object.values(deltas).some(value => value !== 0);

	const div = document.createElement('div');
	div.classList.add('pf1-accountant');

	const preface = document.createElement('span');
	preface.classList.add('prelude');
	preface.textContent = 'Currency:';
	div.append(preface, ' ');

	const makeChange = (msg) => {
		const cel = document.createElement('span');
		cel.classList.add('change');
		cel.textContent = msg;
		return cel;
	};

	if (anyChange) {
		if (game.settings.get(MODULE_ID, CFG.SETTINGS.exactCurrency)) {
			let s = false;
			for (const [type, value] of Object.entries(deltas)) {
				if (value == 0) continue;
				if (s) div.append(', ');
				s = true;
				div.append(makeChange(`${signNum(value)} ${type}`));
			}

			div.dataset.tooltip = `${signNum(gp)} gp`;
		}
		else
			div.append(makeChange(`${signNum(gp)} gp`));
	}
	else
		div.append(makeChange('no change'));

	// else html.push(currencies.join(', '));

	return div.outerHTML;
};

const printCurrencyCard = async (actor, currency) => {
	if (!Object.values(currency.delta).find(v => v != 0)) {
		console.log('Deltas:', Object.values(currency.delta));
		console.log('Delta is not zero:', Object.values(currency.delta).find(v => v != 0));
		console.log('No currency change detected, ignoring.');
		return;
	}

	const msgData = {
		content: createCardContents(currency.delta),
		flags: { [MODULE_ID]: { accounting: true, delta: currency.delta } },
		speaker: ChatMessage.getSpeaker({ actor }),
	};

	// Collect all GMs and owning users
	const users = new Set();
	game.users.filter(u => actor.testUserPermission(u, CONST.DOCUMENT_OWNERSHIP_LEVELS.OWNER))
		.forEach(u => users.add(u.id));

	// Whisper to above users
	msgData.whisper = Array.from(users);

	return ChatMessage.create(msgData);
};

const updateLastCard = async (actor, ud) => {
	const cm = game.messages.get(lastAccountingMsgId);

	if (!cm) return printCurrencyCard(actor, ud);

	const sameActor = actor ? cm.speaker?.actor === actor.id : false;
	if (!sameActor) { // shouldn't happen
		console.error('%cAccounting%c | Last card isn\'t for us!', CFG.COLOR.main, CFG.COLOR.unset);
		return printCurrencyCard(actor, ud);
	}

	const odelta = cm.flags?.[MODULE_ID].delta;
	for (const c of currencyTypes)
		ud.delta[c] = ud.delta[c] + (odelta?.[c] ?? 0);

	const msgData = {
		flags: { [MODULE_ID]: { accounting: true, delta: ud.delta } },
		content: createCardContents(ud.delta),
	};

	lastAccountingMsgId = cm.id;
	return cm.update(msgData);
};

/**
 * @param {ChatMessage} cm
 * @param {JQuery} html
 */
const renderChatMessageEvent = (cm, [html]) => {
	const acc = cm.getFlag(MODULE_ID, 'accounting');
	if (acc === undefined) {
		notMyAccountingCounter++;
		return;
	}

	if (cm.isAuthor) {
		notMyAccountingCounter = 0;
		lastAccountingMsgId = cm.id;
	}

	const wt = html.querySelector('.whisper-to');
	if (wt) wt.style.display = 'none';

	html.style.borderColor = null;
	html.classList.add(MODULE_ID);
};

/**
 * @param {Actor} actor
 * @param {object} update
 * @param {object} options
 */
const preUpdateActorEvent = (actor, update, options) => {
	delete actor._tempAccountingMonitor; // Delete old one if present
	if (options.diff === false && options.recursive === false) return;
	if (!update.system) return; // No system data change
	if (!update.system.currency && !update.system.altCurrency) return; // No currency change
	if (actor.pack) return;

	const ud = new Currency(actor.toObject().system);
	actor._tempAccountingMonitor = ud;
};

/**
 * @param {Acctor} actor Actor
 * @param {Item} item Container if relevant
 * @param {object} oldCurrency  Transaction details
 */
const finalizeTransaction = (actor, item, oldCurrency) => {
	const cm = game.messages.get(lastAccountingMsgId),
		lastStamp = cm?.timestamp ?? 0,
		timeSince = Date.now() - lastStamp,
		sameActor = actor ? cm?.speaker?.actor === actor.id : false,
		grace = game.settings.get(MODULE_ID, CFG.SETTINGS.gracePeriod),
		maxDistance = game.settings.get(MODULE_ID, CFG.SETTINGS.interveningMessages);

	const currency = new Currency(actor.system);

	// Establish deltas
	for (const c of currencyTypes) {
		const d0 = (currency.base[c] ?? oldCurrency.base[c]) - oldCurrency.base[c],
			d1 = (currency.alt[c] ?? oldCurrency.alt[c]) - oldCurrency.alt[c];
		currency.delta[c] = d0 + d1;
	}

	console.log('%cAccounting%c | Transaction:', CFG.COLOR.main, CFG.COLOR.unset, currency.delta);

	if (!sameActor || notMyAccountingCounter > maxDistance || timeSince > grace * 1_000) {
		printCurrencyCard(actor, currency);
	}
	else {
		if (sameActor && grace > 0) updateLastCard(actor, currency);
		else printCurrencyCard(actor, currency);
	}
};

/**
 * @param {Actor} actor
 * @param _update
 * @param _options
 * @param {string} userId
 */
const updateActorEvent = (actor, _update, _options, userId) => {
	if (game.user.id !== userId) return; // Only triggering user should handle things for simplicity
	const ud = actor._tempAccountingMonitor;
	if (ud == undefined) return;
	delete actor._tempAccountingMonitor;

	finalizeTransaction(actor, null, ud);
};

/**
 * @param {Item} item
 * @param {object} update
 * @param {object} context
 */
const preUpdateItemEvent = (item, update, context) => {
	delete item._tempAccountingMonitor;
	if (!update.system) return;
	if (item.type !== 'container') return; // Only containers can contain money
	if (context.diff === false || context.recursive === false) return;
	if (item.pack) return;

	const ud = new Currency(item.toObject().system);

	if (ud) {
		if (!Object.values(ud.delta).find(v => v != 0)) return; // no change
		item._tempAccountingMonitor = ud;
	}
};

/**
 * @param {Item} item
 * @param _update
 * @param _options
 * @param {string} userId
 */
const updateItemEvent = (item, _update, _options, userId) => {
	if (game.user.id !== userId) return; // Only triggering user should handle things from here on out for simplicity
	if (item.type !== 'container') return; // Only containers can contain money

	const ud = item._tempAccountingMonitor;
	if (ud == undefined) return;
	delete item._tempAccountingMonitor;

	const actor = item.parent;
	if (!actor) {
		console.warn('%cACCOUNTANT%c | Could not find owning actor for item:', CFG.COLOR.main, CFG.COLOR.unset, item.name, item);
		return;
	}

	finalizeTransaction(actor, item, ud);
};

Hooks.on('renderChatMessage', renderChatMessageEvent);

Hooks.on('updateActor', updateActorEvent);
Hooks.on('preUpdateActor', preUpdateActorEvent);

Hooks.on('preUpdateItem', preUpdateItemEvent);
Hooks.on('updateItem', updateItemEvent);

Hooks.once('init', () => {
	game.settings.register(
		MODULE_ID,
		CFG.SETTINGS.gracePeriod,
		{
			name: 'Grace period',
			hint: 'Period (in seconds) for how long recent accounting message can be updated with new total. Reduces log spamming. 0 disables.',
			type: Number,
			default: 60,
			range: { min: 0, max: 300, step: 5 },
			scope: 'world',
			config: true,
		},
	);

	game.settings.register(
		MODULE_ID,
		CFG.SETTINGS.interveningMessages,
		{
			name: 'Max intervening messages',
			hint: 'New currency message is printed instead of updating regardless of the passed time if there\'s this many messages since.',
			type: Number,
			default: 3,
			range: { min: 0, max: 20, step: 1 },
			scope: 'world',
			config: true,
		},
	);

	game.settings.register(
		MODULE_ID,
		CFG.SETTINGS.exactCurrency,
		{
			name: 'Exact currency',
			hint: 'Shows exact currency gained or lost. If disabled, everything is converted to gold.',
			type: Boolean,
			default: true,
			scope: 'world',
			config: true,
		},
	);
});
