# Change Log

## 1.2.0.1

- Maintenance

## 1.2.0

- New: Tooltip that shows the total change as gold if exact currency is enabled.

## 1.1.2

- Fix: Chat message whisper target was not correctly generated.

## 1.1.1

- Fix: Poor order of operations that could lead to at least spurious logging if not erroneous reports.
- Fix: Actor owner would not see the accounting message if GM or another user updated the actor.

## 1.1.0

- Change: Module ID changed, please uninstall the old one.
- Fix: Ignore updates with diff/recursive false (e.g. imports).
- Minimum required versions updated to PF1 0.82.5 and Foundry v10

## 1.0.1.5

- Fix: Cannot read properties of null (reading 'style') if message visibility was changed.

## 1.0.1.4

- Fix: Ignore actors in compendiums.

## 1.0.1.3

- Fix: Player message combining was nonfunctional.

## 1.0.1.2

- Fix: Errors when actor update happens with no money change.

## 1.0.0

- Foundry v10 compatibilty
- Maintenance

## 0.1.1

- Container support

## 0.1.0.1

- Remove log spamming

## 0.1.0

- Initial
