const CFG = {
	id: 'pf1-action-tracker',
	socket: 'module.pf1-action-tracker',
	// Actions that have infinite uses and never deplete
	INFINITE_USES: ['free', 'nonaction'],
	SETTINGS: {
		nonactions: 'nonactions',
		moveActions: 'moveActions',
		directPath: 'directPath',
		startMarker: 'startMarker',
		keepMarker: 'keepMarker',
		transparency: 'transparency',
	},
	COLORS: {
		main: 'color:goldenrod',
		unset: 'color:unset',
		label: 'mediumseagreen',
	},
};

const createNode = (node = 'span', text = '', classes = [], { tooltip = null, isHTML = false, data } = {}) => {
	const n = document.createElement(node);
	if (isHTML) n.innerHTML = text;
	else n.textContent = text;
	if (classes.length) n.classList.add(...classes);
	if (tooltip) n.dataset.tooltip = tooltip;
	if (data) {
		Object.entries(data)
			.forEach(([key, value]) => n.setAttribute(`data-${key}`, value));
	}
	return n;
};

/** @type {ActionTrackerInterface} */
let actionTracker,
	/** @type {ActionTrackerInterface} */
	actionTrackerAlt;

class Point {
	x;
	y;
	z;
	/**
	 * @param {number} x
	 * @param {number} y
	 * @param {number} z
	 */
	constructor(x, y, z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	/**
	 * @param {Point} p1
	 * @returns {number}
	 */
	distanceTo(p1) {
		const d = Math.hypot(this.x - p1.x, this.y - p1.y);
		const v = this.z - p1.z;
		const rd = v != 0 ? Math.hypot(v, d) : d;
		return rd;
	}

	get valid() {
		const valid = this.x !== undefined && this.y !== undefined && this.z !== undefined;
		if (!valid) console.warn('Invalid Point:', this);
		return valid;
	}

	/**
	 * @param {Point} p1
	 */
	equals(p1) {
		return this.x === p1.x && this.y === p1.y && this.z === p1.z;
	}
}

class Marker {
	#at;
	#graphic;

	constructor(at) {
		this.#at = at;
	}

	get combatant() {
		return this.#at.combatant;
	}

	/**
	 * @param {Token} token
	 */
	async create(token) {
		let marker = this.#graphic;
		if (!marker) {
			const tokenDoc = token.document,
				texture = await loadTexture(tokenDoc.texture.src),
				sprite = new PIXI.Sprite(texture);

			// Align by center
			sprite.anchor.set(0.5, 0.5);

			const tokenRatioX = token.w / texture.width,
				tokenRatioY = token.h / texture.height;
			const { scaleX, scaleY } = tokenDoc.texture;
			sprite.scale.set(tokenRatioX * scaleX, tokenRatioY * scaleY);

			// Fix positioning to be just below the token
			sprite.sort = tokenDoc.sort;
			sprite.elevation = tokenDoc.elevation;

			// sprite.isShadow = true;
			// sprite.tint = 0xFFFFFF; // this resets tint, but it was never set
			sprite.alpha = 0.3; // Make this configurable?

			sprite._atMarker = true;
			this.#graphic = marker = sprite;
			canvas.primary.addChild(sprite);
			// canvas.primary.sortDirty = true; // Atropos told to do this, but PIXI says .addChild() does this automatically
		}
		marker.position.x = token.document.x + token.w / 2;
		marker.position.y = token.document.y + token.h / 2;
	}

	remove() {
		const g = this.#graphic;
		const m = canvas.primary.children.find(c => c._atMarker == true && c === g);
		if (m) canvas.primary?.removeChild(m);
		this.#graphic = null;
	}
}

class ActionTracker {
	/** @type {Actor} */
	actor;
	/** @type {Combatant} */
	combatant;

	/** @type {Marker} */
	marker;

	/**
	 * @param {Actor} actor
	 * @param {Combatant} combatant
	 */
	constructor(actor, combatant) {
		this.actor = actor;
		this.combatant = combatant;
		this.marker = new Marker(this);
		// console.log(actor?.name, actor?.id, combatant?.id, actor, combatant);
	}

	/**
	 * Consume actions.
	 *
	 * @param {object} activation
	 * @param {pf1.components.ItemAction} action
	 * @param {object} options
	 * @param {boolean} options.full Full-Attack
	 */
	useAction(activation, action, { full = false } = {}) {
		const { type, cost = 1 } = activation;
		if (!game.combat) return;
		const combatant = game.combat.combatant;
		const offTurn = combatant?.actor !== this.actor;

		console.debug('%cACTION TRACKER%c |', CFG.COLORS.main, CFG.COLORS.unset,
			action.item.name, '|', action.name, '|', cost, type, '| full:', full, 'offturn:', offTurn);

		const unchained = game.settings.get('pf1', 'unchainedActionEconomy');

		const types = [];

		switch (type) {
			case 'passive': return; // Ignore
			case 'attack':
				if (offTurn) {
					this.aoo += cost;
					types.push('aoo');
				}
				else if (full) {
					if (unchained) {
						this.action += cost;
						types.push('action');
					}
					else {
						this.standard += cost;
						this.move += cost;
						types.push('standard', 'move');
					}
				}
				else {
					if (unchained) {
						this.action += cost;
						types.push('action');
					}
					else {
						this.standard += cost;
						types.push('standard');
					}
				}
				break;
			case 'standard':
				this.standard += cost;
				types.push('standard');
				break;
			case 'move':
				this.move += cost;
				types.push('move');
				break;
			case 'swift':
				this.swift += cost;
				types.push('swift');
				break;
			case 'immediate':
				this.immediate += cost;
				types.push('immediate');
				break;
			case 'reaction':
				this.reaction += cost;
				types.push('reaction');
				break;
			case 'aoo':
				this.aoo += cost;
				types.push('aoo');
				break;
			case 'full':
			case 'round':
				if (unchained) {
					this.action += 3 * cost; // 3 actions per full/round action.
					types.push('action');
				}
				else {
					this.standard += cost;
					this.move += cost;
					types.push('standard', 'move');
				}
				break;
			case 'free':
				this.free += cost;
				types.push('free');
				break;
			case 'nonaction':
				this.nonaction += cost;
				types.push('nonaction');
				break;
			case 'special':
			case 'hour':
			case 'minute':
				console.warn('action unsuitable for combat');
				return;
		}

		// Update UI
		if (!offTurn) {
			// console.log('UPDATE UI');
			actionTracker?.useAction(types, cost, this);
			actionTrackerAlt?.useAction(types, cost, this);
		}
		else {
			console.log('%cACTION TRACKER%c | Off-turn action',
				CFG.COLORS.main, CFG.COLORS.unset,
				{ activation }, this);
		}

		if (this.combatant) {
			this.combatant._actionTracker?.UI.useAction(types, cost, this);
			this.combatant._actionTracker?.UIPop?.useAction(types, cost, this);
		}

		// TODO: Update UI
	}

	resetPath() {
		this.path = [];
	}

	/**
	 * @param {Point} p1
	 * @param {object} options
	 * @param {boolean} options.merge
	 */
	addPathNode(p1, { merge = false } = {}) {
		this.path.push(p1);
		/** @type {Point} */
		const p0 = this.path[0];
		if (this.path.length > 1 && p0.equals(p1)) {
			console.log('%cACTION TRACKER%c | Returned to origin, resetting path.',
				CFG.COLORS.main, CFG.COLORS.unset);
			this.resetPath();
			this.addPathNode(p0);
		}
		this.updateDistanceLabel();
	}

	updateDistanceLabel() {
		actionTracker?.updateDistanceLabel(this);
		actionTrackerAlt?.updateDistanceLabel(this);
	}

	action = 0;

	get action1() {
		return Math.max(Math.floor((this.action + 2) / 3), 0);
	}

	get action2() {
		return Math.max(Math.floor((this.action + 1) / 3), 0);
	}

	get action3() {
		return Math.max(Math.floor(this.action / 3), 0);
	}

	standard = 0;
	move = 0;
	reaction = 0;
	swift = 0;
	immediate = 0;
	aoo = 0;
	free = 0;
	nonaction = 0;
	step = false;

	path = [];

	get distance() {
		let distance = 0;
		if (this.path.length < 2) return 0;
		this.path.reduce((p0, p1) => {
			distance += p0.distanceTo(p1);
			return p1;
		});
		return distance;
	}

	get distanceDirect() {
		if (this.path.length == 0) return 0;
		const p0 = this.path[0],
			p1 = this.path.at(-1);
		return p0.distanceTo(p1);
	}

	/**
	 * Reset actions at start of turn.
	 */
	resetActions() {
		this.action = 0;
		this.standard = 0;
		this.move = 0;

		this.reaction = 0;
		this.swift = 0;
		// Immediate actions
		if (this.immediate > 0) this.swift = 1;
		this.immediate = 0;
		this.aoo = 0;
		this.free = 0;
		this.nonaction = 0;
		this.step = false;

		this.path = [];
	}
}

/**
 * @param {Actor} actor
 * @returns {boolean}
 */
const isVisible = (actor) => {
	if (!actor) return false;
	// TODO: Check visibility, too.
	const t = game.settings.get(CFG.id, CFG.SETTINGS.transparency);
	const levels = CONST.DOCUMENT_OWNERSHIP_LEVELS ?? CONST.DOCUMENT_PERMISSION_LEVELS;

	switch (t) {
		case 'owner': return actor.isOwner;
		case 'player': return actor.hasPlayerOwner;
		case 'limited': return actor.testUserPermission(game.user, levels.LIMITED);
		case 'generic': return actor.testUserPermission(game.user, levels.LIMITED) || actor.hasPlayerOwner;
		case 'all': return true;
	}

	return false;
};

/**
 * @param {Combatant} combatant
 * @returns {ActionTracker}
 */
const prepareCombatant = (combatant) => {
	combatant._actionTracker ??= new ActionTracker(combatant.actor, combatant);
	return combatant._actionTracker;
};

/**
 * @param {Combatant} combatant
 * @returns {ActionTracker}
 */
const resetCombatant = (combatant) => {
	combatant._actionTracker?.resetActions();
	combatant._actionTracker?.UI?.resetActions();
	combatant._actionTracker?.UIPop?.resetActions();
	return combatant._actionTracker;
};

/**
 * @param {Token} token
 * @param {Point} p1 Override
 * @returns {Point}
 */
const transformTokenPosition = (token, p1) => {
	const scene = token.scene;
	const { size, distance } = scene.grid;

	// Reduce distances to cell counts. .5 is for pointing to center of cell, allowing token wiggling within a cell to match correct one.
	const { x, y, elevation: z } = token.document;
	return new Point(
		Math.floor((p1?.x ?? x) / size + .5),
		Math.floor((p1?.y ?? y) / size + .5),
		Math.floor((p1?.z ?? z) / distance),
		distance,
	);
};

// Clear all markers on scene
const clearMarkers = () => {
	const markers = canvas.primary.children.filter(c => c._atMarker == true);
	markers.forEach(m => canvas.primary?.removeChild(m));
};

/**
 * @param {Token} actor
 * @param {Combatant} combatant
 * @param {boolean} reset
 */
const setTokenPosition = async (combatant, reset = false) => {
	const token = combatant.token?.object;
	if (!token) return;

	// console.log('setActorPosition:', actor, combatant, reset);
	/** @type {ActionTracker} */
	const at = combatant._actionTracker;
	if (!at) return;
	at.resetPath();
	const t = combatant.token?.object;
	if (!t) return; // void console.error('Combatant token missing?', combatant); // Different scene probably, or theater of the mind
	const u = transformTokenPosition(t);
	at.addPathNode(u);

	// TODO: Draw symbol at actor starting location.
	updateTokenMarker(t);
};

const updateTokenMarker = (token) => {
	const startMarker = game.settings.get(CFG.id, CFG.SETTINGS.startMarker);
	if (!startMarker) return;

	const at = token.document.combatant._actionTracker;
	if (!at) return;

	// console.log('Token Visible?', token.isVisible);
	if (!token.isVisible)
		return void at.marker.remove();

	// console.log('DRAWING START LOCATION');
	at.marker.create(token);
};

function ActionValueLabel(value, depleted = false) {
	return !depleted && value == 0 ? '✔' : value <= 1 ? '✘' : value;
}

class CombatantInterface {
	/** @type {Actor} */
	actor;
	/** @type {Combatant} */
	combatant;
	/** @type {ActionTracker} */
	at;

	/**
	 * @param {Element} html
	 * @param {CombatTracker} app
	 * @param combatant
	 */
	constructor(html, app, combatant) {
		combatant._actionTracker ??= new ActionTracker(combatant.actor, combatant);
		const at = combatant._actionTracker;

		this.combatant = combatant;
		this.actor = combatant.actor;
		this.at = prepareCombatant(this.combatant);
		const oldAt = app.popOut ? at.UIPop : at.UI;
		this.render(html, oldAt);
		if (!oldAt) {
			this.resetActions();
			if (app.popOut) at.UIPop = this;
			else at.UI = this;
		}
	}

	actions = {
		reaction: null,
		swift: null,
		immediate: null,
		aoo: null,
	};

	/**	@type {Element} */
	element;

	/**
	 * @param {Element} html
	 * @param {CombatantInterface} oldAT
	 */
	render(html, oldAT) {
		const effects = html.querySelector('.token-effects');

		let d;
		if (oldAT) {
			d = oldAT.element;
			this.actions = oldAT.actions;
		}
		else {
			d = createNode('div', null, ['action-tracker', 'combatant']);

			const unchained = game.settings.get('pf1', 'unchainedActionEconomy');
			if (unchained) {
				this.actions.reaction = createNode('span', ActionValueLabel(0), ['action', 'reaction'], { data: { tooltip: 'Reaction', action: 'reaction' } });
				d.append(this.actions.reaction);
			}
			else {
				this.actions.swift = createNode('span', ActionValueLabel(0), ['action', 'swift'], { data: { tooltip: 'Swift', action: 'swift' } });
				this.actions.immediate = createNode('span', ActionValueLabel(0), ['action', 'immediate'], { data: { tooltip: 'Immediate', action: 'immediate' } });
				d.append(this.actions.swift, this.actions.immediate);
			}

			this.actions.aoo = createNode('span', null, ['action', 'aoo'], { data: { tooltip: 'Attack of Opportunity', action: 'aoo' } });
			d.append(this.actions.aoo);
		}

		this.element = d;
		effects.after(d);
	}

	/**
	 * @param {Element} el
	 * @param {number} value
	 * @param {string} type Action type
	 * @param depleted
	 */
	setActionValue(el, value, type, depleted = false) {
		try {
			const label = ActionValueLabel(value, depleted);
			const tel = el.firstChild;
			if (tel) tel.textContent = label;
			else el.prepend(label);
			el.classList.toggle('ready', value == 0);
			if (!CFG.INFINITE_USES.includes(type)) {
				el.classList.toggle('depleted', depleted || value > 0);
				el.classList.toggle('overused', value > 1);
			}
		}
		catch (err) {
			console.error(el, '\n', value, type, '\n', err);
		}
	}

	/**
	 * @param {string[]} types
	 * @param {number} cost
	 * @param {ActionTracker} ac
	 */
	useAction(types, cost, ac) {
		types.forEach(t => {
			if (!['aoo', 'swift', 'immediate'].includes(t)) return;
			this.setActionValue(this.actions[t], ac[t], t);
		});
	}

	resetActions() {
		// console.warn('Resetting combatant');
		const ac = this.at;
		Object.values(this.actions).forEach(el => {
			if (!el) return;
			const value = ac?.[el.dataset.action] ?? 0;
			this.setActionValue(el, value, el.dataset.action);
		});
	}
}

class ActionTrackerInterface {
	/** @type {CombatTracker} */
	app;

	/** @type {Combatant} */
	combatant;
	/** @type {Actor} */
	actor;

	/**
	 * @param {CombatTracker} app
	 * @param {Element} html
	 */
	constructor(app, html) {
		this.app = app;
		this.createElements();
		this.setCombatant(app.combats.find(c => c.isActive)?.combatant);
		// this.render(html);
	}

	setCombatant(combatant) {
		if (!combatant) return;
		this.combatant = combatant;
		this.actor = combatant.actor;
		this.at = prepareCombatant(combatant);
	}

	render(html) {
		this.controls = html.querySelector('[data-tab="combat"] header');
		this.resetActions();
		this.controls.after(this.elements);
		this.handleCombatants(html);
		this.app.setPosition(); // Fix sizing
	}

	/**
	 * @param {Element} html
	 */
	handleCombatants(html) {
		html.querySelectorAll('.directory-list .combatant[data-combatant-id]')
			.forEach(el => {
				const combat = this.app.viewed,
					combatantId = el.dataset.combatantId,
					combatant = combat.combatants.get(combatantId);

				const actor = combatant?.actor;
				if (isVisible(actor))
					new CombatantInterface(el, this.app, combatant);
			});
	}

	/**
	 * @param {Element} el
	 * @param {number} value
	 * @param type
	 * @param depleted
	 */
	setActionValue(el, value, type, depleted = false) {
		const nonactions = game.settings.get(CFG.id, CFG.SETTINGS.nonactions);
		if (!nonactions && ['free', 'nonaction'].includes(type)) return;

		const label = ActionValueLabel(value, depleted);
		const tel = el.firstChild;
		if (tel) tel.textContent = label;
		else el.prepend(label);
		el.classList.toggle('ready', value == 0);
		if (!CFG.INFINITE_USES.includes(type)) {
			el.classList.toggle('depleted', depleted || value > 0);
			el.classList.toggle('overused', value > 1);
		}
	}

	resetActions() {
		const ac = this.at;
		Object.values(this.actions).forEach(el => {
			if (!el) return;
			const value = ac?.[el.dataset.action] ?? 0;
			this.setActionValue(el, value, el.dataset.action);
		});

		this.elements.classList.toggle('obscured', !isVisible(this.actor));
	}

	updateDistanceLabel() {
		/** @type {ActionTracker} */
		const at = this.combatant._actionTracker;
		if (!at) {
			console.error('%cACTION TRACKER%c | No AC',
				CFG.COLORS.main, CFG.COLORS.unset, this.actor.name, this.actor.id);
			return;
		}

		// let system = game.settings.get('pf1', 'distanceUnits'); // override
		// if (system === 'default') system = game.settings.get('pf1', 'units');

		const scene = game.canvas.scene,
			grid = scene.grid;

		const routedPath = game.settings.get(CFG.id, CFG.SETTINGS.directPath) !== true;

		const d = Math.round(routedPath ? at.distance : at.distanceDirect) * grid.distance;

		const el = this.distance;
		const label = `${d} ${grid.units}`;
		const tel = el.firstChild;
		if (tel) tel.textContent = label;
		else el.prepend(label);
	}

	/**
	 * @param {string[]} types
	 * @param {number} cost
	 * @param {ActionTracker} ac
	 */
	useAction(types, cost, ac) {
		const combatant = game.combat.combatant;
		if (!combatant.actor) return console.warn('No actor combatant active');
		if (combatant.actor !== ac.actor) return console.warn('useAction received bad actor:', combatant.actor.name);

		types.forEach(t => {
			// console.log('Updating UI:', t, ac[t]);
			if (t === 'action') {
				// special handler because reasons
				for (const sa of ['action1', 'action2', 'action3'])
					this.setActionValue(this.actions[sa], ac[sa], t);
			}
			else {
				this.setActionValue(this.actions[t], ac[t], t);

				// Deplete related action
				if (t == 'swift')
					this.setActionValue(this.actions.immediate, ac.immediate, 'immediate', true);
				else if (t === 'immediate')
					this.setActionValue(this.actions.swift, ac.swift, 'swift', true);
			}
		});
	}

	/** @type {Element} */
	elements;
	/** @type {Element} */
	controls;
	actions = {
		/** @type {Element} */
		action1: null,
		/** @type {Element} */
		action2: null,
		/** @type {Element} */
		action3: null,
		/** @type {Element} */
		standard: null,
		/** @type {Element} */
		move: null,
		/** @type {Element} */
		swift: null,
		/** @type {Element} */
		immediate: null,
		/** @type {Element} */
		reaction: null,
		/** @type {Element} */
		free: null,
		/** @type {Element} */
		nonaction: null,
		/** @type {Element} */
		step: null,
		/** @type {Element} */
		aoo: null,
	};

	distance = null;

	createElements() {
		const acts = createNode('div', null, ['action-tracker']);

		const unchained = game.settings.get('pf1', 'unchainedActionEconomy');
		const mainActions = createNode('span', null, ['main-actions']);
		if (unchained) {
			this.actions.action1 = createNode('span', null, ['action', 'generic'], { data: { tooltip: 'PF1.Activation.action.Single', action: 'action1' } });
			this.actions.action2 = createNode('span', null, ['action', 'generic'], { data: { tooltip: 'PF1.Activation.action.Single', action: 'action2' } });
			this.actions.action3 = createNode('span', null, ['action', 'generic'], { data: { tooltip: 'PF1.Activation.action.Single', action: 'action3' } });
			this.actions.reaction = createNode('span', null, ['action', 'reaction'], { data: { tooltip: 'PF1.Activation.reaction.Single', action: 'reaction' } });
			mainActions.append(this.actions.action1, this.actions.action2, this.actions.action3);
			acts.append(mainActions, this.actions.reaction);
		}
		else {
			this.actions.standard = createNode('span', null, ['action', 'standard'], { data: { tooltip: 'PF1.Activation.standard.Single', action: 'standard' } });
			this.actions.move = createNode('span', null, ['action', 'move'], { data: { tooltip: 'PF1.Activation.move.Single', action: 'move' } });
			this.actions.swift = createNode('span', null, ['action', 'swift'], { data: { tooltip: 'PF1.Activation.swift.Single', action: 'swift' } });
			this.actions.immediate = createNode('span', null, ['action', 'immediate'], { data: { tooltip: 'PF1.Activation.immediate.Single', action: 'immediate' } });
			mainActions.append(this.actions.standard, this.actions.move);
			acts.append(mainActions, this.actions.swift, this.actions.immediate);
		}

		const nonactions = game.settings.get(CFG.id, CFG.SETTINGS.nonactions);
		if (nonactions) {
			this.actions.free = createNode('span', null, ['action', 'free'], { data: { tooltip: 'Free', action: 'free' } });
			this.actions.nonaction = createNode('span', null, ['action', 'nonaction'], { data: { tooltip: 'PF1.Activation.nonaction.Single', action: 'nonaction' } });
			acts.append(this.actions.free, this.actions.nonaction);
		}

		this.actions.aoo = createNode('span', null, ['action', 'aoo'], { data: { tooltip: 'PF1.Activation.aoo.Single', action: 'aoo' } });
		acts.append(this.actions.aoo);

		this.distance = createNode('span', '0', ['action', 'movement'], { data: { tooltip: 'PF1.Movement.Label', action: 'movement' } });
		acts.append(this.distance);

		return this.elements = acts;
	}

	start() {
		this.elements.classList.toggle('obscured', !isVisible(this.actor));
	}

	end() {
		this.elements.classList.toggle('obscured', true);
	}
}

/**
 * @param {Combat} combat
 */
function resetActionTracker(combat) {
	// console.log('ACTION TRACKER | Reset Combatant');
	const combatant = combat.combatant;

	if (combatant?.actor) {
		actionTracker?.setCombatant(combatant);
		actionTrackerAlt?.setCombatant(combatant);
	}

	actionTracker?.resetActions();
	actionTrackerAlt?.resetActions();
}

/**
 * @param {Combat} combat
 * @param {boolean} reset
 */
const anyCombatUpdate = async (combat, reset = false) => {
	const combatant = combat.combatant;

	prepareCombatant(combatant);
	resetCombatant(combatant);

	resetActionTracker(combat);

	if (!game.settings.get(CFG.id, CFG.SETTINGS.keepMarker))
		clearMarkers();

	await setTokenPosition(combatant, true);
};

/**
 * @param {Combat} combat
 * @param {object} update
 * @param {object} context
 */
const combatUpdate = (combat, update, context) => {
	if (context.recursive === false || context.diff === false) return;

	if (update.turn === undefined && update.round === undefined) return;
	anyCombatUpdate(combat, true);
};

/**
 * @param {Combat} combat
 * @param data
 * @param userId
 */
const combatCreate = (combat, data, userId) => {
	anyCombatUpdate(combat, true);

	actionTracker?.start();
	actionTrackerAlt?.start();
};

const combatDelete = () => {
	clearMarkers();

	if (!game.combat) {
		actionTracker?.end();
		actionTrackerAlt?.end();
	}
};

/**
 * @param {Combatant} combatant
 */
const createCombatant = (combatant) => {
	prepareCombatant(combatant);
	if (game.combat?.combatant === combatant)
		resetActionTracker(combatant.combat);
};

const deleteCombatant = (combatant) => {
	/** @type {Marker} */
	const marker = combatant._atMarker;
	if (marker) marker.remove();

	if (game.combat?.combatants.length == 0) {
		actionTracker?.end();
		actionTrackerAlt?.end();
	}
	else {
		const newCombatant = game.combat?.combatant;
		if (combatant === actionTracker.combatant && newCombatant) {
			actionTracker?.setCombatant(newCombatant);
			actionTrackerAlt?.setCombatant(newCombatant);
		}
	}
};

/**
 * @param {CombatTracker} app
 * @param {JQuery} html
 * @param {object} options
 */
const renderCombatTracker = (app, [html], options) => {
	app.__actionTracker ??= new ActionTrackerInterface(app, html);
	/** @type {ActionTrackerInterface} */
	const ac = app.__actionTracker;
	if (ac.app === app) ac.render(html);
	else {
		console.log('ACTION TRACKER | App mismatch:',
			CFG.COLORS.main, CFG.COLORS.unset,
			app.appId, '!=', ac.app.appId);
	}

	if (app.popOut) actionTrackerAlt = ac;
	else actionTracker = ac;
};

/**
 * @param {ActionUse} usage
 */
const itemUseHandler = (usage) => {
	const { action, actor, item, shared, token } = usage;

	const actionData = action.data,
		activation = action.activation;

	if (!actor) return; // Unlikely to be true

	// Full attack, only if the action is attack or full-round
	const fullAttack = action === 'full' || shared.fullAttack || false;

	const combatant = actor.getCombatants()[0];
	if (!combatant) return;

	prepareCombatant(combatant);

	/** @type {ActionTracker} */
	const at = combatant._actionTracker;
	at.useAction(activation, action, { full: fullAttack });

	Promise.resolve()
		.then(() => game.socket.emit(CFG.socket, { uuid: actor.uuid, itemId: item.id, actionId: action.id, activation, full: fullAttack }));
};

/**
 * @param {TokenDocument} doc
 * @param {object} update
 * @param {object} context
 */
const detectMovement = (doc, update, context) => {
	if (context.recursive === false || context.diff === false) return;

	const combatant = doc.combatant;
	if (!combatant) return;
	if (doc.combatant !== game.combat?.combatant) return;
	if (!isVisible(doc.actor)) return;

	/** @type {ActionTracker} */
	const at = combatant._actionTracker;
	if (at) {
		const t = doc.object;
		const p1 = new Point(update.x, update.y, update.elevation);
		const p0 = transformTokenPosition(t, p1);
		if (!p0.valid) return;
		at.addPathNode(p0, { merge: update.elevation !== undefined });
	}
};

Hooks.once('init', function registerSettings() {
	game.settings.register(
		CFG.id,
		CFG.SETTINGS.nonactions,
		{
			name: 'Free & Nonactions',
			hint: 'Display tracking for free and nonactions.',
			type: Boolean,
			default: false,
			scope: 'client',
			config: true,
		});

	game.settings.register(
		CFG.id,
		CFG.SETTINGS.directPath,
		{
			name: 'Direct Path',
			hint: 'Display direct path distance instead of distance based on waypoints.',
			type: Boolean,
			default: false,
			scope: 'world',
			config: true,
		});

	game.settings.register(
		CFG.id,
		CFG.SETTINGS.startMarker,
		{
			name: 'Start marker',
			hint: 'Display marker for where a token started their turn at. This allows easy tracking of token movement and where to return to to reset distance calculations.',
			type: Boolean,
			default: true,
			scope: 'world',
			config: true,
		});

	game.settings.register(
		CFG.id,
		CFG.SETTINGS.keepMarker,
		{
			name: 'Sticky markers',
			hint: 'Keep start markers until the combatant\'s turn comes up again.',
			type: Boolean,
			default: true,
			scope: 'world',
			config: true,
		},
	);

	game.settings.register(
		CFG.id,
		CFG.SETTINGS.transparency,
		{
			name: 'Transparency',
			hint: 'Permission level required to see action usage and movement.',
			type: String,
			default: 'generic',
			choices: {
				owner: 'Owner',
				player: 'Player-owned',
				limited: 'Limited',
				generic: 'Player-owned or Limited',
				all: 'All',
			},
			scope: 'world',
			config: true,
			requiresReload: true,
		},
	);

	if (game.user?.isGM) {
		const t = game.settings.get(CFG.id, CFG.SETTINGS.transparency);
		if (t === 'observer') {
			game.settings.set(CFG.id, CFG.SETTINGS.transparency, 'limited')
				.then(() => ui.notifications.info('Action Tracker | Observer transparency migrated to Limited', { permanent: true }));
		}
	}
});

Hooks.on('updateCombat', combatUpdate);
Hooks.on('createCombat', combatCreate);
Hooks.on('deleteCombat', combatDelete);
Hooks.on('createCombatant', createCombatant);
Hooks.on('deleteCombatant', deleteCombatant);
Hooks.on('renderCombatTracker', renderCombatTracker);

Hooks.once('ready', () => {
	console.log('%cACTION TRACKER%c | Initialize for combat',
		CFG.COLORS.main, CFG.COLORS.unset);
	const combat = game.combat;
	if (combat) {
		combat._atMarkers = {};
	}
	const combatant = combat?.combatant,
		actor = combatant?.actor;

	if (actor) {
		actionTracker?.setCombatant(combatant);
		actionTrackerAlt?.setCombatant(combatant);

		if (isVisible(actor))
			setTokenPosition(combatant, true);
	}

	game.modules.get(CFG.id).api = {
		clearMarkers,
	};
});

Hooks.on('pf1PreDisplayActionUse', itemUseHandler);

Hooks.on('updateToken', detectMovement);

Hooks.on('canvaReady', function canvasReadyEvent() {
	game.combat.combatants
		.forEach(c => {
			const at = prepareCombatant(c);
			at.marker.remove();
			updateTokenMarker(c.token);
		});

	actionTracker?.resetActions();
	actionTrackerAlt?.resetActions();
});

/**
 * @param {object} data
 * @param {string} userId Sender user ID
 */
const socketListener = async (data, userId) => {
	const { uuid, itemId, actionId, activation, full } = data,
		doc = await fromUuid(uuid),
		/** @type {Actor} */
		actor = doc instanceof Actor ? doc : doc.actor;

	// Test if current user is allowed to see the action
	if (isVisible(actor)) {
		const user = game.users.get(userId);
		// Test if the sender is allowed to actually report this kind of event (sanity check)
		if (actor.testUserPermission(user, CONST.DOCUMENT_OWNERSHIP_LEVELS.OWNER)) {
			const action = actor.items.get(itemId).actions.get(actionId);

			const { type, cost = 1 } = activation;
			console.debug('%cACTION TRACKER%c', CFG.COLORS.main, CFG.COLORS.unset,
				'| User:', (game.users.get(userId)?.name || userId), '|', action.item.name, '|', action.name, '|', cost, type);

			const c = game.combat?.combatants.find(c => c.actor === actor);
			/** @type {ActionTracker} */
			const ac = c._actionTracker;
			ac.useAction(activation, action, { full });
		}
		else {
			// console.debug('%cACTION TRACKER%c', CFG.COLORS.main, CFG.COLORS.unset, '| Message:', user.name, 'for', actor.name);
		}
	}
	else {
		// console.debug('%cACTION TRACKER%c | NOT VISIBLE', CFG.COLORS.main, CFG.COLORS.unset);
	}
};

Hooks.once('init', () => {
	game.socket.on(CFG.socket, socketListener);
});
