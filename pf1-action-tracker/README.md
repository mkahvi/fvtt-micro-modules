# Action Tracker for Pathfinder 1

![Supported Foundry Versions](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fversion%3Fstyle%3Dflat%26url%3Dhttps%3A%2F%2Fgitlab.com%2Fmkahvi%2Ffvtt-micro-modules%2F-%2Fraw%2Fmaster%2Fpf1-action-tracker%2Fmodule.json)

Display used and unused actions by current combatant.

Distance is counted from waypoints. These waypoints can be reset by returning to the starting location. Elevation changes may not be calculated correctly for the distance.

## Flaws

- The action tracking does not persist past refresh.
  - The data is only stored in memory.
- Movement distance does not automatically consume standard, move, or other actions.
  - There's no way to detect forced movement, stepping into adjacent difficult terrain, and various other common cases where the movement does not count for actions or counts for more than the base movement would suggest.
  - Forced movement, such as falling, can not be ignored for move distance.
  - Difficult terrain, squeezing, etc. is not accounted for either.
- Likely misbehaves with multiple combats (untested).

## Screencaps

![Screencap](./screencap.png)

## API

```js
const api = game.modules.get('pf1-action-tracker').api;
api.clearMarkers(); // Remove all scene start markers
```

## Install

Manifest URL: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/master/pf1-action-tracker/module.json>

PF1 v9.6 requires manual install: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/1fa3640061efb098ecf87c2ac9462226528224be/pf1-action-tracker/pf1-action-tracker.zip>

Foundry v9 requires manual install: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/blob/ae23c29afdf98fd97645aa912b0cd7be53f8a35d/pf1-action-tracker/pf1-action-tracker.zip>

## Future Ideas

These are ideas that'd be cool to implement. No promises that they will.

- Record current state, to persist past refresh. Possibly as data on the combatant, actor if that isn't possible.
- Allow editing the move history to make it clearer free/forced movement are accounted for correctly.

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT license](./LICENSE).
