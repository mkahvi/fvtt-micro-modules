# Change Log

## 0.5.0.3

- Foundry v12 compatibility

## 0.5.0.1

- Fix: Erroring when using items outside of combat.

## 0.5.0

- Change: Tracking is now more combatant oriented than actor.
- Fix: Errors with combatants with no actors associated with them.
- Change: Foundry v10 support removed.

## 0.4.3.2

- Fix: Start marker position could be corrupt (appear at top-left corner) at refresh if combat was active.

## 0.4.3.1

- Fix: Odd behaviour with imports or similar updates.

## 0.4.3

- Fix: Erroring with no scene.
- Change: Minimum PF1 version increased to 9.4

## 0.4.2

- Fix: Full attack detection now requires the action is attack or full round.

## 0.4.1.1

- Fix: Start marker scaling now matches token.

## 0.4.1

- Change: Auto-reload with some settings was swapped for `requiresReload`.
- Change: Observer permission level swapped for Limited in all instances.
- Fix: Properly use _transparency_ setting (previous fix hardcoded limited permission test).

## 0.4.0.3

- Fix for action usage not being visible to others than the one activating them.

## 0.4.0.2

- Fix action usage not being detected.
- Fix distances being displayed weird if any other than 5 ft scene measure was used.

## 0.4.0.1

- Fix marker removal.

## 0.4.0

- Foundry v10 compatibility

## 0.3.0

- Unaccounted for improvements

## 0.2.0.2

- Fix lingering markers causing issues.

## 0.2.0.1

- Fix markers not being cleared when combat ends.
- Fix several errors with no combat or actors out of combat.
- Fix errors on new combat.

## 0.2

- Token route for distance calculation.
- Start location marker & sticky marker option.
- Other combatant swift, immediate and AoO tracking.
- Basic transparency setting to limit visibility.

## 0.1.1.1 Hotfix

## 0.1.1 Action and non-action support

## 0.1 Initial
