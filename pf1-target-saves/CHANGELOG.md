# Change Log

## 1.2.0

- PF1v11 compatibility. Support for older dropped.
- Foundry v12 compatibility. Support for older dropped.

## 1.1.0.1

- Fix: Sticky Waiting for GM for saves made by the GM.

## 1.1.0

- PF1v9 compatibility.
- Fix permission test on chat messages.

## 1.0.0.2

- Fix an obscure error?

## 1.0.0.1

- Removed debug output

## 1.0.0

- Hijack roll saving throw button on card to hook them into the target display.
- Reroll detection (GM can accept or reject these).
- Chat messages have markers for if a reroll was accepted, rejected, or if they're waiting for GM to decide.

## 0.1.0.1

- Fix: Show interface only to GMs.

## 0.1 Initial
