# Target Saves for Pathfinder 1e

![Supported Foundry Versions](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fversion%3Fstyle%3Dflat%26url%3Dhttps%3A%2F%2Fgitlab.com%2Fmkahvi%2Ffvtt-micro-modules%2F-%2Fraw%2Fmaster%2Fpf1-target-saves%2Fmodule.json)

Enhances built-in targeting.

- Adds easy distinction of failed and succeeded saves to the target display.
- Adds quick rolls for all PC and NPC targets.
- Adds quick selection of all targets that failed or succeeded the save.

## Screencaps

![Message States](./img/save-states.png)

![Reroll Dialog](./img/reroll-dialog.png)

![Override Dialog](./img/override-dialog.png)

![Context Menu](./img/context-menu.png)

## Install

Manifest URL: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/master/pf1-target-saves/module.json>

### Pre PF1 v11.0

Manual install: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/75812215fa6ffe6c687f2371e11bc11ca5bc4d3a/pf1-target-saves/pf1-target-saves.zip>

### Pre PF1 v9.0

Manual Install: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/blob/31ba450bb48e04aa065686a49e295e4cdabba626/pf1-target-saves/pf1-target-saves.zip>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
