const CFG = /** @type {const} */ ({
	module: 'pf1-target-saves',
	socketFlag: undefined,
	SETTINGS: {
		allowReroll: 'allowReroll',
		rerollConfirm: 'rerollConfirm',
	},
	FLAGS: {
		rejected: 'rejected',
		from: 'from',
		original: 'original',
	},
});

const getDocData = (doc) => game.release.generation >= 10 ? doc : doc.data;

// Console log colors
const gnt = 'color:mediumseagreen',
	gvt = 'color:yellowgreen',
	nn = 'color:mediumpurple',
	bnt = 'color:orangered',
	bvt = 'color:orange';

/**
 * TODO:
 * - Ability to select NPCs/PCs before saves resolve.
 */

/**
 * @param {string} node
 * @param {string} text
 * @param {string[]} classes
 * @param {object} options
 * @returns {Element}
 */
function createNode(node = 'span', text = null, classes = []) {
	const n = document.createElement(node);
	if (text) n.textContent = text;
	if (classes.length) n.classList.add(...classes.filter(c => !!c));
	return n;
}

/**
 * @param {string[]} uuids
 * @returns {TokenDocument[]}
 */
async function resolveUuids(uuids) {
	return Promise.all(uuids.map(uuid => fromUuid(uuid)))
		.then(rv => rv.filter(pt => !!pt));
}

/**
 * @param {object} options
 * @param {User} options.user
 * @param {Token} options.token
 * @param {number} options.newSave
 * @param {number} options.oldSave
 * @returns {Promise<boolean>}
 */
function confirmOverrideDialog({ user, token, newSave, oldSave }) {
	return Dialog.confirm({
		title: 'Override saving throw?',
		content: `<p><em class='user-name'>${user.name}</em> wants to replace their previous save for <em class='token-name'>${token.name}</em>.</p><p>Old save: <span class='save-result fake-inline-roll old-save'>${oldSave}</span><br>New save: <span class='save-result fake-inline-roll new-save'>${newSave}</span><p>Do you wish to allow it?</p>`,
		yes: () => true,
		no: () => false,
		defaultYes: false,
		rejectClose: true,
		options: {
			classes: ['dialog', 'mod-target-saves'],
		},
	});
}

/**
 * @param {TokenDocument[]} tokens
 * @param {"ref"|"fort"|"will"} saveId
 * @param {ChatMessage} cm
 * @param {Event} event
 */
async function rollTokens(tokens, saveId, cm, event) {
	const update = {};
	console.log('Rolling saves:', tokens);

	for (const t of tokens) {
		const actor = t.actor;
		if (!actor) continue;

		// console.log(t.name, actor.name, t.id, actor.id);

		let message;
		try {
			message = await actor.rollSavingThrow(saveId, { event, skipDialog: event.shiftKey, reference: cm.uuid });
		}
		catch (err) {
			// Saving throw errored. Likely due to the actor being a trap, vehicle, or such
			ui.notifications.error(`"${t.name}" failed to roll their saving throw.`);
			console.error(err, { actor });
			continue;
		}

		const total = message?.rolls?.[0]?.total;

		if (total != null) {
			const uuid = t.uuid;
			update[uuid] = { save: { [saveId]: total } };
		}
	}

	if (!foundry.utils.isEmpty(update)) {
		await cm.setFlag('pf1', 'targetDefense', update);
	}
}

/**
 * @param {string[]} tokenUuids
 */
async function selectTokens(tokenUuids) {
	const tokens = await resolveUuids(tokenUuids);

	console.log('Selecting tokens:', tokens);

	tokens.forEach((t, i) => {
		console.log(i);
		t.object.control({ releaseOthers: i === 0 });
	});

	if (!tokens.length) canvas.tokens.releaseAll();
}

/**
 * @param {string[]} tokenUuids
 * @param {HTMLButtonElement} bn NPC button
 * @param {HTMLButtonElement} bp PC button
 * @param {TokenDocument[]} pcs PC tokens
 * @param {TokenDocument[]} npcs NPC tokens
 */
async function mapTokens(tokenUuids, bn, bp, pcs, npcs) {
	const tokens = await resolveUuids(tokenUuids);
	tokens.forEach(t => {
		const actor = t.actor;
		if (!actor) return;
		if (actor.hasPlayerOwner) pcs.push(t);
		else npcs.push(t);
	});

	bn.enabled = true;
	bp.enabled = true;

	bn.dataset.targets = npcs.length;
	bp.dataset.targets = pcs.length;
	bn.textContent += ` [${npcs.length}]`;
	bp.textContent += ` [${pcs.length}]`;
}

/**
 * @param {ChatMessage} cm
 * @returns {{dc: number, type: string}}
 */
function getMessageSaveInfo(cm) {
	const meta = cm.system;

	const v113 = foundry.utils.isNewerVersion(game.system.version, '11.2');

	const dc = v113 ? meta.save?.dc : meta.action?.dc,
		type = meta.save?.type;

	if (type === undefined || !Number.isSafeInteger(dc)) return {};

	return { dc, type };
}

/**
 * @param {Element} html
 * @returns {{targets:NodeList, targetsEl:Element}}
 */
function getTargetElements(html) {
	const card = html.querySelector('.pf1.chat-card[data-item-id]');
	const targetsEl = card?.querySelector('div.attack-targets');
	const targets = targetsEl?.querySelectorAll('.target[data-uuid]');
	return { targets, targetsEl };
}

/**
 * @param {Event} ev
 * @param {ChatMessage} cm
 * @param {Element} html
 * @param {object} options
 * @param type
 */
async function buttonEventHandler(ev, cm, html, type) {
	const rolls = cm.getFlag('pf1', 'targetDefense');

	const allowReroll = game.settings.get(CFG.module, CFG.SETTINGS.allowReroll);
	// const { targets } = getTargetElements(html);

	// ui.notifications.warn('Reroll not allowed.');

	const initialActors = canvas.tokens.controlled.map(t => t.document)
		.map(token => ({ actor: token.actor, token, uuid: token.uuid }));

	const actors = [], prompts = [];
	for (const act of initialActors) {
		const { token } = act;
		const oldRoll = foundry.utils.getProperty(rolls, act.uuid);

		// Reroll handling
		const oldSave = oldRoll?.save[type];
		if (oldSave !== undefined) {
			if (!allowReroll) {
				ui.notifications.warn(`${token?.name} can't reroll saving throw.`);
				continue;
			}
			else {
				const p = foundry.applications.api.DialogV2.confirm({
					window: { title: 'Reroll saving throw?' },
					content: `<p><em class='token-name'>${token.name}</em> has already rolled.<br>Do you wish to reroll their saving throw?</p><p>Previous result was <span class='save-result fake-inline-roll old-save'>${oldSave}</span></p>`,
					defaultYes: false,
					yes: { default: false },
					no: { default: true },
					rejectClose: false,
					classes: ['mod-target-saves'],
				});

				p.then(reroll => reroll ? actors.push(act) : null);

				prompts.push(p);
				continue;
			}
		}
		actors.push(act);
	}

	// Wait for prompts to resolve
	await Promise.allSettled(prompts);

	if (actors.length == 0) return;

	const promises = [];
	const updateData = {};

	function handleResult(msg, uuid) {
		const total = msg?.rolls?.[0]?.total;
		if (total != null) {
			const d = { save: { [type]: total } };
			if (!cm.isAuthor && !cm.isOwner) d.messageId = msg.id;
			updateData[uuid] = d;
		}
	}

	let noSound = false;
	for (const { actor, uuid } of initialActors) {
		const p = actor.rollSavingThrow(type, { event: ev, noSound: noSound, skipPrompt: ev.shiftKey, reference: cm.uuid })
			.then(msg => handleResult(msg, uuid));
		noSound = true;
		promises.push(p);
	}

	function saveChanges() {
		if (foundry.utils.isEmpty(updateData)) return;

		if (cm.isAuthor || cm.isOwner)
			cm.setFlag('pf1', 'targetDefense', updateData);
		else
			game.socket?.emit(CFG.socketFlag, { messageId: cm.id, update: updateData });
	}

	Promise.allSettled(promises).then(saveChanges);
}

/**
 * @param {ChatMessage} cm
 * @param {Element} html
 * @param {object} options
 * @param type
 */
function hookRollSaveButton(cm, html, type) {
	// Take over control of normal saving throw button to allow displaying them for targets
	// This should be core PF1 feature

	let buttonActive = false;
	const button = html.querySelector('button[data-action="save"]');
	button?.addEventListener('click', ev => {
		ev.preventDefault();
		// Prevent PF1 handling the click
		ev.stopPropagation();
		ev.stopImmediatePropagation();

		// Do actual handling
		if (buttonActive)
			return ui.notifications.warn('You need to resolve previous roll before rolling again');

		buttonActive = true;
		buttonEventHandler(ev, cm, html, type)
			.finally(() => buttonActive = false);
	});
}

/**
 * @param {ChatMessage} cm
 * @param {JQuery} html
 * @param {object} options
 * @param _options
 */
function checkSavesAndAddButtons(cm, [html], _options) {
	const { type, dc } = getMessageSaveInfo(cm);
	if (!type || dc === undefined) return;

	const rolls = cm.getFlag('pf1', 'targetDefense');

	const { targets, targetsEl } = getTargetElements(html);
	if (!targetsEl) return;

	const failed = [], succeeded = [], all = [], missing = [];

	const obscureDCs = game.settings.get('pf1', 'obscureSaveDCs');

	const handleTarget = (sEl, save, uuid) => {
		const st = sEl.dataset.save || sEl.dataset.savingThrow;
		if (st !== type)
			sEl.classList.add('invalid');
		else {
			if (!game.user.isGM && obscureDCs) return;

			const value = save?.[type];
			if (value === undefined) {
				sEl.classList.add('indeterminate');
				missing.push(uuid);
			}
			else if (value >= dc) {
				sEl.classList.add('success');
				succeeded.push(uuid);
			}
			else {
				sEl.classList.add('failure');
				failed.push(uuid);
			}
		}
	};

	for (const t of targets) {
		const uuid = t.dataset.uuid;
		const save = foundry.utils.getProperty(rolls, uuid)?.save;
		all.push(uuid);
		t.querySelectorAll('.saving-throws [data-saving-throw],.saving-throws [data-save]')
			.forEach(sEl => handleTarget(sEl, save, uuid));
	}

	// PF1v11.3 finally does this hooking
	if (!foundry.utils.isNewerVersion(game.system.version, '11.2'))
		hookRollSaveButton(cm, html, type);

	// Add buttons
	if (!game.user.isGM) return;

	const d = createNode('div', null, ['mod-target-saves']);

	if (missing.length) {
		const rollBs = createNode('div', null, ['buttons', 'roll']);
		const bn = createNode('button', 'NPCs', ['roll']);
		const bp = createNode('button', 'Players', ['roll']);
		rollBs.append(
			createNode('label', 'Roll', ['label']),
			bn,
			bp,
		);
		d.append(rollBs);

		bn.enabled = false;
		bp.enabled = false;

		const pcs = [], npcs = [];

		bn.addEventListener('click', (ev) => {
			ev.preventDefault();
			ev.stopPropagation();

			rollTokens(npcs, type, cm, ev);
		});
		bp.addEventListener('click', (ev) => {
			ev.preventDefault();
			ev.stopPropagation();
			rollTokens(pcs, type, cm, ev);
		});

		mapTokens(missing, bn, bp, pcs, npcs);
	}

	if (failed.length || succeeded.length) {
		const selectBs = createNode('div', null, ['buttons', 'select']);
		const bf = createNode('button', `Failed [${failed.length}]`, ['select', 'failed']);
		bf.dataset.targets = failed.length;
		const bs = createNode('button', `Succeeded [${succeeded.length}]`, ['select', 'succeeded']);
		bs.dataset.targets = succeeded.length;

		bs.addEventListener('click', (ev) => {
			ev.preventDefault();
			ev.stopPropagation();
			selectTokens(succeeded, cm, ev);
		});
		bf.addEventListener('click', (ev) => {
			ev.preventDefault();
			ev.stopPropagation();
			selectTokens(failed, cm, ev);
		});

		selectBs.append(createNode('label', 'Select', ['label']), bf, bs);
		d.append(selectBs);
	}

	targetsEl?.after(d);
}

/**
 *
 * @param root0
 * @param root0.messageId
 * @param root0.update
 * @param userId
 */
async function saveNotifyReceiver({ messageId, update }, userId) {
	const user = game.users.get(userId);
	const cm = game.messages.get(messageId);
	const { type, dc } = getMessageSaveInfo(cm);
	if (!type || dc === undefined || !cm) {
		console.warn('Received bad message from user', user.name, `[${user.id}]`, { messageId, update });
		return;
	}

	const rolls = cm.getFlag('pf1', 'targetDefense');

	// console.log({ rolls, update });

	const html = document.querySelector(`#chat-log .chat-message[data-message-id="${messageId}"]`);
	const { targets } = getTargetElements(html);
	if (!targets) return;

	const actualUpdate = {};
	const pps = [];

	const enrichedTargets = Array.from(targets).map(t => {
		const d = {
			element: t,
			uuid: t.dataset.uuid,
		};

		const p = fromUuid(d.uuid).then(token => d.token = token);
		pps.push(p);

		return d;
	});

	await Promise.allSettled(pps);

	const promises = [];
	enrichedTargets?.forEach(tData => {
		const { element, uuid, token } = tData;
		if (!token?.actor) return;

		if (update[uuid] === undefined) return; // Not in provided update
		const newSave = update[uuid].save?.[type];
		if (newSave === undefined || !Number.isFinite(newSave)) {
			console.error('Invalid save', update[uuid].save, 'for', token.name, 'from', user.name);
			return;
		}

		const oldSave = foundry.utils.getProperty(rolls, uuid)?.save?.[type];

		// console.log(user.name, { newSave, oldSave });

		const rejectUpdate = () => {
			const saveMsgId = update[uuid].messageId;
			delete update[uuid]; // Delete rejected message info
			if (saveMsgId) {
				game.messages.get(saveMsgId)
					?.setFlag(CFG.module, CFG.FLAGS.rejected, true);
			}
		};

		const acceptUpdate = (accepted, original = false) => {
			if (!accepted) {
				console.log(`%cNew save rejected from %c${user.name} %cfor %c${token.name}`, bnt, bvt, bnt, bvt);
				return rejectUpdate();
			}

			console.log(`%cAccepted %c${type} %csave reroll of %c${newSave} %cfrom %c${user.name} %cfor %c${token.name}`, gnt, gvt, gnt, nn, gnt, gvt, gnt, gvt);

			const saveMsgId = update[uuid].messageId;
			if (saveMsgId) {
				const msg = game.messages?.get(saveMsgId);
				if (original)
					msg?.setFlag(CFG.module, CFG.FLAGS.original, true);
				else
					msg?.setFlag(CFG.module, CFG.FLAGS.rejected, false);
			}

			delete update[uuid]; // Delete accepted message info
			actualUpdate[uuid] = { save: { [type]: newSave } };
		};

		// Auto-reject identical roll
		if (oldSave === newSave) {
			console.log(`%cAuto-rejected identical %c${type} %csave reroll of %c${newSave} %cfrom %c${user.name} %cfor %c${token.name}`, bnt, bvt, bnt, nn, bnt, bvt, bnt, bvt);
			return rejectUpdate();
		}

		if (oldSave !== undefined) {
			console.log(`%cPre-existing %c${type} %csave %c${oldSave} %cfor %c${token.name}`, 'color:orange', 'color:yellowgreen', 'color:orange', 'color:mediumpurple', 'color:orange', 'color:yellowgreen');
			if (game.settings.get(CFG.module, CFG.SETTINGS.allowReroll)) {
				if (!game.settings.get(CFG.module, CFG.SETTINGS.rerollConfirm))
					return acceptUpdate(true);

				const p = confirmOverrideDialog({ user, token, newSave, oldSave });

				p
					.then(acceptUpdate)
					.catch(() => {
						delete update[uuid]; // Ignored
						console.log(`%cIgnored %c${type} %csave reroll of %c${newSave} %cfrom %c${user.name} %cfor %c${token.name}`, bnt, bvt, bnt, nn, bnt, bvt, bnt, bvt);
					});
				promises.push(p);
			}
			else {
				console.log(`%cRejected %c${type} %csave reroll of %c${newSave} %cfrom %c${user.name} %cfor %c${token.name}`, bnt, bvt, bnt, nn, bnt, bvt, bnt, bvt);
				rejectUpdate();
			}
		}
		else
			acceptUpdate(true, true);
	});

	if (promises.length)
		await Promise.allSettled(promises);

	// Report bad poopery
	if (!foundry.utils.isEmpty(update))
		console.warn('Received bad updates:', update);

	// Do update
	if (!foundry.utils.isEmpty(actualUpdate))
		await cm.setFlag('pf1', 'targetDefense', actualUpdate);
}

function registerSettings() {
	game.settings.register(
		CFG.module,
		CFG.SETTINGS.allowReroll,
		{
			name: 'Allow Reroll',
			hint: 'Allow players to reroll their saves. Recommended to leave on true to allow hero point rerolls and similar to function correctly.',
			scope: 'world',
			type: Boolean,
			default: true,
			config: true,
		},
	);

	game.settings.register(
		CFG.module,
		CFG.SETTINGS.rerollConfirm,
		{
			name: 'Reroll requires confirmation',
			hint: 'If players are allowed to reroll, the reroll needs confirmation from GM before it is accepted. GM is prompted to accept the new roll.',
			scope: 'world',
			type: Boolean,
			default: true,
			config: true,
		},
	);
}

/**
 * @param {ChatMessage} cm
 * @param {JQuery} html
 * @param {object} options
 */
function renderSaveAcceptState(cm, [html], options) {
	const from = fromUuidSync(cm.system.reference),
		rejected = cm.getFlag(CFG.module, CFG.FLAGS.rejected),
		original = cm.getFlag(CFG.module, CFG.FLAGS.original);

	if (original) return;
	if (!from && !rejected) return;

	html.classList.add('mod-target-saves');
	const flavor = html.querySelector('.message-content > .flavor-text');

	let msg, cls;
	if (rejected !== undefined) {
		html.classList.add(rejected ? 'rejected-saving-throw' : 'accepted-saving-throw');

		msg = rejected ? game.i18n.localize('TargetSaves.Rejected') : game.i18n.localize('TargetSaves.Accepted');
		cls = rejected ? 'rejected-message' : 'accepted-message';
	}
	else {
		const author = cm.author ?? cm.user; // v12 compatibility
		if (author?.isGM) return; // Ignore GM messages
		msg = game.i18n.localize('TargetSaves.Waiting');
		cls = 'ambivalent-message';
	}

	const statusEl = createNode('span', msg, ['mod-target-saves', 'saving-throw-acceptance', cls]);
	flavor?.append(statusEl);
}

function registerSocketListener() {
	CFG.socketFlag = `module.${CFG.module}`;
	if (!game.user.isGM) return;
	game.socket?.on(CFG.socketFlag, saveNotifyReceiver);
}

/**
 * @param {Element} html
 */
function contextMessageData(html) {
	const msgId = html.dataset.messageId,
		cm = game.messages.get(msgId),
		from = fromUuidSync(cm.system.reference),
		rejected = cm.getFlag(CFG.module, CFG.FLAGS.rejected),
		original = cm.getFlag(CFG.module, CFG.FLAGS.original);
	return { from, rejected, original, message: cm };
}

/**
 *
 * @param options
 * @param options.actor
 * @param options.scene
 * @param options.token
 */
function getSpeaker({ actor: actorId, scene: sceneId, token: tokenId } = {}) {
	const scene = game.scenes?.get(sceneId),
		token = scene?.tokens.get(tokenId),
		actor = token?.actor ?? game.actors?.get(actorId);
	return { token, actor };
}

/**
 * @param _
 * @param {object[]} entries
 */
function insertSaveAcceptanceContextMenuOptions(_, entries) {
	if (game.user.isGM)
		insertSaveAcceptanceContextMenuOptionsForGM(entries);

	entries.unshift({
		name: 'TargetSaves.Focus',
		icon: '<i class="fas fa-search"></i>',
		condition: function ([html]) {
			const { from } = contextMessageData(html);
			return !!from;
		},
		callback: function ([html]) {
			const { from } = contextMessageData(html);
			const source = html.parentElement?.querySelector(`[data-message-id="${from?.id}"]`);
			source?.scrollIntoView({ block: 'start' });
		},
	});
}

function insertSaveAcceptanceContextMenuOptionsForGM(entries) {
	entries.unshift({
		name: 'TargetSaves.Accept',
		icon: '<i class="fas fa-thumbs-up"></i>',
		condition: function ([html]) {
			const { from, rejected, original } = contextMessageData(html);
			if (original) return false;
			const notHandled = rejected === undefined;
			if (!from && notHandled) return false;
			return notHandled;
		},
		callback: async function ([html]) {
			const { from, message: currentMsg } = contextMessageData(html);
			const newSave = currentMsg.rolls?.[0]?.total;
			if (newSave === undefined)
				return ui.notifications.warn('Could not find saving throw roll.');

			const rolls = from.getFlag('pf1', 'targetDefense') ?? {};
			const meta = from.system;
			const type = meta.save?.type;

			const { token, actor } = getSpeaker(getDocData(currentMsg).speaker);
			const uuid = token?.uuid;
			const oldSave = foundry.utils.getProperty(rolls, uuid)?.save?.[type];

			if (oldSave !== undefined) {
				const user = currentMsg.user;
				const reroll = await confirmOverrideDialog({ user, token, newSave, oldSave })
					.catch(() => console.log(`%cIgnored %c${type} %csave reroll of %c${newSave} %cfrom %c${user.name} %cfor %c${token.name}`, bnt, bvt, bnt, nn, bnt, bvt, bnt, bvt));
				if (reroll !== true) return console.log('%cCancelled accepting reroll', bnt);
			}

			from.setFlag('pf1', 'targetDefense', { [uuid]: { save: { [type]: newSave } } });
			currentMsg.setFlag(CFG.module, CFG.FLAGS.rejected, false);
		},
	},
	{
		name: 'TargetSaves.Reject',
		icon: '<i class="fas fa-thumbs-down"></i>',
		condition: function ([html]) {
			const { from, rejected, original } = contextMessageData(html);
			if (original) return false;
			const notHandled = rejected === undefined;
			if (!from && notHandled) return false;
			return notHandled;
		},
		callback: function ([html]) {
			const { message } = contextMessageData(html);
			message?.setFlag(CFG.module, CFG.FLAGS.rejected, true);
		},
	});
}

/**
 * @param {ChatMessage} msg
 */
function preCreateMessage(msg) {
	if (msg.type !== 'check') return;

	const saveType = msg.system?.save?.type;
	if (!saveType) return;

	if (msg.system.reference) {
		const refMsg = fromUuidSync(msg.system.reference);
		if (!(refMsg instanceof ChatMessage)) return;
		if (refMsg?.type !== 'action') return;
	}

	// Auto-accept GM save messages
	if (game.user.isGM) msg.updateSource({ flags: { [CFG.module]: { [CFG.FLAGS.rejected]: false } } });
}

Hooks.once('init', registerSettings);
Hooks.once('ready', registerSocketListener);

Hooks.on('renderChatMessage', checkSavesAndAddButtons);
Hooks.on('renderChatMessage', renderSaveAcceptState);

Hooks.on('preCreateChatMessage', preCreateMessage);

Hooks.on('getChatLogEntryContext', insertSaveAcceptanceContextMenuOptions);
