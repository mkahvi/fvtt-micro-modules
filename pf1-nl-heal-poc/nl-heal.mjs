// BASSE CONFIGURATION
const MODULE_ID = 'pf1-nl-heal-poc',
	LAST_CHECK = 'lastCheck',
	ALL_ACTORS = 'allActors',
	ALL_SCENES = 'allScenes',
	DUAL_HEAL = 'dualHeal',
	BURN_FLAG = 'burnNonlethal',
	BLOCK_HEAL_FLAG = 'preventNonlethalHeal',
	PERIOD = 3_600;

// HELPER FUNCTIONS
// Last recorded check for ticks that was acted on
const lastCheck = () => game.settings.get(MODULE_ID, LAST_CHECK);
// Record new last check timestamp
const setCheck = (t) => game.settings.set(MODULE_ID, LAST_CHECK, t);
// Next tick in X seconds
const nextPeriodTick = () => PERIOD - game.time.worldTime % PERIOD;
// Currently accrued ticks
const periodTickCount = () => Math.floor((game.time.worldTime - lastCheck()) / PERIOD);
// Count how many ticks would occur if time was set to `t`
const countTicks = (t) => Math.floor((t - lastCheck()) / PERIOD);

// Get prototype token (v9 + v10 support)
const getProtoToken = (actor) => game.release.generation >= 10 ? actor.prototypeToken : actor.data.token;
// Get document system data (v9 + v10 support)
const getSystemData = (doc) => game.release.generation >= 10 ? doc?.system : doc?.data.data;
// Get system data prefix (v9 + v10 support)
const getPrefix = () => game.release.generation >= 10 ? 'system' : 'data';

// Get all scenes setting state
const allScenes = () => game.settings.get(MODULE_ID, ALL_SCENES);
// Get all actors setting state
const allActors = () => game.settings.get(MODULE_ID, ALL_ACTORS);

// COLOR CONFIGURATION
const COLORS = {
	main: 'color:mediumseagreen',
	unset: 'color:unset',
	label: 'color:goldenrod',
	number: 'color:mediumpurple',
};

// REGISTER SETTINGS
Hooks.once('init', function registerSettings() {
	game.settings.register(
		MODULE_ID,
		LAST_CHECK,
		{
			default: game.time.worldTime - game.time.worldTime % PERIOD,
			type: Number,
			scope: 'world',
			config: false,
		},
	);

	game.settings.register(
		MODULE_ID,
		ALL_ACTORS,
		{
			name: 'NonlethalHealing.Settings.AllActors.Label',
			hint: 'NonlethalHealing.Settings.AllActors.Hint',
			type: Boolean,
			default: false,
			scope: 'world',
			config: true,
		},
	);

	game.settings.register(
		MODULE_ID,
		ALL_SCENES,
		{
			name: 'NonlethalHealing.Settings.AllScenes.Label',
			hint: 'NonlethalHealing.Settings.AllScenes.Hint',
			type: Boolean,
			default: false,
			scope: 'world',
			config: true,
		},
	);

	game.settings.register(
		MODULE_ID,
		DUAL_HEAL,
		{
			name: 'NonlethalHealing.Settings.DualHeal.Label',
			hint: 'NonlethalHealing.Settings.DualHeal.Hint',
			type: Boolean,
			default: true,
			scope: 'world',
			config: true,
		},
	);

	// TODO: Dry option?
});

/**
 * Burned NL damage
 *
 * @param {Actor} actor
 * @returns {number} Number from 0 to infinity
 */
const getBurn = (actor) => {
	if (actor.hasItemBooleanFlag(BLOCK_HEAL_FLAG)) return Number.POSITIVE_INFINITY;

	if (actor.hasItemBooleanFlag(BURN_FLAG)) {
		return actor.items.reduce((total, item) => {
			if (item.type === 'buff' && item.isActive && item.hasItemBooleanFlag(BURN_FLAG))
				return total + (getSystemData(item).level ?? 0);
			return total;
		}, 0);
	}

	return 0;
};

// MAIN FUNCTION
async function timeBasedNonlethalHealing(time, _delta) {
	const last = lastCheck();
	if (time < last) {
		console.debug('NONLETHAL HEALING | Time rolled back, ignoring');
		return; // Only progress if time has moved forward from last check
	}
	const ticks = periodTickCount();
	if (ticks <= 0) return console.log('%cNONLETHAL HEALING%c | Time until next period tick:', COLORS.main, COLORS.unset, nextPeriodTick());
	console.log('%cNONLETHAL HEALING%c | Period ticks passed:', COLORS.main, COLORS.unset, ticks);

	const nlPath = `${getPrefix()}.attributes.hp.nonlethal`;

	function healActor(actor) {
		const actorData = getSystemData(actor);
		const hd = actorData.attributes.hd.total ?? 0,
			nl = actorData.attributes.hp.nonlethal ?? 0;

		const burn = getBurn(actor);
		const blocked = !Number.isFinite(burn),
			limited = !blocked && burn > 0;

		const doHeal = !blocked && hd > 0 && nl > 0;

		const newNL = doHeal ? Math.max(burn, nl - hd * ticks) : null,
			delta = nl - newNL,
			didHeal = doHeal && delta !== 0;
		return [
			didHeal ? {
				_id: actor.id,
				[nlPath]: newNL,
			} : null,
			hd,
			{ old: nl, new: newNL, delta: nl - newNL, blocked, limited, burn },
		];
	}

	const sV = game.scenes.viewed,
		sA = game.scenes?.active;

	const doAllScenes = allScenes(),
		doAllActors = allActors();

	const scenes = new Set(doAllScenes ? game.scenes.contents : [sV, sA].filter(s => !!s));
	const actors = new Set((doAllActors ? game.actors.contents : game.actors.filter(a => a.hasPlayerOwner))
		.filter(a => getProtoToken(a)?.actorLink && ['character', 'npc'].includes(a.type))); // Only linked actors of PC/NPC type

	console.log(`%cNONLETHAL HEALING%c | Actors found: %c${actors.size}`, COLORS.main, COLORS.unset, COLORS.number);

	async function processActors(actorList) {
		const actorUpdates = [];
		for (const actor of actorList) {
			const [update, _hd, nl] = healActor(actor);
			if (update) {
				console.debug(`%cNONLETHAL HEALING%c | Actor healed: %c${actor.name}%c by %c${nl.delta}`, COLORS.main, COLORS.unset, COLORS.label, COLORS.unset, COLORS.number, { update });
				actorUpdates.push(update);
			}

			if (nl.blocked) console.debug(`%cNONLETHAL HEALING%c | Actor blocked: %c${actor.name}%c`, COLORS.main, COLORS.unset, COLORS.label, nl);
			else if (nl.burn) console.debug(`%cNONLETHAL HEALING%c | Actor burned: %c${actor.name}%c by %c${nl.burn}`, COLORS.main, COLORS.unset, COLORS.label, COLORS.unset, COLORS.number, nl);
		}
		console.log(`%cNONLETHAL HEALING%c | Actors updated: %c${actorUpdates.length}`, COLORS.main, COLORS.unset, COLORS.number);

		if (actorUpdates.length > 0) {
			return Actor.updateDocuments(actorUpdates);
		}
	}

	await processActors(actors);

	const secondaryActors = new Set();

	if (scenes.length == 0) {
		console.warn('NONLETHAL HEALING | No valid scenes found');
	}

	for (const scene of scenes) {
		const tokens = scene.tokens
			.filter(t => {
				const actor = t.actor;
				if (!actor) return false; // Skip tokens that do not represent an actor
				if (!['character', 'npc'].includes(actor.type)) return false;
				if (!t.isLinked) return true; // Unlinked are fair game
				if (doAllActors || actor.hasPlayerOwner) return false; // already processed

				// Collect remaining linked actors to prevent multiple heals for same actor across scenes
				secondaryActors.add(actor);
				return false;
			});

		// TODO:

		if (tokens.length == 0) {
			continue;
		}
		console.log(`%cNONLETHAL HEALING%c | Scene: %c${scene.name}%c | Tokens found: %c${tokens.length}`, COLORS.main, COLORS.unset, COLORS.label, COLORS.unset, COLORS.number);

		let tokenUpdates = 0;
		for (const token of tokens) {
			const actor = token.actor;
			const [update, _hd, nl] = healActor(actor);
			if (update) {
				delete update._id;
				console.debug(`%cNONLETHAL HEALING%c | Token healed: %c${token.name}%c by %c${nl.delta}`, COLORS.main, COLORS.unset, COLORS.label, COLORS.unset, COLORS.number, { update });
				tokenUpdates++;
				await actor.update(update); // TODO: Do batch scene update instead
			}
			if (nl.blocked) console.debug(`%cNONLETHAL HEALING%c | Token blocked: %c${token.name}%c`, COLORS.main, COLORS.unset, COLORS.label, nl);
			else if (nl.burn) console.debug(`%cNONLETHAL HEALING%c | Token burned: %c${token.name}%c by %c${nl.burn}`, COLORS.main, COLORS.unset, COLORS.label, COLORS.unset, COLORS.number, nl);
		}
		console.log(`%cNONLETHAL HEALING%c | Scene: %c${scene.name}%c | Tokens updated: %c${tokenUpdates}`, COLORS.main, COLORS.unset, COLORS.label, COLORS.unset, COLORS.number);
	}

	if (secondaryActors.size > 0) {
		console.log(`%cNONLETHAL HEALING%c | More actors found: %c${secondaryActors.size}`, COLORS.main, COLORS.unset, COLORS.number);

		await processActors(secondaryActors);
	}

	console.debug('NONLETHAL HEALING | Setting heal timestamp:', time);
	setCheck(time);
}

/**
 * @param {Actor} actor
 * @param {object} update
 * @param {object} context
 */
const dualHealing = (actor, update, context) => {
	if (context.recursive === false || context.diff === false) return;

	// console.log({ actor, update, options, user: userId });
	if (!game.settings.get(MODULE_ID, DUAL_HEAL)) return;

	const prefix = getPrefix();

	const newHpU = foundry.utils.getProperty(update, `${prefix}.attributes.hp.value`);
	// No HP change, ignore.
	if (newHpU === undefined) return;

	const oldNL = foundry.utils.getProperty(getSystemData(actor), 'attributes.hp.nonlethal');
	if (!(oldNL > 0)) return console.log('NL HEAL | HP update | No NL damage.');

	const nlPath = `${prefix}.attributes.hp.nonlethal`;
	const newNLU = foundry.utils.getProperty(update, nlPath);

	// NL changed, don't touch it more.
	if (newNLU !== undefined) return console.log('NL HEAL | HP update | NL adjusted, ignoring.');

	const oldHp = foundry.utils.getProperty(getSystemData(actor), 'attributes.hp.value');
	// Don't use clamped new HP for diff since the overheal still fully affects NL
	const hpDiff = newHpU - oldHp;
	if (hpDiff <= 0) return console.log('NL HEAL | HP update | HP reduced or unchanged, ignoring.');

	// Get burn if any
	const burn = getBurn(actor);
	if (!Number.isFinite(burn)) return console.log('NL HEAL | HP update | NL heal blocked');

	const newNL = Math.max(burn, newNLU ?? 0);
	const healedNL = Math.max(burn, newNL - hpDiff);

	if (healedNL === oldNL) return console.log('NL HEAL | HP update | New NL is same as old, ignoring.', { burn });
	const updateData = { [nlPath]: healedNL };
	if (game.release.generation >= 10)
		actor.updateSource(updateData);
	else
		actor.data.update(updateData);
};

Hooks.on('preUpdateActor', dualHealing);

// INITIALIZATION
Hooks.once('ready', () => {
	const mod = game.modules.get(MODULE_ID);
	if (game.user.isGM) {
		Hooks.on('updateWorldTime', timeBasedNonlethalHealing);

		// REGISTER API
		mod.api = {
			nextHeal: nextPeriodTick,
			lastCheck: lastCheck,
			countTicks,
			setLastCheck(time) {
				const t = Number(time), ts = Number.isSafeInteger(t);
				if (ts) game.settings.set(MODULE_ID, LAST_CHECK, t);
				console.assert(ts, 'time is invalid number');
			},
			resetTime: function () {
				this.setLastCheck(game.time.worldTime - PERIOD);
			},
			forceCheck: function () {
				this.resetTime();
				timeBasedNonlethalHealing(game.time.worldTime, 0);
			},
		};
	}

	console.log(`%cNONLETHAL HEALING%c | %c${mod.version}%c | Initialized`,
		COLORS.main, COLORS.unset, COLORS.number, COLORS.unset);
});
