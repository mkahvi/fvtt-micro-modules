# Enhanced Nonlethal Healing for Pathfinder 1e (Proof of Concept)

![Supported Foundry Versions](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fversion%3Fstyle%3Dflat%26url%3Dhttps%3A%2F%2Fgitlab.com%2Fmkahvi%2Ffvtt-micro-modules%2F-%2Fraw%2Fmaster%2Fpf1-nl-heal-poc%2Fmodule.json)

Enhances nonlethal healing by applying it with passing time.

Also optionally heals NL damage when normal health is healed (ignored if NL was also changed).

Can be configured to prevent certain amount of NL damage being healed per actor.

## Configuration

### Module

- All Scenes – Process all scenes. This is slow if there's lot of actors that need healing and not recommended. If disabled only viewed and active scenes are processed.
- All Actors – Processes all actors. If disabled, processes only player owned actors.
- Dual Heal – Apply normal health healing to NL damage also.

### Per Actor

- Buffs with `burnNonlethal` boolean flag _stack_ their item levels to total of NL damage that can't be healed.
- Any item with `preventNonlethalHeal` boolean flag block nonlethal healing entirely for the actor.

## API

```js
const api = game.modules.get('pf1-nl-heal-poc').api;
api.setLastCheck(t); // Set last check time to `t`
api.setLastCheck(api.lastCheck() - 3_600); // Rolls back last check time by an hour to add extra hour of healing to the next tick
api.setLastCheck(api.lastCheck() + (8 * 3_600)); // Roll last check forward by 8 hours to ignore healing due to rest (assuming time is also rolled forward by 8 hours).
api.nextHeal(); // Seconds until next heal tick
api.lastCheck(); // Time when last heal tick occurred
api.countTicks(game.time.worldTime + 10_000); // Returns number of heal ticks that would occur if time was progressed by this much.
api.resetTime(); // Same as api.setLastCheck(game.time.worldTime - 3_600);
api.forceCheck(); // resets time and artificially triggers world time event handler
```

## Install

Manifest URL: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/master/pf1-nl-heal-poc/module.json>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
