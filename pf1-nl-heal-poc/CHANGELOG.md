# Change Log

## 0.2.0.1

- Fix: Odd behaviour with imports or similar updates.

## 0.2

- Dual healing function (optional).
  - Healing normal health heals nonlethal damage, too.

## 0.1.1

- Improved API
  - Added resetTime() which is shorthand for setLastCheck() with special variables
  - Added forceCheck() which is shorthand for resetTime() followed by immediate NL heal check.

## 0.1 Initial
