export class CasterAbilityFilter extends pf1.applications.compendiumBrowser.filters.CheckboxFilter {
	static label = 'CasterAbilityFilter.Label';
	static type = 'class';
	static indexField = 'system.casting.ability';

	prepareChoices() {
		super.prepareChoices();

		this.choices = this.constructor.getChoicesFromConfig(pf1.config.abilities);
	}
}

Hooks.once('init', () => {
	const browser = pf1.applications.compendiumBrowser.ClassBrowser;

	browser.filterClasses.push(CasterAbilityFilter);
});
