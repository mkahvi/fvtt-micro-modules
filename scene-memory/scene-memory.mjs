const CFG = {
	id: 'scene-memory',
	SETTINGS: {
		autoReset: 'autoReset',
	},
	FLAGS: {
		lastScene: 'last'
	}
}

let api;

class SceneMemoryManager extends FormApplication {
	constructor(options) {
		if (!game.user.isGM) throw new Error('Invalid user');
		super(options);
	}

	static get defaultOptions() {
		const options = super.defaultOptions;
		return {
			...options,
			title: 'Scene Memory',
			classes: [...options.classes, 'scene-memory-manager'],
			template: 'modules/scene-memory/scene-memory.hbs',
			submitOnChange: true,
			submitOnClose: false,
			closeOnSubmit: false,
			width: 460,
			height: 'auto',
		}
	}

	getData() {
		return {
			users: game.users
				.map(u => ({ ...u, scene: game.scenes?.get(u.getFlag(CFG.id, CFG.FLAGS.lastScene)) }))
		}
	}

	_updateObject(event, formData) {
		//
	}

	async _onButton(event) {
		event.preventDefault();

		const el = event.currentTarget;
		const action = el.dataset.action;
		switch (action) {
			case 'reset':
				await api.reset();
				this.render();
				break;
		}
	}

	/**
	 * @override
	 * @param {JQuery<HTMLElement>} jq
	 */
	activateListeners(jq) {
		super.activateListeners(jq);
		const html = this.form;

		html.querySelectorAll('button[data-action]')
			.forEach(el => el.addEventListener('click', this._onButton.bind(this)));
	}
}

Hooks.once('init', () => {
	game.settings.register(CFG.id, CFG.SETTINGS.autoReset, {
		name: 'Auto Reset On Active',
		hint: 'Automatically reset last saved scene when new scene is activated.',
		type: Boolean,
		default: true,
		scope: 'world',
		config: true,
	});
});

Hooks.on('canvasInit', (canvas) => {
	if (canvas.scene) {
		if (canvas.scene.active) {
			const autoReset = game.settings.get(CFG.id, CFG.SETTINGS.autoReset);
			if (autoReset)
				return void game.user.unsetFlag(CFG.id, CFG.FLAGS.lastScene);
		}

		// const last = game.user?.getFlag(CFG.id, CFG.FLAGS.lastScene);
		game.user.setFlag(CFG.id, CFG.FLAGS.lastScene, canvas.scene.id);
	}
});

Hooks.once('ready', () => {
	api = {
		reset: async () => {
			for (const user of game.users) {
				await user.unsetFlag(CFG.id, CFG.FLAGS.lastScene);
			}
		},
		open: () => new SceneMemoryManager().render(true, { focsu: true })
	};

	game.modules.get(CFG.id).api = api;
});

function getEarlyUser() {
	let user = game.user;
	if (user) return user.getFlag(CFG.id, CFG.FLAGS.lastScene);
	user = game.data.users?.find(u => u._id === game.userId);
	return user?.flags?.[CFG.id]?.[CFG.FLAGS.lastScene];
}

function restoreLastOnInit(wrapped) {
	const canvasInitialized = canvas.ready || game.settings.get('core', 'noCanvas');
	if (canvasInitialized) return wrapped();

	const last = game.user?.getFlag(CFG.id, CFG.FLAGS.lastScene);
	return this.get(last) || wrapped();
}

Hooks.once('init', () => {
	/* global libWrapper */
	libWrapper.register(CFG.id, 'Scenes.prototype.current', restoreLastOnInit, libWrapper.MIXED);
});
