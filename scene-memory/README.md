# Scene Memory

![Supported Foundry Versions](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fversion%3Fstyle%3Dflat%26url%3Dhttps%3A%2F%2Fgitlab.com%2Fmkahvi%2Ffvtt-micro-modules%2F-%2Fraw%2Fmaster%2Fscene-memory%2Fmodule.json)

Remember which scene user was last in and load that on refresh instead of the active scene.

GM can reset saved scenes at any point.

Returning to active scene resets the memorized scene. This can be disabled by GM in module settings.

## API

```js
game.modules.get("scene-memory").api.open(); // open control dialog
```

## Install

Manifest URL: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/master/scene-memory/module.json>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
