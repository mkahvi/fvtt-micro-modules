const implementDarkvision = () => {
	/* global VisionMode */
	const originalDarkvision = CONFIG.Canvas.visionModes.darkvision;
	CONFIG.Canvas.visionModes.darkvision = new VisionMode({
		id: 'darkvision',
		label: 'VISION.ModeDarkvision',
		canvas: {
			shader: originalDarkvision.canvas.shader,
			uniforms: { contrast: 0, saturation: -1.0, brightness: 0 }
		},
		lighting: {
			// levels: {[VisionMode.LIGHTING_LEVELS.DIM]: VisionMode.LIGHTING_LEVELS.BRIGHT}, // Foundry sets dim to bright
			background: { visibility: VisionMode.LIGHTING_VISIBILITY.REQUIRED }
		},
		vision: {
			darkness: { adaptive: false },
			defaults: { attenuation: 0, contrast: 0, saturation: -1.0, brightness: 0 }
		}
	});

	console.log('👀 Alternate Darkvision 👀 enabled!');
}

Hooks.once('init', implementDarkvision);
