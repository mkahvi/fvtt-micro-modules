const CFG = {
	id: 'forest-of-mirrors',
};

export class ForestTender extends FormApplication {
	constructor(options) {
		if (!game.user.isGM) throw new Error('You\'re not a valid tender');
		super(options);

		this._onSceneUpdateBound = this._onSceneUpdate.bind(this);
		Hooks.on('updateScene', this._onSceneUpdateBound);
	}

	static get defaultOptions() {
		const options = super.defaultOptions;
		return {
			...options,
			classes: [...options.classes, 'forest-tender'],
			template: 'modules/forest-of-mirrors/tender.hbs',
			title: 'Forest Tender',
			submitOnChange: true,
			submitOnClose: false,
			closeOnSubmit: false,
			height: 'auto',
			width: 320,
		};
	}

	getData() {
		return {
			fog: game.settings.get(CFG.id, 'fog'),
			lost: game.settings.get(CFG.id, 'lost'),
			mirage: game.settings.get(CFG.id, 'mirage'),
			noActive: !game.scenes.active,
		}
	}

	static open() {
		const app = new this();
		app.render(true, { focus: true });
		return app;
	}

	async _updateObject(event, formData) {
		this.form.classList.add('submitting');
		await game.settings.set(CFG.id, 'fog', formData.fog);
		await game.settings.set(CFG.id, 'lost', formData.lost);
		await game.settings.set(CFG.id, 'mirage', formData.mirage);
		this.render();
	}

	_onSceneUpdate(scene, changed, context, userId) {
		//
		console.log(...arguments);
		if (changed.active !== undefined) this.render();
	}

	close(...args) {
		super.close(...args);
		Hooks.off('updateScene', this._onSceneUpdateBound);
	}

	/**
	 * @param {Event} event
	 */
	_onAction(event) {
		const el = event.currentTarget;
		const action = el.dataset.action;
		switch (action) {
			case 'deactivate':
				game.scenes.active?.update({ active: false });
				break;
		}
	}

	activateListeners(jq) {
		super.activateListeners(jq);

		const html = this.form;

		html.querySelector('button[data-action="deactivate"]')?.addEventListener('click', ev => this._onAction(ev));
	}
}
