# Forest of Mirrors

![Supported Foundry Versions](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fversion%3Fstyle%3Dflat%26url%3Dhttps%3A%2F%2Fgitlab.com%2Fmkahvi%2Ffvtt-micro-modules%2F-%2Fraw%2Fmaster%2Fforest-of-mirrors%2Fmodule.json)

Allows GM to attempt to trick players by providing misleading information in scene navigation.

|Feature|Purpose|
|:---|:---|
|Obscuring Fog|Players no longer see which scene they're on.|
|Lost & Confused|Player navigation bar is completely hidden.|
|Mirage|Players see all others as if they were on same scene.|

Purpose: Creating scenarios where players are seeing things, are separated from the others without their knowledge, or other confusion.

**Recommendation**: Combine with other modules, such as [Scene Memory](https://gitlab.com/mkahvi/fvtt-micro-modules/-/tree/master/scene-memory).

## Usage

If your players know about this module (and are the kind to check active modules and the like), I'd recommended you keep this module enabled even in games where you don't use it, just to lull them into uncertainty.

### Obscuring Fog

No special steps needed.

### Lost & Confused

No special steps needed.

### Mirage

Recommendation:

- Disable active scene
  - The forest tender dialog provides a button for doing this.
- Disallow player navigation to other scenes.
- Pull players to specific scenes.
- Combine with other trickery.

## API

Open GUI:

```js
game.modules.get("forest-of-mirrors").api.tender();
```

## Install

Manifest URL: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/master/forest-of-mirrors/module.json>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
