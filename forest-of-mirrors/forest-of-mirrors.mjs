const CFG = {
	id: 'forest-of-mirrors',
};

document.body.classList.add('forest-of-mirrors-initializing');

/**
 * @param {User} user
 * @returns {Element}
 */
function playerPip(user) {
	const p = document.createElement('li');
	p.classList.add('scene-player');
	p.style.background = user.color;
	p.border = `1px solid ${user.border.css}`;
	p.innerText = user.name[0];
	return p;
}

/**
 * @param {Element} html
 */
function applyForest(html) {
	html ||= document.getElementById('navigation');

	// Obscuring Fog
	if (game.settings.get(CFG.id, 'fog')) {
		if (game.user.isGM)
			html.classList.add('forested', 'forestlord');
		else {
			html.classList.add('forested', 'player');
			// Currently viewed scene
			html.querySelectorAll('.view')
				.forEach(el => el.classList.remove('view'));
			// .gm marks scenes only GM can navigate to
			html.querySelectorAll('.gm')
				.forEach(el => el.classList.remove('gm'));
		}
	}
	else {
		html.classList.remove('forested');
		const sid = canvas.scene?.id;
		if (sid)
			html.querySelector(`.scene[data-scene-id="${sid}"]`).classList.add('view');
	}

	// Lost & Confused
	if (game.settings.get(CFG.id, 'lost')) {
		if (game.user.isGM)
			html.classList.add('lost', 'forestlord');
		else {
			html.classList.add('lost', 'player');
			setTimeout(() => {
				if (game.settings.get(CFG.id, 'fog'))
					html.classList.add('wandering-the-dark');
			}, 200);
		}
	}
	else {
		html.classList.remove('wandering-the-dark', 'lost');
	}

	// Mirage
	if (game.settings.get(CFG.id, 'mirage')) {
		if (game.user.isGM)
			html.classList.add('mirage', 'forestlord');
		else {
			html.classList.add('mirage', 'player');
			const users = game.users.filter(u => u.active);
			const playerList = html.querySelector('.scene.view .scene-players');
			playerList?.replaceChildren(...users.map(playerPip));
		}
	}
	else {
		html.classList.remove('mirage');
	}
}

/**
 * @param {SceneNavigation} app
 * @param {JQuery<HTMLElement>} html
 * @param {object} options
 */
function mirrorMirror(app, [html], options) {
	applyForest(html);
}

Hooks.on('renderSceneNavigation', mirrorMirror);

Hooks.once('init', () => {
	game.settings.register(CFG.id, 'fog', {
		name: 'Hide player location',
		hint: 'Hides which scene players are for all players.',
		default: false,
		type: Boolean,
		config: true,
		scope: 'world',
		onChange: () => applyForest()
	});
	game.settings.register(CFG.id, 'lost', {
		name: 'Hide player nav bar',
		hint: 'Hides the entire navigation bar from players.',
		default: false,
		type: Boolean,
		config: true,
		scope: 'world',
		onChange: () => applyForest()
	});

	game.settings.register(CFG.id, 'mirage', {
		name: 'Confuse player locations',
		hint: 'Players see all others in the same scene.',
		default: false,
		type: Boolean,
		config: true,
		scope: 'world',
		onChange: (value) => {
			applyForest();
			if (!value) ui.nav.render();
		},
	});
});

Hooks.on('renderSceneNavigation', () => applyForest());

// #loading-bar

Hooks.on('ready', () => {
	game.modules.get(CFG.id).api = {
		control: () => {
			//
		},
		fog: (value) => game.settings.set(CFG.id, 'fog', !!value),
		lost: (value) => game.settings.set(CFG.id, 'lost', !!value),
		tender: () => import('./forestlord.mjs').then(m => m.ForestTender.open()),
	};

	document.body.classList.remove('forest-of-mirrors-initializing');
});
