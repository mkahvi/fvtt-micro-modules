# Change Log

## 1.0.1.2

- Fix: Activation cost and type editing was severely misbehaving with unchained action economy enabled on latest master.

## 1.0.1.1

- Fix: Round activation type causing false equivalency.

## 1.0.1

- Fix: Nonaction was not considered needing to be the same for chained and unchained activation.
- Fix: Activation type equivalencies were not accounting for few cases (swift=action, 1 round=3 actions).
- Fix: Some warnings displayed wrong text.
- Change: Many more activation types have been marked as not needing cost configuration. Needs update on system side to match.
- Added: Fix button for cases where the solution is obvious.
  - This for now only happens with activation types and costs

## 1.0.0.2

- Change: Finite charges option no longer declares error for lacking max formula as supported by PF1v11.

## 1.0.0.1

- Fix: Deprecation warnings from `{{#select}}` usage.

## 1.0.0

- Change: Special, Passive, Full-Round, Nonaction and Free activation no longer provides numeric configuration.
- Change: Minimum cost of actions increased to `1` from `0`, since zero is meaningless.
- New: Action type/cost mismatch is warned about if some kind of equivalency is known.
- New: Minimal PF1v11 support.
- Major version bump

## 0.2.8.2

- Fix: Locked compendiums had the inputs still editable.

## 0.2.8.1

- Fix: Underlined text no longer cause warnings.
- Fix: Secret blocks no longer cause warnings.

## 0.2.8

- Fix: Non-GMs get warnings about bad description.
- Fix: Non-GMs can see warnings about bad unidentified description if it's visible to them.
- Fix: Bad description detection now works more reliably for unwanted styles and properties.

## 0.2.7

- Change: Last edited by user is now optional, defaults to enabled. The setting is per client.
- Change: Last edited line is now always shown even if unknown to avoid UI twitching.

## 0.2.6

- New: Last edited by user & date display for item sheets.

## 0.2.5.1

- Fix: Change target detection was invalid.

## 0.2.5

- Fix: Errors in conditional modifiers highlighted action tab.
- New: Conditional modifiers are now properly checked.
- New: Changes tab is now checked.
- Fix: Better support for spell descriptions in PF1 v10.

## 0.2.4.1

- Fix: Folder selector.

## 0.2.4

- New: Folder selector for packs.

## 0.2.3

- Fix: Spells would incorrectly warn about missing description.
- New: Warn about missing template size formula.

## 0.2.2.1

- Fix: Activation cost labels were flipped.
- Fix: Max formula error was incorrectly warning about it even when it was there.

## 0.2.2

- Fix: Activation type handling was incorrect.
- New: Warn about missing main item description.
- New: Warn about partially configured aura.
- New: Warn about missing max charges formula.

## 0.2.1

- PF1v9 compatibility

## 0.2.0

- New: Descriptions with `style` properties are highlighted as errors.
- Fix: Some warnings caused the inputs to become rather unusuable.

## 0.1.0 Initial

- New: Allow editing both chained and unchained action economy at the same time.
- New: Warn about ammunition malconfiguration.
- New: Warn about saving throw malconfiguration.
- New: Warn about nonfunctional damage.
- New: Warn about missing damage type.
- New: Warn about missing damage formula.
