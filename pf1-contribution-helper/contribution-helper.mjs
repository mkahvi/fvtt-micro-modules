const CFG = {
	id: 'pf1-contribution-helper',
};

const missingData = 'contribution-missing',
	missingDataLink = 'contribution-missing-linked',
	malformedData = 'contribution-malformed',
	warning = 'contribution-warning',
	badTab = 'contribution-tab';

const templates = {
	'contribution-activation': `modules/${CFG.id}/templates/activation.hbs`,
	'contribution-folder': `modules/${CFG.id}/templates/folder.hbs`,
};

const handlebarsOptions = {
	allowProtoMethodsByDefault: true,
	allowProtoPropertiesByDefault: true,
	preventIndent: true,
};

/** Action types that are identical on both systems */
const activationOverlap = new Set(['minute', 'hour', 'special', 'passive', 'attack', 'aoo', 'free', 'nonaction']);

/** Action types with expected equivalency */
const activationExpectations = {
	immediate: [['reaction', 1]],
	swift: [['action', 1]],
	standard: [['action', 2]],
	move: [['action', 1]],
	full: [['action', 3]],
	round: [['action', 3]],
};

/** Maximum costs */
const activationCostLimits = {
	'': 1, // False, but this disables selection
	swift: 1,
	passive: 1,
	immediate: 1,
	move: 1,
	standard: 1,
	action: 3,
	attack: 1,
	aoo: 1,
	nonaction: 1,
	free: 1,
	full: 1,
	minute: Infinity,
	round: Infinity,
	hour: Infinity,
	special: 1,
};

/** Activation types that should not have cost editable (1 or less allowed) */
const costlessActivations = new Set(
	Object.entries(activationCostLimits)
		.filter(([_, cost]) => cost <= 1)
		.map(([type]) => type),
);

const renderTemplate = (id, data) => {
	const d = document.createElement('div');
	d.innerHTML = Handlebars.partials[id](data, handlebarsOptions);
	return d.childNodes;
};

Hooks.once('setup', () => {
	loadTemplates(templates);
});

// Convert pack tree
function convertTree(tree, parent) {
	const parentPath = parent?.path ? (parent.path + ' / ') : '';
	return {
		path: parentPath + (tree.folder?.name ?? ''),
		id: tree.folder?.id,
		name: tree.folder?.name,
		get children() { return tree.children.map(c => convertTree(c, this)); },
	};
}

function addFix(el, doc, fix, label) {
	const a = document.createElement('a');
	a.innerHTML = '<i class="fa-solid fa-wrench fa-flip-horizontal fa-fw"></i>';
	a.classList.add('contrib-helper-fixer');
	a.dataset.fix = JSON.stringify(fix);
	a.dataset.tooltip = label;
	el.after(a);

	a.addEventListener('click', ev => {
		ev.preventDefault();
		ev.stopPropagation();

		if (doc.item) console.log('Applying fix to', doc.name, 'in', doc.item.name, fix);
		else console.log('Applying fix to', doc.name, fix);
		doc.update(fix);
	},
	{ once: true });
}

Hooks.on('renderItemSheet', (app, [html]) => {
	if (app.item.getFlag('core', 'sheetClass')) return;

	const locked = !app.isEditable;

	let highlightDescriptionTab = false,
		highlightDetailsTab = false,
		hightlightChangesTab = false;

	const item = app.item,
		itemData = item.system;

	// Add last editor display
	const header = html.querySelector('form header');
	if (header) {
		if (game.settings.get(CFG.id, 'lastEditor')) {
			const user = game.users.get(item._stats.lastModifiedBy);

			const div = document.createElement('div');
			div.classList.add('contribution-helper', 'last-editor');
			const parts = [
				'<label>',
				game.i18n.localize('PF1.Contribution.LastEditedBy'),
				'</label> ',
				'<span class="name">',
				user?.name || game.i18n.localize('PF1.Contribution.UnknownUser'),
				'</span>',
			];
			if (item._stats.modifiedTime !== null) {
				const date = item._stats.modifiedTime !== null ? new Date(item._stats.modifiedTime) : null;
				parts.push(' @ <span class="date">', date.toLocaleString(), '</span>');
			}
			div.innerHTML = parts.join('');
			if (!user) div.querySelector('.name')?.classList.add('unknown-user');
			header.append(div);
		}
	}

	const testDescription = (desc, el) => {
		if (!desc || !el) return;

		const content = document.createElement('div');
		content.innerHTML = desc;

		let badstyle = 0, badprops = 0;
		for (const stEl of content.querySelectorAll('[style]')) {
			// Ignore Foundry's daft underline method
			if (stEl.style.cssText.trim().replace(/\s+/g, '') !== 'text-decoration:underline;')
				badstyle += 1;
		}
		for (const idEl of content.querySelectorAll('[id]')) {
			if (!idEl.classList.contains('secret') && !/^secret-\w{16}$/.test(idEl.id))
				badprops += 3;
		}
		if (/(font-family|box-sizing|user-select)/.test(desc)) badstyle += 1;
		if (badstyle | badprops) {
			// Potentially bad content
			highlightDescriptionTab = true;
			el.classList.add(malformedData);
			const p = document.createElement('p');
			p.classList.add(warning);
			p.style.cssText += 'flex:0;'; // specific need for description
			const msg = [];
			if (badstyle > 0) {
				msg.push(game.i18n.localize('PF1.Contribution.Error.Malformed.Styled'));
				if (badstyle == 1)
					msg.push(game.i18n.localize('PF1.Contribution.Error.Malformed.Undetermined'));
			}
			if (badprops) {
				msg.push(game.i18n.localize('PF1.Contribution.Error.Malformed.UnwantedProps'));
			}
			p.innerHTML = '<i class="fa-solid fa-triangle-exclamation"></i> ' + msg.join('<br>');
			el.before(p);
		}
	};

	const desc = itemData.description?.value ?? itemData.shortDescription;
	const descel = html.querySelector('.editor-content[data-edit="system.description.value"],.editor-content[data-edit="system.shortDescription"]')?.closest('.editor');
	if (descel) {
		if (desc?.length) {
			testDescription(desc, descel);
		}
		else {
			// No description
			highlightDescriptionTab = true;
			if (descel) {
				descel.classList.add(missingData);
				descel.dataset.tooltip = 'PF1.Contribution.Error.Missing.Description';
			}
		}
	}

	// Unidentified description
	const undesc = itemData.description?.unidentified;
	if (undesc?.length) {
		const undescel = html.querySelector('.editor-content[data-edit="system.description.unidentified"]')?.closest('.editor');
		if (undescel) {
			testDescription(undesc, undescel);
		}
	}

	// Partial aura configuration
	if (!!itemData.aura?.school && !(itemData.cl > 0)) {
		const wel = html.querySelector('input[name="system.cl"]');
		if (wel) {
			wel.classList.add(missingData);
			html.querySelector('input[name="system.aura.school"]')?.classList.add(missingDataLink);
		}
		highlightDetailsTab = true;
	}
	if (itemData.cl > 0 && !itemData.aura?.school) {
		const wel = html.querySelector('select[name="system.aura.school"]');
		if (wel) {
			wel.classList.add(missingData);
			html.querySelector('input[name="system.cl"]')?.classList.add(missingDataLink);
		}
		highlightDetailsTab = true;
	}

	// Missing max formula
	if (['day', 'week'].includes(itemData.uses?.per)) {
		if (!(itemData.uses?.maxFormula?.trim().length > 0)) {
			const el = html.querySelector('input[name="system.uses.maxFormula"]');
			el?.classList.add(missingData);
			highlightDetailsTab = true;
		}
	}

	// Folder interface for packs
	const pack = game.packs.get(item.pack);
	if (pack) {
		const advanced = html.querySelector('.tab[data-tab="advanced"]');
		if (advanced) {
			const tree = convertTree(pack.tree);
			const folderData = {
				locked,
				tree,
				folder: item.folder?.id,
			};
			const nodes = renderTemplate('contribution-folder', folderData);
			advanced.append(...nodes);
		}
	}

	const ctab = html.querySelector('.tab[data-tab=\'changes\']');
	if (ctab) {
		ctab.querySelectorAll('.formula').forEach(el => {
			if (!el.value || el.value.trim() === '') {
				el.classList.add(missingData);
				el.dataset.tooltip = 'PF1.Contribution.Error.Missing.Formula';
				hightlightChangesTab = true;
			}
		});
		ctab.querySelectorAll('.change-target,.context-note-target').forEach(el => {
			if (!el.textContent.trim()) {
				el.classList.add(missingData);
				el.dataset.tooltip = 'PF1.Contribution.Error.Missing.Subject';
				hightlightChangesTab = true;
			}
		});
		ctab.querySelectorAll('textarea').forEach(el => {
			if (!el.value || el.value.trim() === '') {
				el.classList.add(missingData);
				el.dataset.tooltip = 'PF1.Contribution.Error.Missing.Text';
				hightlightChangesTab = true;
			}
		});
	}

	if (highlightDescriptionTab) html.querySelector('nav .item[data-tab="description"]')?.classList.add(badTab);
	if (highlightDetailsTab) html.querySelector('nav .item[data-tab="details"]')?.classList.add(badTab);
	if (hightlightChangesTab) html.querySelector('nav .item[data-tab="changes"]')?.classList.add(badTab);
});

Hooks.on('renderItemActionSheet', (app, [html]) => {
	const action = app.object;

	const actionData = action instanceof foundry.abstract.DataModel ? action : action.data;
	const actionSource = action instanceof foundry.abstract.DataModel ? action._source : action.data;

	const unchained = game.settings.get('pf1', 'unchainedActionEconomy');

	const locked = !app.isEditable;

	let highlightActionTab = false,
		highlightUsageTab = false,
		highlightDescriptionTab = false,
		highlightMiscTab = false,
		highlightConditionalsTab = false;

	// Allow editing chained and unchained actions
	const activation = html.querySelector('[name="activation.unchained.type"],[name="activation.type"]');
	if (activation) {
		const activationData = {
			locked,
			system: actionData,
			source: actionSource,
			actionCostMax: activationCostLimits[actionSource.activation?.type],
			get noActionCost() { return this.actionCostMax <= 1; },
			unActionCostMax: activationCostLimits[actionSource.activation?.unchained?.type],
			get noUnActionCost() { return this.unActionCostMax <= 1; },
			config: pf1.config,
			unchained,
		};
		const nodes = renderTemplate('contribution-activation', activationData);
		activation.closest('.form-group').replaceWith(...nodes);

		const typeEl = html.querySelector('[name="activation.type"]');
		const unTypeEl = html.querySelector('[name="activation.unchained.type"]');
		const actTypes = [typeEl, unTypeEl];

		actTypes.forEach(el => {
			if (!el) return;
			if (!el.value) {
				el.classList.add(missingData);
				el.dataset.tooltip = 'PF1.Contribution.Error.Missing.ActivationType';
				highlightUsageTab = true;
			}
		});

		// Action cost must be at least 1
		const actCost = html.querySelector('[name="activation.cost"]');
		const unActCost = html.querySelector('[name="activation.unchained.cost"]');
		const actCosts = [actCost, unActCost];
		actCosts.forEach(el => {
			if (el.type !== 'number') return;
			if (!(el.valueAsNumber >= 1)) {
				el.classList.add(malformedData);
				el.dataset.tooltip = 'PF1.Contribution.Error.Invalid.ActivationCostLow';
				highlightUsageTab = true;
			}
		});

		// Equivalent type must have equivalent type & cost
		const activationType = actionSource.activation?.type || '';
		const unActivationType = actionSource.activation?.unchained?.type || '';
		if (activationOverlap.has(activationType)) {
			if (activationType !== unActivationType) {
				actTypes.forEach(el => {
					el.classList.add(malformedData);
					el.dataset.tooltip = 'PF1.Contribution.Error.Invalid.ActivationType';
				});

				addFix(typeEl, action, { 'activation.type': unActivationType }, game.i18n.format('PF1.Contribution.Error.Fix.ApplyType', { type: pf1.config.abilityActivationTypes[unActivationType] }));
				addFix(unTypeEl, action, { 'activation.unchained.type': activationType }, game.i18n.format('PF1.Contribution.Error.Fix.ApplyType', { type: pf1.config.abilityActivationTypes_unchained[activationType] }));

				highlightUsageTab = true;
			}

			if (!costlessActivations.has(activationType)) {
				const nc = actionSource.activation?.cost || 0;
				const unc = actionSource.activation?.unchained?.cost || 0;
				if (nc !== unc) {
					actCosts.forEach(el => {
						el.classList.add(malformedData);
						el.dataset.tooltip = 'PF1.Contribution.Error.Invalid.ActivationCost';
						highlightUsageTab = true;
					});
					addFix(actCost, action, { 'activation.cost': unc }, game.i18n.format('PF1.Contribution.Error.Fix.ApplyCost', { cost: unc }));
					addFix(unActCost, action, { 'activation.unchained.cost': nc }, game.i18n.format('PF1.Contribution.Error.Fix.ApplyCost', { cost: nc }));
				}
			}
		}

		// Activation equivalencies
		const expectedActivation = activationExpectations[activationType];
		if (expectedActivation) {
			let fixCost, fixUnCost;
			let warnType = true, warnCost = false;
			for (const [option, cost] of expectedActivation) {
				if (unActivationType === option) {
					const unCost = actionSource.activation?.unchained?.cost || 0;
					const actCostValue = actCost.valueAsNumber || 1;
					const expectedUnCost = cost * actCostValue;
					if (expectedUnCost !== unCost) {
						warnCost = true;
						if (!costlessActivations.has(activationType)) {
							const reverseCost = Math.max(1, Math.floor(unCost / cost));
							fixCost = { update: { 'activation.cost': reverseCost }, value: reverseCost };
						}
						if (!costlessActivations.has(unActivationType))
							fixUnCost = { update: { 'activation.unchained.cost': expectedUnCost }, value: expectedUnCost };
					}
					warnType = false;
					break;
				}
			}

			if (warnType) {
				actTypes.forEach(el => {
					el.classList.add(malformedData);
					el.dataset.tooltip = 'PF1.Contribution.Error.Invalid.ActivationType';
				});
				const newAct = expectedActivation[0][0]; // Default to first proposed
				addFix(unTypeEl, action, { 'activation.unchained.type': newAct }, game.i18n.format('PF1.Contribution.Error.Fix.ApplyType', { type: pf1.config.abilityActivationTypes_unchained[newAct] }));
				// TODO: Add equivalent for standard activation type?
				highlightUsageTab = true;
			}
			if (warnCost) {
				actCosts.forEach(el => {
					el.classList.add(malformedData);
					el.dataset.tooltip = 'PF1.Contribution.Error.Invalid.ActivationCost';
				});
			}
			if (fixCost) addFix(actCost, action, fixCost.update, game.i18n.format('PF1.Contribution.Error.Fix.ApplyCost', { cost: fixCost.value }));
			if (fixUnCost) addFix(unActCost, action, fixUnCost.update, game.i18n.format('PF1.Contribution.Error.Fix.ApplyCost', { cost: fixUnCost.value }));
		}
	}
	// TODO: Inverse search of activation equivalencies?

	// Ammunition misconfiguration
	// OBSOLETE with PF1v11
	if (actionData.usesAmmo && !actionData.ammoType) {
		const at = html.querySelector('[name="ammoType"]');
		at.classList.add(missingData);
		at.dataset.tooltip = 'PF1.Contribution.Error.Missing.AmmoType';
		highlightUsageTab = true;
	}

	// Partially configured saves
	// Effect set but not type
	if (actionData.save.description && !actionData.save.type) {
		const type = html.querySelector('[name="save.type"]');
		if (type) {
			type.classList.add(missingData);
			type.dataset.tooltip = 'PF1.Contribution.Error.Missing.SaveType';
			highlightActionTab = true;
		}
	}
	// Type set but no effect
	if (!actionData.save.description && actionData.save.type) {
		const desc = html.querySelector('[name="save.description"]');
		if (desc) {
			desc.classList.add(missingData);
			desc.dataset.tooltip = 'PF1.Contribution.Error.Missing.SaveEffect';
			highlightActionTab = true;
		}
	}
	// DC offset defined with nothing else
	if (actionData.save.dc && (!actionData.save.type || !actionData.save.description)) {
		const dc = html.querySelector('[name="save.dc"]');
		if (dc) {
			dc.classList.add(missingData);
			dc.dataset.tooltip = 'PF1.Contribution.Error.OrphanedDC';
			highlightActionTab = true;
		}
	}
	// DC defined as 0
	if (actionData.save.dc === '0') {
		const dc = html.querySelector('[name="save.dc"]');
		if (dc) {
			dc.classList.add(missingData);
			dc.dataset.tooltip = 'PF1.Contribution.Error.BadSaveDC';
			highlightActionTab = true;
		}
	}
	// Non-spell with no DC formula
	if (!actionData.save.dc && actionData.save.type && action.item.type !== 'spell') {
		const dc = html.querySelector('[name="save.dc"]');
		if (dc) {
			dc.classList.add(missingData);
			dc.dataset.tooltip = 'PF1.Contribution.Error.Missing.SaveDC';
			highlightActionTab = true;
		}
	}

	// Crit/Noncrit damage without normal damage
	const damage = actionData.damage;
	if (damage.parts.length == 0) {
		// Nonfunctional damage parts
		if (damage.nonCritParts.length || damage.critParts.length) {
			const dmg = html.querySelector('ol.damage-parts');
			const p = document.createElement('p');
			p.classList.add(warning);
			p.innerHTML = '<i class="fa-solid fa-triangle-exclamation"></i> ' + game.i18n.localize('PF1.Contribution.Error.Missing.BaseDamage');
			dmg.after(p);
			highlightActionTab = true;
		}
	}

	// Missing damage type
	html.querySelectorAll('.damage-type-visual.empty').forEach(el => {
		el.classList.add(missingData);
		el.dataset.tooltip = 'PF1.Contribution.Error.Missing.DamageType';
		const tab = el.closest('.tab');
		if (tab.dataset.tab === 'action')
			highlightActionTab = true;
		else if (tab.dataset.tab === 'conditionals')
			highlightConditionalsTab = true;
	});

	// Missing damage formula
	html.querySelectorAll('input.damage-formula').forEach(el => {
		if (!el.value.trim()) {
			el.classList.add(missingData);
			el.dataset.tooltip = 'PF1.Contribution.Error.Missing.Formula';
			highlightActionTab = true;
		}
	});

	// Missing template size
	if (actionData.measureTemplate?.type && !actionData.measureTemplate?.size) {
		highlightMiscTab = true;
		html.querySelector('[name="measureTemplate.size"]')?.classList.add(missingData);
	}

	// Bad action description
	if (actionData.description?.length) {
		if (/ style=['"]/.test(actionData.description)) {
			highlightDescriptionTab = true;
			const el = html.querySelector('.editor-content[data-edit="description"]')?.closest('.editor');
			el?.classList.add(malformedData);
			if (el) {
				const p = document.createElement('p');
				p.classList.add(warning);
				p.innerHTML = '<i class="fa-solid fa-triangle-exclamation"></i> ' + game.i18n.localize('PF1.Contribution.Error.Malformed.Styled');
				el.before(p);
			}
		}
	}

	// Missing conditional formulas
	const ctab = html.querySelector('.tab[data-tab=\'conditionals\']');
	if (ctab) {
		ctab.querySelectorAll('.formula').forEach(el => {
			if (!el.value || el.value.trim() === '') {
				el.classList.add(missingData);
				el.dataset.tooltip = 'PF1.Contribution.Error.Missing.Formula';
				highlightConditionalsTab = true;
			}
		});
		ctab.querySelectorAll('.conditional-name').forEach(el => {
			if (!el.value || el.value.trim() === '') {
				el.classList.add(missingData);
				el.dataset.tooltip = 'PF1.Contribution.Error.Missing.Name';
				highlightConditionalsTab = true;
			}
		});
		ctab.querySelectorAll('.conditional-target').forEach(el => {
			if (el.value === '') {
				el.classList.add(missingData);
				el.dataset.tooltip = 'PF1.Contribution.Error.Missing.Subject';
				highlightConditionalsTab = true;
			}
		});
	}

	// Highlight problematic tabs
	if (highlightDescriptionTab) html.querySelector('nav .item[data-tab="description"]').classList.add(badTab);
	if (highlightActionTab) html.querySelector('nav .item[data-tab="action"]').classList.add(badTab);
	if (highlightUsageTab) html.querySelector('nav .item[data-tab="activation"]').classList.add(badTab);
	if (highlightMiscTab) html.querySelector('nav .item[data-tab="misc"]').classList.add(badTab);
	if (highlightConditionalsTab) html.querySelector('nav .item[data-tab="conditionals"]').classList.add(badTab);
});

Hooks.once('init', () => {
	game.settings.register(CFG.id, 'lastEditor', {
		name: 'PF1.Contribution.LastEditor',
		type: Boolean,
		default: true,
		scope: 'client',
		config: true,
	});

	Handlebars.registerHelper('contrib-helper-selected', function (id, selected) {
		if (!selected) return;
		if (id === selected) return new Handlebars.SafeString('selected');
	});
});
