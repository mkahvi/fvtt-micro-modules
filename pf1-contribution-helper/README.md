# Contribution Helper for Pathfinder 1e

![Supported Foundry Versions](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fversion%3Fstyle%3Dflat%26url%3Dhttps%3A%2F%2Fgitlab.com%2Fmkahvi%2Ffvtt-micro-modules%2F-%2Fraw%2Fmaster%2Fpf1-contribution-helper%2Fmodule.json)

Adds additional interfaces and warnings for easier editing of the system for contribution purposes.

## Screencaps

Editing both chained and unchained action cost:  
![Action Type](./img/screencaps/action-type.png)

Warnings about bad damage configurations:  
![Bad Damage](./img/screencaps/bad-damage.png)

Warnings about bad DC configurations:  
![No Save DC](./img/screencaps/missing-save-dc.png)

## Install

Manifest URL: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/master/pf1-contribution-helper/module.json>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT license](./LICENSE).
