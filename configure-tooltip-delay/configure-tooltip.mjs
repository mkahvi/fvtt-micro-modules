const MODULE_ID = 'configure-tooltip-delay';

Hooks.once('init', () => {
	game.settings.register(
		MODULE_ID,
		'delay',
		{
			name: 'Delay',
			hint: 'Tooltip delay in milliseconds. Foundry default is 500ms.',
			type: Number,
			range: {
				min: 20,
				max: 2000,
				step: 10,
			},
			requiresReload: false,
			default: TooltipManager.TOOLTIP_ACTIVATION_MS,
			onChange: value => TooltipManager.TOOLTIP_ACTIVATION_MS = value,
			scope: 'client',
			config: true
		}
	);

	TooltipManager.TOOLTIP_ACTIVATION_MS = game.settings.get(MODULE_ID, 'delay') ?? 250;
});
