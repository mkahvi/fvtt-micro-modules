let template;
getTemplate('modules/pack-sourcing/templates/pf1.hbs').then(t => template = t);

/**
 *
 * @param {ItemSheet} sheet
 * @param {JQuery} html
 */
const injectSourcing = (sheet, [html]) => {
	if (!(sheet instanceof pf1.applications.item.ItemSheetPF)) return;

	const tab = html.querySelector('.tab[data-tab="advanced"]');

	const item = sheet.item;
	const source = item._stats?.compendiumSource ?? item.flags?.core?.sourceId;
	const info = source ? fromUuidSync(source) : {};

	const alwaysAllowDelete = game.settings.get('pack-sourcing', 'alwaysAllowDelete');

	let inconsistentData = false;
	if (item.isOwner) {
		if (!info) inconsistentData = true;
		else {
			let inconsistencies = 0;
			if (item._source.name !== info?.name) inconsistencies += 1;
			// Problem: Can't test more without fetching the source document.
			if (inconsistencies > 0) inconsistentData = true;
		}
	}

	const templateData = {
		present: !!source,
		found: !!info,
		uuid: source,
		name: info?.name,
		img: info?.img,
		pack: info?.pack,
		allowDelete: item.isOwner ? (alwaysAllowDelete || inconsistentData) : false,
	};

	const fragment = document.createElement('template');
	fragment.innerHTML = template(templateData);

	const button = fragment.content.querySelector('button.delete-source-id');
	button?.addEventListener('click', ev => {
		ev.preventDefault();
		ev.stopPropagation();
		item.unsetFlag('core', 'sourceId');
	});

	tab?.append(fragment.content);
};

Hooks.on('renderItemSheet', injectSourcing);
