Hooks.once('init', () => {
	game.settings.register('pack-sourcing', 'gmOnly', {
		name: 'GM-only',
		hint: 'If enabled, only GM has access to the interface. Otherwise everyone does.',
		type: Boolean,
		requiresReload: true,
		default: true,
		scope: 'world',
		config: true,
	});

	game.settings.register('pack-sourcing', 'alwaysAllowDelete', {
		name: 'Always Allow Delete',
		hint: 'If disabled, deletion is allowed only if viewed item is detected to be sufficiently inconsistent with source.',
		type: Boolean,
		requiresReload: false,
		default: false,
		scope: 'client',
		config: true,
	});
});

Hooks.once('ready', () => {
	if (game.settings.get('pack-sourcing', 'gmOnly') && !game.user.isGM) return;

	const mod = game.modules.get('pack-sourcing');
	import(`./systems/${game.system.id}.mjs`)
		.then(() => console.log(`Pack Sourcing | ${mod.version} | Ready for system: ${game.system.id}`));
});
