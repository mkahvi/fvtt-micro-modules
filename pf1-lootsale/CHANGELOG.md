# Change Log

## 0.4.0

- Foundry v10 compatibility

## 0.3.0

- Fix double render.
- Post chat message about sale.
- Disable for uneditable sheets.
- Improved messaging.

## 0.2.0 Foundry v9 compatibility

## 0.1.2.2 Foundry 0.7.x cross-compatibiltiy

## 0.1.2.1 Hotfix for .id of undefined

## 0.1.2 Foundry 0.8.x compatibility

## 0.1.1.1 Hotfix for strange rounding error in some circumstances (9 copper) causing sale to fail

## 0.1.1 System dependency added for module.json

## 0.1 Initial version
