const CFG = { module: 'mkah-pf1-lootsale' };

const getSystemData = (doc) => game.release.generation >= 10 ? doc.system : doc.data.data;

/* Split gp into gp, sp and cp */
function splitFunds(cp) {
	const funds = { gp: 0, sp: 0, cp: 0 };
	funds.gp = Math.floor(cp / 100);
	cp -= funds.gp * 100;
	funds.sp = Math.floor(cp / 10);
	cp -= funds.sp * 10;
	funds.cp = cp;
	return funds;
}

async function lootSale(actor, sale, itemList) {
	console.groupCollapsed('LOOT SALE');

	try {
		// SANITY CHECK
		if (!actor.isOwner) {
			const msg = game.i18n.localize('LootSale.NotOwner');
			console.log(msg);
			return ui.notifications.warn(msg);
		}

		if (actor.items.contents.length === 0) {
			const msg = game.i18n.localize('LootSale.NoItems');
			console.log(msg);
			return ui.notifications.warn(msg);
		}

		const cValue = actor.sheet.calculateSellItemValue({ inLowestDenomination: true }),
			cSale = splitFunds(cValue);

		if (cSale.gp !== sale.gp || cSale.sp !== sale.sp || cSale.cp !== sale.cp) {
			const msg = game.i18n.localize('LootSale.SaleValueMismatch');
			console.log(msg);
			return ui.notifications.warn(msg);
		}
		const equals = (a, b) => a.length === b.length && a.every((v, i) => v === b[i]);
		if (!equals(actor.items.map(i => i.id), itemList)) {
			const msg = game.i18n.localize('LootSale.ItemListMismatch');
			console.log(msg);
			return ui.notifications.warn(msg);
		}

		// PERFORM SALE
		console.log('SALE VALUE:', sale);

		const funds = duplicate(getSystemData(actor).currency);
		console.log('OLD FUNDS:', funds);

		funds.gp += sale.gp;
		funds.sp += sale.sp;
		funds.cp += sale.cp;

		console.log('NEW FUNDS:', funds);
		console.log('ITEM IDs: ', itemList);

		await actor.update({ 'data.currency': funds }, { render: false })
			.then(() => actor.deleteEmbeddedDocuments('Item', itemList));

		console.log('SALE COMPLETE');
		ui.notifications.info(game.i18n.localize('LootSale.Completed'));

		ChatMessage.create({
			flavor: game.i18n.format('LootSale.Seller', { user: game.user.name }),
			content: game.i18n.format('LootSale.Sale', { ...sale }),
			speaker: { actor: actor, token: actor.token },
		});
	}
	finally {
		console.groupEnd();
	}
}

/**
 * @param {Actor} actor
 */
async function lootSaleQuery(actor) {
	const itemList = actor.items.map(i => i.id);

	// TODO: Don't mention non-inventory items in case they exist?

	const saleValue = actor.sheet.calculateSellItemValue({ inLowestDenomination: true });
	const sale = splitFunds(saleValue);

	const html = '<div class=\'m-k-sale-dialog flexcol\'><p>' + game.i18n.format('LootSale.Prompt', { itemCount: itemList.length, ...sale }) + '</div>';

	return new Promise(
		(resolve) => new Dialog(
			{
				title: game.i18n.localize('LootSale.Confirm'),
				content: html,
				buttons: {
					sell: {
						label: game.i18n.localize('LootSale.Sell'),
						icon: '<i class="fas fa-coins"></i>',
						callback: _ => {
							lootSale(actor, sale, itemList);
							resolve(true);
						}
					},
					cancel: {
						label: game.i18n.localize('LootSale.Cancel'),
						icon: '<i class="fas fa-power-off"></i>',
					}
				},
				close: _ => resolve(false),
				default: 'cancel',
			},
			{ width: 320 }
		).render(true)
	);
}

async function saleButtonClick(event) {
	event.preventDefault();
	event.stopPropagation();
	this.disabled = true; // probably unnecessary

	if (game.modules.get(CFG.module).debug) console.log('LOOT SALE | SALE BUTTON |', arguments);

	const aId = event.target.dataset.actorId,
		tId = event.target.dataset.tokenId;

	const token = tId ? canvas.tokens.get(tId) : undefined;
	const actor = token ? token.actor : game.actors.get(aId);

	if (!actor) {
		const msg = game.i18n.format('LootSale.NoActor', { actorId: aId });
		console.error('LOOT SALE |', msg);
		return ui.notifications.warn(msg);
	}

	const b = this;
	return lootSaleQuery(actor).then(() => b.disabled = false);
}

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param {Object} options
 */
function injectSaleButton(sheet, [html], options) {
	if (!sheet.isEditable) return;

	if (game.modules.get(CFG.module).debug) console.log('LOOT SALE | Injecting button', arguments);

	const opts = html.querySelector('.summary.options');
	const li = document.createElement('li');
	const actorId = sheet.actor.id,
		tokenId = options.token?.id ?? '';
	li.innerHTML = `<button type='button' class='m-k-sale-button' data-actor-id='${actorId}' data-token-id='${tokenId}'>${game.i18n.localize('LootSale.SellContents')}</button>`;
	const button = li.firstChild;
	opts.after(button);
	button.addEventListener('click', saleButtonClick);
}

Hooks.on('renderActorSheetPFNPCLoot', injectSaleButton);

Hooks.once('ready', () => {
	game.modules.get(CFG.module).debug = false;

	console.log('LOOT SALE | READY');
});
