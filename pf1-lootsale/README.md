# Pathfinder 1 Lootsale

![Supported Foundry Versions](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fversion%3Fstyle%3Dflat%26url%3Dhttps%3A%2F%2Fgitlab.com%2Fmkahvi%2Ffvtt-micro-modules%2F-%2Fraw%2Fmaster%2Fpf1-lootsale%2Fmodule.json)

Adds a sell button to the built-in lootsheet to convert held items into gold.

☐ It makes me feel icky. I want the feature, but I feel like there's something missing.

## Configuration

Not available.

## Limitations

None known.

## Install

Manifest URL: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/master/pf1-lootsale/module.json>

Latest for 0.7.x (manual install only): <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/e4dde591136e68515c0542f2fc62998acae23069/pf1-lootsale/module.zip?inline=false>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
