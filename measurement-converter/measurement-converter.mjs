const CFG = {
	id: 'measurement-converter',
	saveDelay: 1_500,
	SETTINGS: {
		position: 'position',
		astro: 'astro',
		archaic: 'archaic',
		autoOpen: 'autoOpen'
	}
}

class PointModel extends foundry.abstract.DataModel {
	static defineSchema() {
		const fields = foundry.data.fields;
		return {
			x: new fields.NumberField({ integer: true, min: 0, initial: 180 }),
			y: new fields.NumberField({ integer: true, min: 0, initial: 100 }),
		};
	}
}

const registerSettings = () => {
	/*
	game.settings.register(CFG.id, CFG.SETTINGS.position,
		{
			type: PointModel,
			config: false,
			default: {},
			scope: 'client'
		});
	*/

	game.settings.register(CFG.id, CFG.SETTINGS.astro, {
		name: 'MeasurementConverter.Settings.Astro',
		// hint: 'AAAAA',
		type: Boolean,
		default: true,
		config: true,
		scope: 'client',
		onChange: MeasureConverterUI.render,
	});

	game.settings.register(CFG.id, CFG.SETTINGS.archaic, {
		name: 'MeasurementConverter.Settings.Archaic',
		// hint: 'BBBBB',
		type: Boolean,
		default: true,
		config: true,
		scope: 'client',
		onChange: MeasureConverterUI.render,
	});

	game.settings.register(CFG.id, CFG.SETTINGS.autoOpen, {
		name: 'MeasurementConverter.Settings.AutoOpen',
		hint: 'MeasurementConverter.Settings.AutoOpenHint',
		type: Boolean,
		default: true,
		config: true,
		scope: 'client',
	});
}

// Constants
const DECA = 10, // Exact
	HECTO = 100, // Exact
	KILO = 1000, // Exact
	inchesInFeet = 12, // Exact
	inchesInYard = 36, // Exact
	feetInYard = 3, // Exact
	// metersInYard = 0.9144,
	cmInInch = 2.54,
	feetInMile = 5280, // Exact
	mInNMile = 1852, // Exact
	ftInNMile = 6076, // Not-exact
	yardsInMile = 1760, // Exact
	kmInParsec = 3.086e+13, // Not-exact
	pcInLy = 0.306601, // Not-exact
	lyInParsec = 3.26156, // Not-exact
	auInLy = 63241.077, // Not-exact
	auInParsec = 206264.806247096, // Not-exact
	kmInAu = 149597870.700, // Exact
	milesInLeague = 3; // Exact

const units = {
	distance: [
		{
			// Centimeters
			key: 'cm',
			conversions: {
				in: v => v / cmInInch,
				m: v => v / HECTO,
				km: v => v / KILO / HECTO,
			}
		},
		{
			// Inches
			key: 'in',
			conversions: {
				ft: v => v / inchesInFeet,
				yd: v => v / inchesInYard,
				cm: v => v * cmInInch,
			},
		},
		{
			// Feet
			key: 'ft',
			archaic: true,
			conversions: {
				in: v => v * inchesInFeet,
				yd: v => v / feetInYard,
				mi: v => v / feetInMile,
				nmi: v => v / ftInNMile,
				m: v => v * (1200 / 3937), // "Exact"
			}
		},
		{
			// Yards
			key: 'yd',
			archaic: true,
			conversions: {
				in: v => v * inchesInYard,
				ft: v => v * feetInYard,
				mi: v => v / yardsInMile,
			}
		},
		{
			// Meters
			key: 'm',
			conversions: {
				cm: v => v * HECTO,
				km: v => v / KILO,
				nmi: v => v / mInNMile,
			},
		},
		{
			// Kilometers
			key: 'km',
			conversions: {
				m: v => v * KILO,
				cm: v => v * KILO * DECA,
				pc: v => v / kmInParsec,
			}
		},
		{
			// Miles
			key: 'mi',
			archaic: true,
			conversions: {
				ft: v => v * feetInMile,
				yd: v => v * yardsInMile,
				league: v => v / milesInLeague,
				m: v => v * 1_609.344,
			}
		},
		{
			// Nautical Miles
			key: 'nmi',
			conversions: {
				ft: v => v * mInNMile,
			}
		},
		{
			// Leagues
			key: 'league',
			archaic: true,
			noSymbol: true,
			conversions: {
				mi: v => v * milesInLeague,
			}
		},
		{
			// Astronomical Units
			key: 'au',
			astro: true,
			conversions: {
				ly: v => v / auInLy,
				pc: v => v / auInParsec,
			},
		},
		{
			// Light-years
			key: 'ly',
			astro: true,
			conversions:
			{
				au: v => v * auInLy,
				pc: v => v * lyInParsec,
			}
		},
		{
			// Parsecs
			key: 'pc',
			astro: true,
			conversions: {
				km: v => v * kmInParsec,
				au: v => v * auInParsec,
				ly: v => v / pcInLy,
			},
		},
	],
	mass: [
		{
			key: 'gr',
			archaic: true,
			conversions: {
				lb: v => v / 7000,
				'℥': v => v / 7000 * 16,
				// g: v => v / 64.79891, // Not exact
			}
		},
		{
			key: 'dr',
			archaic: true,
			conversions: {
				'℥': v => v / 16,
				lb: v => v / 256,
			}
		},
		{
			key: 'g',
			conversions: {
				kg: v => v / KILO,
				lb: v => v / 453.59237
			}
		},
		{
			key: 'kg',
			conversions: {
				g: v => v * KILO,
				t: v => v / 1_000,
				lb: v => v * 0.45359237,
			}
		},
		{
			key: 'lb',
			archaic: true,
			conversions: {
				gr: v => v * 7000,
				'℥': v => v * 16,
				g: v => v * 453.59237,
			}
		},
		{
			key: '℥',
			archaic: true,
			conversions: {
				gr: v => v * 480,
				dr: v => v * 16,
			}
		},
		{
			key: 't',
			archaic: true,
			conversions: {
				kg: v => v * 1_000,
			}
		},
	],
	volume: [
		{
			// Millilitres
			key: 'mL',
			conversions: {
				L: v => v / 1_000,
			}
		},
		{
			// Litres
			key: 'L',
			conversions: {
				mL: v => v * 1_000,
				m3: v => v / 1_000,
				gal: v => v / 4.54609,
			}
		},
		{
			// Ounces
			key: '℥',
			archaic: true,
			conversions: {
				cup: v => v / 10,
				pt: v => v / 20,
				qt: v => v / 40,
				gal: v => v / 160,
			}
		},
		{
			// Drachms (same as teaspoon)
			key: 'dr',
			archaic: true,
			conversions: {
				'℥': v => v / 8,
				pt: v => v / 2,
				T: v => v / 4,
			},
		},
		{
			// Tablespoons
			key: 'T',
			archaic: true,
			conversions: {
				dr: v => v * 4,
				cup: v => v / 16,
			},
		},
		{
			// Cups
			key: 'cup',
			archaic: true,
			conversions: {
				'℥': v => v * 10,
				pt: v => v / 2,
				T: v => v * 16,
			}
		},
		{
			// Pints
			key: 'pt',
			archaic: true,
			conversions: {
				'℥': v => v * 20,
				cup: v => v * 2,
				gal: v => v / 8,
			}
		},
		{
			// Cubic metre
			key: 'm3',
			conversions: {
				L: v => v * 1000,
				gal: v => v * 220,
			}
		},
		{
			// Quart
			key: 'qt',
			conversions: {
				'℥': v => v * 40,
			}
		},
		{
			// Gallon
			key: 'gal',
			archaic: true,
			conversions: {
				'℥': v => v * 160,
				m3: v => v / 220,
				bu: v => v / 8,
				L: v => v * 4.54609,
			}
		},
		{
			// Bushel
			key: 'bu',
			archaic: true,
			conversions: {
				gal: v => v * 8,
			}
		}
	],
};

class Converter {
	#unit;
	#value;
	#state;
	#parent;
	#conversions;

	/**
	 * @param {number} value
	 * @param {string} unit
	 * @param {object} conversions
	 * @param {Converter} [parent]
	 * @param {object} [state]
	 * @param {boolean} [autoDerive=false] Automatically derive values in other units
	 */
	constructor(value, unit, conversions = {}, { parent, state, autoDerive = false } = {}) {
		// console.log({ [unit]: value }, 'from', { [parent?.unit]: parent?.value }, state);
		this.#unit = unit;
		this.#value = value;
		this.#parent = parent;
		this.#state = parent?.state ?? state;
		this.#conversions = conversions;

		if (this.state) this.state[unit] = this;

		if (autoDerive) this.derive();
	}

	derive() {
		const state = this.state;
		this.#state ??= {}
		state[this.unit] = this;

		const conversions = this.conversions;
		// Auto-convert
		const convertibles = Object.keys(conversions);
		// console.log('Can convert', unit, 'to', convertibles);
		for (const sunit of convertibles) {
			if (state[sunit]) continue;
			const newValue = conversions[sunit](this.value);
			// console.log({ [sunit]: newValue }, 'from', { [unit]: value }, state);
			const fn = convert[state._category][sunit];
			try {
				fn(newValue, this);
				state[sunit].derive();
			}
			catch (err) {
				console.error(err, { category: state._category, unit: sunit });
			}
		}
	}

	get unit() { return this.#unit; }

	get value() { return this.#value; }

	get state() { return this.#state; }

	get parent() { return this.#parent; }

	get conversions() { return this.#conversions; }

	to = new Proxy(this, {
		get(obj, unit) {
			const cat = obj.state._category;
			const nv = obj.conversions[unit](obj.value);
			return convert[cat][unit](nv, obj);
		}
	});
}

const initConverters = () => ({
	init(value, unit, parent) { return this[unit](value, parent); }
});

const convert = {
	distance: initConverters(),
	mass: initConverters(),
	volume: initConverters(),
}

const addConverter = (category, key, conversions) => {
	convert[category][key] = (value, parent)=> new Converter(value, key, conversions, { parent, state: { _category: category }, autoDerive: true });
}

class MeasureConverterUI extends FormApplication {
	/** @type {MeasureConverterUI} */
	static instance;

	constructor() {
		const options = {
			top: 100,
			left: 180,
		};

		super(null, options);
		// const { x, y } = game.settings.get(CFG.id, CFG.SETTINGS.position);

		this.constructor.instance = this;

		this.values = { distance: {}, mass: {}, volume: {} };
		this.getDistances()
			.forEach(i => this.values.distance[i.key] = null);
		this.getMasses()
			.forEach(i => this.values.mass[i.key] = null);
		this.getVolumes()
			.forEach(i => this.values.volume[i.key] = null);
	}

	get title() {
		return game.i18n.localize('MeasurementConverter.Title');
	}

	static get defaultOptions() {
		const options = super.defaultOptions;
		return {
			...options,
			id: CFG.id,
			baseApplication: 'MeasureConverterUI',
			classes: [...options.classes, CFG.id],
			resizable: { resizeX: true, resizeY: false },
			height: 'auto',
		}
	}

	get template() {
		return `modules/${CFG.id}/${CFG.id}.hbs`;
	}

	getDistances() {
		return [
			{ key: 'cm', archaic: false, astro: false },
			{ key: 'in', archaic: true, astro: false },
			{ key: 'ft', archaic: true, astro: false },
			{ key: 'yd', archaic: true, astro: false },
			{ key: 'm', archaic: false, astro: false },
			{ key: 'km', archaic: false, astro: false },
			{ key: 'mi', archaic: true, astro: false, hint: true },
			{ key: 'nmi', archaic: false, astro: false, hint: true },
			{ key: 'league', archaic: true, astro: false, hint: true, noSymbol: true },
			{ key: 'au', archaic: false, astro: true },
			{ key: 'ly', archaic: false, astro: true },
			{ key: 'pc', archaic: false, astro: true },
		];
	}

	getMasses() {
		return [
			{ key: 'gr', archaic: true, astro: false },
			{ key: 'dr', archaic: true, astro: false },
			{ key: 'g', archaic: false, astro: false },
			{ key: '℥', archaic: true, astro: false },
			{ key: 'lb', archaic: true, astro: false },
			{ key: 'kg', archaic: false, astro: false },
			{ key: 't', archaic: false, astro: false },
		];
	}

	getVolumes() {
		return [
			{ key: 'dr', archaic: true, astro: false },
			{ key: 'T', archaic: true, astro: false },
			{ key: '℥', archaic: true, astro: false },
			{ key: 'cup', archaic: true, astro: false },
			{ key: 'pt', archaic: true, astro: false },
			{ key: 'L', archaic: false, astro: false },
			{ key: 'gal', archaic: true, astro: false },
			{ key: 'm3', archaic: false, astro: false, symbol: '㎥' },

		];
	}

	getData() {
		const context = {
			settings: {
				astro: game.settings.get(CFG.id, CFG.SETTINGS.astro),
				archaic: game.settings.get(CFG.id, CFG.SETTINGS.archaic),
				distance: true,
				mass: true,
				volume: true,
			},
			distance: this.getDistances(),
			mass: this.getMasses(),
			volume: this.getVolumes(),
		};

		const categories = {
			distance: 'Distance',
			mass: 'Mass',
			volume: 'Volume'
		};

		const addData = (cat) => {
			const values = this.values[cat] ?? {};
			context[cat].forEach(i => {
				i.label = `MeasurementConverter.${categories[cat]}.${i.key}`;
				i.value = values[i.key];
				i.active = values._active == i.key;
				i.show = !(!context.settings.archaic && i.archaic) && !(!context.settings.astro && i.astro);
				i.symbol = i.symbol ?? i.key;
			});
		}

		addData('distance');
		addData('mass');
		addData('volume');

		return context;
	}

	_onChange(ev) {
		const name = ev.target.name;
		const value = parseFloat(ev.target.value);

		const [category, unit] = name.split('.');
		this.values[category][unit] = value;

		if (Number.isNaN(value) || value === 0) {
			this._reset(category);
		}
		else {
			const values = this.values[category];
			values._active = unit;

			const base = convert[category].init(value, unit);

			Object.entries(base.state)
				.forEach(([sunit, cvt]) => values[sunit] = cvt.value);

			this.render();
		}
	}

	_reset(cat) {
		let values;
		if (cat === 'distance')
			values = this.values.distance;
		if (cat === 'mass')
			values = this.values.mass;
		if (cat === 'volume')
			values = this.values.volume;

		Object.keys(values).forEach(key => values[key] = null);
		delete values._active;
		this.render();
	}

	activateListeners([html]) {
		html.querySelectorAll('input')
			.forEach(el => {
				el.addEventListener('change', this._onChange.bind(this), { passive: true });
			});
	}

	_savePosition() {
		/*
		const old = game.settings.get(CFG.id, CFG.SETTINGS.position);
		game.settings.set(CFG.id, CFG.SETTINGS.position, { x, y })
			.then((rv) => {
				if (rv.x === old.x && rv.y === old.y)
					console.log(`METRIC TRAINER | Positon Not Saved: x(${x}), y(${y})`);
				else
					console.log(`METRIC TRAINER | Positon Saved: x(${rv.x}), y(${rv.y})`);
			});
			*/
	}

	reRender = foundry.utils.debounce(() => this.render(), 200);

	static render() {
		Object.values(ui.windows).forEach(app => {
			if (app instanceof MeasureConverterUI)
				app.reRender();
		});
	}
}

Hooks.once('init', registerSettings);

Hooks.once('ready', async () => {
	units.distance.forEach(m => addConverter('distance', m.key, m.conversions));
	units.mass.forEach(m => addConverter('mass', m.key, m.conversions));
	units.volume.forEach(m => addConverter('volume', m.key, m.conversions));

	// Init
	new MeasureConverterUI();

	// API
	const api = {
		open: () => MeasureConverterUI.instance.render(true, { focus: true }),
		convert,
	};

	game.modules.get(CFG.id).api = api;

	if (game.settings.get(CFG.id, CFG.SETTINGS.autoOpen))
		api.open();
});
