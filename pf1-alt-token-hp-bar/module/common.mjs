export const CFG = {
	module: 'mkah-pf1-alt-token-hp-bar',
	SETTINGS: {
		effectiveColor: 'effectiveColor',
		doubleSided: 'doubleSided',
		colorBlind: 'colorBlind',
		colors: 'color',
		dyingType: 'dyingType',
		slimBars: 'slimBars',
		markers: 'markers',
		labels: 'labels'
	},
	colors: {
		damage: 0x530404, // 0x770000, // 0xFF0000,
		healing: 0x00FF00,
		nonlethal: 0xff29db,
		temp: 0x66CCFF,
		tempmax: 0x440066, // health capacity with temporary HP
		negmax: 0x550000,
		death: 0xFF0000,
		markers: {
			h: 0xFFFFFF, // 0x77DD99,
			q: 0xFFFFFF,
			e: 0xFFFFFF,
		}
	}
};

export const maxPrecision = function (num, decimalPlaces = 0, type = 'round') {
	const p = Math.pow(10, decimalPlaces || 0),
		n = num * p * (1 + Number.EPSILON);
	return Math[type](n) / p;
};
