import { CFG } from './common.mjs';

/**
 * @param {PIXI.Graphics} bar
 * @param {Number} markers
 */
export const drawMarkers = (bar, markers, { w, h, vO } = {}) => {
	const v1 = h + vO;
	// Half mark
	const h0 = w / 2;
	const colors = CFG.colors.markers;
	bar.lineStyle(1, colors.h, 0.6).moveTo(h0, vO).lineTo(h0, v1);
	// Quarter
	if (markers > 1) {
		const h1 = w / 4,
			h2 = w - h1;
		bar.lineStyle(1, colors.q, 0.4)
			.moveTo(h1, vO).lineTo(h1, v1)
			.moveTo(h2, vO).lineTo(h2, v1);
	}
	// Eight
	if (markers > 2) {
		const h1 = w / 8,
			h2 = w - h1;
		const h3 = h1 * 3,
			h4 = w - h3;
		bar.lineStyle(1, colors.e, 0.2)
			.moveTo(h1, vO).lineTo(h1, v1)
			.moveTo(h2, vO).lineTo(h2, v1)
			.moveTo(h3, vO).lineTo(h3, v1)
			.moveTo(h4, vO).lineTo(h4, v1);
	}
}
