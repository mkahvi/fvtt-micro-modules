import { CFG } from './common.mjs';

Hooks.once('init', () => {
	game.settings.register(CFG.module, CFG.SETTINGS.slimBars, {
		name: 'Slim bars',
		hint: 'Display slimmer resource bars.',
		type: Boolean,
		default: true,
		scope: 'client',
		config: true,
	});

	game.settings.register(CFG.module, CFG.SETTINGS.effectiveColor, {
		name: 'Color by effective health',
		hint: 'Use effective HP (temp + current) instead of current HP for coloring the HP bar.',
		type: Boolean,
		default: false,
		scope: 'client',
		config: true,
	});

	game.settings.register(CFG.module, CFG.SETTINGS.doubleSided, {
		name: 'Double-sided dying meter',
		hint: 'Dying meter extends from both sides towards middle, instead of from right to left.',
		type: Boolean,
		default: true,
		scope: 'client',
		config: true,
	});

	game.settings.register(CFG.module, CFG.SETTINGS.colors, {
		default: {},
		scope: 'client',
		config: false,
	});

	/*
	game.settings.registerMenu(CFG.module, CFG.SETTINGS.colors, {
		id: `${CFG.module}-color-config`,
		label: 'Color configuration',
		hint: 'Customize colors.',
		icon: 'fas fa-palette',
		type: ColorEditor,
		restricted: false,
	});
	*/

	game.settings.register(CFG.module, CFG.SETTINGS.dyingType, {
		name: 'Dying display type',
		hint: 'How to display dying meter. Affects all users. Double sided dying meter is disabled with unbound versions.',
		type: Number,
		choices: {
			0: 'Negative Ability score',
			1: 'Negative Ability score (unbound)',
			2: 'Negative max HP',
			3: 'Negative max HP (unbound)',
		},
		default: 0,
		scope: 'world',
		config: true,
	});

	game.settings.register(CFG.module, CFG.SETTINGS.markers, {
		name: 'Markers',
		hint: 'Display additional markers.',
		type: Number,
		choices: {
			0: 'None',
			1: 'Half',
			2: 'Quarter',
			3: 'Eight',
		},
		default: 1,
		scope: 'world',
		config: true,
	});

	game.settings.register(CFG.module, CFG.SETTINGS.labels, {
		name: 'Labels',
		hint: 'Display numeric labels for actors you have at least limited permission to.',
		type: Boolean,
		default: true,
		scope: 'client',
		config: true,
	});

	// TODO: Configure style (cur+temp vs effective)
	// TODO: Configure size
});
