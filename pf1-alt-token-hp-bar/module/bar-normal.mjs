import { CFG } from './common.mjs';
import { drawMarkers } from './markers.mjs';
import { drawLabels } from './labels.mjs';

/**
 * @param {number} number      The Bar number
 * @param {PIXI.Graphics} bar  The Bar container
 * @param {object} data        Resource data for this bar
 * @this {Token}
 */
export function drawOrdinaryBar(number, bar, data) {
	bar.clear(); // Clear drawings
	bar.removeChildren(); // Remove text labels

	// console.log(number, bar, data);
	const slim = game.settings.get(CFG.module, CFG.SETTINGS.slimBars),
		barScale0s = slim ? 24 : 12,
		barScale1m = slim ? 4 : 8;

	const clamp = Math.clamp ?? Math.clamped;

	const val = Number(data.value);
	const pct = clamp(val, 0, data.max) / data.max;

	// Determine sizing
	let h = Math.max(canvas.dimensions.size / barScale0s, barScale1m);
	const hO = 2,
		vO = number === 0 ? -2 : 2,
		w = this.w - hO * 2;
	const bs = clamp(h / 8, 1, 2);
	if (this.height >= 2) h *= 1.6; // Enlarge the bar for large tokens

	// Determine the color to use
	const blk = 0x000000;
	let color;
	if (number === 0) color = Color.fromRGBvalues(1 - pct / 2, pct, 0);
	else color = Color.fromRGBvalues(0.5 * pct, 0.7 * pct, 0.5 + pct / 2);

	// Draw the bar
	bar.beginFill(blk, 0.5).lineStyle(bs, blk, 1.0).drawRoundedRect(hO, vO, w, h, 3);
	bar.beginFill(color, 1.0).lineStyle(bs, blk, 1.0).drawRoundedRect(hO, vO, pct * w, h, 2);

	const markers = game.settings.get(CFG.module, CFG.SETTINGS.markers);
	if (markers > 0) drawMarkers(bar, markers, { w, h, vO });

	if (game.settings.get(CFG.module, CFG.SETTINGS.labels)) {
		const actor = this.document.actor;
		const levels = CONST.DOCUMENT_OWNERSHIP_LEVELS ?? CONST.DOCUMENT_PERMISSION_LEVELS;
		if (actor.testUserPermission(game.user, levels.LIMITED))
			drawLabels(bar, data, { slim, number });
	}

	// Set position
	const posY = number === 0 ? this.h - h : 0;
	bar.position.set(0, posY);
}
