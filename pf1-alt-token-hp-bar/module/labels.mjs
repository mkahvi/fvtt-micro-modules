import { getTextStyle } from './text.mjs';

/**
 *
 * @param {PIXI.Graphics} bar
 * @param {object} config
 * @param {number} config.value Current actual health
 * @param {number} config.temp Temporary HP
 * @param {number} config.cur Effective health
 * @param {number} config.max Max effective health
 * @param {object} options
 * @param {boolean} options.slim
 */
export const drawLabels = (bar, config, options) => {
	const { value, temp, cur, max } = config;

	const style = getTextStyle(options)
	const fontSize = style.fontSize;
	const isTop = options.number === 1;
	// Vertical offset (positive goes downward)
	const yOff = isTop ? 2 * fontSize - 1 : -1;

	// Left
	const curLabel = temp > 0 ? `${value}+${temp}` : `${value}`;
	const curT = new PreciseText(curLabel, style);
	curT.anchor.set(0.5, 1);
	const halfC = curT.width / 2;
	curT.x = halfC + 2;
	curT.y = yOff;
	bar.addChild(curT);

	// Right
	const maxT = new PreciseText(`${max}`, style);
	maxT.anchor.set(0.5, 1);
	const halfM = maxT.width / 2;
	maxT.x = bar.width - halfM;
	maxT.y = yOff;
	bar.addChild(maxT);
}
