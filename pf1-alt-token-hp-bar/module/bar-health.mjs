import { CFG } from './common.mjs';
import { drawMarkers } from './markers.mjs';
import { drawLabels } from './labels.mjs';

/**
 * Originally adapted from DND5e
 *
 * @ref https://gitlab.com/foundrynet/dnd5e/-/blob/1db8dfe5b8126ba2efb98f5daff3932a0bd94f1a/module/token.js#L65-138
 *
 * @param {number} number      The Bar number
 * @param {PIXI.Graphics} bar  The Bar container
 * @param {object} _data       Resource data for this bar
 * @this {Token}
 */
export function drawHealthBar(number, bar, _data) {
	bar.clear(); // Remove drawings
	bar.removeChildren(); // Remove text labels

	// Extract health data
	const actor = this.document.actor;
	const { abilities, attributes } = actor.system;
	if (!abilities || !attributes) return;

	/**
	 * @type {{value: number, max: number, nonlethal: number, temp: number}}
	 */
	const { value, max, nonlethal, temp } = attributes.hp;

	// Make HP properly visible at low health; rendering bug caused by (rounded?) border
	/*
	const haveHP = value > 0, haveNL = nonlethal > 0, haveTemp = temp > 0;
	if (haveHP) value++;
	if (haveNL) nonlethal++;
	if (haveHP || haveNL || haveTemp) max++; // Adjust max also to make sure the bar doesn't act stupid
	*/

	// Differentiate between effective maximum and displayed maximum
	// if (effective > 0 && value < 0) temp++;

	const clamp = Math.clamp ?? Math.clamped; // v12 changed the function

	const effective = value + temp,
		effectiveMax = Math.max(effective, max),
		tempmax = max + Math.max(0, temp),
		// Allocate percentages of the total
		maxPct = max / effectiveMax, // Normal max health percentage
		tempPct = clamp(temp, 0, effectiveMax) / effectiveMax, // Temp HP percentage
		nlPct = clamp(nonlethal, 0, effectiveMax) / effectiveMax, // Nonlethal percentage
		valuePct = clamp(value, 0, effectiveMax) / effectiveMax, // Current HP percentage
		// Determine colors to use
		effectiveColor = game.settings.get(CFG.module, CFG.SETTINGS.effectiveColor),
		colorPct = effectiveColor ? clamp(effective, 0, effectiveMax) / effectiveMax : clamp(value, 0, max) / max, //
		blk = 0x000000,
		color = CFG.colors,
		// Size scaling
		slim = game.settings.get(CFG.module, CFG.SETTINGS.slimBars),
		barScale0s = slim ? 24 : 12,
		barScale1m = slim ? 4 : 8;

	// Determine the container size (logic borrowed from core)
	const hO = 2,
		vO = -2,
		w = this.w - hO * 2;
	let h = Math.max(canvas.dimensions.size / barScale0s, barScale1m);
	if (this.height >= 2) h *= 1.6;
	const bs = clamp(h / 8, 1, 2); // What is this? Scales from 1 to 2 based on height.
	const bs1 = bs + 1; // Some kind of padding size

	// Overall bar container
	bar.clear();
	bar.removeChildren();
	bar.beginFill(blk, 0.5)
		.lineStyle(bs, blk, 1.0)
		.drawRoundedRect(hO, vO, w, h, 2);

	// Extra max health due to temp HP
	if (tempmax > 0) {
		// const pct = max / effectiveMax;
		bar.beginFill(color.tempmax, 1.0)
			// .lineStyle(1, blk, 1.0)
			.lineStyle(1, blk, 1.0)
			.drawRoundedRect(hO, vO, w, h, 2);
		// .drawRoundedRect(pct*w, 0, (1-pct)*w, h, 2);
	}
	// Maximum HP penalty
	else if (tempmax < 0) {
		// TODO: Negative levels and Con drain and similar
		/*
		const pct = (max + tempmax) / max;
		bar.beginFill(c.negmax, 1.0).lineStyle(1, blk, 1.0).drawRoundedRect(pct * w, 0, (1 - pct) * w, h, 2);
		*/
	}

	// Damage
	if (value < max) {
		bar.beginFill(color.damage, 1.0)
			.lineStyle(bs, blk, 1.0)
			.drawRoundedRect(hO, vO, maxPct * w, h, 2);
	}

	// Health bar
	if (value > 0) {
		// Normal health
		const hpColor = effective > nonlethal ? Color.fromRGBvalues(1 - colorPct / 2, colorPct, 0) : 0x775555;
		// TODO: Different color if below NL

		bar.beginFill(hpColor, 1.0)
			.lineStyle(bs, blk, 1.0)
			.drawRoundedRect(hO, vO, valuePct * w, h, 2);
	}

	if (nonlethal > 0 && effective > 0) {
		bar.beginFill(color.nonlethal, 0.3)
			.lineStyle(0)
			.drawRoundedRect(hO, vO, nlPct * w, h, 0);
	}

	// Temporary hit points
	if (temp > 0) {
		const height = clamp(h / 2, 2, 4),
			halfHeight = height / 2,
			middle = h / 2 - halfHeight + vO;
		bar.beginFill(color.temp, 1.0)
			.lineStyle(0)
			.drawRect(valuePct * w + 0.5 + hO, middle, tempPct * w - bs1, height);
	}

	if (effective < 0) {
		// Alternate bar when dying
		const type = game.settings.get(CFG.module, CFG.SETTINGS.dyingType) ?? 0;

		const getDeathThreshold = () => {
			switch (type) {
				// Max HP
				case 3:
				case 2: return max;
				// Con score
				default:
				case 1:
				case 0: return abilities[attributes.hpAbility]?.total ?? 0;
			}
		};

		const deathThreshold = getDeathThreshold(type);

		const getDeathValue = () => {
			switch (type) {
				// Unbound
				case 3:
				case 1: return Math.abs(value);
				// Bound
				default:
				case 2:
				case 0: return clamp(Math.abs(value), 0, deathThreshold);
			}
		};

		const deathPct = getDeathValue() / deathThreshold,
			doubleSided = [0, 2].includes(type) ? game.settings.get(CFG.module, CFG.SETTINGS.doubleSided) : false,
			height = clamp(h / 2, 1, 2),
			halfHeight = height / 2,
			middle = h / 2 - halfHeight + vO,
			deathPctActual = deathPct * w;

		if (doubleSided) {
			// Left to right
			bar.beginFill(color.death, 1.0)
				.lineStyle(0)
				.drawRect(bs1 + hO, middle, deathPctActual / 2 - bs1, height);
			// Right to left
			bar.beginFill(color.death, 1.0)
				.lineStyle(0)
				.drawRect(w - bs1 + hO, middle, -deathPctActual / 2 + bs1, height);
		}
		else {
			// Right to left only
			bar.beginFill(color.death, 1.0)
				.lineStyle(0)
				.drawRect(w - bs1 + hO, middle, -deathPctActual + 2 * bs1, height);
		}
	}

	const markers = game.settings.get(CFG.module, CFG.SETTINGS.markers);
	if (markers > 0) drawMarkers(bar, markers, { w, h, vO });

	const labelConfig = {
		value,
		temp,
		cur: effective,
		max: effectiveMax,
	};

	if (game.settings.get(CFG.module, CFG.SETTINGS.labels)) {
		const levels = CONST.DOCUMENT_OWNERSHIP_LEVELS ?? CONST.DOCUMENT_PERMISSION_LEVELS;
		if (actor.testUserPermission(game.user, levels.LIMITED))
			drawLabels(bar, labelConfig, { slim, number });
	}

	// Set position
	const posY = number === 0 ? this.h - h : 0;
	bar.position.set(0, posY);
}
