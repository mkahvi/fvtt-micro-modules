export const getTextStyle = ({ slim, sharpen, size = 12 } = {}) => {
	return new PIXI.TextStyle({
		fontFamily: 'Signika',
		fontSize: size,
		fontVariant: 'small-caps',
		fill: '#FFFFFF',
		stroke: '#111111',
		strokeThickness: 1,
		dropShadow: false,
		// dropShadowColor: '#000000',
		// dropShadowBlur: 1,
		// dropShadowAngle: 0,
		// dropShadowDistance: 0,
		align: 'center',
		wordWrap: false,
		padding: 1,
	});
}
