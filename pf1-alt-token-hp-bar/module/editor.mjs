import { CFG, maxPrecision } from './common.mjs';

const simplifyArray = (a) => a.map(i => maxPrecision(i, 2));

export class ColorEditor extends FormApplication {
	constructor() {
		super();
	}

	get template() {
		return 'modules/mkah-pf1-alt-token-hp-bar/editor.hbs';
	}

	static get defaultOptions() {
		const _defaults = super.defaultOptions;
		return foundry.utils.mergeObject(_defaults, {
			title: 'Token HP Bar Color Editor',
			classes: [..._defaults.classes, 'mkah-pf1-alt-token-hp-bar'],
		});
	}

	/**
	 * @param {Event} event
	 * @param {object} formData
	 */
	_updateObject(event, formData) {

	}

	/**
	 * @param {JQuery} jq
	 */
	activateListeners(jq) {
		const html = jq[0];
		const ed = html.querySelector('.config');

		const colors = {
			background: '#000000',
			healthy: '#00FF00',
			bloodied: '#ff29db',
			wounded: '#ff0000',
			scaled: null,
			missing: '#610505',
			temp: '#14b1ff',
			dying: '#FF0000',
		};

		function updateHealthScaling() {
			html.style.setProperty('--alt-hp-scaled', '#005577');
			const hiC = PIXI.utils.hex2rgb(PIXI.utils.string2hex(colors.healthy)),
				loC = PIXI.utils.hex2rgb(PIXI.utils.string2hex(colors.wounded)),
				midC = PIXI.utils.hex2rgb(PIXI.utils.string2hex(colors.bloodied));

			function quad(t, b, c, d) {
				if ((t /= d / 2) < 1) return c / 2 * t * t + b;
				return -c / 2 * (--t * (t - 2) - 1) + b;
			}

			console.log('- - - - -');

			/**
			 * @param {number} s Start
			 * @param {number} e End
			 * @param {number} p0 Percent
			 * @param weighing
			 */
			function linear(p0, s, e, weighing = 1) {
				const p1 = Math.clamped((p0 - 0.5) * 1.1 + 0.5, 0, 1);
				// p *= weighing;
				console.log('Percent: ', p0, p1);
				return e + p1 * (s - e);
			}

			// Simple color scaler
			function colorScale(rc0, mc0, lc0, p0, fun) {
				const [R0, G0, B0] = p0 > 0.5 ? rc0 : mc0,
					[R1, G1, B1] = p0 > 0.5 ? mc0 : lc0;

				const p1 = p0 > 0.5 ? 1 - (1 - p0) * 2 : p0 * 2;
				console.log(maxPrecision(p0, 3), '->', maxPrecision(p1, 3));

				const Rd = fun(p1, R0, R1),
					Gd = fun(p1, G0, G1),
					Bd = fun(p1, B0, B1);

				console.log(simplifyArray([Rd, Gd, Bd]));
				return [Rd, Gd, Bd];
			}

			console.log('HI:', hiC);
			console.log('LO:', loC);

			const full = hiC,
				high = colorScale(hiC, midC, loC, 0.72, linear),
				mid = colorScale(hiC, midC, loC, 0.45, linear),
				low = colorScale(hiC, midC, loC, 0.17, linear);

			console.log(simplifyArray(full));
			console.log(simplifyArray(high));
			console.log(simplifyArray(mid));
			console.log(simplifyArray(low));

			html.style.setProperty('--alt-hp-full', PIXI.utils.hex2string(Color.fromRGBvalues(...full)));
			html.style.setProperty('--alt-hp-high', PIXI.utils.hex2string(Color.fromRGBvalues(...high)));
			html.style.setProperty('--alt-hp-mid', PIXI.utils.hex2string(Color.fromRGBvalues(...mid)));
			html.style.setProperty('--alt-hp-low', PIXI.utils.hex2string(Color.fromRGBvalues(...low)));
		}

		function updateInputs(events) {
			const type = this.type,
				name = this.name,
				value = this.value,
				input = ed.querySelector(`input[name="${name}"][type="${type === 'text' ? 'color' : 'text'}"]`);

			input.value = value;
			html.style.setProperty(`--alt-hp-${name}`, value);
			colors[name] = value;

			if (['healthy', 'wounded', 'bloodied'].includes(name))
				updateHealthScaling();
		}

		for (const input of ed.querySelectorAll('input')) {
			const type = input.type;
			const name = input.name;
			const color = colors[name];
			if (color) input.value = color;
			input.addEventListener('change', updateInputs);
			if (type === 'text') html.style.setProperty(`--alt-hp-${name}`, color);
		}

		updateHealthScaling();
	}
}
