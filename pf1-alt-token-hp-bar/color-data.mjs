export class RGBColor8 {
	red;
	green;
	blue;
	constructor({ red, green, blue } = {}) {
		this.red = red;
		this.green = green;
		this.blue = blue;
	}
}

/**
 * Converts 8bits RGB color to CMYK
 * @param {Object} color
 * @param {Object} color.red
 * @param {Object} color.green
 * @param {Object} color.blue
 */
export function RGBtoCMYK(color) {
	const { red, green, blue } = color;

	const R = red / 255,
		G = green / 255,
		B = blue / 255,
		K = 1 - Math.max(R, G, B),
		iK = 1 - K,
		C = (1 - R - K) / iK,
		M = (1 - G - K) / iK,
		Y = (1 - B - K) / iK;

	return { cyan: C, magenta: M, yellow: Y, black: K };
}

/**
 * Converts floating point CMYK color to RGB
 * @param {Object} color
 * @param {Object} color.red
 * @param {Object} color.green
 * @param {Object} color.blue
 * @param {Number} depth Depth of color used. 255 for 8 bit colors.
 */
export function CMYKtoRGB(color, depth = 255) {
	const { cyan: C, magenta: M, yellow: Y, black: K } = color;
	return new RGBColor8({ red: depth * (1 - C) * (1 - K), green: depth * (1 - M) * (1 - K), blue: depth * (1 - Y) * (1 - K) });
}

export class ColorData {
	dying;
	healthy;
	wounded;
	temp;
	missing;
	constuctor({ dying, healthy, temp } = {}) {
		this.dying = RGBColor8(dying);
		this.health = RGBColor8(healthy);
		this.temp = RGBColor8(temp);
	}
}
