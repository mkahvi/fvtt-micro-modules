// TODO:
// - Something when under 0 HP
// - Negative levels
// - Con drain and similar effects

import { CFG } from './module/common.mjs';
// import { ColorEditor } from './editor.mjs';
import './module/settings.mjs';

import { drawHealthBar } from './module/bar-health.mjs';
import { drawOrdinaryBar } from './module/bar-normal.mjs';

const overrideTokenBars = () => {
	// const originalDrawBar = CONFIG.Token.objectClass.prototype._drawBar;

	CONFIG.Token.objectClass.prototype._drawHPBar = drawHealthBar;
	CONFIG.Token.objectClass.prototype._drawOtherBar = drawOrdinaryBar;

	CONFIG.Token.objectClass.prototype._drawBar = function _drawBarOverride(number, bar, data) {
		if (!this.document.actor) return;
		if (data.attribute === 'attributes.hp') return this._drawHPBar(number, bar, data);
		return this._drawOtherBar(number, bar, data);
	};
};

// Hooks.once('pf1.postInit', overrideTokenBars); // 0.81.3 and before
Hooks.once('pf1PostInit', overrideTokenBars); // 0.82.0 and onward
