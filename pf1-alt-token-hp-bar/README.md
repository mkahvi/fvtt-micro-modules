# Alt Token Health (HP) Bar for Pathfinder 1

![Supported Foundry Versions](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fversion%3Fstyle%3Dflat%26url%3Dhttps%3A%2F%2Fgitlab.com%2Fmkahvi%2Ffvtt-micro-modules%2F-%2Fraw%2Fmaster%2Fpf1-alt-token-hp-bar%2Fmodule.json)

Alters token health bar to display temporary hit points and nonlethal damage.

- Temporary hit points are rendered as separate minibar after the normal health.
- Nonlethal damage is drawn as a tint over normal health.
  - Normal health is drawn differently if effective health is less than nonlethal damage, signifying loss of consciousness.
- When below 0 effective health, the bar is replaced with dying meter, extending from right to left.

## Flaws

- This is ignorant of special rules where dying threshold is altered as there are no clear hints on the actors currently for them (as of PF1 0.80.9).
  - For example mythic hard to kill feature, undead, constructs, etc.
    - Undead and constructs are simple enough to consider however, so this part of the flaw is negligible.
- Color blindness has not been considered sufficiently.
- Renders extremely poorly if grid resolution is much below 100 pixels.

## Screencaps

Damaged with some temporary hit points:  
![Damaged](./screencaps/damaged.png)  
~20 hp, ~10 THP, out of 110 HP

Slightly damaged with massive THP pool:  
![Overbuffer](./screencaps/overbuffer.png)  
~100 HP out of ~110 HP with ~50 THP

Dying:  
![Dying](./screencaps/dying.png)  
-5 HP with 12 Con

Optional double-sided dying:  
![Double-sided dying](./screencaps/double-sided-dying.png)  
Same as above

## Configuration

### Slim Bars

Display bars significantly smaller than normal, making them much more compact, sleek, aerodynamic, and maybe save power by not turning as many pixels different color? None of these qualities are guaranteed. I just like smaller bars.

### Color by Effective Health

Colors based on effective health (temporary + actual) instead of actual health.

### Double-sided Dying Meter

Starts the dying meter from both left and right, converging into the middle. Gives perhaps better idea of when someone is close to final death, or not.

### Dying Display Type

- Negative Con score
- Negative Con score (unbound)
- Negative Max HP
- Negative Max HP (unbound)

Negative Con score gives easy tracking of when someone dies.

Negative Max HP obfuscates the point of death.

Unbound versions let the dying meter escape bounds of the health bar. This was added due to a request of it being comical.

### Markers

Adds little blips to 50%, 25%, or 12.5% points on the bars.

## Install

Manifest URL: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/master/pf1-alt-token-hp-bar/module.json>

Foundry v9 install manually via: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/blob/d7a230a3eb9494c493a6f03bc739286a0cd981f9/pf1-alt-token-hp-bar/pf1-alt-token-hp-bar.zip>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT license](./LICENSE)
