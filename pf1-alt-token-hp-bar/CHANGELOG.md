# Change Log

## 0.5.0.1

- Fix for bad compat update

## 0.5.0

- New: Foundry v12 compatibility.
- Fix: Better handling of tokens with no actors

## 0.4.1

- Change: Remove deprecation warnings about rgb2hex from Pixi.

## 0.4.0.3

- Fix: Fix serious memory leak in text label drawing.
- Fix: Text labels would behave strangely and leave graphics artefacts when tokens changed size.
- Fix: Draw labels setting was not respected.
- Fix: Non-HP labels were shown to all.

## 0.4.0

- New: Optional text labels.

## 0.3.0

- Foundry v10 and PF1 0.82.2 compatibility

## 0.2.1

- Bundling via esbuild

## 0.2.0

- New: Slim bars
- New: Death threshold type configuration.
- New: Optional half, quarter, and eight markers. Default half marker is enabled.
- New: Nonlethal damage display as tinted overlay.
  - Normal health turns different color once NL is equal to effective health (staggered or worse).
- New: Optional doublesided dying meter.

## 0.1.0 Initial release
