const CFG = {
	id: 'mana-dev-tools',
};

/**
 * @param {string} node
 * @param {string} content
 * @param {string[]} classes
 * @param {object} options
 * @param {boolean} options.isHTML
 * @returns {Element}
 */
const createNode = (node, content, classes = [], { isHTML = false } = {}) => {
	const d = document.createElement(node);
	if (content) {
		if (isHTML) d.innerHTML = content;
		else d.textContent = content;
	}
	if (classes.length) d.classList.add(...classes);
	return d;
};

class DevMenu {
	/**	@type {Element} */
	element;

	static instance;

	constructor() {
		this.render();
	}

	render() {
		// Constant Access Icon
		const d = createNode('div');
		d.id = 'mana-dev-tools';
		const i = createNode('i', null, ['fa-solid', 'fa-screwdriver-wrench', 'icon']);
		d.append(i);

		const menu = createNode('ul', null, ['menu', 'window']);

		const menuSc = createNode('ul', null, ['menu', 'scripts']);
		const init = createNode('li', 'Init', ['entry', 'script']);
		init.dataset.type = 'script';
		init.dataset.script = 'init';
		const setup = createNode('li', 'Setup', ['entry', 'script']);
		setup.dataset.type = 'script';
		setup.dataset.script = 'setup';
		const ready = createNode('li', 'Ready', ['entry', 'script']);
		ready.dataset.type = 'script';
		ready.dataset.script = 'ready';
		menuSc.append(init, setup, ready);
		menu.append(menuSc);
		menuSc.before(createNode('h4', 'Scripts'));

		const menuO = createNode('ul', null, ['menu', 'other']);
		const css = createNode('li', 'CSS', ['entry', 'other', 'css']);
		css.dataset.type = 'app';
		css.dataset.app = 'css';
		menuO.append(css);
		menu.append(menuO);
		menuO.before(createNode('h4', 'Other'));
		d.append(menu);

		document.body.append(d);
		this.element = d;
		this.constructor.instance = this;

		this.activateListeners();
	}

	/**
	 * @param {Event} event
	 */
	_onAction(event) {
		const el = event.target;
		switch (el.dataset.type) {
			case 'script': {
				new ScriptEditor(el.dataset.script).render(true, { focus: true });
				break;
			}
			case 'app': {
				this._onOpenApp(event, el.dataset.app);
			}
		}
	}

	_onOpenApp(event, appId) {
		switch (appId) {
			case 'css':
				new CSSEditor().render(true, { focus: true });
				break;
		}
	}

	activateListeners() {
		const el = this.element;

		el.querySelectorAll('li.entry')
			.forEach(sel => sel.addEventListener('click', this._onAction.bind(this)));
	}
}

class ScriptEditor extends FormApplication {
	type;
	script;

	static instance = { init: null, ready: null, setup: null };

	constructor(type) {
		super();

		this.type = type;
	}

	get title() {
		return `Dev Tools Script: ${this.type}`;
	}

	get id() {
		return `dev-tools-script-editor-${this.type}`;
	}

	static get defaultOptions() {
		const options = super.defaultOptions;
		return {
			...options,
			template: 'modules/mana-dev-tools/text-editor.hbs',
			width: 540,
			height: 380,
			resizable: true,
			dragDrop: [{ dragSelector: null, dropSelector: 'textarea' }],
			classes: [...options.classes, 'mana-dev-tools', 'script-editor'],
			submitOnClose: false,
			closeOnSubmit: false,
			submitOnChange: false,
		};
	}

	getData() {
		return {
			text: game.settings.get(CFG.id, 'scripts')[this.type],
		};
	}

	_canDragStart() {
		return false;
	}

	_canDragDrop(selector) {
		return true;
	}

	_onDrop(event) {
		const dropData = TextEditor.getDragEventData(event);

		const el = this.form.querySelector('textarea'),
			start = el.selectionStart,
			end = el.selectionEnd;

		const paste = (text) => {
			const len = text.length;
			el.value = el.value.slice(0, el.selectionStart) + text + el.value.slice(el.selectionStart);
			// Shift selection
			el.setSelectionRange(start + len, end + len);
		};

		switch (dropData.type) {
			case 'Item':
			case 'Actor':
				paste(`Hotbar.toggleDocumentSheet("${dropData.uuid}");\n`);
				break;
			default:
				if (dropData.uuid)
					paste(`${dropData.uuid}\n`);
				break;
		}
	}

	_textInputHandler(ev) {
		const el = ev.target;
		switch (ev.key) {
			case 'Tab': {
				ev.preventDefault();
				ev.stopPropagation();

				const start = el.selectionStart,
					end = el.selectionEnd;

				// Add
				if (!ev.shiftKey) {
					el.value = el.value.slice(0, el.selectionStart) + '\t' + el.value.slice(el.selectionStart);
					// Shift selection
					el.setSelectionRange(start + 1, end + 1);
				}
				// Remove
				else {
					const pos = el.value.lastIndexOf('\t', el.selectionStart);
					if (pos >= 0) {
						el.value = el.value.slice(0, pos) + el.value.slice(pos + 1);
						// Shift selection
						el.setSelectionRange(start - 1, end - 1);
					}
				}
				return false;
			}
		}
	}

	_onSave(event) {
		event.preventDefault();

		const data = new FormDataExtended(this.form).object;

		const scripts = foundry.utils.deepClone(game.settings.get(CFG.id, 'scripts'));
		scripts[this.type] = data.script;
		game.settings.set(CFG.id, 'scripts', scripts);

		this.close();
	}

	activateListeners(jq) {
		super.activateListeners(jq);

		this.form.querySelector('textarea')
			.addEventListener('keydown', this._textInputHandler.bind(this));

		this.form.querySelector('button.save')
			.addEventListener('click', this._onSave.bind(this));
	}
}

class CSSEditor extends ScriptEditor {
	get title() {
		return 'Dev Tools CSS';
	}

	get id() {
		return 'dev-tools-css-editor';
	}

	getData() {
		return {
			text: game.settings.get(CFG.id, 'css'),
		};
	}

	_onDrop(event) {
		// TODO: Paste as raw text
	}

	_onSave(event) {
		event.preventDefault();

		const { text } = new FormDataExtended(this.form).object;

		game.settings.set(CFG.id, 'css', text);

		this.close();
	}

	static initStyle(css) {
		let cssEl = document.getElementById('dev-tools-css-entry');
		if (!cssEl) {
			cssEl = document.createElement('style');
			cssEl.type = 'text/css';
			cssEl.id = 'dev-tools-css-entry';
			document.head.append(cssEl);
		}
		cssEl.innerText = css;
	}
}

function execScript(trigger) {
	const script = game.settings.get(CFG.id, 'scripts')[trigger];
	if (!script) return;

	console.log(`MDT | Executing script: ${trigger}\n`, script);

	new Function(`{\n${script}\n}`).call();
}

const missingTranslations = new Set();

function i18nLocalize(wrapped, path, opts) {
	const value = wrapped(path, opts);
	if (value === path) {
		if (!missingTranslations.has(path)) {
			// Ignore translations with spaces or unexpected punctuation and ending punctionation
			// Likely pre-translated things or user inputs that are passed to localize
			if (!/[\s!?]/.test(path) && !/[!?.]$/.test(path)) {
				missingTranslations.add(path);
				console.warn('MDT | Missing translation:', path);
			}
		}
	}
	return value;
}

function onInit() {
	/* global libWrapper */
	libWrapper.register(CFG.id, 'Localization.prototype.localize', i18nLocalize, 'WRAPPER');
	libWrapper.register(CFG.id, 'Localization.prototype.format', i18nLocalize, 'WRAPPER');

	game.settings.register(CFG.id, 'scripts', {
		type: Object,
		default: { init: '', setup: '', ready: '' },
		scope: 'world',
		config: false,
	});

	game.settings.register(CFG.id, 'css', {
		type: String,
		default: '',
		scope: 'world',
		config: false,
		onChange: (css) => CSSEditor.initStyle(css),
	});

	CSSEditor.initStyle(game.settings.get(CFG.id, 'css'));

	execScript('init');
}

function onSetup() {
	execScript('setup');
}

function onReady() {
	new DevMenu();

	execScript('ready');
}

Hooks.once('init', onInit);
Hooks.once('setup', onSetup);
Hooks.once('ready', onReady);
