# Mana's Dev Tools

![Supported Foundry Versions](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fversion%3Fstyle%3Dflat%26url%3Dhttps%3A%2F%2Fgitlab.com%2Fmkahvi%2Ffvtt-micro-modules%2F-%2Fraw%2Fmaster%2Fdev-tools%2Fmodule.json)

Some simple tools for helping with development.

## Usage

Top-left anvil logo has a little icon overlayed it via which you can access the functionality.

## Features

- Init, Setup and Ready scripts.
  - Simple method to add arbitrary code to run at the various stages that does not infect anything you release.
  - Actor & Item drag&drop can be used for quick creation of sheet toggle code.
    - Other things get their UUID pasted.

## Documentation

### Scripts

The scripts are executed synchronously, so no top-level await is possible. This is to ensure they finish executing during the hook handler's execution time and not some arbitrary time after by default.

## Install

Manual only.

<!-- Manifest URL: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/master/dev-tools/module.json> -->

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
