function actorHasProficiencyForSure() {
	return true;
}

function overrideActorProficiency(cls) {
	cls.prototype.hasArmorProficiency = actorHasProficiencyForSure;
	cls.prototype.hasWeaponProficiency = actorHasProficiencyForSure;
}

function overrideItemProficiency(cls) {
	const realPrepareBaseData = cls.prototype.prepareBaseData;
	cls.prototype.prepareBaseData = function itemHasProficiencyForSure() {
		this.system.proficient = true;
		return realPrepareBaseData.call(this);
	};
}

const postInit = () => {
	overrideActorProficiency(pf1.documents.actor.ActorPF);
	overrideItemProficiency(pf1.documents.item.ItemPF);

	overrideActorProficiency(pf1.documents.actor.ActorNPCPF);
	overrideActorProficiency(pf1.documents.actor.ActorCharacterPF);
};

Hooks.once('pf1.postInit', postInit); // 0.81.3 and before
// Hooks.once('pf1PostInit', postInit); // 0.82.0 and onward

function inMemoryProficiencyOverride(item) {
	// Alter only in memory, no point updating the underlying data
	item.system.proficient = true;
}

/**
 * @param {ItemSheet} sheet
 * @param {JQuery} html
 */
function disableProficientToggle(sheet, [html]) {
	const prof = html.querySelector('input[name="system.proficient"]');
	if (prof) {
		prof.disabled = true;
		prof.checked = true;
	}
}

Hooks.on('updateItem', inMemoryProficiencyOverride);
Hooks.on('renderItemSheet', disableProficientToggle);
