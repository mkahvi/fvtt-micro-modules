const CFG = {
	id: 'pf1-rest-end',
	SETTINGS: {
		defaultEnabled: 'defaultEnabled',
	},
};

const conditions = {
	fatigued: {
		label: 'PF1.CondFatigued',
	},
	exhausted: {
		label: 'PF1.CondExhausted',
	},
	pf1_sleep: {
		label: 'PF1.CondSleep',
	},
};

Hooks.once('ready', () => {
	if (isNewerVersion(game.system.version, '9.6')) {
		conditions.sleep = conditions.pf1_sleep;
		delete conditions.pf1_sleep;
	}

	for (const [key, data] of Object.entries(conditions)) {
		// PF1v10
		if (pf1.registry.conditions) {
			const cond = pf1.registry.conditions.get(key);
			if (cond) {
				data.label = cond.name;
				data.img = cond.texture;
			}
		}
		// PF1v9 and older
		else {
			data.img = pf1.config.conditionTextures[key];
		}
	}
});

/**
 * @param {Actor} actor
 * @param {object} data
 */
async function endConditions(actor, data) {
	const { conditions } = foundry.utils.expandObject(data);
	for (const [key, state] of Object.entries(conditions)) {
		if (state) await actor.setCondition(key, false);
	}
}

/**
 * @param {Actor} actor
 * @param {object} options
 * @param {object} update
 * @param {object} itemUpdates
 */
async function restEnd(actor, options, update, itemUpdates) {
	const conds = deepClone(conditions);

	for (const [key, data] of Object.entries(conds)) {
		if (!actor.hasCondition(key)) delete conds[key];
	}

	if (foundry.utils.isEmpty(conds)) return;

	const context = {
		defaultEnd: game.settings.get(CFG.id, CFG.SETTINGS.defaultEnabled),
		conditions: conds,
	};

	Dialog.prompt({
		title: game.i18n.format('RestEnd.Title', { actor: actor.name }),
		content: await renderTemplate(`modules/${CFG.id}/dialog.hbs`, context),
		label: game.i18n.localize('RestEnd.Confirm'),
		callback: (html) => endConditions(actor, new FormDataExtended(html.querySelector('form')).object),
		default: 'confirm',
		rejectClose: false,
		options: {
			jQuery: false,
			focus: true,
			classes: [...Dialog.defaultOptions.classes, 'rest-end'],
		},
	});
}

function registerSettings() {
	game.settings.register(CFG.id, CFG.SETTINGS.defaultEnabled, {
		name: 'RestEnd.DefaultEnabled',
		hint: 'RestEnd.DefaultEnabledHint',
		type: Boolean,
		default: false,
		config: true,
		scope: 'client',
	});

	game.modules.get(CFG.id).api = {
		prompt: (actor) => restEnd(actor),
	};
}

Hooks.on('pf1ActorRest', restEnd);
Hooks.once('init', registerSettings);
