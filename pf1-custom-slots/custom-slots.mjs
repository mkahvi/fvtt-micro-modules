const CFG = {
	id: 'pf1-custom-slots',
	SETTINGS: {
		slots: 'slots'
	}
}

class SlotConfig extends FormApplication {
	constructor(...args) {
		super(...args);

		this.config = deepClone(game.settings.get(CFG.id, CFG.SETTINGS.slots));
	}

	static get defaultOptions() {
		const options = super.defaultOptions;
		return {
			...options,
			classes: [...options.classes, 'pf1-custom-slots-config'],
			title: game.i18n.localize('CustomSlots.Config.Label'),
			template: `modules/${CFG.id}/slot-editor.hbs`,
			width: 480,
			height: 'auto',
			resizable: true,
			submitOnChange: true,
			submitOnClose: false,
			closeOnSubmit: false,
		}
	}

	getData() {
		return {
			hasSlots: !foundry.utils.isEmpty(this.config),
			slots: this.config,
		}
	}

	/**
	 * @param {Event} event
	 * @param {object} formData
	 */
	_updateObject(event, formData) {
		formData = foundry.utils.expandObject(formData);

		this.config = formData.slots;

		this.render();
	}

	async _onDelete(event, id) {
		const confirmed = await Dialog.prompt({
			title: game.i18n.localize('CustomSlots.DelEntry'),
			content: `<p>${game.i18n.format('CustomSlots.DelEntryHint', { id })}</p>`,
			callback: () => true,
			label: game.i18n.localize('CustomSlots.DelConfirm'),
			rejectClose: false,
		});

		if (confirmed !== true) return;

		console.log('CUSTOM SLOTS | Removing:', { id, label: this.config[id] });
		delete this.config[id];
		this.render();
	}

	async _onCreate(event) {
		const newslot = await Dialog.wait({
			title: game.i18n.localize('CustomSlots.NewEntry'),
			content: `<form><div class='form-group'><label>${game.i18n.localize('CustomSlots.ID')}</label><input type='text' name='slot.id' required autofocus></div><div class='form-group'><label>${game.i18n.localize('CustomSlots.Label')}</label><input type='text' name='slot.label' required></div><hr></form>`,
			buttons: {
				create: {
					label: 'Save',
					callback: (html) => foundry.utils.expandObject(new FormDataExtended(html.querySelector('form')).object).slot,
				}
			},
			default: 'create',
			close: () => null,
		},
		{
			jQuery: false,
			rejectClose: false,
		});
		if (!newslot) return;
		let { id, label } = newslot;

		id = id.slugify({ strict: true });
		label = label.trim();

		console.log({ newslot, id, label });
		if (!id || !label) return;

		if (CFG.original[id] !== undefined)
			return void ui.notifications.error(game.i18n.format('CustomSlots.IDConflict', { id }));

		console.log('CUSTOM SLOTS | Adding:', { id, label: this.config[id] });

		this.config[id] = label;
		this.render();
	}

	_onAction(event) {
		event.preventDefault();

		const el = event.target;
		switch (el.dataset.action) {
			case 'delete':
				return this._onDelete(event, el.dataset.delete);
			case 'create':
				return this._onCreate(event);
		}
	}

	_saveConfiguration(event) {
		event.preventDefault();

		console.log('CUSTOM SLOTS | Saving Configuration\n', this.config);

		game.settings.set(CFG.id, CFG.SETTINGS.slots, this.config);
		this.close();
	}

	/**
	 * @param {JQuery<HTMLElement>} jq
	 */
	activateListeners(jq) {
		super.activateListeners(jq);

		const html = this.form;

		html.querySelectorAll('a.action')
			.forEach(el => el.addEventListener('click', this._onAction.bind(this)));

		html.querySelector('button')
			.addEventListener('click', this._saveConfiguration.bind(this));
	}
}

function registerSettings() {
	game.settings.register(CFG.id, CFG.SETTINGS.slots, {
		type: Object,
		default: {},
		requiresReload: true,
		onChange: () => initSlots(),
		config: false,
		scope: 'world',
	});

	game.settings.registerMenu(CFG.id, CFG.SETTINGS.slots, {
		label: 'CustomSlots.Config.Label',
		type: SlotConfig,
		restricted: true,
	});
}

function initSlots() {
	const slots = game.settings.get(CFG.id, CFG.SETTINGS.slots);
	for (const [key, label] of Object.entries(slots)) {
		pf1.config.equipmentSlots.wondrous[key] = label;
	}
}

Hooks.once('init', () => {
	registerSettings();
	CFG.original = deepClone(pf1.config.equipmentSlots.wondrous);
	initSlots();
});
