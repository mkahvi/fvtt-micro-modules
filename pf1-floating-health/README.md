# Floating Health for Pathfinder 1e

![Supported Foundry Versions](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fversion%3Fstyle%3Dflat%26url%3Dhttps%3A%2F%2Fgitlab.com%2Fmkahvi%2Ffvtt-micro-modules%2F-%2Fraw%2Fmaster%2Fpf1-floating-health%2Fmodule.json)

Displays little floating numbers when health is adjusted.

## Install

Manifest URL: <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/master/pf1-floating-health/module.json>

Latest for pre-PF1v9 (manual install only): <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/10cb68fe50249dbd36eb9d71a570d9d9ae330a47/pf1-floating-health/pf1-floating-health.zip>

Latest for Foundry v10 (manual install only): <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/acae364816d4d366345ff49d6aae05fffb8cd453/pf1-floating-health/pf1-floating-health.zip>

Latest for Foundry v9 (manual install only): <https://gitlab.com/mkahvi/fvtt-micro-modules/-/raw/2a50f97432d41c8238fa7d8c593ba28ba56fe872/pf1-floating-health/pf1-floating-health.zip?inline=false>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
