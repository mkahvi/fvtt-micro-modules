const CFG = {
	id: /** @type {const} */ ('mkah-pf1-floating-health'),
	SETTINGS: {
		visibility: /** @type {const} */ ('visibility'),
		playerOwned: /** @type {const} */ ('playerOwned'),
		visibleName: /** @type {const} */ ('visibleName'),
		visibleBars: /** @type {const} */ ('visibleBars'),
		presentation: /** @type {const} */ ('presentation'),
	},
};

const formatter = new Intl.NumberFormat('nu', { signDisplay: 'always' });
export const signNum = (num) => formatter.format(num);

const hpKeys = ['offset', 'temp', 'nonlethal'];

class PresentationModel extends foundry.abstract.DataModel {
	static defineSchema() {
		const fields = foundry.data.fields;
		return {
			hitpoints: new fields.SchemaField({
				label: new fields.StringField({ initial: 'MKAh.FloatingHealth.Shorthand.value' }),
				positive: new fields.SchemaField({ fill: new fields.ColorField({ initial: '#00FF00' }) }),
				negative: new fields.SchemaField({ fill: new fields.ColorField({ initial: '#FF0000' }) }),
			}),
			vigor: new fields.SchemaField({
				label: new fields.StringField({ initial: 'MKAh.FloatingHealth.Shorthand.vigor' }),
				positive: new fields.SchemaField({ fill: new fields.ColorField({ initial: '#00FF00' }) }),
				negative: new fields.SchemaField({ fill: new fields.ColorField({ initial: '#FF0000' }) }),
			}),
			wounds: new fields.SchemaField({
				label: new fields.StringField({ initial: 'MKAh.FloatingHealth.Shorthand.wounds' }),
				positive: new fields.SchemaField({ fill: new fields.ColorField({ initial: '#00FF00' }) }),
				negative: new fields.SchemaField({ fill: new fields.ColorField({ initial: '#FF0000' }) }),
			}),
			temp: new fields.SchemaField({
				label: new fields.StringField({ initial: 'MKAh.FloatingHealth.Shorthand.temp' }),
				positive: new fields.SchemaField({ fill: new fields.ColorField({ initial: '#55FF00' }) }),
				negative: new fields.SchemaField({ fill: new fields.ColorField({ initial: '#FF3300' }) }),
			}),
			nonlethal: new fields.SchemaField({
				label: new fields.StringField({ initial: 'MKAh.FloatingHealth.Shorthand.nonlethal' }),
				positive: new fields.SchemaField({ fill: new fields.ColorField({ initial: '#AA0077' }) }),
				negative: new fields.SchemaField({ fill: new fields.ColorField({ initial: '#00AA33' }) }),
			}),
		};
	}
}

let hpCfg = null;
Hooks.once('i18nInit', () => {
	hpCfg = new PresentationModel();
	Object.values(hpCfg)
		.forEach((value) => value.label = game.i18n.localize(value.label));
});

const visibleOptions = [CONST.TOKEN_DISPLAY_MODES.ALWAYS, CONST.TOKEN_DISPLAY_MODES.HOVER];

/**
 * @param {Token} token
 * @param root0
 * @param root0.restricted
 * @param root0.visibleBars
 * @param root0.visibleName
 * @param root0.playerOwned
 * @param root0.user
 */
function testPermission(token, { restricted, visibleBars, visibleName, playerOwned, user }) {
	const actor = token.actor;
	if (!actor) return false; // Sanity check

	if (actor.testUserPermission(user, 'LIMITED')) return true;
	if (!restricted) return true;
	if (playerOwned && actor.hasPlayerOwner) return true;

	if (visibleBars && visibleOptions.includes(token.displayBars)) return true;
	if (visibleName && visibleOptions.includes(token.displayName)) return true;

	return false;
}

/**
 * @param {string} key
 * @param {object} update
 * @param {object} data
 * @returns {number}
 */
function getDelta(key, update, data) {
	if (update?.[key] === undefined) return 0;
	const ohv = data[key] ?? 0,
		nhv = update[key] ?? ohv ?? 0,
		delta = nhv - ohv;
	return delta;
}

/**
 * Build HP diffs.
 *
 * @param {object} data
 * @param {object} old
 */
function diffHealth(data, old) {
	if (!data || !old) return;

	const diff = {};

	const nhp = data.attributes?.hp;
	if (nhp !== undefined) {
		const ohp = old.attributes?.hp;
		if (ohp !== undefined) {
			hpKeys.forEach(k => {
				const delta = getDelta(k, nhp, ohp);
				if (k == 'offset') k = 'hitpoints';
				if (delta != 0) diff[k] = delta;
			});
		}
	}

	const wounds = data.attributes?.wounds,
		vigor = data.attributes?.vigor;

	if (wounds) {
		const owp = old.attributes?.wounds;
		if (owp !== undefined) {
			const delta = getDelta('offset', wounds, owp);
			if (delta != 0) diff.wounds = delta;
		}
	}
	if (vigor) {
		const ovp = old.attributes?.vigor;
		if (ovp !== undefined) {
			hpKeys.forEach(k => {
				const delta = getDelta(k, vigor, ovp);
				if (delta != 0) {
					if (k == 'offset') k = 'vigor';
					diff[k] = delta;
				}
			});
		}
	}

	return diff;
}

/**
 * Render floating numbers.
 *
 * Async to allow the calling functions to not care when this finishes;
 *
 * @param {Token[]} tokens
 * @param {object} hpDiffs
 */
async function renderFloaters(tokens, hpDiffs) {
	const permissionOptions = {
		restricted: game.settings.get(CFG.id, CFG.SETTINGS.visibility),
		playerOwned: game.settings.get(CFG.id, CFG.SETTINGS.playerOwned),
		visibleBars: game.settings.get(CFG.id, CFG.SETTINGS.visibleBars),
		visibleName: game.settings.get(CFG.id, CFG.SETTINGS.visibleName),
		user: game.user,
	};

	for (const t of tokens) {
		if (!testPermission(t, permissionOptions)) continue;

		// Skip unseen tokens that you lack correct permissions on
		if (!t.actor.testUserPermission(game.user, CONST.DOCUMENT_PERMISSION_LEVELS.OBSERVER) && !t.isVisible)
			continue;

		for (const [key, value] of Object.entries(hpDiffs)) {
			if (value == 0) continue; // Skip deltas of 0
			const ck = value < 0 ? 'negative' : 'positive';
			const cfg = hpCfg[key];
			const value0 = key != 'nonlethal' ? value : -value; // Maybe flip the value
			const floaterData = {
				anchor: CONST.TEXT_ANCHOR_POINTS.CENTER,
				direction: value0 < 0 ? CONST.TEXT_ANCHOR_POINTS.BOTTOM : CONST.TEXT_ANCHOR_POINTS.TOP,
				// duration: 2000,
				fontSize: 32,
				fill: cfg[ck].fill,
				stroke: 0x000000,
				strokeThickness: 3,
				jitter: 0.3,
			};

			canvas.interface.createScrollingText(t.center, `${cfg.label} ${signNum(value)}`, floaterData); // v10
		}
	}
}

/**
 * Handle linked actors.
 *
 * @param {ActorPF} doc
 * @param {object} diff
 * @param {object} options
 * @param _userId
 */
function preUpdateActorEvent(doc, diff, options, _userId) {
	const dhp = diffHealth(diff.system, doc.system);
	if (dhp && Object.keys(dhp).length) options._hpDiffs = dhp;
}

function updateActorEvent(actor, _data, options, _userId) {
	const dhp = options._hpDiffs;
	if (!dhp) return;

	/** @type {Token[]} */
	const tokens = actor.getActiveTokens(true);
	if (tokens.length == 0) return;

	renderFloaters(tokens, dhp);
}

Hooks.on('preUpdateActor', preUpdateActorEvent);

Hooks.on('updateActor', updateActorEvent);

Hooks.once('init', function registerSettings() {
	game.settings.register(CFG.id, CFG.SETTINGS.visibility, {
		name: 'Restricted visibility',
		hint: 'If enabled, one of the following criteria need to be fulfilled. Limited permission is always sufficient.',
		type: Boolean,
		default: false,
		scope: 'world',
		config: true,
	});

	game.settings.register(CFG.id, CFG.SETTINGS.playerOwned, {
		name: 'Criteria: Player owned',
		hint: 'Token has player owner.',
		type: Boolean,
		default: true,
		scope: 'world',
		config: true,
	});

	game.settings.register(CFG.id, CFG.SETTINGS.visibleBars, {
		name: 'Criteria: Visible bars',
		hint: 'Token has player visible bars.',
		type: Boolean,
		default: true,
		scope: 'world',
		config: true,
	});

	game.settings.register(CFG.id, CFG.SETTINGS.visibleName, {
		name: 'Criteria: Visible name',
		hint: 'Token has player visible name.',
		type: Boolean,
		default: false,
		scope: 'world',
		config: true,
	});

	// TODO: Add color configuration
	/*
	game.settings.register(CFG.id, CFG.SETTINGS.presentation, {
		type: PresentationModel,
		default: () => new PresentationModel(),
		scope: 'client',
		config: false,
	});
	*/
});
