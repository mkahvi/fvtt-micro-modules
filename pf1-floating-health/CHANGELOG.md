# Change Log

## 0.5.0

- Fix: Unlinked token handling for Foundry v11.
- Support for Foundry v10 and older removed.
- Starfinder support removed since it has inbuilt support for floating health.

## 0.4.0.3

- Fix: Floaters were visible even if the token wasn't.

## 0.4.0.2

- Fix: Errors with actorless tokens

## 0.4.0 – 0.4.0.1

- Foundry v10 support

## 0.3.0

- Added support for wounds & vigor for Pathfinder.
- Changed shield & stamina colors for Starfinder.
- Added some controls for who can see the floaters.

## 0.2

- Starfinder support

## 0.1

- Initial release
